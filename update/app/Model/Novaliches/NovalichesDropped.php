<?php

namespace App\Model\Novaliches;

use Illuminate\Database\Eloquent\Model;

class NovalichesDropped extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'branch',
        'program',
        'school',
        'email',
        'contact_no',

    ];

}
