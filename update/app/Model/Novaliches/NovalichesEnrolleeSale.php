<?php

namespace App\Model\Novaliches;

use Illuminate\Database\Eloquent\Model;

class NovalichesEnrolleeSale extends Model
{
    protected $fillable = [
    	'cash_sales'
    ];
}
