<?php

namespace App\Model\Novaliches;

use Illuminate\Database\Eloquent\Model;

class NovalichesBudget extends Model
{
    protected $fillable = [
        'date',
        'amount',
        'category',
        'remarks',
        
    ];
}
