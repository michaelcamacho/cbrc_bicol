<?php

namespace App\Model\Novaliches;

use Illuminate\Database\Eloquent\Model;

class NovalichesPettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
