<?php

namespace App\Model\Novaliches;

use Illuminate\Database\Eloquent\Model;

class NovalichesReservation extends Model
{
	protected $fillable = [
    		'enrollee_id',
            'name',
            'branch',
            'program',
            'prog',
            'school',
            'email',
            'contact_no',
            'reservation_fee',
        ];
}
