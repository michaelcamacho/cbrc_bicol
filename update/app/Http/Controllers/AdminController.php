<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Branch;
use App\Program;
/* Novaliches */
use App\Model\Novaliches\NovalichesAgri;
use App\Model\Novaliches\NovalichesBooksInventorie;
use App\Model\Novaliches\NovalichesBooksSale;
use App\Model\Novaliches\NovalichesBudget;
use App\Model\Novaliches\NovalichesCivil;
use App\Model\Novaliches\NovalichesCrim;
use App\Model\Novaliches\NovalichesDiscount;
use App\Model\Novaliches\NovalichesDropped;
use App\Model\Novaliches\NovalichesExpense;
use App\Model\Novaliches\NovalichesIelt;
use App\Model\Novaliches\NovalichesLet;
use App\Model\Novaliches\NovalichesNclex;
use App\Model\Novaliches\NovalichesNle;
use App\Model\Novaliches\NovalichesPsyc;
use App\Model\Novaliches\NovalichesMid;
use App\Model\Novaliches\NovalichesOnline;
use App\Model\Novaliches\NovalichesReceivable;
use App\Model\Novaliches\NovalichesS1Sale;
use App\Model\Novaliches\NovalichesS2Sale;
use App\Model\Novaliches\NovalichesScholar;
use App\Model\Novaliches\NovalichesSocial;
use App\Model\Novaliches\NovalichesTuition;
use App\Model\Novaliches\NovalichesPettyCash;


use DB;
use Auth;
use App\User;
use App\Role;

class AdminController extends Controller
{


public function __construct()
	{
		$this->middleware('role:admin');
		
	}


public function dashboard(){
		$novaliches_let_1=NovalichesS1Sale::where('program','=','LET')->sum('amount_paid');

    	$novaliches_nle_1=NovalichesS1Sale::where('program','=','NLE')->sum('amount_paid');

		$novaliches_crim_1=NovalichesS1Sale::where('program','=','Criminology')->sum('amount_paid');

		$novaliches_civil_1=NovalichesS1Sale::where('program','=','Civil Service')->sum('amount_paid');
		
		$novaliches_psyc_1=NovalichesS1Sale::where('program','=','Psycometrician')->sum('amount_paid');

		$novaliches_nclex_1=NovalichesS1Sale::where('program','=','NCLEX')->sum('amount_paid');
		
		$novaliches_ielts_1=NovalichesS1Sale::where('program','=','IELTS')->sum('amount_paid');

		$novaliches_social_1=NovalichesS1Sale::where('program','=','Social')->sum('amount_paid');

		$novaliches_agri_1=NovalichesS1Sale::where('program','=','Agriculture')->sum('amount_paid');

		$novaliches_mid_1=NovalichesS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$novaliches_online_1=NovalichesS1Sale::where('program','=','Online Only')->sum('amount_paid');

		$novaliches_let_2=NovalichesS2Sale::where('program','=','LET')->sum('amount_paid');

    	$novaliches_nle_2=NovalichesS2Sale::where('program','=','NLE')->sum('amount_paid');

		$novaliches_crim_2=NovalichesS2Sale::where('program','=','Criminology')->sum('amount_paid');

		$novaliches_civil_2=NovalichesS2Sale::where('program','=','Civil Service')->sum('amount_paid');

		$novaliches_psyc_2=NovalichesS2Sale::where('program','=','Psycometrician')->sum('amount_paid');

		$novaliches_nclex_2=NovalichesS2Sale::where('program','=','NCLEX')->sum('amount_paid');

		$novaliches_ielts_2=NovalichesS2Sale::where('program','=','IELTS')->sum('amount_paid');

		$novaliches_social_2=NovalichesS2Sale::where('program','=','Social')->sum('amount_paid');

		$novaliches_agri_2=NovalichesS2Sale::where('program','=','Agriculture')->sum('amount_paid');

		$novaliches_mid_2=NovalichesS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$novaliches_online_2=NovalichesS1Sale::where('program','=','Online Only')->sum('amount_paid');


				 $total_s1 = $novaliches_let_1 + $novaliches_nle_1 + $novaliches_crim_1 + $novaliches_civil_1 + $novaliches_psyc_1 + $novaliches_nclex_1 + $novaliches_ielts_1 + $novaliches_social_1 + $novaliches_agri_1 + $novaliches_mid_1 + $novaliches_online_1;

				 $total_s2 = $novaliches_let_2 + $novaliches_nle_2 + $novaliches_crim_2 + $novaliches_civil_2 + $novaliches_psyc_2 + $novaliches_nclex_2 + $novaliches_ielts_2 + $novaliches_social_2 + $novaliches_agri_2 + $novaliches_mid_2 + $novaliches_online_2;

				 $total_novaliches_s1 = $novaliches_let_1 + $novaliches_nle_1 + $novaliches_crim_1 + $novaliches_civil_1 + $novaliches_psyc_1 + $novaliches_nclex_1 +  $novaliches_ielts_1 + $novaliches_social_1 + $novaliches_agri_1 + $novaliches_mid_1 + $novaliches_online_1;

				 $total_novaliches_s2 = $novaliches_let_2 + $novaliches_nle_2 + $novaliches_crim_2 + $novaliches_civil_2 + $novaliches_psyc_2 + $novaliches_nclex_2 +  $novaliches_ielts_2 + $novaliches_social_2 + $novaliches_agri_2 + $novaliches_mid_2 + $novaliches_online_2;

				 


	$novaliches_expense 	  =	NovalichesExpense::sum('amount');

	$total_expense = $novaliches_expense;

	$novaliches_let = NovalichesLet::where('status','=','Enrolled')->count();

	$novaliches_nle = NovalichesNle::where('status','=','Enrolled')->count();

	$novaliches_crim = NovalichesCrim::where('status','=','Enrolled')->count();

	$novaliches_civil = NovalichesCivil::where('status','=','Enrolled')->count();

	$novaliches_psyc = NovalichesPsyc::where('status','=','Enrolled')->count();

	$novaliches_nclex = NovalichesNclex::where('status','=','Enrolled')->count();

	$novaliches_ielts = NovalichesIelt::where('status','=','Enrolled')->count();

	$novaliches_social = NovalichesSocial::where('status','=','Enrolled')->count();

	$novaliches_agri = NovalichesAgri::where('status','=','Enrolled')->count();

	$novaliches_mid = NovalichesMid::where('status','=','Enrolled')->count();

	$novaliches_online = NovalichesOnline::where('status','=','Enrolled')->count();

	$total_novaliches = $novaliches_let + $novaliches_nle + $novaliches_crim + $novaliches_civil + $novaliches_psyc + $novaliches_nclex + $novaliches_ielts + $novaliches_social + $novaliches_agri + $novaliches_mid + $novaliches_online;


	$total_enrollee = $total_novaliches;


	$novaliches_receivable = NovalichesReceivable::sum('balance');

	$total_receivable = $novaliches_receivable;

	$novaliches1_dis = NovalichesS1Sale::sum('discount');
	$novaliches2_dis = NovalichesS2Sale::sum('discount');

	$discount = $novaliches1_dis +
				$novaliches2_dis;



    	return view ('/admin.dashboard')
    	->with('total_s1',$total_s1)
    	->with('total_s2',$total_s2)
    	->with('total_novaliches_s1',$total_novaliches_s1)
		->with('total_novaliches_s2',$total_novaliches_s2)
		->with('total_expense',$total_expense)
		->with('novaliches_expense',$novaliches_expense)
		->with('total_novaliches',$total_novaliches)
		->with('total_enrollee',$total_enrollee)
		->with('total_receivable',$total_receivable)
		->with('discount',$discount);
    }

public function total_program(){

 		$let_1_sale=NovalichesS1Sale::where('program','=','LET')->sum('amount_paid');

    	$nle_1_sale=NovalichesS1Sale::where('program','=','NLE')->sum('amount_paid');

		$crim_1_sale=NovalichesS1Sale::where('program','=','Criminology')->sum('amount_paid');

		$civil_1_sale=NovalichesS1Sale::where('program','=','Civil Service')->sum('amount_paid');

		$psyc_1_sale=NovalichesS1Sale::where('program','=','Psycometrician')->sum('amount_paid');

		$nclex_1_sale=NovalichesS1Sale::where('program','=','NCLEX')->sum('amount_paid');

		$ielts_1_sale=NovalichesS1Sale::where('program','=','IELTS')->sum('amount_paid');

		$social_1_sale=NovalichesS1Sale::where('program','=','Social')->sum('amount_paid');

		$agri_1_sale=NovalichesS1Sale::where('program','=','Agriculture')->sum('amount_paid');

		$mid_1_sale=NovalichesS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$online_1_sale=NovalichesS1Sale::where('program','=','Online Only')->sum('amount_paid');

		$let_2_sale=NovalichesS2Sale::where('program','=','LET')->sum('amount_paid');

    	$nle_2_sale=NovalichesS2Sale::where('program','=','NLE')->sum('amount_paid');

		$crim_2_sale=NovalichesS2Sale::where('program','=','Criminology')->sum('amount_paid');

		$civil_2_sale=NovalichesS2Sale::where('program','=','Civil Service')->sum('amount_paid');

		$psyc_2_sale=NovalichesS2Sale::where('program','=','Psycometrician')->sum('amount_paid');

		$nclex_2_sale=NovalichesS2Sale::where('program','=','NCLEX')->sum('amount_paid');

		$ielts_2_sale=NovalichesS2Sale::where('program','=','IELTS')->sum('amount_paid');

		$social_2_sale=NovalichesS2Sale::where('program','=','Social')->sum('amount_paid');

		$agri_2_sale=NovalichesS2Sale::where('program','=','Agriculture')->sum('amount_paid');

		$mid_2_sale=NovalichesS2Sale::where('program','=','Midwifery')->sum('amount_paid');

		$online_2_sale=NovalichesS2Sale::where('program','=','Online Only')->sum('amount_paid');

		$let = 	NovalichesLet::where('status','=','Enrolled')->count();

		$nle = 	NovalichesNle::where('status','=','Enrolled')->count();

		$crim = NovalichesCrim::where('status','=','Enrolled')->count();

		$civil= NovalichesCivil::where('status','=','Enrolled')->count();

		$psyc = NovalichesPsyc::where('status','=','Enrolled')->count();

		$nclex = NovalichesNclex::where('status','=','Enrolled')->count();

		$ielts = NovalichesIelt::where('status','=','Enrolled')->count();

		$social = NovalichesSocial::where('status','=','Enrolled')->count();

		$agri = NovalichesAgri::where('status','=','Enrolled')->count();

		$mid = NovalichesMid::where('status','=','Enrolled')->count();

		$online = NovalichesOnline::where('status','=','Enrolled')->count();

	return view ('admin.total-program')
	->with('let_1_sale',$let_1_sale)
	->with('nle_1_sale',$nle_1_sale)
	->with('crim_1_sale',$crim_1_sale)
	->with('civil_1_sale',$civil_1_sale)
	->with('psyc_1_sale',$psyc_1_sale)
	->with('nclex_1_sale',$nclex_1_sale)
	->with('ielts_1_sale',$ielts_1_sale)
	->with('social_1_sale',$social_1_sale)
	->with('agri_1_sale',$agri_1_sale)
	->with('mid_1_sale',$mid_1_sale)
	->with('online_1_sale',$online_1_sale)
	->with('let_2_sale',$let_2_sale)
	->with('nle_2_sale',$nle_2_sale)
	->with('crim_2_sale',$crim_2_sale)
	->with('civil_2_sale',$civil_2_sale)
	->with('psyc_2_sale',$psyc_2_sale)
	->with('nclex_2_sale',$nclex_2_sale)
	->with('ielts_2_sale',$ielts_2_sale)
	->with('social_2_sale',$social_2_sale)
	->with('agri_2_sale',$agri_2_sale)
	->with('mid_2_sale',$mid_1_sale)
	->with('online_2_sale',$online_1_sale)
	->with('let',$let)
	->with('nle',$nle)
	->with('crim',$crim)
	->with('civil',$civil)
	->with('psyc',$psyc)
	->with('nclex',$nclex)
	->with('ielts',$ielts)
	->with('social',$social)
	->with('agri',$agri)
	->with('mid',$mid)
	->with('online',$online);

}

public function total_agri(){
	$program = "Agriculture";
	$a_prog = NovalichesAgri::where('status','=','Enrolled')->get();


	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_civil(){
	$program = "Civil Service";
	$a_prog = NovalichesCivil::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_crim(){
	$program = "Criminology";
	$a_prog = NovalichesCrim::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_ielts(){
	$program = "IELTS";
	$a_prog = NovalichesIelt::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_let(){
	$program = "LET";
	$a_prog = NovalichesLet::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_nclex(){
	$program = "NCLEX";
	$a_prog = NovalichesNclex::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_nle(){
	$program = "NLE";
	$a_prog = NovalichesNle::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_psyc(){
	$program = "Psycometrician";
	$a_prog = NovalichesPsyc::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_social(){
	$program = "Social Work";
	$a_prog = NovalichesSocial::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_mid(){
	$program = "Midwifery";
	$a_prog = NovalichesMid::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_online(){
	$program = "Online Only";
	$a_prog = NovalichesOnline::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_dropped(){

	$a_let=NovalichesLet::where('status','=','Dropped')->get();

    $a_nle=NovalichesNle::where('status','=','Dropped')->get();

    $a_crim=NovalichesCrim::where('status','=','Dropped')->get();

    $a_civil=NovalichesCivil::where('status','=','Dropped')->get();

    $a_psyc=NovalichesPsyc::where('status','=','Dropped')->get();

    $a_nclex=NovalichesNclex::where('status','=','Dropped')->get();

    $a_ielt=NovalichesIelt::where('status','=','Dropped')->get();

    $a_social=NovalichesSocial::where('status','=','Dropped')->get();

    $a_agri=NovalichesAgri::where('status','=','Dropped')->get();

    $a_mid=NovalichesMid::where('status','=','Dropped')->get();

    $a_online=NovalichesOnline::where('status','=','Dropped')->get();

    return view ('admin.total-dropped')
    ->with('a_let',$a_let)
    ->with('a_nle',$a_nle)
    ->with('a_crim',$a_crim)
    ->with('a_civil',$a_civil)
    ->with('a_psyc',$a_psyc)
    ->with('a_nclex',$a_nclex)
    ->with('a_ielt',$a_ielt)
    ->with('a_social',$a_social)
    ->with('a_agri',$a_agri)
    ->with('a_mid',$a_mid)
    ->with('a_online',$a_online);
}

public function total_scholars(){

	$a_let=NovalichesLet::where('category','=','Scholar')->get();

    $a_nle=NovalichesNle::where('category','=','Scholar')->get();

	$a_crim=NovalichesCrim::where('category','=','Scholar')->get();

	$a_civil=NovalichesCivil::where('category','=','Scholar')->get();

	$a_psyc=NovalichesPsyc::where('category','=','Scholar')->get();

	$a_nclex=NovalichesNclex::where('category','=','Scholar')->get();

	$a_ielt=NovalichesIelt::where('category','=','Scholar')->get();

	$a_social=NovalichesSocial::where('category','=','Scholar')->get();

	$a_agri=NovalichesAgri::where('category','=','Scholar')->get();

	$a_mid=NovalichesMid::where('status','=','Scholar')->get();

    $a_online=NovalichesOnline::where('status','=','Scholar')->get();


    return view ('admin.total-scholars')
    ->with('a_let',$a_let)
    ->with('a_nle',$a_nle)
    ->with('a_crim',$a_crim)
    ->with('a_civil',$a_civil)
    ->with('a_psyc',$a_psyc)
    ->with('a_nclex',$a_nclex)
    ->with('a_ielt',$a_ielt)
    ->with('a_social',$a_social)
    ->with('a_agri',$a_agri)
    ->with('a_mid',$a_mid)
    ->with('a_online',$a_online);;
}

public function tuition_fees(){

	$a_tuition = NovalichesTuition::all();
    $branch = Branch::all();
	$program = Program::all();
	
	return view ('admin.tuition-fees')
	->with('a_tuition',$a_tuition)
	->with('branch',$branch)
	->with('program',$program);
}
public function insert_tuition(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];

	if($branch == 'Novaliches'){
		NovalichesTuition::insert($input);
	}

return response()->json([
            'success' => true,
            'message' => 'New tuition has been added',
        ]);

}

public function update_tuition(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];
$id = $input['id'];

	if($branch == 'Novaliches'){
		NovalichesTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}

return response()->json([
            'success' => true,
            'message' => '1 tuition has been updated',
        ]);

}

public function delete_tuition(Request $request){
		$input = $request->except(['_token']);

		if($input['branch'] == 'Novaliches'){

              NovalichesTuition::where('id','=',$input['id'])->delete();
           
    }

              return response()->json([
            'success' => true,
            'message' => '1 tuition has been deleted',
        ]);
      
         }

public function discounts(){
	$branch = Branch::all();
	$a_discount = NovalichesDiscount::all();
	$program = Program::all();
	return view ('admin.discounts')
	->with('a_discount',$a_discount)
	->with('branch',$branch)
	->with('program',$program);
}

public function insert_discount(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];

	if($branch == 'Novaliches'){
		NovalichesDiscount::insert($input);
	}

return response()->json([
            'success' => true,
            'message' => 'New discount has been added',
        ]);

}

public function update_discount(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];
$id = $input['id'];

	if($branch == 'Novaliches'){
		NovalichesDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}
	

return response()->json([
            'success' => true,
            'message' => '1 discount has been updated',
        ]);

}

public function delete_discount(Request $request){
		$input = $request->except(['_token']);

		if($input['branch'] == 'Novaliches'){

              NovalichesDiscount::where('id','=',$input['id'])->delete();
    }

       return response()->json([
            'success' => true,
            'message' => '1 discount has been deleted',
        ]);
      
         }
    

public function total_books(){
	$branch = Branch::all();
	$a_book = NovalichesBooksInventorie::all();

    $a_sale = NovalichesBooksSale::all();

	$program = Program::all();
	return view ('admin.total-books')
	->with('a_book',$a_book)
	->with('a_sale',$a_sale)
	->with('branch',$branch)
	->with('program',$program);
}

public function insert_book(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];

	if($branch == 'Novaliches'){
		NovalichesBooksInventorie::insert($input);
	}

return response()->json([
            'success' => true,
            'message' => 'New book has been added',
        ]);

}

public function update_book(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];
$id = $input['id'];

	if($branch == 'Novaliches'){
		NovalichesBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	

return response()->json([
            'success' => true,
            'message' => 'Book inventories has been updated',
        ]);

}

public function delete_book(Request $request){
		$input = $request->except(['_token']);

		if($input['branch'] == 'Novaliches'){

              NovalichesBooksInventorie::where('id','=',$input['id'])->delete();

    }

    return response()->json([
            'success' => true,
            'message' => '1 book record has been deleted',
        ]);
}

public function total_expense(){

	$a_sale = NovalichesExpense::all();
	
	
	return view ('admin.total-expense')
	->with('a_sale',$a_sale);
}

public function user(){
	$branch = Branch::all();
	$user = User::where('show','=','1')->get();

	return view ('admin.user')->with('user',$user)->with('branch',$branch);
}

public function insert_user(Request $request){

$input = $request->except(['_token']);

$branch = $input['branch'];

	if($branch == 'Main'){
		User::insert([
			'role' => 'Admin',
			'username' =>$input['username'],
			'email' => $input['email'],
			'password' => Hash::make($input['password']),
			'name' => $input['name'],
			'branch'	=> $input['branch'],
			'show'		=> '1',
			'remember_token' => str_random(60),
		]);

		$user_id = User::max('id');

		DB::table('role_user')->insert([

            'user_id' => $user_id,
            'role_id'    => '1',
            ]); 

		return response()->json([
            'success' => true,
            'message' => 'New Administrator has been added',
        ]);
	}
	if($branch == 'Novaliches'){
		User::insert([
			'role' => 'Member',
			'username' =>$input['username'],
			'email' => $input['email'],
			'password' => Hash::make($input['password']),
			'name' => $input['name'],
			'branch'	=> $input['branch'],
			'show'		=> '1',
			'remember_token' => str_random(60),
		]);

		$user_id = User::max('id');

		DB::table('role_user')->insert([

            'user_id' => $user_id,
            'role_id'    => '2',
            ]); 

		return response()->json([
            'success' => true,
            'message' => 'New Member has been added',
        ]);
	}


}
public function update_user(Request $request){

$input = $request->except(['_token']);
$id = $input['id'];
$branch = $input['branch'];

	if($branch == 'Main'){
		if($input['password'] == null){
		User::where('id','=',$id)->update([
			'role' => 'Admin',
			'username' =>$input['username'],
			'email' => $input['email'],
			'name' => $input['name'],
			'branch'	=> $input['branch'],
		]);
		}

		else{
		User::where('id','=',$id)->update([
			'role' => 'Admin',
			'username' =>$input['username'],
			'password' => Hash::make($input['password']),
			'email' => $input['email'],
			'name' => $input['name'],
			'branch'	=> $input['branch'],
		]);
		}



		DB::table('role_user')->where('user_id','=',$id)->update([

            'user_id' => $id,
            'role_id'    => '1',
            ]); 

		return response()->json([
            'success' => true,
            'message' => 'User has been updated',
        ]);
	}
	if($branch == 'Novaliches'){
		if($input['password'] == null){
		User::where('id','=',$id)->update([
			'role' => 'Admin',
			'username' =>$input['username'],
			'email' => $input['email'],
			'name' => $input['name'],
			'branch'	=> $input['branch'],
		]);
		}

		else{
		User::where('id','=',$id)->update([
			'role' => 'Member',
			'username' =>$input['username'],
			'email' => $input['email'],
			'password' => Hash::make($input['password']),
			'name' => $input['name'],
			'branch'	=> $input['branch'],
		]);
	}

		DB::table('role_user')->where('user_id','=',$id)->update([

            'user_id' => $id,
            'role_id'    => '2',
            ]); 

		return response()->json([
            'success' => true,
            'message' => 'User has been updated',
        ]);
	}
	
}
public function delete_user(Request $request){

	if(isset($request->id)){
              $todo = User::findOrFail($request->id);
              $todo->delete();
              return response()->json([
            'success' => true,
            'message' => 'User has been deleted',
        ]);
        }


}

}
