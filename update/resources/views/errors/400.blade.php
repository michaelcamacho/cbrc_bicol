@extends('errors.main')
@section('title')
400
@stop
@section('main-content')
    <div id="wrapper" class="wrapper">
        <div class="content-wrapper">
            <main class="main-wrapper">
                <div class="page-title">
                    <h1 class="color-white">503</h1>
                </div>
                <h3 class="mr-b-5 color-white">You Seem To Be Trying To Find His Way Home</h3>
                
                <a href="/" class="btn btn-outline-white btn-rounded btn-block fw-700 text-uppercase">Back to home page</a>
            </main>
        </div>
        <!-- .content-wrapper -->
    </div>
@endsection
