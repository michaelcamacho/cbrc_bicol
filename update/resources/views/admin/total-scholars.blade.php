@extends('main')
@section('title')
 Total Scholar
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Total Scholar Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Total Scholar Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="total-en" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                     <thead>
                       
                        <tr>
                            <th>Name</th>
                            <th>CBRC ID</th>
                            <th>Branch</th>
                            <th>Program</th>
                            <th>School</th>
                            <th>Contact #</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($a_let))
                            @foreach($a_let as $a_lets)
                        <tr>
                            <td>{{$a_lets->last_name}}, {{$a_lets->first_name}} {{$a_lets->middle_name}}</td>
                            <td>{{$a_lets->cbrc_id}}</td>
                            <td>{{$a_lets->branch}}</td>
                            <td>{{$a_lets->program}}</td>
                            <td>{{$a_lets->school}}</td>
                            <td>{{$a_lets->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($a_nle))
                            @foreach($a_nle as $a_nles)
                        <tr>
                            <td>{{$a_nles->last_name}}, {{$a_nles->first_name}} {{$a_nles->middle_name}}</td>
                            <td>{{$a_nles->cbrc_id}}</td>
                            <td>{{$a_nles->branch}}</td>
                            <td>{{$a_nles->program}}</td>
                            <td>{{$a_nles->school}}</td>
                            <td>{{$a_nles->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($a_crim))
                            @foreach($a_crim as $a_crims)
                        <tr>
                            <td>{{$a_crims->last_name}}, {{$a_crims->first_name}} {{$a_crims->middle_name}}</td>
                            <td>{{$a_crims->cbrc_id}}</td>
                            <td>{{$a_crims->branch}}</td>
                            <td>{{$a_crims->program}}</td>
                            <td>{{$a_crims->school}}</td>
                            <td>{{$a_crims->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($a_civil))
                            @foreach($a_civil as $a_civils)
                        <tr>
                            <td>{{$a_civils->last_name}}, {{$a_civils->first_name}} {{$a_civils->middle_name}}</td>
                            <td>{{$a_civils->cbrc_id}}</td>
                            <td>{{$a_civils->branch}}</td>
                            <td>{{$a_civils->program}}</td>
                            <td>{{$a_civils->school}}</td>
                            <td>{{$a_civils->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                        @if(isset($a_psyc))
                            @foreach($a_psyc as $a_psycs)
                        <tr>
                            <td>{{$a_psycs->last_name}}, {{$a_psycs->first_name}} {{$a_psycs->middle_name}}</td>
                            <td>{{$a_psycs->cbrc_id}}</td>
                            <td>{{$a_psycs->branch}}</td>
                            <td>{{$a_psycs->program}}</td>
                            <td>{{$a_psycs->school}}</td>
                            <td>{{$a_psycs->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                        @if(isset($a_nclex))
                            @foreach($a_nclex as $a_nclexs)
                        <tr>
                            <td>{{$a_nclexs->last_name}}, {{$a_nclexs->first_name}} {{$a_nclexs->middle_name}}</td>
                            <td>{{$a_nclexs->cbrc_id}}</td>
                            <td>{{$a_nclexs->branch}}</td>
                            <td>{{$a_nclexs->program}}</td>
                            <td>{{$a_nclexs->school}}</td>
                            <td>{{$a_nclexs->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($a_ielt))
                            @foreach($a_ielt as $a_ielts)
                        <tr>
                            <td>{{$a_ielts->last_name}}, {{$a_ielts->first_name}} {{$a_ielts->middle_name}}</td>
                            <td>{{$a_ielts->cbrc_id}}</td>
                            <td>{{$a_ielts->branch}}</td>
                            <td>{{$a_ielts->program}}</td>
                            <td>{{$a_ielts->school}}</td>
                            <td>{{$a_ielts->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($a_social))
                            @foreach($a_social as $a_socials)
                        <tr>
                            <td>{{$a_socials->last_name}}, {{$a_socials->first_name}} {{$a_socials->middle_name}}</td>
                            <td>{{$a_socials->cbrc_id}}</td>
                            <td>{{$a_socials->branch}}</td>
                            <td>{{$a_socials->program}}</td>
                            <td>{{$a_socials->school}}</td>
                            <td>{{$a_socials->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($a_agri))
                            @foreach($a_agri as $a_agris)
                        <tr>
                            <td>{{$a_agris->last_name}}, {{$a_agris->first_name}} {{$a_agris->middle_name}}</td>
                            <td>{{$a_agris->cbrc_id}}</td>
                            <td>{{$a_agris->branch}}</td>
                            <td>{{$a_agris->program}}</td>
                            <td>{{$a_agris->school}}</td>
                            <td>{{$a_agris->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($a_mid))
                            @foreach($a_mid as $a_mids)
                        <tr>
                            <td>{{$a_mids->last_name}}, {{$a_mids->first_name}} {{$a_mids->middle_name}}</td>
                            <td>{{$a_mids->cbrc_id}}</td>
                            <td>{{$a_mids->branch}}</td>
                            <td>{{$a_mids->program}}</td>
                            <td>{{$a_mids->school}}</td>
                            <td>{{$a_mids->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($a_online))
                            @foreach($a_online as $a_onlines)
                        <tr>
                            <td>{{$a_onlines->last_name}}, {{$a_onlines->first_name}} {{$a_onlines->middle_name}}</td>
                            <td>{{$a_onlines->cbrc_id}}</td>
                            <td>{{$a_onlines->branch}}</td>
                            <td>{{$a_onlines->program}}</td>
                            <td>{{$a_onlines->school}}</td>
                            <td>{{$a_onlines->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                    </tbody>           
                    
                </table>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
  $('.ad-record').addClass('active');
  $('.ad-record').addClass('collapse in');
});

</script>
@stop