@extends('main')
@section('title')
Yesterday Transaction Report
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
.btn-group {
    top: 0px;
}

@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/report-sidebar.css') }}">

@stop
@section('main-content')

<main class="main-wrapper clearfix" id="let">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }} as of {{ $date }}</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Reports</li>
                            <li class="breadcrumb-item active">Yesterday Transaction Report</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <section>
            <div class="container-fluid marg">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-9 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0" id="top">LET</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="report-let" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Sales from Enrollees</th>
                            <th>Sales from Books</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         
                        <tr>
                            <td>{{$let_sale}} Php</td>
                            <td>{{$let_book}} Php</td>
                        </tr>
                      
                    </tbody>                  
                    
                </table>
                
                                </div>
                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                        
                        <div class="col-md-3 widget-holder" id="sidebar">
                        	<div class="widget-bg services-menu-widget" style="position: absolute;">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Programs</h5>
                                   <br/>        
                                </div>
                                
                                <div class="widget-body clearfix" style="padding-top: 0">
                            	<ul class="menu service-menu nav-pills nav-stacked">
                                <li class="menu-item"><a href="#let" class="scroll">LET</a></li>
                                <li class="menu-item "><a href="#nle" class="scroll">NLE</a></li>
                                <li class="menu-item "><a href="#crim" class="scroll">Criminology</a></li>
                                <li class="menu-item "><a href="#civil" class="scroll">Civil Service</a></li>
                                <li class="menu-item "><a href="#psyc" class="scroll">Psychometrician</a></li>
                                <li class="menu-item "><a href="#nclex" class="scroll">NCLEX</a></li>
                                <li class="menu-item "><a href="#ielts" class="scroll">IELTS</a></li>
                                <li class="menu-item "><a href="#social" class="scroll">Social Work</a></li>
                                <li class="menu-item "><a href="#agri" class="scroll">Agriculture</a></li>
                                <li class="menu-item "><a href="#mid" class="scroll">Midwifery</a></li>
                                <li class="menu-item "><a href="#online" class="scroll">Online Only</a></li>
                           		 </ul>
                        	
                                </div>
                				</div>
                            </div>
                        
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid marg -->
        </section>
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->

            <section id="nle">
            	<br/><br/><br/>
            <div class="container-fluid marg">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-9 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">NLE</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="report-nle" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Sales from Enrollees</th>
                            <th>Sales from Books</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         
                        <tr>
                            <td>{{$nle_sale}} Php</td>
                            <td>{{$nle_book}} Php</td>
                        </tr>
                      
                    </tbody>                  
                    
                </table>
                
                                </div>
                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid marg -->
            </section>


            <section id="crim">
            	<br/><br/><br/>
            <div class="container-fluid marg">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-9 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Criminology</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="report-crim" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Sales from Enrollees</th>
                            <th>Sales from Books</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         
                        <tr>
                            <td>{{$crim_sale}} Php</td>
                            <td>{{$crim_book}} Php</td>
                        </tr>
                      
                    </tbody>                  
                    
                </table>
                
                                </div>
                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid marg -->
            </section>

            <section id="civil">
            	<br/><br/><br/>
            <div class="container-fluid marg">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-9 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Civil Service</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="report-civil" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Sales from Enrollees</th>
                            <th>Sales from Books</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         
                        <tr>
                            <td>{{$civil_sale}} Php</td>
                            <td>{{$civil_book}} Php</td>
                        </tr>
                      
                    </tbody>                  
                    
                </table>
                
                                </div>
                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid marg -->
            </section>

            <section id="psyc">
            	<br/><br/><br/>
            <div class="container-fluid marg">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-9 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Psychometrician</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="report-psyc" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Sales from Enrollees</th>
                            <th>Sales from Books</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         
                        <tr>
                            <td>{{$psyc_sale}} Php</td>
                            <td>{{$psyc_book}} Php</td>
                        </tr>
                      
                    </tbody>                  
                    
                </table>
                
                                </div>
                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid marg -->
            </section>

            <section id="nclex">
            	<br/><br/><br/>
            <div class="container-fluid marg">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-9 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">NCLEX</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="report-nclex" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Sales from Enrollees</th>
                            <th>Sales from Books</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         
                        <tr>
                            <td>{{$nclex_sale}} Php</td>
                            <td>{{$nclex_book}} Php</td>
                        </tr>
                      
                    </tbody>                  
                    
                </table>
                
                                </div>
                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid marg -->
            </section>

            <section id="ielts">
            	<br/><br/><br/>
            <div class="container-fluid marg">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-9 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">IELTS</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="report-ielts" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Sales from Enrollees</th>
                            <th>Sales from Books</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         
                        <tr>
                            <td>{{$ielts_sale}} Php</td>
                            <td>{{$ielts_book}} Php</td>
                        </tr>
                      
                    </tbody>                  
                    
                </table>
                
                                </div>
                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid marg -->
            </section>

            <section id="social">
            	<br/><br/><br/>
            <div class="container-fluid marg">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-9 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Social Work</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="report-social" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Sales from Enrollees</th>
                            <th>Sales from Books</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         
                        <tr>
                            <td>{{$social_sale}} Php</td>
                            <td>{{$social_book}} Php</td>
                        </tr>
                      
                    </tbody>                  
                    
                </table>
                
                                </div>
                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid marg -->
            </section>

            <section id="agri">
            	<br/><br/><br/>
            <div class="container-fluid marg">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-9 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Agriculture</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="report-agri" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Sales from Enrollees</th>
                            <th>Sales from Books</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         
                        <tr>
                            <td>{{$agri_sale}} Php</td>
                            <td>{{$agri_book}} Php</td>
                        </tr>
                      
                    </tbody>                  
                    
                </table>
                
                                </div>
                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid marg -->
            </section>

            <section id="mid">
            	<br/><br/><br/>
            <div class="container-fluid marg">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-9 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Midwifery</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="report-mid" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Sales from Enrollees</th>
                            <th>Sales from Books</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         
                        <tr>
                            <td>{{$mid_sale}} Php</td>
                            <td>{{$mid_book}} Php</td>
                        </tr>
                      
                    </tbody>                  
                    
                </table>
                
                                </div>
                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid marg -->
            </section>

            <section id="online">
            	<br/><br/><br/>
            <div class="container-fluid marg">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-9 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Online Only</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="report-online" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Sales from Enrollees</th>
                            <th>Sales from Books</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         
                        <tr>
                            <td>{{$online_sale}} Php</td>
                            <td>{{$online_book}} Php</td>
                        </tr>
                      
                    </tbody>                  
                    
                </table>
                
                                </div>
                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid marg -->
            </section>
            <br/><br/><br/><br/><br/><br/>
        </main>

@stop

@section('js')

<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/affix.js') }}"></script>
<script src="{{ asset('/js/report-sidebar.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
  $('.reports').addClass('active');
  $('.reports').addClass('collapse in');

  });
</script>

@stop