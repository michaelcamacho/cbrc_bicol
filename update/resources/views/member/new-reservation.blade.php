@extends('main')
@section('title')
NEW RESERVATION
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
</style>
<link href="{{ asset('css/select.css') }}" rel="stylesheet" />
@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }}</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{Auth::user()->position. 'dashboard'}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">New Reservation</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">New Reservation</h5>
                                   <br/>
                                    <form class="form-material" method="POST" data-toggle="validator" enctype="multipart/form-data" id="form" action="{{Auth::user()->position. 'insert-reservation'}}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input type="hidden" name="branch" id="branch" value="{{Auth::user()->branch}}">
                                                    <input type="hidden" id="receipt" value=""> 
                                                    <input class="form-control" type="text" 
                                                    value="{{ $date }}" name="date" id="date" readonly>
                                                    <label>Date</label>
                                                </div>
                                            </div>
                                         
                                        </div>

                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <select class="form-control" name="program" id="program" required>
                                                        <option value="" disable="true" selected="true">--- Select Program ---</option>
                                                        @if(isset($program))
                                                        @foreach($program as $programs)
                                                        <option value="{{$programs->aka}}">{{$programs->program_name}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                    <label>Program</label>
                                                </div>
                                            </div>
                                            
                                           
                                        </div>

                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group input-has-value">
                                                   <select class="form-control js-example-basic-single" name="name" id="student" required>
                                                        <option value="" disable="true"  selected="true">--- Select Student ---</option>
                                                       
                                                    </select>
                                                    <label>Student Name</label>
                                                </div>
                                            </div>
                                            
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group input-has-value">
                                                   <select class="form-control" name="season" id="season" required>
                                                        <option value="" disable="true"  selected="true">--- Select Season ---</option>
                                                        <option value="Season 1">Season 1</option>
                                                        <option value="Season 2">Season 2</option>
                                                       
                                                    </select>
                                                    <label>Season</label>
                                                </div>
                                            </div>
                                            
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group input-has-value">
                                                   <select class="form-control" name="year" id="year" required>
                                                        <option value="" disable="true"  selected="true">--- Select Year ---</option>
                                                        <option value="2018">2018</option>
                                                        <option value="2019">2019</option>
                                                        <option value="2019">2020</option>
                                                    </select>
                                                    <label>Year</label>
                                                </div>
                                            </div>
                                            
                                        </div>
                                         
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input class="form-control" id="amount_paid" name="amount_paid" type="number" min="0" step="0.01" required>
                                                    <label>Amount Paid</label>
                                                </div>
                                            </div>
                                           
                                        </div>
                                       
                                        <div class="form-actions btn-list">
                                            <button class="btn btn-primary print" type="submit" id="button-prevent">Add New Reservation</button>
                                            {{--<button class="btn btn-outline-default" type="button">Cancel</button>--}}
                                        </div>
                                    </form>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/prevent.js') }}"></script>
{{--<script src="{{ asset('/js/book-fetch.js') }}"></script>--}}
<script type="text/javascript">
    $(document).ready(function() {
  $('.new-reservation').addClass('active');
});


    $('#program').on('change', function(e){
            console.log(e);

            var program = e.target.value;

            $.get({{Auth::user()->position}}'json-student?program=' + program,function(data){
                console.log(data);

                 $('#student').empty();
                 $('#student').append('<option value="" disable="true" selected="true">--- Select Student ---</option>');

                $('#amount').val('');

                  $.each(data, function(index, studentObj){
                    $('#student').append('<option value="'+ studentObj.last_name +', '+ studentObj.first_name+ ' ' + studentObj.middle_name+ '*'+ studentObj.school +'*'+ studentObj.email +'*'+ studentObj.contact_no +'*'+ studentObj.id +'">'+ studentObj.last_name +', '+ studentObj.first_name+' '+ studentObj.middle_name+'</option>');
                });
                  $.each(data, function(index, tuitionObj){
                    $('#id').val(tuitionObj.id);
                })

            });
        });




</script>
<script src="{{ asset('/js/print-reservation.js') }}"></script>
@stop