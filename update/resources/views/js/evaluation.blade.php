<script type="text/javascript">
    $(document).ready(function() {
  $('.evaluate').addClass('active');
  $('.evaluate').addClass('collapse in');
});
    $('#program').on('change', function(e){
            console.log(e);

            var prog = e.target.value;
            var program = prog.split("*");

            $.get({{Auth::user()->position}}'json-class?program=' + program[0],function(data){
                console.log(data);

                 $('#class').empty();
                 $('#class').append('<option value="" disable="true" selected="true">--- Select Class ---</option>');

                 
                 $('#subject').empty();
                 $('#subject').append('<option value="" disable="true" selected="true">--- Select Subject ---</option>');
                 
                 $('#section').empty();
                 $('#section').append('<option value="" disable="true" selected="true">--- Select Section ---</option>');

                 $('#lecturer').empty();
                 $('#lecturer').append('<option value="" disable="true" selected="true">--- Select Lecturer ---</option>');
                 

                  $.each(data, function(index, lecObj){
                    $('#class').append('<option value="'+ lecObj.aka +'*'+ lecObj.class +'">'+ lecObj.class +'</option>');
                })

            });
        });

    $('#class').on('change', function(e){
            console.log(e);

            var classes = e.target.value;

            var clas = classes.split("*");
            

            var prog = $('#program').val();
            var program = prog.split("*");

            $.get({{Auth::user()->position}}'json-subject?program=' + program[0] + '&class=' + clas[0],function(data){
                console.log(data);

                 
                 $('#subject').empty();
                 $('#subject').append('<option value="" disable="true" selected="true">--- Select Subject ---</option>');
                 
                 $('#section').empty();
                 $('#section').append('<option value="" disable="true" selected="true">--- Select Section ---</option>');

                 $('#lecturer').empty();
                 $('#lecturer').append('<option value="" disable="true" selected="true">--- Select Lecturer ---</option>');
                 

                  $.each(data, function(index, lecObj){
                    $('#subject').append('<option value="'+ lecObj.aka +'*'+ lecObj.subject +'">'+ lecObj.subject +'</option>');
                })

            });
        });

     $('#subject').on('change', function(e){
            console.log(e);
            var subj = e.target.value;
            var subject = subj.split("*");

            var classes = $('#class').val();
            var clas = classes.split("*");

            var prog = $('#program').val();
            var program = prog.split("*");

            $.get({{Auth::user()->position}}'json-section?program=' + program[0] + '&class=' + clas[0] + '&subject=' +subject[0],function(data){
                console.log(data);

                $('#section').empty();
                 $('#section').append('<option value="" disable="true" selected="true">--- Select Section ---</option>');

                 $('#lecturer').empty();
                 $('#lecturer').append('<option value="" disable="true" selected="true">--- Select Lecturer ---</option>');


                  $.each(data, function(index, lecObj){
                    $('#section').append('<option value="'+ lecObj.section +'">'+ lecObj.section +'</option>');
                })

            });
        });

    $('#section').on('change', function(e){
            console.log(e);
            var section = e.target.value;

            var classes = $('#class').val();
            var clas = classes.split("*");

            var subj = $('#subject').val();
            var subject = subj.split("*");

            var prog = $('#program').val();
            var program = prog.split("*");

            $('#lecturer').empty();
                 $('#lecturer').append('<option value="" disable="true" selected="true">--- Select Lecturer ---</option>');

            $.get({{Auth::user()->position}}'json-lecturer?program=' + program[0] + '&class=' + clas[0] + '&subject=' +subject[0] + '&section=' +section,function(data){
                console.log(data);


                  $.each(data, function(index, lecObj){
                    $('#lecturer').append('<option value="'+ lecObj.lecturer +'*'+ lecObj.id +'">'+ lecObj.lecturer +'</option>');
                })

            });
        });

    $('#lecturer').on('change', function(e){
            console.log(e);
            var section = $('#section').val();

            var classes = $('#class').val();
            var clas = classes.split("*");

            var subj = $('#subject').val();
            var subject = subj.split("*");

            var prog = $('#program').val();
            var program = prog.split("*");

            var lec = e.target.value;
            var lecturer = lec.split("*");

            $.get({{Auth::user()->position}}'json-lecturer?program=' + program[0] + '&class=' + clas[0] + '&subject=' +subject[0] + '&section=' +section,function(data){
                console.log(data);

            $.each(data, function(index, lecObj){$('#eval_date').val(lecObj.date);});
            $.each(data, function(index, lecObj){$('#eval_ambassador').val(lecObj.review_ambassador);});
            $.each(data, function(index, lecObj){$('#eval_branch').val(lecObj.branch);});
            $.each(data, function(index, lecObj){$('#excellentA').val(lecObj.excellentA);});
            $.each(data, function(index, lecObj){$('#excellentB').val(lecObj.excellentB);});
            $.each(data, function(index, lecObj){$('#excellentC').val(lecObj.excellentC);});
            $.each(data, function(index, lecObj){$('#excellentD').val(lecObj.excellentD);});
            $.each(data, function(index, lecObj){$('#excellentE').val(lecObj.excellentE);});
            $.each(data, function(index, lecObj){$('#excellentF').val(lecObj.excellentF);});
            $.each(data, function(index, lecObj){$('#excellentG').val(lecObj.excellentG);});
            $.each(data, function(index, lecObj){$('#goodA').val(lecObj.goodA);});
            $.each(data, function(index, lecObj){$('#goodB').val(lecObj.goodB);});
            $.each(data, function(index, lecObj){$('#goodC').val(lecObj.goodC);});
            $.each(data, function(index, lecObj){$('#goodD').val(lecObj.goodD);});
            $.each(data, function(index, lecObj){$('#goodE').val(lecObj.goodE);});
            $.each(data, function(index, lecObj){$('#goodF').val(lecObj.goodF);});
            $.each(data, function(index, lecObj){$('#goodG').val(lecObj.goodG);});
            $.each(data, function(index, lecObj){$('#fairA').val(lecObj.fairA);});
            $.each(data, function(index, lecObj){$('#fairB').val(lecObj.fairB);});
            $.each(data, function(index, lecObj){$('#fairC').val(lecObj.fairC);});
            $.each(data, function(index, lecObj){$('#fairD').val(lecObj.fairD);});
            $.each(data, function(index, lecObj){$('#fairE').val(lecObj.fairE);});
            $.each(data, function(index, lecObj){$('#fairF').val(lecObj.fairF);});
            $.each(data, function(index, lecObj){$('#fairG').val(lecObj.fairG);});
            $.each(data, function(index, lecObj){$('#poorA').val(lecObj.poorA);});
            $.each(data, function(index, lecObj){$('#poorB').val(lecObj.poorB);});
            $.each(data, function(index, lecObj){$('#poorC').val(lecObj.poorC);});
            $.each(data, function(index, lecObj){$('#poorD').val(lecObj.poorD);});
            $.each(data, function(index, lecObj){$('#poorE').val(lecObj.poorE);});
            $.each(data, function(index, lecObj){$('#poorF').val(lecObj.poorF);});
            $.each(data, function(index, lecObj){$('#poorG').val(lecObj.poorG);});
            $.each(data, function(index, lecObj){$('#vpoorA').val(lecObj.verypoorA);});
            $.each(data, function(index, lecObj){$('#vpoorB').val(lecObj.verypoorB);});
            $.each(data, function(index, lecObj){$('#vpoorC').val(lecObj.verypoorC);});
            $.each(data, function(index, lecObj){$('#vpoorD').val(lecObj.verypoorD);});
            $.each(data, function(index, lecObj){$('#vpoorE').val(lecObj.verypoorE);});
            $.each(data, function(index, lecObj){$('#vpoorF').val(lecObj.verypoorF);});
            $.each(data, function(index, lecObj){$('#vpoorG').val(lecObj.verypoorG);});

            
            var excellentA = parseInt($('#excellentA').val());
            var excellentB = parseInt($('#excellentB').val());
            var excellentC = parseInt($('#excellentC').val());
            var excellentD = parseInt($('#excellentD').val());
            var excellentE = parseInt($('#excellentE').val());
            var excellentF = parseInt($('#excellentF').val());
            var excellentG = parseInt($('#excellentG').val());
            var goodA = parseInt($('#goodA').val());
            var goodB = parseInt($('#goodB').val());
            var goodC = parseInt($('#goodC').val());
            var goodD = parseInt($('#goodD').val());
            var goodE = parseInt($('#goodE').val());
            var goodF = parseInt($('#goodF').val());
            var goodG = parseInt($('#goodG').val());
            var fairA = parseInt($('#fairA').val());
            var fairB = parseInt($('#fairB').val());
            var fairC = parseInt($('#fairC').val());
            var fairD = parseInt($('#fairD').val());
            var fairE = parseInt($('#fairE').val());
            var fairF = parseInt($('#fairF').val());
            var fairG = parseInt($('#fairG').val());
            var poorA = parseInt($('#poorA').val());
            var poorB = parseInt($('#poorB').val());
            var poorC = parseInt($('#poorC').val());
            var poorD = parseInt($('#poorD').val());
            var poorE = parseInt($('#poorE').val());
            var poorF = parseInt($('#poorF').val());
            var poorG = parseInt($('#poorG').val());
            var vpoorA = parseInt($('#vpoorA').val());
            var vpoorB = parseInt($('#vpoorB').val());
            var vpoorC = parseInt($('#vpoorC').val());
            var vpoorD = parseInt($('#vpoorD').val());
            var vpoorE = parseInt($('#vpoorE').val());
            var vpoorF = parseInt($('#vpoorF').val());
            var vpoorG = parseInt($('#vpoorG').val());

            $('#totalA').val(excellentA+goodA+fairA+poorA+vpoorA);
           $('#totalB').val(excellentB+goodB+fairB+poorB+vpoorB);
           $('#totalC').val(excellentC+goodC+fairC+poorC+vpoorC)
           $('#totalD').val(excellentD+goodD+fairD+poorD+vpoorD)
           $('#totalE').val(excellentE+goodE+fairE+poorE+vpoorE)
           $('#totalF').val(excellentF+goodF+fairF+poorF+vpoorF)
           $('#totalG').val(excellentG+goodG+fairG+poorG+vpoorG)

            $('#eval_program').val(program[1]);
            $('#eval_class').val(clas[1]);
            $('#eval_subj').val(subject[1]);
            $('#eval_section').val(section);
            $('#eval_lecturer').val(lecturer[0]);
            $('#eval_id').val(lecturer[1]);
                              
                        });
                    });

$('#lecturer').on('change', function(e){
            console.log(e);
            var section = $('#section').val();

            var classes = $('#class').val();
            var clas = classes.split("*");

            var subj = $('#subject').val();
            var subject = subj.split("*");

            var prog = $('#program').val();
            var program = prog.split("*");

            var lec = e.target.value;
            var lecturer = lec.split("*");

            $.get({{Auth::user()->position}}'json-lecturerb?program=' + program[0] + '&class=' + clas[0] + '&subject=' +subject[0] + '&section=' +section,function(data){
                console.log(data);


            $.each(data, function(index, lecObj){$('#excellentH').val(lecObj.excellentH);});
            $.each(data, function(index, lecObj){$('#excellentI').val(lecObj.excellentI);});
            $.each(data, function(index, lecObj){$('#excellentJ').val(lecObj.excellentJ);});
            $.each(data, function(index, lecObj){$('#excellentK').val(lecObj.excellentK);});
            $.each(data, function(index, lecObj){$('#excellentL').val(lecObj.excellentL);});
            $.each(data, function(index, lecObj){$('#excellentM').val(lecObj.excellentM);});
            $.each(data, function(index, lecObj){$('#excellentN').val(lecObj.excellentN);});
            $.each(data, function(index, lecObj){$('#excellentO').val(lecObj.excellentO);});
            $.each(data, function(index, lecObj){$('#excellentP').val(lecObj.excellentP);});
            $.each(data, function(index, lecObj){$('#excellentQ').val(lecObj.excellentQ);});
            $.each(data, function(index, lecObj){$('#goodH').val(lecObj.goodH);});
            $.each(data, function(index, lecObj){$('#goodI').val(lecObj.goodI);});
            $.each(data, function(index, lecObj){$('#goodJ').val(lecObj.goodJ);});
            $.each(data, function(index, lecObj){$('#goodK').val(lecObj.goodK);});
            $.each(data, function(index, lecObj){$('#goodL').val(lecObj.goodL);});
            $.each(data, function(index, lecObj){$('#goodM').val(lecObj.goodM);});
            $.each(data, function(index, lecObj){$('#goodN').val(lecObj.goodN);});
            $.each(data, function(index, lecObj){$('#goodO').val(lecObj.goodO);});
            $.each(data, function(index, lecObj){$('#goodP').val(lecObj.goodP);});
            $.each(data, function(index, lecObj){$('#goodQ').val(lecObj.goodQ);});
            $.each(data, function(index, lecObj){$('#fairH').val(lecObj.fairH);});
            $.each(data, function(index, lecObj){$('#fairI').val(lecObj.fairI);});
            $.each(data, function(index, lecObj){$('#fairJ').val(lecObj.fairJ);});
            $.each(data, function(index, lecObj){$('#fairK').val(lecObj.fairK);});
            $.each(data, function(index, lecObj){$('#fairL').val(lecObj.fairL);});
            $.each(data, function(index, lecObj){$('#fairM').val(lecObj.fairM);});
            $.each(data, function(index, lecObj){$('#fairN').val(lecObj.fairN);});
            $.each(data, function(index, lecObj){$('#fairO').val(lecObj.fairO);});
            $.each(data, function(index, lecObj){$('#fairP').val(lecObj.fairP);});
            $.each(data, function(index, lecObj){$('#fairQ').val(lecObj.fairQ);});
            $.each(data, function(index, lecObj){$('#poorH').val(lecObj.poorH);});
            $.each(data, function(index, lecObj){$('#poorI').val(lecObj.poorI);});
            $.each(data, function(index, lecObj){$('#poorJ').val(lecObj.poorJ);});
            $.each(data, function(index, lecObj){$('#poorK').val(lecObj.poorK);});
            $.each(data, function(index, lecObj){$('#poorL').val(lecObj.poorL);});
            $.each(data, function(index, lecObj){$('#poorM').val(lecObj.poorM);});
            $.each(data, function(index, lecObj){$('#poorN').val(lecObj.poorN);});
            $.each(data, function(index, lecObj){$('#poorO').val(lecObj.poorO);});
            $.each(data, function(index, lecObj){$('#poorP').val(lecObj.poorP);});
            $.each(data, function(index, lecObj){$('#poorQ').val(lecObj.poorQ);});
            $.each(data, function(index, lecObj){$('#vpoorH').val(lecObj.verypoorH);});
            $.each(data, function(index, lecObj){$('#vpoorI').val(lecObj.verypoorI);});
            $.each(data, function(index, lecObj){$('#vpoorJ').val(lecObj.verypoorJ);});
            $.each(data, function(index, lecObj){$('#vpoorK').val(lecObj.verypoorK);});
            $.each(data, function(index, lecObj){$('#vpoorL').val(lecObj.verypoorL);});
            $.each(data, function(index, lecObj){$('#vpoorM').val(lecObj.verypoorM);});
            $.each(data, function(index, lecObj){$('#vpoorN').val(lecObj.verypoorN);});
            $.each(data, function(index, lecObj){$('#vpoorO').val(lecObj.verypoorO);});
            $.each(data, function(index, lecObj){$('#vpoorP').val(lecObj.verypoorP);});
            $.each(data, function(index, lecObj){$('#vpoorQ').val(lecObj.verypoorQ);});

            var excellentH = parseInt($('#excellentH').val());
            var excellentI = parseInt($('#excellentI').val());
            var excellentJ = parseInt($('#excellentJ').val());
            var excellentK = parseInt($('#excellentK').val());
            var excellentL = parseInt($('#excellentL').val());
            var excellentM = parseInt($('#excellentM').val());
            var excellentN = parseInt($('#excellentN').val());
            var excellentO = parseInt($('#excellentO').val());
            var excellentP = parseInt($('#excellentP').val());
            var excellentQ = parseInt($('#excellentQ').val());
            var goodH = parseInt($('#goodH').val());
            var goodI = parseInt($('#goodI').val());
            var goodJ = parseInt($('#goodJ').val());
            var goodK = parseInt($('#goodK').val());
            var goodL = parseInt($('#goodL').val());
            var goodM = parseInt($('#goodM').val());
            var goodN = parseInt($('#goodN').val());
            var goodO = parseInt($('#goodO').val());
            var goodP = parseInt($('#goodP').val());
            var goodQ = parseInt($('#goodQ').val());
            var fairH = parseInt($('#fairH').val());
            var fairI = parseInt($('#fairI').val());
            var fairJ = parseInt($('#fairJ').val());
            var fairK = parseInt($('#fairK').val());
            var fairL = parseInt($('#fairL').val());
            var fairM = parseInt($('#fairM').val());
            var fairN = parseInt($('#fairN').val());
            var fairO = parseInt($('#fairO').val());
            var fairP = parseInt($('#fairP').val());
            var fairQ = parseInt($('#fairQ').val());
            var poorH = parseInt($('#poorH').val());
            var poorI = parseInt($('#poorI').val());
            var poorJ = parseInt($('#poorJ').val());
            var poorK = parseInt($('#poorK').val());
            var poorL = parseInt($('#poorL').val());
            var poorM = parseInt($('#poorM').val());
            var poorN = parseInt($('#poorN').val());
            var poorO = parseInt($('#poorO').val());
            var poorP = parseInt($('#poorP').val());
            var poorQ = parseInt($('#poorQ').val());
            var vpoorH = parseInt($('#vpoorH').val());
            var vpoorI = parseInt($('#vpoorI').val());
            var vpoorJ = parseInt($('#vpoorJ').val());
            var vpoorK = parseInt($('#vpoorK').val());
            var vpoorL = parseInt($('#vpoorL').val());
            var vpoorM = parseInt($('#vpoorM').val());
            var vpoorN = parseInt($('#vpoorN').val());
            var vpoorO = parseInt($('#vpoorO').val());
            var vpoorP = parseInt($('#vpoorP').val());
            var vpoorQ = parseInt($('#vpoorQ').val());

            $('#totalH').val(excellentH+goodH+fairH+poorH+vpoorH);
            $('#totalI').val(excellentI+goodI+fairI+poorI+vpoorI);
            $('#totalJ').val(excellentJ+goodJ+fairJ+poorJ+vpoorJ);
            $('#totalK').val(excellentK+goodK+fairK+poorK+vpoorK);
            $('#totalL').val(excellentL+goodL+fairL+poorL+vpoorL);
            $('#totalM').val(excellentM+goodM+fairM+poorM+vpoorM);
            $('#totalN').val(excellentN+goodN+fairN+poorN+vpoorN);
            $('#totalO').val(excellentO+goodO+fairO+poorO+vpoorO);
            $('#totalP').val(excellentP+goodP+fairP+poorP+vpoorP);
            $('#totalQ').val(excellentQ+goodQ+fairQ+poorQ+vpoorQ);
                  
            });
        });

$('#clear').on('click',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');

          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('program', 'lets');
               $.ajax({
               type: 'POST',
               url: "{{Auth::user()->position.'clear-lecturers'}}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: formData,
               processData:false,
                contentType: false,
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
          });
</script>