@extends('errors.main')
@section('title')
Thank You
@stop
@section('main-content')
<div id="wrapper" class="wrapper">
        <div class="content-wrapper">
            <main class="main-wrapper">
                <div class="page-title">
                    <h1 class="color-white">Hoooray!</h1>
                </div>
                <h3 class="mr-b-5 color-white">Thank You</h3>
                <h4 class="mr-b-5 color-white">For Choosing <br/>CBRC - NOVALICHES</h4>
            </main>
        </div>
        <!-- .content-wrapper -->
    </div>
@endsection
