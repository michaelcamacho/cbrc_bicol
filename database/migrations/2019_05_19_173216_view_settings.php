<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ViewSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Account')->nullable();
            $table->string('Branch')->nullable();
            $table->string('Season')->nullable();
            $table->string('Year')->nullable();
            $table->timestamps();
        });
        Schema::create('expense_setups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('branch')->nullable();
            $table->string('Season')->nullable();
            $table->string('Year')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_settings');
        Schema::dropIfExists('expense_setups');
    }
}
