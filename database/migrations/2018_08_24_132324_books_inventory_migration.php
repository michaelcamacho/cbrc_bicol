<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BooksInventoryMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('novaliches_books_inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('branch')->nullable();
            $table->string('program')->nullable();
            $table->string('book_title')->nullable();
            $table->string('book_author')->nullable();
            $table->float('price')->nullable();
            $table->string('available')->nullable();

            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('novaliches_books_inventories');

    }
}
