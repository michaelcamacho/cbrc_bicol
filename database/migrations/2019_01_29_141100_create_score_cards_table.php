<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoreCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('novaliches_score_cards', function (Blueprint $table) {
         
                $table->increments('id');
                $table->string('year')->nullable();
                $table->string('date')->nullable();
                $table->string('season')->nullable();
            //teachers
                $table->string('beed')->nullable();
                $table->string('math')->nullable();
                $table->string('tle')->nullable();
                $table->string('english')->nullable();
                $table->string('filipino')->nullable();
                $table->string('biosci')->nullable();
                $table->string('mapeh')->nullable();
                $table->string('values')->nullable();
                $table->string('afa')->nullable();
                $table->string('ufo')->nullable();
            //end teachers

            //nle
                $table->string('nles_retakers')->nullable();
                $table->string('nles_1stTimers')->nullable();
            //end nle
            
            //crim
                $table->string('crims_retakers')->nullable();
                $table->string('crims_1stTimers')->nullable();
            //end crim  

            //civilservice
                $table->string('civils_retakers')->nullable();
                $table->string('civils_1stTimers')->nullable();
            //end civil
           
            //psycs
                $table->string('psycs_retakers')->nullable();
                $table->string('psycs_1stTimers')->nullable();
            //end psycs
           
            //nclexes
                $table->string('nclexes_retakers')->nullable();
                $table->string('nclexes_1stTimers')->nullable();
            //end nclexes
            
            //ielts
                $table->string('ielts_retakers')->nullable();
                $table->string('ielts_1stTimers')->nullable();
            //end ielts
            
            //socials
                $table->string('socials_retakers')->nullable();
                $table->string('socials_1stTimers')->nullable();
            //end socials
            
            //agris
                $table->string('agris_retakers')->nullable();
                $table->string('agris_1stTimers')->nullable();
            //end agris
            
            
            //midwifery
            $table->string('mids_retakers')->nullable();
            $table->string('mids_1stTimers')->nullable();
            //end midwifery 
            
            //onlines
                $table->string('onlines_retakers')->nullable();
                $table->string('onlines_1stTimers')->nullable();
            //end onlines
            
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('novaliches_score_cards');
    }
}
