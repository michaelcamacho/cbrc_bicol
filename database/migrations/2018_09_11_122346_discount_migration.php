<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DiscountMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('novaliches_discounts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('branch')->nullable();
            $table->string('program')->nullable();
            $table->string('category')->nullable();
            $table->string('discount_category')->nullable();
            $table->string('discount_amount')->nullable();

            $table->timestamps();

        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('novaliches_discounts');      

    }
}
