<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CashBookMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('novaliches_book_cashes', function (Blueprint $table) {
            $table->increments('id');
            $table->float('cash')->nullable();
            $table->timestamps();
        });


         Schema::create('novaliches_remits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date')->nullable();
            $table->string('category')->nullable();
            $table->string('season')->nullable();
            $table->float('amount')->nullable();
            $table->string('remarks')->nullable();
            $table->string('author')->nullable();
            $table->timestamps();
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('novaliches_book_cashes');

        Schema::dropIfExists('novaliches_remits');
       

        
    }
}
