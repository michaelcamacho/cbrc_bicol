<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LecturerMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('novaliches_lecturer_a_evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date')->nullable();
            $table->string('lecturer')->nullable();
            $table->string('branch')->nullable();
            $table->string('program')->nullable();
            $table->string('section')->nullable();
            $table->string('class')->nullable();
            $table->string('subject')->nullable();
            $table->string('aka_class')->nullable();
            $table->string('aka_subject')->nullable();
            $table->string('review_ambassador')->nullable();
            $table->integer('excellentA')->nullable();
            $table->integer('goodA')->nullable();
            $table->integer('fairA')->nullable();
            $table->integer('poorA')->nullable();
            $table->integer('verypoorA')->nullable();
            $table->integer('excellentB')->nullable();
            $table->integer('goodB')->nullable();
            $table->integer('fairB')->nullable();
            $table->integer('poorB')->nullable();
            $table->integer('verypoorB')->nullable();
            $table->integer('excellentC')->nullable();
            $table->integer('goodC')->nullable();
            $table->integer('fairC')->nullable();
            $table->integer('poorC')->nullable();
            $table->integer('verypoorC')->nullable();
            $table->integer('excellentD')->nullable();
            $table->integer('goodD')->nullable();
            $table->integer('fairD')->nullable();
            $table->integer('poorD')->nullable();
            $table->integer('verypoorD')->nullable();
            $table->integer('excellentE')->nullable();
            $table->integer('goodE')->nullable();
            $table->integer('fairE')->nullable();
            $table->integer('poorE')->nullable();
            $table->integer('verypoorE')->nullable();
            $table->integer('excellentF')->nullable();
            $table->integer('goodF')->nullable();
            $table->integer('fairF')->nullable();
            $table->integer('poorF')->nullable();
            $table->integer('verypoorF')->nullable();
            $table->integer('excellentG')->nullable();
            $table->integer('goodG')->nullable();
            $table->integer('fairG')->nullable();
            $table->integer('poorG')->nullable();
            $table->integer('verypoorG')->nullable();
            $table->timestamps();

        });
Schema::create('novaliches_lecturer_b_evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date')->nullable();
            $table->string('lecturer')->nullable();
            $table->string('branch')->nullable();
            $table->string('program')->nullable();
            $table->string('section')->nullable();
            $table->string('class')->nullable();
            $table->string('subject')->nullable();
            $table->string('aka_class')->nullable();
            $table->string('aka_subject')->nullable();
            $table->string('review_ambassador')->nullable();
            $table->integer('excellentH')->nullable();
            $table->integer('goodH')->nullable();
            $table->integer('fairH')->nullable();
            $table->integer('poorH')->nullable();
            $table->integer('verypoorH')->nullable();
            $table->integer('excellentI')->nullable();
            $table->integer('goodI')->nullable();
            $table->integer('fairI')->nullable();
            $table->integer('poorI')->nullable();
            $table->integer('verypoorI')->nullable();
            $table->integer('excellentJ')->nullable();
            $table->integer('goodJ')->nullable();
            $table->integer('fairJ')->nullable();
            $table->integer('poorJ')->nullable();
            $table->integer('verypoorJ')->nullable();
            $table->integer('excellentK')->nullable();
            $table->integer('goodK')->nullable();
            $table->integer('fairK')->nullable();
            $table->integer('poorK')->nullable();
            $table->integer('verypoorK')->nullable();
            $table->integer('excellentL')->nullable();
            $table->integer('goodL')->nullable();
            $table->integer('fairL')->nullable();
            $table->integer('poorL')->nullable();
            $table->integer('verypoorL')->nullable();
            $table->integer('excellentM')->nullable();
            $table->integer('goodM')->nullable();
            $table->integer('fairM')->nullable();
            $table->integer('poorM')->nullable();
            $table->integer('verypoorM')->nullable();
            $table->integer('excellentN')->nullable();
            $table->integer('goodN')->nullable();
            $table->integer('fairN')->nullable();
            $table->integer('poorN')->nullable();
            $table->integer('verypoorN')->nullable();
            $table->integer('excellentO')->nullable();
            $table->integer('goodO')->nullable();
            $table->integer('fairO')->nullable();
            $table->integer('poorO')->nullable();
            $table->integer('verypoorO')->nullable();
            $table->integer('excellentP')->nullable();
            $table->integer('goodP')->nullable();
            $table->integer('fairP')->nullable();
            $table->integer('poorP')->nullable();
            $table->integer('verypoorP')->nullable();
            $table->integer('excellentQ')->nullable();
            $table->integer('goodQ')->nullable();
            $table->integer('fairQ')->nullable();
            $table->integer('poorQ')->nullable();
            $table->integer('verypoorQ')->nullable();
            $table->timestamps();

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('novaliches_lecturer_a_evaluations');

        Schema::dropIfExists('novaliches_lecturer_b_evaluations');

    }
}
