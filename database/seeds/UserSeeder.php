<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //1
        DB::table('users')->insert([
            'username' => 'admin',
            'email' => 'admin@cbrc-novaliches.com',
            'password' => bcrypt('@dmin'),
            'name' =>'New Administrator',
            'role'=>'Admin',
            'branch'=>'Main',
            'show' => '1',
            'remember_token' => str_random(60),
        ]);
        //2
        DB::table('users')->insert([
            'username' => 'novaliches',
            'email' => 'novaliches@cbrc-novaliches.com',
            'password' => bcrypt('novaliches'),
            'name' =>'Abet Gapido',
            'role'=>'Member',
            'branch'=>'Novaliches',
            'show' => '1',
            'remember_token' => str_random(60),
        ]);
        //3

            DB::table('users')->insert([
            'username' => 'cipherfusion',
            'email' => 'mark.ledesma@cipherfusionphilippines.com',
            'password' => bcrypt('ALPHAcipher0909'),
            'name' =>'Mark Anthony Ledesma',
            'role'=>'Admin',
            'branch'=>'Developer',
            'show' => '0',
            'remember_token' => str_random(60),
        ]);
            //4
            DB::table('users')->insert([
            'username' => 'ra_novaliches',
            'email' => 'mary@cbrc-novaliches.com',
            'password' => bcrypt('grace0099'),
            'name' =>'Mary Grace Tolentino',
            'role'=>'ra_novaliches',
            'branch'=>'Novaliches',
            'show' => '0',
            'remember_token' => str_random(60),
        ]);
        
        //6
        DB::table('users')->insert([
            'username' => 'enrollment',
            'email' => 'enrollment@cbrc-novaliches.com',
            'password' => bcrypt('enrollment'),
            'name' =>'John Doe2',
            'role'=>'novaliches_enrollment',
            'branch'=>'Novaliches',
            'show' => '1',
            'remember_token' => str_random(60),
        ]);
        //7
        DB::table('users')->insert([
            'username' => 'book_cashier',
            'email' => 'book_cashier@cbrc-novaliches.com',
            'password' => bcrypt('book_cashier'),
            'name' =>'John Doe3',
            'role'=>'novaliches_cashier',
            'branch'=>'Novaliches',
            'show' => '1',
            'remember_token' => str_random(60),
        ]);
        


        //1
         DB::table('roles')->insert([
            'name' => 'admin',
            'display_name' =>'Administrator',
            'description'=>'Admin of this app'
        ]);
        //2
          DB::table('roles')->insert([
            'name' => 'novaliches',
            'display_name' =>'Member',
            'description'=>'Member of this app'
        ]);
        //3
          DB::table('roles')->insert([
            'name' => 'ra_novaliches',
            'display_name' =>'Review Ambassador',
            'description'=>'RA of this app'
        ]);
        
        //5
        DB::table('roles')->insert([
            'name' => 'novaliches_enrollment',
            'display_name' =>'Enrollment',
            'description'=>'Enrollment of this app'
        ]);
        //6
        DB::table('roles')->insert([
            'name' => 'novaliches_cashier',
            'display_name' =>'Book Cashier',
            'description'=>'Book Cashier of this app'
        ]);
        


        //1
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' =>'1'
        ]);
        //2
          DB::table('role_user')->insert([
            'user_id' => '2',
            'role_id' =>'2'
        ]);
        
          DB::table('role_user')->insert([
            'user_id' => '3',
            'role_id' =>'1'
        ]);
        //3
        DB::table('role_user')->insert([
            'user_id' => '4',
            'role_id' =>'3'
        ]);
       
        //5
        DB::table('role_user')->insert([
            'user_id' => '5',
            'role_id' =>'4'
        ]);
        //6
        DB::table('role_user')->insert([
            'user_id' => '6',
            'role_id' =>'5'
        ]);
        
        
    }
}
