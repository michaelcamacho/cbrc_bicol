<?php

use Illuminate\Database\Seeder;

class BranchesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('branches')->insert([
        	'branch_name' => 'Novaliches',
            'aka' => 'novaliches',
        ]);
    
    }
}
