<?php


Route::get('/', function () {
    return view('auth.login');
});

/* START DAET */
/* App */
Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


Route::get('/user/activation/{token}', 'Auth\RegisterController@userActivation');

Route::get('cbrc/online-registration','RegistrationController@index');

Route::post('new-register','RegistrationController@register');

Route::get('thank-you','RegistrationController@thank_you');

Route::get('/json-major','MainController@fetch_major');


/* Admin */
Route::get('/dashboard','AdminController@dashboard')->middleware('auth');
Route::get('/total-program','AdminController@total_program')->middleware('auth');
Route::get('/total-agri','AdminController@total_agri')->middleware('auth');
Route::get('/total-civil','AdminController@total_civil')->middleware('auth');
Route::get('/total-crim','AdminController@total_crim')->middleware('auth');
Route::get('/total-ielts','AdminController@total_ielts')->middleware('auth');
Route::get('/total-let','AdminController@total_let')->middleware('auth');
Route::get('/total-nclex','AdminController@total_nclex')->middleware('auth');
Route::get('/total-nle','AdminController@total_nle')->middleware('auth');
Route::get('/total-psyc','AdminController@total_psyc')->middleware('auth');
Route::get('/total-mid','AdminController@total_mid')->middleware('auth');
Route::get('/total-online','AdminController@total_online')->middleware('auth');
Route::get('/total-social','AdminController@total_social')->middleware('auth');
Route::get('/total-dropped','AdminController@total_dropped')->middleware('auth');
Route::get('/total-scholars','AdminController@total_scholars')->middleware('auth');
Route::get('/tuition-fees','AdminController@tuition_fees')->middleware('auth');
Route::get('/discounts','AdminController@discounts')->middleware('auth');
Route::get('/total-books','AdminController@total_books')->middleware('auth');
Route::get('/total-expense','AdminController@total_expense')->middleware('auth');
Route::get('/users','AdminController@user')->middleware('auth');
Route::get('/daet/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/naga/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/masbate/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/sorsogon/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/legaspi/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/expense-settings','AdminController@expense_settings')->middleware('auth');
Route::post('/setup-expense','AdminController@expense_setup')->middleware('auth');



// ======================= RA dashbaord Routes ===========================

Route::get('/radashboard/attendance', 'AdminController@attendance');
Route::get('/radashboard/evaluation', 'AdminController@evaluation');
Route::get('/radashboard/lecturer', 'AdminController@lecturer');
Route::get('/radashboard/result-analysis', 'AdminController@result_analysis');



/* Add Data */
Route::post('/insert-tuition','AdminController@insert_tuition')->middleware('auth');
Route::post('/insert-discount','AdminController@insert_discount')->middleware('auth');
Route::post('/insert-book','AdminController@insert_book')->middleware('auth');
Route::post('/insert-user','AdminController@insert_user')->middleware('auth');
Route::post('/save-adsettings','AdminController@save_adsettings')->middleware('auth');

/* Update Data */
Route::post('/update-tuition','AdminController@update_tuition')->middleware('auth');
Route::post('/update-discount','AdminController@update_discount')->middleware('auth');
Route::post('/update-book','AdminController@update_book')->middleware('auth');
Route::post('/update-user','AdminController@update_user')->middleware('auth');

/* Delete Data */
Route::post('/delete-tuition','AdminController@delete_tuition')->middleware('auth');
Route::post('/delete-discount','AdminController@delete_discount')->middleware('auth');
Route::post('/delete-book','AdminController@delete_book')->middleware('auth');
Route::delete('/delete-user','AdminController@delete_user')->middleware('auth');

/* Member */


//==========================================Daet====================================//
/* daet */
Route::get('/daet/dashboard','DaetController@index')->middleware('auth');
Route::get('/daet/add-enrollee','DaetController@add_enrollee')->middleware('auth');
Route::get('/daet/new-payment','DaetController@new_payment')->middleware('auth');
Route::get('/daet/new-reservation','DaetController@new_reservation')->middleware('auth');
Route::get('/daet/add-expense','DaetController@add_expense')->middleware('auth');
Route::get('/daet/add-budget','DaetController@add_budget')->middleware('auth');
Route::get('/daet/book-payment','DaetController@book_payment')->middleware('auth');
Route::get('/daet/new-remit', 'DaetController@new_remit')->middleware('auth');



/* Data fetch */
Route::get('/daet/json-student','DaetController@fetch_student')->middleware('auth');
Route::get('/daet/json-tuition','DaetController@fetch_tuition')->middleware('auth');
Route::get('/daet/json-discount','DaetController@fetch_discount')->middleware('auth');
Route::get('/daet/json-balance','DaetController@fetch_balance')->middleware('auth');
Route::get('/daet/json-expense','DaetController@fetch_expense')->middleware('auth');
Route::get('/daet/json-book','DaetController@fetch_book')->middleware('auth');
Route::get('/daet/json-price','DaetController@fetch_book_price')->middleware('auth');
Route::get('/daet/json-reserved','DaetController@fetch_reserved')->middleware('auth');
Route::get('/daet/json-id','DaetController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/daet/insert-enrollee','DaetController@insert_enrollee')->middleware('auth');
Route::post('/daet/insert-new-payment','DaetController@add_new_payment')->middleware('auth');
Route::post('/daet/insert-new-expense','DaetController@add_new_expense')->middleware('auth');
Route::post('/daet/insert-new-budget','DaetController@insert_new_budget')->middleware('auth');
Route::post('/daet/insert-book-payment', 'DaetController@insert_book_payment')->middleware('auth');
Route::post('/daet/insert-book-transfer', 'DaetController@insert_book_transfer')->middleware('auth');
Route::post('/daet/drop-student', 'DaetController_@drop_student')->middleware('auth');
Route::post('/daet/insert-remit', 'DaetController@insert_remit')->middleware('auth');
Route::post('/daet/insert-reservation','DaetController@insert_reservation')->middleware('auth');
Route::post('/daet/insert-employee','DaetController@insert_employee')->middleware('auth');


/* Update Data */
Route::post('/daet/update-enrollee','DaetController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/daet/delete-enrollee','DaetController@delete_enrollee')->middleware('auth');
/* Tables */

	/* Enrollee tables */
Route::get('/daet/let','DaetController@let_table')->middleware('auth');
Route::get('/daet/nle','DaetController@nle_table')->middleware('auth');
Route::get('/daet/crim','DaetController@crim_table')->middleware('auth');
Route::get('/daet/civil','DaetController@civil_table')->middleware('auth');
Route::get('/daet/psyc','DaetController@psyc_table')->middleware('auth');
Route::get('/daet/nclex','DaetController@nclex_table')->middleware('auth');
Route::get('/daet/ielts','DaetController@ielts_table')->middleware('auth');
Route::get('/daet/social','DaetController@social_table')->middleware('auth');
Route::get('/daet/agri','DaetController@agri_table')->middleware('auth');
Route::get('/daet/mid','DaetController@mid_table')->middleware('auth');
Route::get('/daet/online','DaetController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/daet/tuition','DaetController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/daet/scholar','DaetController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/daet/enrolled','DaetController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/daet/dropped','DaetController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/daet/sales-enrollee','DaetController@sales_enrollee_table')->middleware('auth');
Route::get('/daet/sales-program','DaetController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/daet/receivable-enrollee','DaetController@receivable_enrollee_table')->middleware('auth');
Route::get('/daet/receivable-program','DaetController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/daet/expense','DaetController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/daet/books','DaetController@books_table')->middleware('auth');
Route::get('/daet/remit', 'DaetController@remit')->middleware('auth');
Route::get('/daet/reservation', 'DaetController@reservation_table')->middleware('auth');

Route::post('daet/clear-enrollee','DaetController@clear_enrollee')->middleware('auth');
Route::post('daet/clear-sale-season1','DaetController@clear_sale_season1')->middleware('auth');
Route::post('daet/clear-sale-season2','DaetController@clear_sale_season2')->middleware('auth');
Route::post('daet/clear-receivable','DaetController@clear_receivable')->middleware('auth');
Route::post('daet/clear-expense','DaetController@clear_expense')->middleware('auth');
Route::post('daet/clear-book','DaetController@clear_book')->middleware('auth');
Route::post('daet/clear-reservation','DaetController@clear_reservation')->middleware('auth');

/* Daet Lecturer */
Route::get('daet/lecturer-evaluation','DaetController@lec_eval')->middleware('auth');
Route::get('daet/add-lecturer','DaetController@add_lec')->middleware('auth');
Route::get('daet/evaluate-lecturer','DaetController@eval_lec')->middleware('auth');
Route::get('daet/json-class','DaetController@fetch_class')->middleware('auth');
Route::get('daet/json-subject','DaetController@fetch_subject')->middleware('auth');
Route::get('daet/json-section','DaetController@fetch_section')->middleware('auth');
Route::get('daet/json-lecturer','DaetController@fetch_lecturer')->middleware('auth');
Route::get('daet/json-lecturerb','DaetController@fetch_lecturerb')->middleware('auth');

Route::post('daet/insert-lecturer','DaetController@insert_lecturer')->middleware('auth');
Route::post('daet/insert-evaluation','DaetController@insert_eval')->middleware('auth');

Route::post('daet/clear-lecturers','DaetController@clear_lecturers')->middleware('auth');


/* Reports Daet */

Route::get('daet/today','DaetController@today')->middleware('auth');
Route::get('daet/yesterday','DaetController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Daet */

Route::get('daet/budget','DaetController@budget_record')->middleware('auth');
Route::post('daet/clear-budget','DaetController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('daet/delete-sale/s1/{id}','DaetController@delete_sale1');
Route::get('daet/delete-sale/s2/{id}','DaetController@delete_sale2');
/* End Delete */

/** csv */
Route::post('daet/csv-enrollee','DaetController@csv_enrollee')->middleware('auth');

/* employee */

Route::get('/daet/add-employee','DaetController@add_employee')->middleware('auth');
Route::get('/daet/employee-record','DaetController@employee_record')->middleware('auth');
Route::post('/daet/update-employee','DaetController@update_employee')->middleware('auth');
Route::get('/daet/delete-employee/{id}','DaetController@delete_employee')->middleware('auth');
Route::get('/taskHistory','DaetController@taskHistory')->middleware('auth');

// Member
Route::get('/daet/view-employee','DaetController@view_employee')->middleware('auth');

// student id
Route::get('/daet/student-id','DaetController@student_id')->middleware('auth');

// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\DaetController@bulletin');
Route::get('/fetch-branches/{bn}','DaetController@fetch_branches')->middleware('auth');

// financialreport 
Route::get('daet/financial-report','DaetController@financialreport')->middleware('auth');

/** scorecard */
Route::get('daet/scorecard-season/{season}','DaetController@scorecard')->middleware('auth');

/* check username and email if exist*/
Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

Route::get('/daet/bookTransferRecord','DaetController@bookTransfer_table')->middleware('auth');
Route::get('/daet/book-transfer','DaetController@book_transfer')->middleware('auth');


Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


Route::get('/radashboard/viewLecturerReports', 'DaetController@lecturerView');
Route::get('/radashboard/onlineCompletion', 'DaetController@onlineCompletion');
Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');


Route::get('/fetch-book-list/{id}','DaetController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/daet/delete-expense/{id}','DaetController@delete_expense')->middleware('auth');


//=============Daet Enrollment =========

/* Data fetch */
Route::get('/daet-enrollment/json-student','DaetEnrollmentController@fetch_student')->middleware('auth');
Route::get('/daet-enrollment/json-tuition','DaetEnrollmentController@fetch_tuition')->middleware('auth');
Route::get('/daet-enrollment/json-discount','DaetEnrollmentController@fetch_discount')->middleware('auth');
Route::get('/daet-enrollment/json-balance','DaetEnrollmentController@fetch_balance')->middleware('auth');
Route::get('/daet-enrollment/json-id','DaetEnrollmentController@fetch_id')->middleware('auth');
Route::get('/daet-enrollment/json-reserved','DaetEnrollmentController@fetch_reserved')->middleware('auth');

/* Add Data */
Route::post('/daet-enrollment/drop-student', 'DaetEnrollmentController_@drop_student')->middleware('auth');
Route::post('/daet-enrollment/insert-enrollee','DaetEnrollmentController@insert_enrollee')->middleware('auth');

Route::get('/daet-enrollment/add-enrollee','DaetEnrollmentController@add_enrollee')->middleware('auth');
Route::post('/daet-enrollment/insert-enrollee','DaetEnrollmentController@insert_enrollee')->middleware('auth');
Route::post('/daet-enrollment/insert-new-payment','DaetEnrollmentController@add_new_payment')->middleware('auth');
Route::post('/daet-enrollment/update-enrollee','DaetEnrollmentController@update_enrollee')->middleware('auth');
Route::post('enrollment-csv-enrollee','DaetEnrollmentController@csv_enrollee')->middleware('auth');

Route::get('/daet-enrollment/new-payment','DaetEnrollmentController@new_payment')->middleware('auth');
Route::get('/daet-enrollment/enrolled','DaetEnrollmentController@enrolled_table')->middleware('auth');
Route::get('/daet-enrollment/sales-enrollee','DaetEnrollmentController@sales_enrollee_table')->middleware('auth');
Route::get('/daet-enrollment/dropped','DaetEnrollmentController@dropped_table')->middleware('auth');

Route::post('/daet-enrollment/insert-reservation','DaetEnrollmentController@insert_reservation')->middleware('auth');
Route::get('/daet-enrollment/new-reservation','DaetEnrollmentController@new_reservation')->middleware('auth');
Route::get('/daet-enrollment/reservation', 'DaetEnrollmentController@reservation_table')->middleware('auth');


/* Enrollee tables */
Route::get('/daet-enrollment/let','DaetEnrollmentController@let_table')->middleware('auth');
Route::get('/daet-enrollment/nle','DaetEnrollmentController@nle_table')->middleware('auth');
Route::get('/daet-enrollment/crim','DaetEnrollmentController@crim_table')->middleware('auth');
Route::get('/daet-enrollment/civil','DaetEnrollmentController@civil_table')->middleware('auth');
Route::get('/daet-enrollment/psyc','DaetEnrollmentController@psyc_table')->middleware('auth');
Route::get('/daet-enrollment/nclex','DaetEnrollmentController@nclex_table')->middleware('auth');
Route::get('/daet-enrollment/ielts','DaetEnrollmentController@ielts_table')->middleware('auth');
Route::get('/daet-enrollment/social','DaetEnrollmentController@social_table')->middleware('auth');
Route::get('/daet-enrollment/agri','DaetEnrollmentController@agri_table')->middleware('auth');
Route::get('/daet-enrollment/mid','DaetEnrollmentController@mid_table')->middleware('auth');
Route::get('/daet-enrollment/online','DaetEnrollmentController@online_table')->middleware('auth');

//============Daet Cashier books=================
Route::get('/daet-cashier/books','DaetCashierController@books_table')->middleware('auth');

Route::post('/daet-cashier/insert-book-payment', 'DaetCashierController@insert_book_payment')->middleware('auth');
Route::get('/daet-cashier/book-payment','DaetCashierController@book_payment')->middleware('auth');

Route::get('/daet-cashier/json-book','DaetCashierController@fetch_book')->middleware('auth');
Route::get('/daet-cashier/json-price','DaetCashierController@fetch_book_price')->middleware('auth');

//==========================================Daet====================================//



//==========================================Naga====================================//
/* naga */
Route::get('/naga/dashboard','NagaController@index')->middleware('auth');
Route::get('/naga/add-enrollee','NagaController@add_enrollee')->middleware('auth');
Route::get('/naga/new-payment','NagaController@new_payment')->middleware('auth');
Route::get('/naga/new-reservation','NagaController@new_reservation')->middleware('auth');
Route::get('/naga/add-expense','NagaController@add_expense')->middleware('auth');
Route::get('/naga/add-budget','NagaController@add_budget')->middleware('auth');
Route::get('/naga/book-payment','NagaController@book_payment')->middleware('auth');
Route::get('/naga/new-remit', 'NagaController@new_remit')->middleware('auth');



/* Data fetch */
Route::get('/naga/json-student','NagaController@fetch_student')->middleware('auth');
Route::get('/naga/json-tuition','NagaController@fetch_tuition')->middleware('auth');
Route::get('/naga/json-discount','NagaController@fetch_discount')->middleware('auth');
Route::get('/naga/json-balance','NagaController@fetch_balance')->middleware('auth');
Route::get('/naga/json-expense','NagaController@fetch_expense')->middleware('auth');
Route::get('/naga/json-book','NagaController@fetch_book')->middleware('auth');
Route::get('/naga/json-price','NagaController@fetch_book_price')->middleware('auth');
Route::get('/naga/json-reserved','NagaController@fetch_reserved')->middleware('auth');
Route::get('/naga/json-id','NagaController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/naga/insert-enrollee','NagaController@insert_enrollee')->middleware('auth');
Route::post('/naga/insert-new-payment','NagaController@add_new_payment')->middleware('auth');
Route::post('/naga/insert-new-expense','NagaController@add_new_expense')->middleware('auth');
Route::post('/naga/insert-new-budget','NagaController@insert_new_budget')->middleware('auth');
Route::post('/naga/insert-book-payment', 'NagaController@insert_book_payment')->middleware('auth');
Route::post('/naga/insert-book-transfer', 'NagaController@insert_book_transfer')->middleware('auth');
Route::post('/naga/drop-student', 'NagaController_@drop_student')->middleware('auth');
Route::post('/naga/insert-remit', 'NagaController@insert_remit')->middleware('auth');
Route::post('/naga/insert-reservation','NagaController@insert_reservation')->middleware('auth');
Route::post('/naga/insert-employee','NagaController@insert_employee')->middleware('auth');


/* Update Data */
Route::post('/naga/update-enrollee','NagaController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/naga/delete-enrollee','NagaController@delete_enrollee')->middleware('auth');
/* Tables */

	/* Enrollee tables */
Route::get('/naga/let','NagaController@let_table')->middleware('auth');
Route::get('/naga/nle','NagaController@nle_table')->middleware('auth');
Route::get('/naga/crim','NagaController@crim_table')->middleware('auth');
Route::get('/naga/civil','NagaController@civil_table')->middleware('auth');
Route::get('/naga/psyc','NagaController@psyc_table')->middleware('auth');
Route::get('/naga/nclex','NagaController@nclex_table')->middleware('auth');
Route::get('/naga/ielts','NagaController@ielts_table')->middleware('auth');
Route::get('/naga/social','NagaController@social_table')->middleware('auth');
Route::get('/naga/agri','NagaController@agri_table')->middleware('auth');
Route::get('/naga/mid','NagaController@mid_table')->middleware('auth');
Route::get('/naga/online','NagaController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/naga/tuition','NagaController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/naga/scholar','NagaController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/naga/enrolled','NagaController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/naga/dropped','NagaController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/naga/sales-enrollee','NagaController@sales_enrollee_table')->middleware('auth');
Route::get('/naga/sales-program','NagaController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/naga/receivable-enrollee','NagaController@receivable_enrollee_table')->middleware('auth');
Route::get('/naga/receivable-program','NagaController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/naga/expense','NagaController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/naga/books','NagaController@books_table')->middleware('auth');
Route::get('/naga/remit', 'NagaController@remit')->middleware('auth');
Route::get('/naga/reservation', 'NagaController@reservation_table')->middleware('auth');

Route::post('naga/clear-enrollee','NagaController@clear_enrollee')->middleware('auth');
Route::post('naga/clear-sale-season1','NagaController@clear_sale_season1')->middleware('auth');
Route::post('naga/clear-sale-season2','NagaController@clear_sale_season2')->middleware('auth');
Route::post('naga/clear-receivable','NagaController@clear_receivable')->middleware('auth');
Route::post('naga/clear-expense','NagaController@clear_expense')->middleware('auth');
Route::post('naga/clear-book','NagaController@clear_book')->middleware('auth');
Route::post('naga/clear-reservation','NagaController@clear_reservation')->middleware('auth');

/* Daet Lecturer */
Route::get('naga/lecturer-evaluation','NagaController@lec_eval')->middleware('auth');
Route::get('naga/add-lecturer','NagaController@add_lec')->middleware('auth');
Route::get('naga/evaluate-lecturer','NagaController@eval_lec')->middleware('auth');
Route::get('naga/json-class','NagaController@fetch_class')->middleware('auth');
Route::get('naga/json-subject','NagaController@fetch_subject')->middleware('auth');
Route::get('naga/json-section','NagaController@fetch_section')->middleware('auth');
Route::get('naga/json-lecturer','NagaController@fetch_lecturer')->middleware('auth');
Route::get('naga/json-lecturerb','NagaController@fetch_lecturerb')->middleware('auth');

Route::post('naga/insert-lecturer','NagaController@insert_lecturer')->middleware('auth');
Route::post('naga/insert-evaluation','NagaController@insert_eval')->middleware('auth');

Route::post('naga/clear-lecturers','NagaController@clear_lecturers')->middleware('auth');


/* Reports Naga */

Route::get('naga/today','NagaController@today')->middleware('auth');
Route::get('naga/yesterday','NagaController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Naga */

Route::get('naga/budget','NagaController@budget_record')->middleware('auth');
Route::post('naga/clear-budget','NagaController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('naga/delete-sale/s1/{id}','NagaController@delete_sale1');
Route::get('naga/delete-sale/s2/{id}','NagaController@delete_sale2');
/* End Delete */

/** csv */
Route::post('naga/csv-enrollee','NagaController@csv_enrollee')->middleware('auth');

/* employee */

Route::get('/naga/add-employee','NagaController@add_employee')->middleware('auth');
Route::get('/naga/employee-record','NagaController@employee_record')->middleware('auth');
Route::post('/naga/update-employee','NagaController@update_employee')->middleware('auth');
Route::get('/naga/delete-employee/{id}','NagaController@delete_employee')->middleware('auth');
Route::get('/taskHistory','NagaController@taskHistory')->middleware('auth');

// Member
Route::get('/naga/view-employee','NagaController@view_employee')->middleware('auth');

// student id
Route::get('/naga/student-id','NagaController@student_id')->middleware('auth');

// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\NagaController@bulletin');
Route::get('/fetch-branches/{bn}','NagaController@fetch_branches')->middleware('auth');

// financialreport 
Route::get('naga/financial-report','NagaController@financialreport')->middleware('auth');

/** scorecard */
Route::get('naga/scorecard-season/{season}','NagaController@scorecard')->middleware('auth');

/* check username and email if exist*/
Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

Route::get('/naga/bookTransferRecord','NagaController@bookTransfer_table')->middleware('auth');
Route::get('/naga/book-transfer','NagaController@book_transfer')->middleware('auth');


Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


Route::get('/radashboard/viewLecturerReports', 'NagaController@lecturerView');
Route::get('/radashboard/onlineCompletion', 'NagaController@onlineCompletion');
Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');


Route::get('/fetch-book-list/{id}','NagaController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/naga/delete-expense/{id}','NagaController@delete_expense')->middleware('auth');


//=============Naga Enrollment =========

/* Data fetch */
Route::get('/naga-enrollment/json-student','NagaEnrollmentController@fetch_student')->middleware('auth');
Route::get('/naga-enrollment/json-tuition','NagaEnrollmentController@fetch_tuition')->middleware('auth');
Route::get('/naga-enrollment/json-discount','NagaEnrollmentController@fetch_discount')->middleware('auth');
Route::get('/naga-enrollment/json-balance','NagaEnrollmentController@fetch_balance')->middleware('auth');
Route::get('/naga-enrollment/json-id','NagaEnrollmentController@fetch_id')->middleware('auth');
Route::get('/naga-enrollment/json-reserved','NagaEnrollmentController@fetch_reserved')->middleware('auth');

/* Add Data */
Route::post('/naga-enrollment/drop-student', 'NagaEnrollmentController_@drop_student')->middleware('auth');
Route::post('/naga-enrollment/insert-enrollee','NagaEnrollmentController@insert_enrollee')->middleware('auth');

Route::get('/naga-enrollment/add-enrollee','NagaEnrollmentController@add_enrollee')->middleware('auth');
Route::post('/naga-enrollment/insert-enrollee','NagaEnrollmentController@insert_enrollee')->middleware('auth');
Route::post('/naga-enrollment/insert-new-payment','NagaEnrollmentController@add_new_payment')->middleware('auth');
Route::post('/naga-enrollment/update-enrollee','NagaEnrollmentController@update_enrollee')->middleware('auth');
Route::post('enrollment-csv-enrollee','NagaEnrollmentController@csv_enrollee')->middleware('auth');

Route::get('/naga-enrollment/new-payment','NagaEnrollmentController@new_payment')->middleware('auth');
Route::get('/naga-enrollment/enrolled','NagaEnrollmentController@enrolled_table')->middleware('auth');
Route::get('/naga-enrollment/sales-enrollee','NagaEnrollmentController@sales_enrollee_table')->middleware('auth');
Route::get('/naga-enrollment/dropped','NagaEnrollmentController@dropped_table')->middleware('auth');

Route::post('/naga-enrollment/insert-reservation','NagaEnrollmentController@insert_reservation')->middleware('auth');
Route::get('/naga-enrollment/new-reservation','NagaEnrollmentController@new_reservation')->middleware('auth');
Route::get('/naga-enrollment/reservation', 'NagaEnrollmentController@reservation_table')->middleware('auth');


/* Enrollee tables */
Route::get('/naga-enrollment/let','NagaEnrollmentController@let_table')->middleware('auth');
Route::get('/naga-enrollment/nle','NagaEnrollmentController@nle_table')->middleware('auth');
Route::get('/naga-enrollment/crim','NagaEnrollmentController@crim_table')->middleware('auth');
Route::get('/naga-enrollment/civil','NagaEnrollmentController@civil_table')->middleware('auth');
Route::get('/naga-enrollment/psyc','NagaEnrollmentController@psyc_table')->middleware('auth');
Route::get('/naga-enrollment/nclex','NagaEnrollmentController@nclex_table')->middleware('auth');
Route::get('/naga-enrollment/ielts','NagaEnrollmentController@ielts_table')->middleware('auth');
Route::get('/naga-enrollment/social','NagaEnrollmentController@social_table')->middleware('auth');
Route::get('/naga-enrollment/agri','NagaEnrollmentController@agri_table')->middleware('auth');
Route::get('/naga-enrollment/mid','NagaEnrollmentController@mid_table')->middleware('auth');
Route::get('/naga-enrollment/online','NagaEnrollmentController@online_table')->middleware('auth');

//============Naga Cashier books=================
Route::get('/naga-cashier/books','NagaCashierController@books_table')->middleware('auth');

Route::post('/naga-cashier/insert-book-payment', 'NagaCashierController@insert_book_payment')->middleware('auth');
Route::get('/naga-cashier/book-payment','NagaCashierController@book_payment')->middleware('auth');

Route::get('/naga-cashier/json-book','NagaCashierController@fetch_book')->middleware('auth');
Route::get('/naga-cashier/json-price','NagaCashierController@fetch_book_price')->middleware('auth');

//==========================================Naga====================================//



//==========================================Masbate====================================//
/* masbate */
Route::get('/masbate/dashboard','MasbateController@index')->middleware('auth');
Route::get('/masbate/add-enrollee','MasbateController@add_enrollee')->middleware('auth');
Route::get('/masbate/new-payment','MasbateController@new_payment')->middleware('auth');
Route::get('/masbate/new-reservation','MasbateController@new_reservation')->middleware('auth');
Route::get('/masbate/add-expense','MasbateController@add_expense')->middleware('auth');
Route::get('/masbate/add-budget','MasbateController@add_budget')->middleware('auth');
Route::get('/masbate/book-payment','MasbateController@book_payment')->middleware('auth');
Route::get('/masbate/new-remit', 'MasbateController@new_remit')->middleware('auth');



/* Data fetch */
Route::get('/masbate/json-student','MasbateController@fetch_student')->middleware('auth');
Route::get('/masbate/json-tuition','MasbateController@fetch_tuition')->middleware('auth');
Route::get('/masbate/json-discount','MasbateController@fetch_discount')->middleware('auth');
Route::get('/masbate/json-balance','MasbateController@fetch_balance')->middleware('auth');
Route::get('/masbate/json-expense','MasbateController@fetch_expense')->middleware('auth');
Route::get('/masbate/json-book','MasbateController@fetch_book')->middleware('auth');
Route::get('/masbate/json-price','MasbateController@fetch_book_price')->middleware('auth');
Route::get('/masbate/json-reserved','MasbateController@fetch_reserved')->middleware('auth');
Route::get('/masbate/json-id','MasbateController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/masbate/insert-enrollee','MasbateController@insert_enrollee')->middleware('auth');
Route::post('/masbate/insert-new-payment','MasbateController@add_new_payment')->middleware('auth');
Route::post('/masbate/insert-new-expense','MasbateController@add_new_expense')->middleware('auth');
Route::post('/masbate/insert-new-budget','MasbateController@insert_new_budget')->middleware('auth');
Route::post('/masbate/insert-book-payment', 'MasbateController@insert_book_payment')->middleware('auth');
Route::post('/masbate/insert-book-transfer', 'MasbateController@insert_book_transfer')->middleware('auth');
Route::post('/masbate/drop-student', 'MasbateController_@drop_student')->middleware('auth');
Route::post('/masbate/insert-remit', 'MasbateController@insert_remit')->middleware('auth');
Route::post('/masbate/insert-reservation','MasbateController@insert_reservation')->middleware('auth');
Route::post('/masbate/insert-employee','MasbateController@insert_employee')->middleware('auth');


/* Update Data */
Route::post('/masbate/update-enrollee','MasbateController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/masbate/delete-enrollee','MasbateController@delete_enrollee')->middleware('auth');
/* Tables */

	/* Enrollee tables */
Route::get('/masbate/let','MasbateController@let_table')->middleware('auth');
Route::get('/masbate/nle','MasbateController@nle_table')->middleware('auth');
Route::get('/masbate/crim','MasbateController@crim_table')->middleware('auth');
Route::get('/masbate/civil','MasbateController@civil_table')->middleware('auth');
Route::get('/masbate/psyc','MasbateController@psyc_table')->middleware('auth');
Route::get('/masbate/nclex','MasbateController@nclex_table')->middleware('auth');
Route::get('/masbate/ielts','MasbateController@ielts_table')->middleware('auth');
Route::get('/masbate/social','MasbateController@social_table')->middleware('auth');
Route::get('/masbate/agri','MasbateController@agri_table')->middleware('auth');
Route::get('/masbate/mid','MasbateController@mid_table')->middleware('auth');
Route::get('/masbate/online','MasbateController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/masbate/tuition','MasbateController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/masbate/scholar','MasbateController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/masbate/enrolled','MasbateController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/masbate/dropped','MasbateController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/masbate/sales-enrollee','MasbateController@sales_enrollee_table')->middleware('auth');
Route::get('/masbate/sales-program','MasbateController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/masbate/receivable-enrollee','MasbateController@receivable_enrollee_table')->middleware('auth');
Route::get('/masbate/receivable-program','MasbateController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/masbate/expense','MasbateController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/masbate/books','MasbateController@books_table')->middleware('auth');
Route::get('/masbate/remit', 'MasbateController@remit')->middleware('auth');
Route::get('/masbate/reservation', 'MasbateController@reservation_table')->middleware('auth');

Route::post('masbate/clear-enrollee','MasbateController@clear_enrollee')->middleware('auth');
Route::post('masbate/clear-sale-season1','MasbateController@clear_sale_season1')->middleware('auth');
Route::post('masbate/clear-sale-season2','MasbateController@clear_sale_season2')->middleware('auth');
Route::post('masbate/clear-receivable','MasbateController@clear_receivable')->middleware('auth');
Route::post('masbate/clear-expense','MasbateController@clear_expense')->middleware('auth');
Route::post('masbate/clear-book','MasbateController@clear_book')->middleware('auth');
Route::post('masbate/clear-reservation','MasbateController@clear_reservation')->middleware('auth');

/* Daet Lecturer */
Route::get('masbate/lecturer-evaluation','MasbateController@lec_eval')->middleware('auth');
Route::get('masbate/add-lecturer','MasbateController@add_lec')->middleware('auth');
Route::get('masbate/evaluate-lecturer','MasbateController@eval_lec')->middleware('auth');
Route::get('masbate/json-class','MasbateController@fetch_class')->middleware('auth');
Route::get('masbate/json-subject','MasbateController@fetch_subject')->middleware('auth');
Route::get('masbate/json-section','MasbateController@fetch_section')->middleware('auth');
Route::get('masbate/json-lecturer','MasbateController@fetch_lecturer')->middleware('auth');
Route::get('masbate/json-lecturerb','MasbateController@fetch_lecturerb')->middleware('auth');

Route::post('masbate/insert-lecturer','MasbateController@insert_lecturer')->middleware('auth');
Route::post('masbate/insert-evaluation','MasbateController@insert_eval')->middleware('auth');

Route::post('masbate/clear-lecturers','MasbateController@clear_lecturers')->middleware('auth');


/* Reports Masbate */

Route::get('masbate/today','MasbateController@today')->middleware('auth');
Route::get('masbate/yesterday','MasbateController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Masbate */

Route::get('masbate/budget','MasbateController@budget_record')->middleware('auth');
Route::post('masbate/clear-budget','MasbateController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('masbate/delete-sale/s1/{id}','MasbateController@delete_sale1');
Route::get('masbate/delete-sale/s2/{id}','MasbateController@delete_sale2');
/* End Delete */

/** csv */
Route::post('masbate/csv-enrollee','MasbateController@csv_enrollee')->middleware('auth');

/* employee */

Route::get('/masbate/add-employee','MasbateController@add_employee')->middleware('auth');
Route::get('/masbate/employee-record','MasbateController@employee_record')->middleware('auth');
Route::post('/masbate/update-employee','MasbateController@update_employee')->middleware('auth');
Route::get('/masbate/delete-employee/{id}','MasbateController@delete_employee')->middleware('auth');
Route::get('/taskHistory','MasbateController@taskHistory')->middleware('auth');

// Member
Route::get('/masbate/view-employee','MasbateController@view_employee')->middleware('auth');

// student id
Route::get('/masbate/student-id','MasbateController@student_id')->middleware('auth');

// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\MasbateController@bulletin');
Route::get('/fetch-branches/{bn}','MasbateController@fetch_branches')->middleware('auth');

// financialreport 
Route::get('masbate/financial-report','MasbateController@financialreport')->middleware('auth');

/** scorecard */
Route::get('masbate/scorecard-season/{season}','MasbateController@scorecard')->middleware('auth');

/* check username and email if exist*/
Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

Route::get('/masbate/bookTransferRecord','MasbateController@bookTransfer_table')->middleware('auth');
Route::get('/masbate/book-transfer','MasbateController@book_transfer')->middleware('auth');


Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


Route::get('/radashboard/viewLecturerReports', 'MasbateController@lecturerView');
Route::get('/radashboard/onlineCompletion', 'MasbateController@onlineCompletion');
Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');


Route::get('/fetch-book-list/{id}','MasbateController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/masbate/delete-expense/{id}','MasbateController@delete_expense')->middleware('auth');


//=============Masbate Enrollment =========

/* Data fetch */
Route::get('/masbate-enrollment/json-student','MasbateEnrollmentController@fetch_student')->middleware('auth');
Route::get('/masbate-enrollment/json-tuition','MasbateEnrollmentController@fetch_tuition')->middleware('auth');
Route::get('/masbate-enrollment/json-discount','MasbateEnrollmentController@fetch_discount')->middleware('auth');
Route::get('/masbate-enrollment/json-balance','MasbateEnrollmentController@fetch_balance')->middleware('auth');
Route::get('/masbate-enrollment/json-id','MasbateEnrollmentController@fetch_id')->middleware('auth');
Route::get('/masbate-enrollment/json-reserved','MasbateEnrollmentController@fetch_reserved')->middleware('auth');

/* Add Data */
Route::post('/masbate-enrollment/drop-student', 'MasbateEnrollmentController_@drop_student')->middleware('auth');
Route::post('/masbate-enrollment/insert-enrollee','MasbateEnrollmentController@insert_enrollee')->middleware('auth');

Route::get('/masbate-enrollment/add-enrollee','MasbateEnrollmentController@add_enrollee')->middleware('auth');
Route::post('/masbate-enrollment/insert-enrollee','MasbateEnrollmentController@insert_enrollee')->middleware('auth');
Route::post('/masbate-enrollment/insert-new-payment','MasbateEnrollmentController@add_new_payment')->middleware('auth');
Route::post('/masbate-enrollment/update-enrollee','MasbateEnrollmentController@update_enrollee')->middleware('auth');
Route::post('enrollment-csv-enrollee','MasbateEnrollmentController@csv_enrollee')->middleware('auth');

Route::get('/masbate-enrollment/new-payment','MasbateEnrollmentController@new_payment')->middleware('auth');
Route::get('/masbate-enrollment/enrolled','MasbateEnrollmentController@enrolled_table')->middleware('auth');
Route::get('/masbate-enrollment/sales-enrollee','MasbateEnrollmentController@sales_enrollee_table')->middleware('auth');
Route::get('/masbate-enrollment/dropped','MasbateEnrollmentController@dropped_table')->middleware('auth');

Route::post('/masbate-enrollment/insert-reservation','MasbateEnrollmentController@insert_reservation')->middleware('auth');
Route::get('/masbate-enrollment/new-reservation','MasbateEnrollmentController@new_reservation')->middleware('auth');
Route::get('/masbate-enrollment/reservation', 'MasbateEnrollmentController@reservation_table')->middleware('auth');


/* Enrollee tables */
Route::get('/masbate-enrollment/let','MasbateEnrollmentController@let_table')->middleware('auth');
Route::get('/masbate-enrollment/nle','MasbateEnrollmentController@nle_table')->middleware('auth');
Route::get('/masbate-enrollment/crim','MasbateEnrollmentController@crim_table')->middleware('auth');
Route::get('/masbate-enrollment/civil','MasbateEnrollmentController@civil_table')->middleware('auth');
Route::get('/masbate-enrollment/psyc','MasbateEnrollmentController@psyc_table')->middleware('auth');
Route::get('/masbate-enrollment/nclex','MasbateEnrollmentController@nclex_table')->middleware('auth');
Route::get('/masbate-enrollment/ielts','MasbateEnrollmentController@ielts_table')->middleware('auth');
Route::get('/masbate-enrollment/social','MasbateEnrollmentController@social_table')->middleware('auth');
Route::get('/masbate-enrollment/agri','MasbateEnrollmentController@agri_table')->middleware('auth');
Route::get('/masbate-enrollment/mid','MasbateEnrollmentController@mid_table')->middleware('auth');
Route::get('/masbate-enrollment/online','MasbateEnrollmentController@online_table')->middleware('auth');

//============Masbate Cashier books=================
Route::get('/masbate-cashier/books','MasbateCashierController@books_table')->middleware('auth');

Route::post('/masbate-cashier/insert-book-payment', 'MasbateCashierController@insert_book_payment')->middleware('auth');
Route::get('/masbate-cashier/book-payment','MasbateCashierController@book_payment')->middleware('auth');

Route::get('/masbate-cashier/json-book','MasbateCashierController@fetch_book')->middleware('auth');
Route::get('/masbate-cashier/json-price','MasbateCashierController@fetch_book_price')->middleware('auth');

//==========================================Masbate====================================//



//==========================================Sorsogon====================================//
/* sorsogon */
Route::get('/sorsogon/dashboard','SorsogonController@index')->middleware('auth');
Route::get('/sorsogon/add-enrollee','SorsogonController@add_enrollee')->middleware('auth');
Route::get('/sorsogon/new-payment','SorsogonController@new_payment')->middleware('auth');
Route::get('/sorsogon/new-reservation','SorsogonController@new_reservation')->middleware('auth');
Route::get('/sorsogon/add-expense','SorsogonController@add_expense')->middleware('auth');
Route::get('/sorsogon/add-budget','SorsogonController@add_budget')->middleware('auth');
Route::get('/sorsogon/book-payment','SorsogonController@book_payment')->middleware('auth');
Route::get('/sorsogon/new-remit', 'SorsogonController@new_remit')->middleware('auth');



/* Data fetch */
Route::get('/sorsogon/json-student','SorsogonController@fetch_student')->middleware('auth');
Route::get('/sorsogon/json-tuition','SorsogonController@fetch_tuition')->middleware('auth');
Route::get('/sorsogon/json-discount','SorsogonController@fetch_discount')->middleware('auth');
Route::get('/sorsogon/json-balance','SorsogonController@fetch_balance')->middleware('auth');
Route::get('/sorsogon/json-expense','SorsogonController@fetch_expense')->middleware('auth');
Route::get('/sorsogon/json-book','SorsogonController@fetch_book')->middleware('auth');
Route::get('/sorsogon/json-price','SorsogonController@fetch_book_price')->middleware('auth');
Route::get('/sorsogon/json-reserved','SorsogonController@fetch_reserved')->middleware('auth');
Route::get('/sorsogon/json-id','SorsogonController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/sorsogon/insert-enrollee','SorsogonController@insert_enrollee')->middleware('auth');
Route::post('/sorsogon/insert-new-payment','SorsogonController@add_new_payment')->middleware('auth');
Route::post('/sorsogon/insert-new-expense','SorsogonController@add_new_expense')->middleware('auth');
Route::post('/sorsogon/insert-new-budget','SorsogonController@insert_new_budget')->middleware('auth');
Route::post('/sorsogon/insert-book-payment', 'SorsogonController@insert_book_payment')->middleware('auth');
Route::post('/sorsogon/insert-book-transfer', 'SorsogonController@insert_book_transfer')->middleware('auth');
Route::post('/sorsogon/drop-student', 'SorsogonController_@drop_student')->middleware('auth');
Route::post('/sorsogon/insert-remit', 'SorsogonController@insert_remit')->middleware('auth');
Route::post('/sorsogon/insert-reservation','SorsogonController@insert_reservation')->middleware('auth');
Route::post('/sorsogon/insert-employee','SorsogonController@insert_employee')->middleware('auth');


/* Update Data */
Route::post('/sorsogon/update-enrollee','SorsogonController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/sorsogon/delete-enrollee','SorsogonController@delete_enrollee')->middleware('auth');
/* Tables */

	/* Enrollee tables */
Route::get('/sorsogon/let','SorsogonController@let_table')->middleware('auth');
Route::get('/sorsogon/nle','SorsogonController@nle_table')->middleware('auth');
Route::get('/sorsogon/crim','SorsogonController@crim_table')->middleware('auth');
Route::get('/sorsogon/civil','SorsogonController@civil_table')->middleware('auth');
Route::get('/sorsogon/psyc','SorsogonController@psyc_table')->middleware('auth');
Route::get('/sorsogon/nclex','SorsogonController@nclex_table')->middleware('auth');
Route::get('/sorsogon/ielts','SorsogonController@ielts_table')->middleware('auth');
Route::get('/sorsogon/social','SorsogonController@social_table')->middleware('auth');
Route::get('/sorsogon/agri','SorsogonController@agri_table')->middleware('auth');
Route::get('/sorsogon/mid','SorsogonController@mid_table')->middleware('auth');
Route::get('/sorsogon/online','SorsogonController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/sorsogon/tuition','SorsogonController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/sorsogon/scholar','SorsogonController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/sorsogon/enrolled','SorsogonController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/sorsogon/dropped','SorsogonController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/sorsogon/sales-enrollee','SorsogonController@sales_enrollee_table')->middleware('auth');
Route::get('/sorsogon/sales-program','SorsogonController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/sorsogon/receivable-enrollee','SorsogonController@receivable_enrollee_table')->middleware('auth');
Route::get('/sorsogon/receivable-program','SorsogonController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/sorsogon/expense','SorsogonController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/sorsogon/books','SorsogonController@books_table')->middleware('auth');
Route::get('/sorsogon/remit', 'SorsogonController@remit')->middleware('auth');
Route::get('/sorsogon/reservation', 'SorsogonController@reservation_table')->middleware('auth');

Route::post('sorsogon/clear-enrollee','SorsogonController@clear_enrollee')->middleware('auth');
Route::post('sorsogon/clear-sale-season1','SorsogonController@clear_sale_season1')->middleware('auth');
Route::post('sorsogon/clear-sale-season2','SorsogonController@clear_sale_season2')->middleware('auth');
Route::post('sorsogon/clear-receivable','SorsogonController@clear_receivable')->middleware('auth');
Route::post('sorsogon/clear-expense','SorsogonController@clear_expense')->middleware('auth');
Route::post('sorsogon/clear-book','SorsogonController@clear_book')->middleware('auth');
Route::post('sorsogon/clear-reservation','SorsogonController@clear_reservation')->middleware('auth');

/* Daet Lecturer */
Route::get('sorsogon/lecturer-evaluation','SorsogonController@lec_eval')->middleware('auth');
Route::get('sorsogon/add-lecturer','SorsogonController@add_lec')->middleware('auth');
Route::get('sorsogon/evaluate-lecturer','SorsogonController@eval_lec')->middleware('auth');
Route::get('sorsogon/json-class','SorsogonController@fetch_class')->middleware('auth');
Route::get('sorsogon/json-subject','SorsogonController@fetch_subject')->middleware('auth');
Route::get('sorsogon/json-section','SorsogonController@fetch_section')->middleware('auth');
Route::get('sorsogon/json-lecturer','SorsogonController@fetch_lecturer')->middleware('auth');
Route::get('sorsogon/json-lecturerb','SorsogonController@fetch_lecturerb')->middleware('auth');

Route::post('sorsogon/insert-lecturer','SorsogonController@insert_lecturer')->middleware('auth');
Route::post('sorsogon/insert-evaluation','SorsogonController@insert_eval')->middleware('auth');

Route::post('sorsogon/clear-lecturers','SorsogonController@clear_lecturers')->middleware('auth');


/* Reports Sorsogon */

Route::get('sorsogon/today','SorsogonController@today')->middleware('auth');
Route::get('sorsogon/yesterday','SorsogonController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Sorsogon */

Route::get('sorsogon/budget','SorsogonController@budget_record')->middleware('auth');
Route::post('sorsogon/clear-budget','SorsogonController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('sorsogon/delete-sale/s1/{id}','SorsogonController@delete_sale1');
Route::get('sorsogon/delete-sale/s2/{id}','SorsogonController@delete_sale2');
/* End Delete */

/** csv */
Route::post('sorsogon/csv-enrollee','SorsogonController@csv_enrollee')->middleware('auth');

/* employee */

Route::get('/sorsogon/add-employee','SorsogonController@add_employee')->middleware('auth');
Route::get('/sorsogon/employee-record','SorsogonController@employee_record')->middleware('auth');
Route::post('/sorsogon/update-employee','SorsogonController@update_employee')->middleware('auth');
Route::get('/sorsogon/delete-employee/{id}','SorsogonController@delete_employee')->middleware('auth');
Route::get('/taskHistory','SorsogonController@taskHistory')->middleware('auth');

// Member
Route::get('/sorsogon/view-employee','SorsogonController@view_employee')->middleware('auth');

// student id
Route::get('/sorsogon/student-id','SorsogonController@student_id')->middleware('auth');

// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\SorsogonController@bulletin');
Route::get('/fetch-branches/{bn}','SorsogonController@fetch_branches')->middleware('auth');

// financialreport 
Route::get('sorsogon/financial-report','SorsogonController@financialreport')->middleware('auth');

/** scorecard */
Route::get('sorsogon/scorecard-season/{season}','SorsogonController@scorecard')->middleware('auth');

/* check username and email if exist*/
Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

Route::get('/sorsogon/bookTransferRecord','SorsogonController@bookTransfer_table')->middleware('auth');
Route::get('/sorsogon/book-transfer','SorsogonController@book_transfer')->middleware('auth');


Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


Route::get('/radashboard/viewLecturerReports', 'SorsogonController@lecturerView');
Route::get('/radashboard/onlineCompletion', 'SorsogonController@onlineCompletion');
Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');


Route::get('/fetch-book-list/{id}','SorsogonController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/sorsogon/delete-expense/{id}','SorsogonController@delete_expense')->middleware('auth');


//=============Sorsogon Enrollment =========

/* Data fetch */
Route::get('/sorsogon-enrollment/json-student','SorsogonEnrollmentController@fetch_student')->middleware('auth');
Route::get('/sorsogon-enrollment/json-tuition','SorsogonEnrollmentController@fetch_tuition')->middleware('auth');
Route::get('/sorsogon-enrollment/json-discount','SorsogonEnrollmentController@fetch_discount')->middleware('auth');
Route::get('/sorsogon-enrollment/json-balance','SorsogonEnrollmentController@fetch_balance')->middleware('auth');
Route::get('/sorsogon-enrollment/json-id','SorsogonEnrollmentController@fetch_id')->middleware('auth');
Route::get('/sorsogon-enrollment/json-reserved','SorsogonEnrollmentController@fetch_reserved')->middleware('auth');

/* Add Data */
Route::post('/sorsogon-enrollment/drop-student', 'SorsogonEnrollmentController_@drop_student')->middleware('auth');
Route::post('/sorsogon-enrollment/insert-enrollee','SorsogonEnrollmentController@insert_enrollee')->middleware('auth');

Route::get('/sorsogon-enrollment/add-enrollee','SorsogonEnrollmentController@add_enrollee')->middleware('auth');
Route::post('/sorsogon-enrollment/insert-enrollee','SorsogonEnrollmentController@insert_enrollee')->middleware('auth');
Route::post('/sorsogon-enrollment/insert-new-payment','SorsogonEnrollmentController@add_new_payment')->middleware('auth');
Route::post('/sorsogon-enrollment/update-enrollee','SorsogonEnrollmentController@update_enrollee')->middleware('auth');
Route::post('enrollment-csv-enrollee','SorsogonEnrollmentController@csv_enrollee')->middleware('auth');

Route::get('/sorsogon-enrollment/new-payment','SorsogonEnrollmentController@new_payment')->middleware('auth');
Route::get('/sorsogon-enrollment/enrolled','SorsogonEnrollmentController@enrolled_table')->middleware('auth');
Route::get('/sorsogon-enrollment/sales-enrollee','SorsogonEnrollmentController@sales_enrollee_table')->middleware('auth');
Route::get('/sorsogon-enrollment/dropped','SorsogonEnrollmentController@dropped_table')->middleware('auth');

Route::post('/sorsogon-enrollment/insert-reservation','SorsogonEnrollmentController@insert_reservation')->middleware('auth');
Route::get('/sorsogon-enrollment/new-reservation','SorsogonEnrollmentController@new_reservation')->middleware('auth');
Route::get('/sorsogon-enrollment/reservation', 'SorsogonEnrollmentController@reservation_table')->middleware('auth');


/* Enrollee tables */
Route::get('/sorsogon-enrollment/let','SorsogonEnrollmentController@let_table')->middleware('auth');
Route::get('/sorsogon-enrollment/nle','SorsogonEnrollmentController@nle_table')->middleware('auth');
Route::get('/sorsogon-enrollment/crim','SorsogonEnrollmentController@crim_table')->middleware('auth');
Route::get('/sorsogon-enrollment/civil','SorsogonEnrollmentController@civil_table')->middleware('auth');
Route::get('/sorsogon-enrollment/psyc','SorsogonEnrollmentController@psyc_table')->middleware('auth');
Route::get('/sorsogon-enrollment/nclex','SorsogonEnrollmentController@nclex_table')->middleware('auth');
Route::get('/sorsogon-enrollment/ielts','SorsogonEnrollmentController@ielts_table')->middleware('auth');
Route::get('/sorsogon-enrollment/social','SorsogonEnrollmentController@social_table')->middleware('auth');
Route::get('/sorsogon-enrollment/agri','SorsogonEnrollmentController@agri_table')->middleware('auth');
Route::get('/sorsogon-enrollment/mid','SorsogonEnrollmentController@mid_table')->middleware('auth');
Route::get('/sorsogon-enrollment/online','SorsogonEnrollmentController@online_table')->middleware('auth');

//============Sorsogon Cashier books=================
Route::get('/sorsogon-cashier/books','SorsogonCashierController@books_table')->middleware('auth');

Route::post('/sorsogon-cashier/insert-book-payment', 'SorsogonCashierController@insert_book_payment')->middleware('auth');
Route::get('/sorsogon-cashier/book-payment','SorsogonCashierController@book_payment')->middleware('auth');

Route::get('/sorsogon-cashier/json-book','SorsogonCashierController@fetch_book')->middleware('auth');
Route::get('/sorsogon-cashier/json-price','SorsogonCashierController@fetch_book_price')->middleware('auth');

//==========================================Sorsogon====================================//



//==========================================Legaspi====================================//
/* legaspi */
Route::get('/legaspi/dashboard','LegaspiController@index')->middleware('auth');
Route::get('/legaspi/add-enrollee','LegaspiController@add_enrollee')->middleware('auth');
Route::get('/legaspi/new-payment','LegaspiController@new_payment')->middleware('auth');
Route::get('/legaspi/new-reservation','LegaspiController@new_reservation')->middleware('auth');
Route::get('/legaspi/add-expense','LegaspiController@add_expense')->middleware('auth');
Route::get('/legaspi/add-budget','LegaspiController@add_budget')->middleware('auth');
Route::get('/legaspi/book-payment','LegaspiController@book_payment')->middleware('auth');
Route::get('/legaspi/new-remit', 'LegaspiController@new_remit')->middleware('auth');



/* Data fetch */
Route::get('/legaspi/json-student','LegaspiController@fetch_student')->middleware('auth');
Route::get('/legaspi/json-tuition','LegaspiController@fetch_tuition')->middleware('auth');
Route::get('/legaspi/json-discount','LegaspiController@fetch_discount')->middleware('auth');
Route::get('/legaspi/json-balance','LegaspiController@fetch_balance')->middleware('auth');
Route::get('/legaspi/json-expense','LegaspiController@fetch_expense')->middleware('auth');
Route::get('/legaspi/json-book','LegaspiController@fetch_book')->middleware('auth');
Route::get('/legaspi/json-price','LegaspiController@fetch_book_price')->middleware('auth');
Route::get('/legaspi/json-reserved','LegaspiController@fetch_reserved')->middleware('auth');
Route::get('/legaspi/json-id','LegaspiController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/legaspi/insert-enrollee','LegaspiController@insert_enrollee')->middleware('auth');
Route::post('/legaspi/insert-new-payment','LegaspiController@add_new_payment')->middleware('auth');
Route::post('/legaspi/insert-new-expense','LegaspiController@add_new_expense')->middleware('auth');
Route::post('/legaspi/insert-new-budget','LegaspiController@insert_new_budget')->middleware('auth');
Route::post('/legaspi/insert-book-payment', 'LegaspiController@insert_book_payment')->middleware('auth');
Route::post('/legaspi/insert-book-transfer', 'LegaspiController@insert_book_transfer')->middleware('auth');
Route::post('/legaspi/drop-student', 'LegaspiController_@drop_student')->middleware('auth');
Route::post('/legaspi/insert-remit', 'LegaspiController@insert_remit')->middleware('auth');
Route::post('/legaspi/insert-reservation','LegaspiController@insert_reservation')->middleware('auth');
Route::post('/legaspi/insert-employee','LegaspiController@insert_employee')->middleware('auth');


/* Update Data */
Route::post('/legaspi/update-enrollee','LegaspiController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/legaspi/delete-enrollee','LegaspiController@delete_enrollee')->middleware('auth');
/* Tables */

	/* Enrollee tables */
Route::get('/legaspi/let','LegaspiController@let_table')->middleware('auth');
Route::get('/legaspi/nle','LegaspiController@nle_table')->middleware('auth');
Route::get('/legaspi/crim','LegaspiController@crim_table')->middleware('auth');
Route::get('/legaspi/civil','LegaspiController@civil_table')->middleware('auth');
Route::get('/legaspi/psyc','LegaspiController@psyc_table')->middleware('auth');
Route::get('/legaspi/nclex','LegaspiController@nclex_table')->middleware('auth');
Route::get('/legaspi/ielts','LegaspiController@ielts_table')->middleware('auth');
Route::get('/legaspi/social','LegaspiController@social_table')->middleware('auth');
Route::get('/legaspi/agri','LegaspiController@agri_table')->middleware('auth');
Route::get('/legaspi/mid','LegaspiController@mid_table')->middleware('auth');
Route::get('/legaspi/online','LegaspiController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/legaspi/tuition','LegaspiController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/legaspi/scholar','LegaspiController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/legaspi/enrolled','LegaspiController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/legaspi/dropped','LegaspiController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/legaspi/sales-enrollee','LegaspiController@sales_enrollee_table')->middleware('auth');
Route::get('/legaspi/sales-program','LegaspiController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/legaspi/receivable-enrollee','LegaspiController@receivable_enrollee_table')->middleware('auth');
Route::get('/legaspi/receivable-program','LegaspiController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/legaspi/expense','LegaspiController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/legaspi/books','LegaspiController@books_table')->middleware('auth');
Route::get('/legaspi/remit', 'LegaspiController@remit')->middleware('auth');
Route::get('/legaspi/reservation', 'LegaspiController@reservation_table')->middleware('auth');

Route::post('legaspi/clear-enrollee','LegaspiController@clear_enrollee')->middleware('auth');
Route::post('legaspi/clear-sale-season1','LegaspiController@clear_sale_season1')->middleware('auth');
Route::post('legaspi/clear-sale-season2','LegaspiController@clear_sale_season2')->middleware('auth');
Route::post('legaspi/clear-receivable','LegaspiController@clear_receivable')->middleware('auth');
Route::post('legaspi/clear-expense','LegaspiController@clear_expense')->middleware('auth');
Route::post('legaspi/clear-book','LegaspiController@clear_book')->middleware('auth');
Route::post('legaspi/clear-reservation','LegaspiController@clear_reservation')->middleware('auth');

/* Daet Lecturer */
Route::get('legaspi/lecturer-evaluation','LegaspiController@lec_eval')->middleware('auth');
Route::get('legaspi/add-lecturer','LegaspiController@add_lec')->middleware('auth');
Route::get('legaspi/evaluate-lecturer','LegaspiController@eval_lec')->middleware('auth');
Route::get('legaspi/json-class','LegaspiController@fetch_class')->middleware('auth');
Route::get('legaspi/json-subject','LegaspiController@fetch_subject')->middleware('auth');
Route::get('legaspi/json-section','LegaspiController@fetch_section')->middleware('auth');
Route::get('legaspi/json-lecturer','LegaspiController@fetch_lecturer')->middleware('auth');
Route::get('legaspi/json-lecturerb','LegaspiController@fetch_lecturerb')->middleware('auth');

Route::post('legaspi/insert-lecturer','LegaspiController@insert_lecturer')->middleware('auth');
Route::post('legaspi/insert-evaluation','LegaspiController@insert_eval')->middleware('auth');

Route::post('legaspi/clear-lecturers','LegaspiController@clear_lecturers')->middleware('auth');


/* Reports Legaspi */

Route::get('legaspi/today','LegaspiController@today')->middleware('auth');
Route::get('legaspi/yesterday','LegaspiController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Legaspi */

Route::get('legaspi/budget','LegaspiController@budget_record')->middleware('auth');
Route::post('legaspi/clear-budget','LegaspiController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('legaspi/delete-sale/s1/{id}','LegaspiController@delete_sale1');
Route::get('legaspi/delete-sale/s2/{id}','LegaspiController@delete_sale2');
/* End Delete */

/** csv */
Route::post('legaspi/csv-enrollee','LegaspiController@csv_enrollee')->middleware('auth');

/* employee */

Route::get('/legaspi/add-employee','LegaspiController@add_employee')->middleware('auth');
Route::get('/legaspi/employee-record','LegaspiController@employee_record')->middleware('auth');
Route::post('/legaspi/update-employee','LegaspiController@update_employee')->middleware('auth');
Route::get('/legaspi/delete-employee/{id}','LegaspiController@delete_employee')->middleware('auth');
Route::get('/taskHistory','LegaspiController@taskHistory')->middleware('auth');

// Member
Route::get('/legaspi/view-employee','LegaspiController@view_employee')->middleware('auth');

// student id
Route::get('/legaspi/student-id','LegaspiController@student_id')->middleware('auth');

// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\LegaspiController@bulletin');
Route::get('/fetch-branches/{bn}','LegaspiController@fetch_branches')->middleware('auth');

// financialreport 
Route::get('legaspi/financial-report','LegaspiController@financialreport')->middleware('auth');

/** scorecard */
Route::get('legaspi/scorecard-season/{season}','LegaspiController@scorecard')->middleware('auth');

/* check username and email if exist*/
Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

Route::get('/legaspi/bookTransferRecord','LegaspiController@bookTransfer_table')->middleware('auth');
Route::get('/legaspi/book-transfer','LegaspiController@book_transfer')->middleware('auth');


Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


Route::get('/radashboard/viewLecturerReports', 'LegaspiController@lecturerView');
Route::get('/radashboard/onlineCompletion', 'LegaspiController@onlineCompletion');
Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');


Route::get('/fetch-book-list/{id}','LegaspiController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/legaspi/delete-expense/{id}','LegaspiController@delete_expense')->middleware('auth');


//=============Legaspi Enrollment =========

/* Data fetch */
Route::get('/legaspi-enrollment/json-student','LegaspiEnrollmentController@fetch_student')->middleware('auth');
Route::get('/legaspi-enrollment/json-tuition','LegaspiEnrollmentController@fetch_tuition')->middleware('auth');
Route::get('/legaspi-enrollment/json-discount','LegaspiEnrollmentController@fetch_discount')->middleware('auth');
Route::get('/legaspi-enrollment/json-balance','LegaspiEnrollmentController@fetch_balance')->middleware('auth');
Route::get('/legaspi-enrollment/json-id','LegaspiEnrollmentController@fetch_id')->middleware('auth');
Route::get('/legaspi-enrollment/json-reserved','LegaspiEnrollmentController@fetch_reserved')->middleware('auth');

/* Add Data */
Route::post('/legaspi-enrollment/drop-student', 'LegaspiEnrollmentController_@drop_student')->middleware('auth');
Route::post('/legaspi-enrollment/insert-enrollee','LegaspiEnrollmentController@insert_enrollee')->middleware('auth');

Route::get('/legaspi-enrollment/add-enrollee','LegaspiEnrollmentController@add_enrollee')->middleware('auth');
Route::post('/legaspi-enrollment/insert-enrollee','LegaspiEnrollmentController@insert_enrollee')->middleware('auth');
Route::post('/legaspi-enrollment/insert-new-payment','LegaspiEnrollmentController@add_new_payment')->middleware('auth');
Route::post('/legaspi-enrollment/update-enrollee','LegaspiEnrollmentController@update_enrollee')->middleware('auth');
Route::post('enrollment-csv-enrollee','LegaspiEnrollmentController@csv_enrollee')->middleware('auth');

Route::get('/legaspi-enrollment/new-payment','LegaspiEnrollmentController@new_payment')->middleware('auth');
Route::get('/legaspi-enrollment/enrolled','LegaspiEnrollmentController@enrolled_table')->middleware('auth');
Route::get('/legaspi-enrollment/sales-enrollee','LegaspiEnrollmentController@sales_enrollee_table')->middleware('auth');
Route::get('/legaspi-enrollment/dropped','LegaspiEnrollmentController@dropped_table')->middleware('auth');

Route::post('/legaspi-enrollment/insert-reservation','LegaspiEnrollmentController@insert_reservation')->middleware('auth');
Route::get('/legaspi-enrollment/new-reservation','LegaspiEnrollmentController@new_reservation')->middleware('auth');
Route::get('/legaspi-enrollment/reservation', 'LegaspiEnrollmentController@reservation_table')->middleware('auth');


/* Enrollee tables */
Route::get('/legaspi-enrollment/let','LegaspiEnrollmentController@let_table')->middleware('auth');
Route::get('/legaspi-enrollment/nle','LegaspiEnrollmentController@nle_table')->middleware('auth');
Route::get('/legaspi-enrollment/crim','LegaspiEnrollmentController@crim_table')->middleware('auth');
Route::get('/legaspi-enrollment/civil','LegaspiEnrollmentController@civil_table')->middleware('auth');
Route::get('/legaspi-enrollment/psyc','LegaspiEnrollmentController@psyc_table')->middleware('auth');
Route::get('/legaspi-enrollment/nclex','LegaspiEnrollmentController@nclex_table')->middleware('auth');
Route::get('/legaspi-enrollment/ielts','LegaspiEnrollmentController@ielts_table')->middleware('auth');
Route::get('/legaspi-enrollment/social','LegaspiEnrollmentController@social_table')->middleware('auth');
Route::get('/legaspi-enrollment/agri','LegaspiEnrollmentController@agri_table')->middleware('auth');
Route::get('/legaspi-enrollment/mid','LegaspiEnrollmentController@mid_table')->middleware('auth');
Route::get('/legaspi-enrollment/online','LegaspiEnrollmentController@online_table')->middleware('auth');

//============Legaspi Cashier books=================
Route::get('/legaspi-cashier/books','LegaspiCashierController@books_table')->middleware('auth');

Route::post('/legaspi-cashier/insert-book-payment', 'LegaspiCashierController@insert_book_payment')->middleware('auth');
Route::get('/legaspi-cashier/book-payment','LegaspiCashierController@book_payment')->middleware('auth');

Route::get('/legaspi-cashier/json-book','LegaspiCashierController@fetch_book')->middleware('auth');
Route::get('/legaspi-cashier/json-price','LegaspiCashierController@fetch_book_price')->middleware('auth');

//==========================================Legaspi====================================//






Route::get('500', function()
{
	abort(500);
});

