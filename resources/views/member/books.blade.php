@extends('main')
@section('title')
 Books Invetories and Sales
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Books Invetories and Sales Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Books Inventories</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                <div id="book-table">
                                    <table id="book" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                                        
                     <thead>
                       
                        <tr>
                            <th>Branch</th>
                            <th>Program</th>
                            <th>Title</th>
                            <th>Price</th>
                            <th>Stock</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($book))
                            @foreach($book as $books)
                        <tr id="row_{{$books->id}}">
                            <td id="branch_{{$books->id}}">{{$books->branch}}</td>
                            <td id="program_{{$books->id}}">{{$books->program}}</td>
                            <td id="book_title_{{$books->id}}">{{$books->book_title}}</td>
                            <td id="price_{{$books->id}}">{{$books->price}}</td>
                            <td id="available_{{$books->id}}">{{$books->available}}</td>
                        </tr>
                       @endforeach
                            @endif


                    </tbody>           
                    
                </table>
            </div>


                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->

            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <br/>
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Books Sales Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                <div id="book-table">
                                    <table id="sale" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                     <thead>
                       
                        <tr>
                            <th>Trans. #</th>
                            <th>Date</th>
                            <th>Branch</th>
                            <th>Program</th>
                            <th>Name</th>
                            <th>Title</th>
                            <th>Amount</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($sale))
                            @foreach($sale as $sales)
                        <tr id="row_{{$books->id}}">
                            <td>{{$sales->id}}</td>
                            <td>{{$sales->date}}</td>
                            <td>{{$sales->branch}}</td>
                            <td>{{$sales->program}}</td>
                            <td>{{$sales->name}}</td>
                            <td>{{$sales->book_title}}</td>
                            <td>{{$sales->amount}}</td>
                            
                        </tr>
                       @endforeach
                            @endif

                    </tbody>           
                    
                </table>
                @role('admin')
                <br/>
                <a class="btn btn-danger clear" id="clear">Clear table</a>
                @endrole
            </div>

             </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
        </main>
        

@stop
@section('js')

<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/book.js') }}"></script>

<script type="text/javascript">
    $('#clear').on('click',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                
               $.ajax({
               type: 'POST',
               url: "{{Auth::user()->position.'clear-book'}}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: formData,
               processData:false,
                contentType: false,
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
          });
</script>

@stop