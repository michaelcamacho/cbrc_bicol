@extends('main')
@section('title')
Employee Records
@stop
@section('css')
<style>
.DTFC_LeftBodyLiner ,.DTFC_RightBodyLiner  {
  overflow: hidden;
  border:none;
}

.modalemployee{
    padding: 2.5em;
    padding-bottom: 0px;

}
.modalupdate{
    padding: 2.5em;
}

#v_lname{
  font-size: 28px;
  font-weight: bold;
}
#v_fname{
  margin-left:5px;
  font-size: 28px;
  font-weight: bold;
}
#v_mname{
  font-size: 28px;
  font-weight: bold;
}
#pname, #contact, #v_address ,#gender, #status, #position,#sss,#pagibig, #philhealth, #Salary, #footer, #hris{
  margin-bottom: 0px;
  font-size: 15px;

}
#titleinfo{
  font-weight: bold;
  font-size: 18px;
  margin-bottom: 5px;
}
.custom-file-upload input[type="file"] {
    display: none;
}
.custom-file-upload .custom-file-upload1 {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
input[type="file"]{
  display: none;
}

#btnupdate{
  cursor: pointer;
  background-color:#f57327;
  color: #fff;
  padding: 10px 20px; 
  margin-left: 40px;
}
</style>

<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Employee Records</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">HR</li>
                            <li class="breadcrumb-item active">Employee Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
<div class="container-fluid">
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h3 class="box-title mr-b-0">Employee Records</h3>
                      </div>
              <div class="widget-body clearfix">
                <table id="test" class="table table-bordered display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                          <tr>
                            <th>Name</th>
                            <th>Employee no.</th>
                            <th>Branch Name</th>
                            <th>Position</th>
                            <th>Employee Status</th>
                            <th>Rate</th>
                            <th>Date Hired</th>
                            <th>BirthDate</th>
                            <th>Gender</th>
                            <th>Status</th>
                            <th>Address</th>
                            <th>Email</th>
                            <th>Contact no.</th>
                            <th>Contact Person</th>
                            <th>Contact Details</th>
                            <th>SSS</th>
                            <th>Philhealth</th>
                            <th>Pag ibig</th>
                            <th>Tin</th>
                            <th> </th>
                            <th>Actions</th>
                        </tr>                  
                    </thead>
                    <tbody>
                        @if(isset($employee))
                            @foreach($employee as $employees)
                        <tr id="row_{{$employees->id}}">
                            <td width="250%" id="a_fullname_{{$employees->id}}" data-lname="{{$employees->last_name}}" data-fname="{{$employees->first_name}}" data-mname="{{$employees->middle_name}}">{{$employees->last_name}} {{$employees->first_name}} {{$employees->middle_name}} </td>
                            <td id="a_employee_no_{{$employees->id}}">{{$employees->employee_no}}</td>
                            <td id="a_branch_{{$employees->id}}">{{$employees->branch_name}}</td>
                            <td id="a_position{{$employees->id}}">{{$employees->position}}</td>
                            <td id="a_employment_status_{{$employees->id}}">{{$employees->employment_status}}</td>
                            <td id="a_rate_{{$employees->id}}">{{$employees->rate}}</td>
                            <td id="a_date_hired_{{$employees->id}}">{{$employees->date_hired}}</td>
                            <td id="a_bithdate_{{$employees->id}}">{{$employees->birthdate}}</td>
                            <td id="a_gender_{{$employees->id}}">{{$employees->gender}}</td>
                            <td id="a_status_{{$employees->id}}">{{$employees->status}}</td> 
                            <td id="a_address_{{$employees->id}}">{{$employees->address}}</td>
                            <td id="a_email_{{$employees->id}}">{{$employees->email}}</td>
                            <td id="a_contact_no_{{$employees->id}}">{{$employees->contact_no}}</td>
                            <td id="a_contact_person_{{$employees->id}}">{{$employees->contact_person}}</td>
                            <td id="a_contact_details_{{$employees->id}}">{{$employees->contact_details}}</td>
                            <td id="a_sss_{{$employees->id}}">{{$employees->sss}}</td>
                            <td id="a_phil_health_{{$employees->id}}">{{$employees->phil_health}}</td>
                            <td id="a_pag_ibig_{{$employees->id}}">{{$employees->pag_ibig}}</td>
                            <td id="a_tin_{{$employees->id}}">{{$employees->tin}}</td>
                            <td id="a_img_{{$employees->id}}" image-path="{{URL::to('/')}}/cover_images/{{$employees->cover_image}}">{{$employees->cover_image}}</td>   
                            <td><a href="#" class="btn btn-primary a_view" data-toggle="modal" data-target="#add-tuition" id="a_view_{{$employees->id}}">View</a> 
                            <a href="#" class="btn btn-success a_update" data-toggle ="modal" data-target = "#updatesModal" id="a_update_{{$employees->id}}">Update</a>
                           <!--  <a href = "/novaliches/delete-employee/{{$employees->id}}" class="btn btn-danger delete" user-id="{{$employees->id}}">Delete</a> -->
                            </td>
                        </tr>
                        @endforeach
                    @endif
                  </tbody>                    
                </table>
               </div>              
              </div>
              <!-- /.widget-bg -->
              </div>
              <!-- /.widget-holder -->
          </div>
          <!-- /.row -->
      </div>
      <!-- /.widget-list -->
  </div>
  <!-- /.container-fluid -->
</main>

<!--================= = View Profile Information =======================-->
@if(isset($employee))
<div class="modal fade" id="add-tuition" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modalemployee">
               <a href="#" class="close" data-dismiss="modal" aria-hidden="true">×</a>
              <div class="modal-body">
                <form>
                @csrf  
                    <div class="container">
                      <div class="row">
                        <div class="col-md-8" style="margin-top: 50px;">
                            <p id="pname"><span id="v_lname"></span><span>,</span><span id="v_fname"></span>
                              <span id="v_mname"></span></p>
                              <p id="v_address"></p>
                              <p id="contact">Contact # : <span id="v_contact_no"></span></p>
                              <p>Email : <span id="v_email"></span></p>
                        </div>
                        <div class="col-md-4">
                          @if(!empty($employee))
                            <img style="max-width: 100%; text-align: center;height: 180px;" src="" id="v_img" class="img-responsive rounded-circle">
                          @endif
                        </div>
                      </div>
                    </div> <!-- end of container of personal information -->
                    <hr>
                    <div class="container">
                        <div class="row">
                          <div class="col-md-6">
                            <p id="titleinfo">PERSONAL INFORMATION</p>
                            <p id = "position"> Employee Number : <span id="v_employee_no"></span></p>
                            <p id = "gender">Gender : <span id="v_gender"></span></p>
                            <p id="status">Status : <span id="v_status"></span></p>
                            <p>Birthdate : <span id="v_birthdate"></span></p>                   
                          </div>
                          <div class="col-md-6">
                            <p id="titleinfo">EMPLOYMENT INFORMATION</p>
                            <p id = "position">Position : <span id="v_position"></span></p>
                            <p id="status">Employment Status : <span id="v_employment_status"></span></p>
                            <p id="Salary">Salary Rate : <span id="v_rate"></span></p>
                            <p>Date Hired : <span id="v_date_hired"></span></p>                   
                          </div>
                        </div>
                    </div> <!-- end of container -->
                    <div class="container">
                        <div class="row">
                          <div class="col-md-6">
                            <p id="titleinfo">GOVERNMENT ID NUMBER</p>
                            <p id = "sss">SSS : <span id="v_sss"></span></p>
                            <p id="philhealth">PhilHealth : <span id="v_phil_health"></span></p>
                            <p id = "pagibig">Pagibig : <span id="v_pag_ibig"></span></p>
                            <p id = "tin">TIN : <span id="v_tin"></span></p>                   
                          </div>
                          <div class="col-md-6">
                            <p id="titleinfo">IN CASE OF EMERGENCY</p>
                            <p id = "sss">Contact Person : <span id="v_contact_person"></span></p>
                            <p id="philhealth">Contact # : <span id="v_contact_details"></span></p>
                          </div>
                        </div>
                    </div>
                    <div class="container">
                      <div class="row">
                        <div class="col-md-12 text-center">
                            <p id="footer">Carl Balita Review center</p>
                            <p id="hris">Human Resources Information System</p>
                        </div>
                      </div>
                    </div>
              </form>
              </div>
            </div>
          </div>
        </div> <!-- end of modal view Page -->
@endif

<!-- =========================== Modal Update ========================================== -->
<div class="modal fade" id="updatesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modalemployee">
               <a href="#" class="close" data-dismiss="modal" aria-hidden="true">×</a>
              <div class="modal-body">
               <form  id="update_form"  enctype="multipart/form-data" action="/novaliches/update-employee" method="post">
                @csrf     
                
                 <input type="hidden" name="update_id" id="update_id">
                 <input type="hidden" name="is_upload" id="is_upload" value="0">
                <div class="container">
                      <div class="row">
                        <div class="col-md-8">
                          <div class="row">
                            <div class="col-md-12">
                              <p style="font-size: 20px; font-weight: bold;">PERSONAL INFORMATION</p>
                            </div>
                            <div class="col-md-4">
                            <div class="form-group">
                            <input form="update_form" class="form-control" name="last_name" id="up_lname" type="text">
                            <label>Last Name</label>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <div class="form-group">
                            <input form="update_form" class="form-control" name="first_name" id="up_fname" type="text">
                            <label>First Name</label>
                        </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                            <input form="update_form" class="form-control" name="middle_name" id="up_mname" type="text">
                            <label>Middle Name</label>
                        </div>
                        </div>
                        <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" form="update_form" name="address" type="text" id="up_address">
                            <label>Address</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input form="update_form" class="form-control" name="contact_no" type="text" id="up_contact_no">
                          <label>Contact Number</label>
                        </div>
                    </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <input form="update_form" class="form-control" name="email" type="text" id="up_email">
                            <label>Email Address</label>
                        </div>
                    </div>

                     <div class="col-md-4">
                        <div class="form-group">
                            <select class="form-control" name="branch_name" id="branch_name">
                                <option value="">----</option>
                                @if(isset($branch_name))
                                  @foreach($branch_name as $branches)
                                    <option value="{{$branches->branch_name}}">{{$branches->branch_name}}</option>
                                  @endforeach
                                @endif
                            </select>
                            <label>Branch</label>
                        </div>
                    </div> 
                  </div>
                </div>
                   <input type="hidden" name="update_id" class="form-control" id="update_img">
                   <div class="col-md-4">
                    <img class="profile-pic rounded-circle" id="up_img" style="height: 200px;width: 200px;" src="">
                    <input type="hidden" name="image" value="" id="up_path">
                        <label style="margin-top: 20px;"><span  id="btnupdate" class="text-center">Update Photo</span>
                       <input class="file-upload" name="cover_image" type="file" accept="image/*" style="font-size: 15px;" />
 
                       </label>
                   </div> 
                </div>
                </div> <!-- end of container -->
                <div class="container">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                             <select class="form-control" name="gender" id="up_gender">
                                  <option value="">----</option>
                                  <option value="MALE">Male</option>
                                  <option value="FEMALE">Female</option>
                              </select>
                            <label>Gender</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                             <select class="form-control" name="status" id="up_status">
                                  <option value="">----</option>
                                  <option value="SINGLE">Single</option>
                                  <option value="MARRIED">Married</option>
                                  <option value="WIDOWED">Widowed</option>
                                  <option value="DIVORSED">Divorsed</option>
                                  <option value="SEPARATED">Separated</option>
                              </select>
                            <label>Status</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input class="form-control" name="birthdate" id="up_birthdate" type="date">
                            <label>Birth Date</label>
                        </div>
                  </div>
                </div>
              </div><!--  end of container -->
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <p  style="font-size: 20px; font-weight: bold;">EMPLOYMENT INFORMATION</p>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <input class="form-control" name="employee_no" type="text" id="up_employee_no">
                        <label>Employee Number</label>
                      </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                            <input class="form-control" name="position" type="text" id="up_position">
                            <label>Position</label>
                        </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                             <select class="form-control" name="employment_status" id="up_employment_status">
                                  <option value="">----</option>
                                  <option value="REGULAR">Regular</option>
                                  <option value="PROBATIONARY">Probationary</option>
                                  <option value="PART TIME">Part Time</option>
                              </select>
                            <label>Employment Status</label>
                        </div>
                  </div>
                  <div class="col-md-4">
                        <div class="form-group">
                            <input class="form-control" name="rate" type="text" id="up_rate">
                            <label>Rate</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input class="form-control" name="date_hired" type="text" id="up_date_hired">
                            <label>Date Hired</label>
                        </div>
                  </div>
               </div>
              </div> <!-- end of container -->
           
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                      <p style="font-size: 20px; font-weight: bold;">EMPLOYMENT INDENTIFICATION NO.</p>
                  </div>
                   <div class="col-md-3">
                        <div class="form-group">
                            <input class="form-control" name="sss" type="text" id="up_sss">
                            <label>SSS</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <input class="form-control" name="phil_health" type="text" id="up_phil_health">
                            <label>PhilHealth</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <input class="form-control" name="pag_ibig" type="text" id="up_pag_ibig">
                            <label>PagIbig</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <input class="form-control" name="tin" type="text" id="up_tin">
                            <label>TIN</label>
                        </div>
                    </div>
                </div>        
              </div>
              <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p style="font-size: 20px; font-weight: bold;">INCASE OF EMERGENCY</p>
                    </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <input class="form-control" name="contact_person" type="text" id="up_contact_person">
                            <label>Contact Person Name</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input class="form-control" name="contact_details" type="text" id="up_contact_details">
                            <label>Contact Person Number</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                       <input  type="submit" class="btn btn-success" value="Update"> 
                    </div>
                </div>
              </div>
          </form>
          </div>
        </div>
      </div>
    </div> <!-- end of modal view Page -->
@stop
@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>


<script type="text/javascript">


  $(document).ready(function() {

$("#branch_name option[value='Main']").remove();

  $('.record').addClass('active');
  $('.record').addClass('collapse in');

  var table = $('#test').DataTable( {
        scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         true,
        fixedColumns:   {
        leftColumns: 1,
        rightColumns: 1
        },
        dom: 'Bfrtip',
        buttons: [
        'excel', 'pdf','csv', 
        
        ]
    });

   $('.a_view').on('click',function(){

    var view_id = $(this).attr('id').replace('a_view_','');
    var lname = $('#a_fullname_' + view_id).data('lname');
    var fname = $('#a_fullname_' + view_id).data('fname');
    var mname = $('#a_fullname_' + view_id).data('mname');
    var bday = $('#a_bithdate_' + view_id).text();
    var gender = $('#a_gender_' + view_id).text();
    var status  = $('#a_status_' + view_id).text();
    var address  = $('#a_address_' + view_id).text();
    var email  = $('#a_email_' + view_id).text(); 
    var contact_no  = $('#a_contact_no_' + view_id).text();
    var contact_person  = $('#a_contact_person_' + view_id).text();
    var contact_details  = $('#a_contact_details_' + view_id).text();
    var employee_no  = $('#a_employee_no_' + view_id).text();
    var position  = $('#a_position' + view_id).text();
    var employment_status  = $('#a_employment_status_' + view_id).text();
    var rate  = $('#a_rate_' + view_id).text();
    var date_hired  = $('#a_date_hired_' + view_id).text();
    var sss  = $('#a_sss_' + view_id).text();
    var phil_health  = $('#a_phil_health_' + view_id).text();
    var pag_ibig  = $('#a_pag_ibig_' + view_id).text();
    var tin  = $('#a_tin_' + view_id).text();
    var img  = $('#a_img_' + view_id).attr('image-path');
    var isUpload = $('#is_upload');

    $('#v_lname').text(lname);
    $('#v_fname').text(fname);
    $('#v_mname').text(mname);
    $('#v_birthdate').text(bday);
    $('#v_gender').text(gender);
    $('#v_status').text(status);
    $('#v_address').val(address);
    $('#v_email').text(email);
    $('#v_address').text(address);
    $('#v_email').text(email);
    $('#v_contact_no').text(contact_no);
    $('#v_contact_person').text(contact_person);
    $('#v_contact_details').text(contact_details);
    $('#v_employee_no').text(employee_no);
    $('#v_position').text(position);
    $('#v_employment_status').text(employment_status);
    $('#v_rate').text(rate);
    $('#v_date_hired').text(date_hired);
    $('#v_sss').text(sss);
    $('#v_phil_health').text(phil_health);
    $('#v_pag_ibig').text(pag_ibig);
    $('#v_tin').text(tin);
    $('#v_img').attr('src',img);
  });

$('.a_update').on('click',function(){
  var update_id = $(this).attr('id').replace('a_update_','');
  var lname = $('#a_fullname_' + update_id).data('lname');
  var fname = $('#a_fullname_' + update_id).data('fname');
  var mname = $('#a_fullname_' + update_id).data('mname');
  var bday = $('#a_bithdate_' + update_id).text();
  var gender = $('#a_gender_' + update_id).text();
  var status  = $('#a_status_' + update_id).text();
  var address  = $('#a_address_' + update_id).text();
  var email  = $('#a_email_' + update_id).text(); 
  var contact_no  = $('#a_contact_no_' + update_id).text();
  var contact_person  = $('#a_contact_person_' + update_id).text();
  var contact_details  = $('#a_contact_details_' + update_id).text();
  var employee_no  = $('#a_employee_no_' + update_id).text();
  var position  = $('#a_position' + update_id).text();
  var employment_status  = $('#a_employment_status_' + update_id).text();
  var rate  = $('#a_rate_' + update_id).text();
  var date_hired  = $('#a_date_hired_' + update_id).text();
  var sss  = $('#a_sss_' + update_id).text();
  var phil_health  = $('#a_phil_health_' + update_id).text();
  var pag_ibig  = $('#a_pag_ibig_' + update_id).text();
  var tin  = $('#a_tin_' + update_id).text();
  var img  = $('#a_img_' + update_id).attr('image-path');
  var branch_name = $('#a_branch_' + update_id).text();


    $('#update_id').val(update_id);
    $('#update_img').val(update_id);
    $('#up_fname').val(fname);
    $('#up_lname').val(lname);
    $('#up_mname').val(mname);
    $('#up_birthdate').val(bday);
    $('#up_gender').val(gender);
    $('#up_status').val(status);
    $('#up_address').val(address);
    $('#up_email').val(email);
    $('#up_contact_no').val(contact_no);
    $('#up_contact_person').val(contact_person);
    $('#up_contact_details').val(contact_details);
    $('#up_employee_no').val(employee_no);
    $('#up_position').val(position);
    $('#up_employment_status').val(employment_status);
    $('#up_rate').val(rate);
    $('#up_date_hired').val(date_hired);
    $('#up_sss').val(sss);
    $('#up_phil_health').val(phil_health);
    $('#up_pag_ibig').val(pag_ibig);
    $('#up_tin').val(tin);
    $('#up_img').attr('src',img);
    $('#up_path').val(img);
    $('#branch_name').val(branch_name);     
  });
}); 

 $('#update_form').on('submit',function(e){
       var updateData = new FormData();
        updateData.append('_token', '{{ csrf_token() }}');
        updateData.append('update_id', $('#update_id').val());
        updateData.append('last_name', $('#up_lname').val());
        updateData.append('first_name',$('#up_fname').val());
        updateData.append('middle_name', $('#up_mname').val());
        updateData.append('birthdate', $('#up_birthdate').val());
        updateData.append('gender', $('#up_gender').val());
        updateData.append('status', $('#up_status').val());
        updateData.append('address', $('#up_address').val());
        updateData.append('email', $('#up_email').val());
        updateData.append('contact_no', $('#up_contact_no').val());
        updateData.append('contact_person',$('#up_contact_person').val());
        updateData.append('contact_details',$('#up_contact_details').val());
        updateData.append('position',$('#up_position').val());
        updateData.append('employment_status',$('#up_employment_status').val());
        updateData.append('rate',$('#up_rate').val());
        updateData.append('date_hired',$('#up_date_hired').val());
        updateData.append('sss',$('#up_sss').val());
        updateData.append('phil_health',$('#up_phil_health').val());
        updateData.append('pag_ibig',$('#up_pag_ibig').val());
        updateData.append('tin',$('#up_tin').val());
        updateData.append('branch_name',$('#branch_name').val());
        updateData.append('cover_image',$('input[type=file]')[0].files[0]); 
        
        
        $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: updateData,
        processData:false,
        contentType: false,
          }).done(function(data){

        var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"}  
        var formData = {
              "Branch_Name":        "Novaliches",
              "emp_id":             $('#update_id').val(),  
              "last_name":          $('#up_lname').val(),      
              "first_name":         $('#up_fname').val(),        
              "middle_name":        $('#up_mname').val(),      
              "address":            $('#up_address').val(),          
              "contact_no":         $('#up_contact_no').val(),  
              "email":              $('#up_email').val(),  
              "birthday":           $('#up_birthdate').val(),   
              "gender":             $('#up_gender').val(),            
              "status":             $('#up_status').val(),           
              "employee_no":        $('input[name=employee_no]').val(),   
              "position":           $('#up_position').val(),
              "employment_status": $('#up_employment_status').val(),
              "rate":               $('#up_rate').val(),
              "Phil_health":        $('#up_phil_health').val(),
              "Pag_ibig":           $('#up_pag_ibig').val(),    
              "SSS":                $('#up_sss').val(),             
              "tin":                $('#up_tin').val(),          
              "contact_person":     $('#up_contact_person').val(),
              "contact_details":    $('#up_contact_details').val(),
              "cover_image":        "{{url('/cover_images')}}/"+data.cover_image,
              "date_hired":         $('#up_date_hired').val(),
              };
             
             $.ajax({
              url: 'https://cbrc.solutions/api/auth/login',
              method: 'POST',
              data: auth
              }).done(function(response){
              var tokenResponse = JSON.stringify(response);
              var token = JSON.parse(tokenResponse);
              var urlToken = token.access_token;   

              var FormToString = JSON.stringify(formData)
              var formToJson = JSON.parse(FormToString);
            
            $.ajax({
              method:'POST',
              url:"https://cbrc.solutions/api/main/employee?token="+urlToken,
              data:formToJson,
                 success:function(){
                         swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  }, error: function(msg) {
                        swal("Error!", "Something went wrong.", "error");
                        alert(msg.status);
                    }
            });
          });

          });//end of done function
         e.preventDefault(); 
      });

</script>

<script type="text/javascript">
$(document).ready(function() {

    
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    } 

    $(".file-upload").on('change', function(e){
       // readURL(this);
        var target = event.target || event.srcElement;

        if (target.value.length == 0) {
          $('#is_upload').val(0);
        } else {
          readURL(this);
          $('#is_upload').val(1);
        }
        
    });
    
    $(".upload-button").on('click', function() {
       $(".file-upload").click();
    });
}); 

</script>

@stop