@extends('main')
@section('title')
NEW BOOK PAYMENT
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
    .btn-danger{
        border-radius: 20px;
        font-size: 12px;
        margin-bottom: 9px;
    }
    .btn-danger:hover{
        background-color: #cc0303;
    }
    #tableTransferUI thead tr td,
    #tableTransferUI tbody tr td{
        padding-left: 20px;
        padding-right: 20px;
        padding-top:5px; 
        text-align: center;
    }
</style>
<link href="{{ asset('css/select.css') }}" rel="stylesheet" />
@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }}</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{Auth::user()->position. 'dashboard'}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">New Book Payment</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Book Transfer</h5>
                                      <br/>
                                   
                                    <!--book payment-->
                                    <div id = "payment_form">
                                    <form class="form-material" method="POST" data-toggle="validator" enctype="multipart/form-data" id="form" action="{{Auth::user()->position. 'insert-book-transfer'}}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input type="hidden" name="branchsender" id="branchsender" value="{{Auth::user()->  branch}}">
                                                    <input type="hidden" id="receipt" value=""> 
                                                    <input class="form-control" type="text" 
                                                    value="{{ $date }}" name="date" id="date" readonly>
                                                    <label>Date</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <select class="form-control" name="transfer_to" id="transfer_to">
                                                        <option value="0" disable="true" selected="true">--- Select Branch ---</option>
                                                       
                                                    <option value="{{ $branch }}">{{$branch}}</option>
                                                       
                                                    </select>
                                                    <label>Transfer to</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <select class="form-control" name="program" id="program">
                                                        <option value="0" disable="true" selected="true">--- Select Program ---</option>
                                                        @if(isset($program))
                                                        @foreach($program as $programs)
                                                        <option value="{{$programs->aka}}">{{$programs->program_name}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                    <label>Program</label>
                                                </div>
                                            </div>
                                        </div>
                                         
                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                     <select class="form-control" name="book" type="text" id="book">
                                                        <option value="" disable="true" selected="true">--- Select Book ---</option>
                                                       
                                                    </select>
                                                    <label>Book Title</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div >
                                            <table id="tableTransferUI" style="text-align:left"> 
                                                <thead style="visibility:hidden">
                                                    <tr>
                                                    <td>book title</td>
                                                    <td>initial Stock</td>
                                                    <td>Quantity to transfer</td>
                                                    <td>final Stock</td>
                                                    </tr>
                                                </thead>
                                                <tbody class="body">

                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div  class="form-group total-amount price" id="total-amount">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label>Total Quantity</label>
                                                <div class="form-group">
                
                                                    <input class="form-control" id="total_quantity" name="total_quantity" type="number" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label>Remarks</label>
                                                <input type="text" class="form-control js-example-basic-single" name="remark" id="remark" required>
                                                <div class="form-group input-has-value">
                                                        
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions btn-list">
                                            <button class="btn btn-primary print" type="submit" id="add" >Add Book Transfer</button>
                                            {{--<button class="btn btn-outline-default" type="button">Cancel</button>--}}
                                        </div>
                                    </form>
                                    </div>
                                    <!-- end of book payment-->



                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/prevent.js') }}"></script>
{{--<script src="{{ asset('/js/book-fetch.js') }}"></script>--}}
@include('js.book-transfer')
<script src="{{ asset('../js/print-book-transfer.js') }}"></script>
@stop