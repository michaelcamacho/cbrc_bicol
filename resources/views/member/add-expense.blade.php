@extends('main')
@section('title')
ADD NEW EXPENSE
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
</style>
@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }}</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{Auth::user()->position. 'dashboard'}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Add New Expense</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Add New Expense</h5>
                                   <br/>
                                    <form class="form-material" method="POST" data-toggle="validator" enctype="multipart/form-data" id="form" action="{{Auth::user()->position. 'insert-new-expense'}}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input type="hidden" name="branch" id="branch" value="{{Auth::user()->branch}}"> 
                                                    <input class="form-control" type="text" 
                                                    value="{{ $date }}" id="date" name="date" readonly>
                                                    <label>Date</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input type="hidden" name="branch" value="{{Auth::user()->position}}">
                                                </div>
                                            </div>
                                           
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <select class="form-control" id="program" name="program" required>
                                                        <option value="" >--- Select Program ---</option>
                                                        <option value="General">General</option>
                                                        @if(isset($program))
                                                        @foreach($program as $programs)
                                                        <option value="{{$programs->program_name}}">{{$programs->program_name}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                    <label>Program</label>
                                                </div>
                                            </div>
                                        </div>

                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <select class="form-control" name="category" id="category" required>
                                                        <option value="" disable="true" selected="true">--- Select Category ---</option>
                                                        <option value="Salary">Salary</option>
                                                        <option value="Tax">Taxes and Licenses</option>
                                                        <option value="Insurance">Insurance</option>
                                                        <option value="Loans">Loans</option>
                                                        <option value="Marketing">Marketing</option>
                                                        <option value="Contribution">Contribution</option>
                                                        <option value="Utilities">Utilities</option>
                                                        <option value="Daily Expense">Daily Expense</option>
                                                        <option value="Lecturer">Lecturer</option>
                                                        <option value="Investment">Investment</option>
                                                        <option value="Books">Books</option>
                                                    </select>
                                                    <label>Expense Category</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group val">
                                                     
                                                </div>
                                            </div>
                                           
                                        </div>

                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                   <select class="form-control" name="sub_category" id="sub_category">
                                                        <option value="" disable="true" selected="true">--- Select Sub Category ---</option>
                                                       
                                                    </select>
                                                    <label id="lblSubCategory">Expense Sub Category</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group val">
                                                     
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                     <input class="form-control" name="amount" type="number" min="0" step="0.01" id="amount">
                                                       
                                                    <label>Amount</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group val">
                                                    
                                                </div>
                                            </div>

                                           
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input class="form-control" name="remarks" id="remarks">
                                                    <label>Remarks</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                   
                                                </div>
                                            </div>
                                           
                                        </div>
                                       
                                        <div class="form-actions btn-list">
                                            <button class="btn btn-primary" type="submit" id="button-prevent">Add New Expense</button>
                                            {{--<button class="btn btn-outline-default" type="button">Cancel</button>--}}
                                        </div>
                                    </form>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/prevent.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
  $('.add-expense').addClass('active');
});


$('#program').on('change', function(e){

    if($('#category').val() == 'Books'){
    var program = $('#program').val();
    var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"};

    $('#lblSubCategory').text('Book Title');
    $('#sub_category').empty();
    $('#sub_category').append('<option value="" disable="true" selected="true">--- Select Book Title ---</option>');

  $.ajax({
              url: 'https://cbrc.solutions/api/auth/login',
              method: 'POST',
              data: auth
              }).done(function(response){
              var tokenResponse = JSON.stringify(response);
              var token = JSON.parse(tokenResponse);
              var urlToken = token.access_token;   
            console.log(urlToken);

                    $.getJSON('https://cbrc.solutions/api/main/books?token='+urlToken,function(data){
                    
                                $('#sub_category').empty();
                                $('#sub_category').append('<option value="0" disable="true" selected="true">--- Select Book ---</option>');
                                    $.each(data, function (i) {
                                        $.each(this, function (key, value) {
                                        
                                            console.log(value.program_name + " = " + program);
                                            if(value.program_name == program ){
                                                

                                              $('#sub_category').append('<option value="'+ value.Title +'">'+  value.Title +' </option>');
                                            }
                                        
                                        });
                                    });

                });//end of on change getJSON
                
        });//end of on change #program 
                 //fetch api book title
    }
});

$('#category').on('change', function(e){
            console.log(e);
            var program = $('#program').val();
            var category = e.target.value;
            var branch = $('#branch').val().toLowerCase();
            var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"};
    
                if($('#category').val() == 'Books'){


                 $('#lblSubCategory').text('Book Title');
                 $('#sub_category').empty();
                 $('#sub_category').append('<option value="" disable="true" selected="true">--- Select Book Title ---</option>');

                 //$('#category').prop('selectedIndex',0);
                 
                 $('#amount').val('');
                 $('#remarks').val('');
                 //$('#total_amount').val('');
                 $.ajax({
              url: 'https://cbrc.solutions/api/auth/login',
              method: 'POST',
              data: auth
              }).done(function(response){
              var tokenResponse = JSON.stringify(response);
              var token = JSON.parse(tokenResponse);
              var urlToken = token.access_token;   
            console.log(urlToken);

                    $.getJSON('https://cbrc.solutions/api/main/books?token='+urlToken,function(data){
                    
                                $('#sub_category').empty();
                                $('#sub_category').append('<option value="0" disable="true" selected="true">--- Select Book ---</option>');
                                    $.each(data, function (i) {
                                        $.each(this, function (key, value) {
                                        
                                            console.log(value.program_name + " = " + program);
                                            if(value.program_name == program ){
                                                

                                              $('#sub_category').append('<option value="'+ value.Title +'">'+  value.Title +' </option>');
                                            }
                                        
                                        });
                                    });

                });//end of on change getJSON
                
        });//end of on change #program 
                 //fetch api book title

                }//end of if books


                if($('#category').val() != 'Books'){
                    $.get({{Auth::user()->position}}'json-expense?category=' + category,function(data){
                console.log(data);

           

                 $('#lblSubCategory').text('Expense Sub Category');
                 $('#sub_category').empty();
                 $('#sub_category').append('<option value="" disable="true" selected="true">--- Select Sub Category ---</option>');

                 //$('#category').prop('selectedIndex',0);
                 
                 $('#amount').val('');
                 $('#remarks').val('');
                 //$('#total_amount').val('');

                  $.each(data, function(index, expenseObj){
                    $('#sub_category').append('<option value="'+ expenseObj.sub_category +'">'+ expenseObj.sub_category+'</option>');
                });


                 });//end of auth
                }//end of if != book

        
        
        });

$('#sub_category').on('change', function(e){

    $('#amount').val('');
    $('#remarks').val('');
    });
</script>
@stop