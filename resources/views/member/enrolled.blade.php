@extends('main')
@section('title')
Enrolled Records
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Enrolled Records</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Enrolled Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Enrolled Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="example-en" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Name</th>
                            <th>CBRC ID</th>
                            <th>Program</th>
                            <th>Section</th>
                            <th>Category</th>
                            <th>Contact #</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Facilitation</th>
                            <th>Birthdate</th>
                            <th>School</th>
                            <th>Major</th>
                            <th>Noa #</th>
                            <th>Take</th>
                            <th>Contact Person</th>
                            <th>Emergency #</th>
                            <th>Date Registered</th>
                            <th>Total Tuition</th>
                            <th>Total Amount</th>
                            <th>Total Balance</th>
                            <th>Facilitation</th>
                            <th>Discount</th>
                        </tr>
                       
                    </thead>
                    <tbody>

                        {{-- season 1  --}}
                         @if(isset($let))
                            @foreach($let as $lets)
                            @if($lets->season == "Season 1")
                        <tr>
                            <td>{{$lets->last_name}}, {{$lets->first_name}} {{$lets->middle_name}}</td>
                            <td>{{$lets->cbrc_id}}</td>
                            <td>{{$lets->course}}</td>
                            <td>{{$lets->section}}</td>
                            <td>{{$lets->category}}</td>
                            <td>{{$lets->contact_no}}</td>
                            <td>{{$lets->email}}</td>
                            <td>{{$lets->address}}</td>
                            <td>{{$lets->facilitation}}</td> 
                            <td>{{$lets->birthdate}}</td>
                            <td>{{$lets->school}}</td>
                            <td>{{$lets->major}}</td>
                            <td>{{$lets->noa_no}}</td>
                            <td>{{$lets->take}}</td>
                            <td>{{$lets->contact_person}}</td>
                            <td>{{$lets->contact_details}}</td>
                            <td>{{\Carbon\Carbon::parse($lets->created_at)->format('M-d-Y')}}</td>
                            @foreach ($sale1 as $s1item)
                                @if($lets->id == $s1item->student_id && $s1item->program=="LET" && $s1item->year == $lets->year && $lets->season == "Season 1")
                                <div style="display:none">
                                    {{$tot_amount_paid = $tot_amount_paid + $s1item->amount_paid}}
                                    {{$total_balance = ($s1item->facilitation_fee+$s1item->tuition_fee) - $tot_amount_paid}}
                                </div>
                                @endif
                            @endforeach
                            <td>{{$s1item->tuition_fee}}</td>
                            <td>{{$tot_amount_paid}}</td>
                            <td>{{$total_balance}}</td>
                            <td>{{$s1item->facilitation_fee}}</td>
                            <td>{{$s1item->discount}}</td>
                            
                        </tr>
                        @endif
                       @endforeach
                            @endif
                        @if(isset($nle))
                            @foreach($nle as $nles)
                            @if($nles->season == "Season 1")
                        <tr>
                            <td>{{$nles->last_name}}, {{$nles->first_name}} {{$nles->middle_name}}</td>
                            <td>{{$nles->cbrc_id}}</td>
                            <td>{{$nles->course}}</td>
                            <td>{{$nles->section}}</td>
                            <td>{{$nles->category}}</td>
                            <td>{{$nles->contact_no}}</td>
                            <td>{{$nles->email}}</td>
                            <td>{{$nles->address}}</td>
                            <td>{{$nles->facilitation}}</td> 
                            <td>{{$nles->birthdate}}</td>
                            <td>{{$nles->school}}</td>
                            <td>{{$nles->major}}</td>
                            <td>{{$nles->noa_no}}</td>
                            <td>{{$nles->take}}</td>
                            <td>{{$nles->contact_person}}</td>
                            <td>{{$nles->contact_details}}</td>
                            <td>{{\Carbon\Carbon::parse($nles->created_at)->format('M-d-Y')}}</td>
                            @foreach ($sale1 as $s1item)
                            @if($nles->id == $s1item->student_id && $s1item->program=="NLE" && $s1item->year == $nles->year && $nles->season == "Season 1")
                            <div style="display:none">
                                {{$tot_amount_paid = $tot_amount_paid + $s1item->amount_paid}}
                                {{$total_balance = ($s1item->facilitation_fee+$s1item->tuition_fee) - $tot_amount_paid}}
                            </div>
                            @endif
                            @endforeach
                            <td>{{$s1item->tuition_fee}}</td>
                            <td>{{$tot_amount_paid}}</td>
                            <td>{{$total_balance}}</td>
                            <td>{{$s1item->facilitation_fee}}</td>
                            <td>{{$s1item->discount}}</td>
                        </tr>
                        @endif
                       @endforeach
                            @endif
                        @if(isset($crim))
                            @foreach($crim as $crims)
                            
                            @if($crims->season == "Season 1")
                        <tr>
                            <td>{{$crims->last_name}}, {{$crims->first_name}} {{$crims->middle_name}}</td>
                            <td>{{$crims->last_name}}, {{$crims->first_name}} {{$crims->middle_name}}</td>
                            <td>{{$crims->cbrc_id}}</td>
                            <td>{{$crims->course}}</td>
                            <td>{{$crims->section}}</td>
                            <td>{{$crims->category}}</td>
                            <td>{{$crims->contact_no}}</td>
                            <td>{{$crims->email}}</td>
                            <td>{{$crims->address}}</td>
                            <td>{{$crims->facilitation}}</td> 
                            <td>{{$crims->birthdate}}</td>
                            <td>{{$crims->school}}</td>
                            <td>{{$crims->major}}</td>
                            <td>{{$crims->noa_no}}</td>
                            <td>{{$crims->take}}</td>
                            <td>{{$crims->contact_person}}</td>
                            <td>{{$crims->contact_details}}</td>
                            <td>{{\Carbon\Carbon::parse($crims->created_at)->format('M-d-Y')}}</td>
                            @foreach ($sale1 as $s1item)
                            @if($crims->id == $s1item->student_id && $s1item->program=="Criminology" && $s1item->year == $crims->year && $crims->season == "Season 1")
                            <div style="display:none">
                                {{$tot_amount_paid = $tot_amount_paid + $s1item->amount_paid}}
                                {{$total_balance = ($s1item->facilitation_fee+$s1item->tuition_fee) - $tot_amount_paid}}
                            </div>
                            @endif
                        @endforeach
                        <td>{{$s1item->tuition_fee}}</td>
                        <td>{{$tot_amount_paid}}</td>
                        <td>{{$total_balance}}</td>
                        <td>{{$s1item->facilitation_fee}}</td>
                        <td>{{$s1item->discount}}</td>
                        </tr>
                        @endif
                       @endforeach
                            @endif
                        @if(isset($civil))
                            @foreach($civil as $civils)
                            @if($civils->season == "Season 1")
                        <tr>
                           <td>{{$civils->last_name}}, {{$civils->first_name}} {{$civils->middle_name}}</td>
                            <td>{{$civils->cbrc_id}}</td>
                            <td>{{$civils->course}}</td>
                            <td>{{$civils->section}}</td>
                            <td>{{$civils->category}}</td>
                            <td>{{$civils->contact_no}}</td>
                            <td>{{$civils->email}}</td>
                            <td>{{$civils->address}}</td>
                            <td>{{$civils->facilitation}}</td> 
                            <td>{{$civils->birthdate}}</td>
                            <td>{{$civils->school}}</td>
                            <td>{{$civils->major}}</td>
                            <td>{{$civils->noa_no}}</td>
                            <td>{{$civils->take}}</td>
                            <td>{{$civils->contact_person}}</td>
                            <td>{{$civils->contact_details}}</td>
                            <td>{{\Carbon\Carbon::parse($civils->created_at)->format('M-d-Y')}}</td>
                            @foreach ($sale1 as $s1item)
                            @if($civils->id == $s1item->student_id && $s1item->program=="Civil Service" && $s1item->year == $civils->year && $civils->season == "Season 1")
                            <div style="display:none">
                                {{$tot_amount_paid = $tot_amount_paid + $s1item->amount_paid}}
                                {{$total_balance = ($s1item->facilitation_fee+$s1item->tuition_fee) - $tot_amount_paid}}
                            </div>
                            @endif
                        @endforeach
                        <td>{{$s1item->tuition_fee}}</td>
                        <td>{{$tot_amount_paid}}</td>
                        <td>{{$total_balance}}</td>
                        <td>{{$s1item->facilitation_fee}}</td>
                        <td>{{$s1item->discount}}</td>
                        </tr>
                        @endif
                       @endforeach
                            @endif
                        @if(isset($psyc))
                            @foreach($psyc as $psycs)
                            
                            @if($psycs->season == "Season 1")
                        <tr>
                            <td>{{$psycs->last_name}}, {{$psycs->first_name}} {{$psycs->middle_name}}</td>
                            <td>{{$psycs->cbrc_id}}</td>
                            <td>{{$psycs->course}}</td>
                            <td>{{$psycs->section}}</td>
                            <td>{{$psycs->category}}</td>
                            <td>{{$psycs->contact_no}}</td>
                            <td>{{$psycs->email}}</td>
                            <td>{{$psycs->address}}</td>
                            <td>{{$psycs->facilitation}}</td> 
                            <td>{{$psycs->birthdate}}</td>
                            <td>{{$psycs->school}}</td>
                            <td>{{$psycs->major}}</td>
                            <td>{{$psycs->noa_no}}</td>
                            <td>{{$psycs->take}}</td>
                            <td>{{$psycs->contact_person}}</td>
                            <td>{{$psycs->contact_details}}</td>
                            <td>{{\Carbon\Carbon::parse($psycs->created_at)->format('M-d-Y')}}</td>
                            @foreach ($sale1 as $s1item)
                            @if($psycs->id == $s1item->student_id && $s1item->program=="Psychometrician" && $s1item->year == $psycs->year && $psycs->season == "Season 1")
                            <div style="display:none">
                                {{$tot_amount_paid = $tot_amount_paid + $s1item->amount_paid}}
                                {{$total_balance = ($s1item->facilitation_fee+$s1item->tuition_fee) - $tot_amount_paid}}
                            </div>
                            @endif
                        @endforeach
                        <td>{{$s1item->tuition_fee}}</td>
                        <td>{{$tot_amount_paid}}</td>
                        <td>{{$total_balance}}</td>
                        <td>{{$s1item->facilitation_fee}}</td>
                        <td>{{$s1item->discount}}</td>
                        </tr>
                        @endif
                       @endforeach
                            @endif
                        @if(isset($nclex))
                            @foreach($nclex as $nclexs)
                            @if($nclexs->season == "Season 1")
                        <tr>
                            <td>{{$nclexs->last_name}}, {{$nclexs->first_name}} {{$nclexs->middle_name}}</td>
                            <td>{{$nclexs->cbrc_id}}</td>
                            <td>{{$nclexs->course}}</td>
                            <td>{{$nclexs->section}}</td>
                            <td>{{$nclexs->category}}</td>
                            <td>{{$nclexs->contact_no}}</td>
                            <td>{{$nclexs->email}}</td>
                            <td>{{$nclexs->address}}</td>
                            <td>{{$nclexs->facilitation}}</td> 
                            <td>{{$nclexs->birthdate}}</td>
                            <td>{{$nclexs->school}}</td>
                            <td>{{$nclexs->major}}</td>
                            <td>{{$nclexs->noa_no}}</td>
                            <td>{{$nclexs->take}}</td>
                            <td>{{$nclexs->contact_person}}</td>
                            <td>{{$nclexs->contact_details}}</td>
                            <td>{{\Carbon\Carbon::parse($nclexs->created_at)->format('M-d-Y')}}</td>
                            @foreach ($sale1 as $s1item)
                            @if($nclexs->id == $s1item->student_id && $s1item->program=="NCLEX" && $s1item->year == $nclexs->year && $nclexs->season == "Season 1")
                            <div style="display:none">
                                {{$tot_amount_paid = $tot_amount_paid + $s1item->amount_paid}}
                                {{$total_balance = ($s1item->facilitation_fee+$s1item->tuition_fee) - $tot_amount_paid}}
                            </div>
                            @endif
                        @endforeach
                        <td>{{$s1item->tuition_fee}}</td>
                        <td>{{$tot_amount_paid}}</td>
                        <td>{{$total_balance}}</td>
                        <td>{{$s1item->facilitation_fee}}</td>
                        <td>{{$s1item->discount}}</td>
                        </tr>
                        @endif
                       @endforeach
                            @endif
                        @if(isset($ielt))
                            @foreach($ielt as $ielts)
                            @if($ielts->season == "Season 1")
                        <tr>
                            <td>{{$ielts->last_name}}, {{$ielts->first_name}} {{$ielts->middle_name}}</td>
                            <td>{{$ielts->cbrc_id}}</td>
                            <td>{{$ielts->course}}</td>
                            <td>{{$ielts->section}}</td>
                            <td>{{$ielts->category}}</td>
                            <td>{{$ielts->contact_no}}</td>
                            <td>{{$ielts->email}}</td>
                            <td>{{$ielts->address}}</td>
                            <td>{{$ielts->facilitation}}</td> 
                            <td>{{$ielts->birthdate}}</td>
                            <td>{{$ielts->school}}</td>
                            <td>{{$ielts->major}}</td>
                            <td>{{$ielts->noa_no}}</td>
                            <td>{{$ielts->take}}</td>
                            <td>{{$ielts->contact_person}}</td>
                            <td>{{$ielts->contact_details}}</td>
                            <td>{{\Carbon\Carbon::parse($ielts->created_at)->format('M-d-Y')}}</td>
                            @foreach ($sale1 as $s1item)
                            @if($ielts->id == $s1item->student_id && $s1item->program=="IELTS" && $s1item->year == $ielts->year && $ielts->season == "Season 1")
                            <div style="display:none">
                                {{$tot_amount_paid = $tot_amount_paid + $s1item->amount_paid}}
                                {{$total_balance = ($s1item->facilitation_fee+$s1item->tuition_fee) - $tot_amount_paid}}
                            </div>
                            @endif
                        @endforeach
                        <td>{{$s1item->tuition_fee}}</td>
                        <td>{{$tot_amount_paid}}</td>
                        <td>{{$total_balance}}</td>
                        <td>{{$s1item->facilitation_fee}}</td>
                        <td>{{$s1item->discount}}</td>
                        </tr>
                        @endif
                       @endforeach
                            @endif
                        @if(isset($social))
                            @foreach($social as $socials)
                            @if($socials->season == "Season 1")
                        <tr>
                            <td>{{$socials->last_name}}, {{$socials->first_name}} {{$socials->middle_name}}</td>
                            <td>{{$socials->cbrc_id}}</td>
                            <td>{{$socials->course}}</td>
                            <td>{{$socials->section}}</td>
                            <td>{{$socials->category}}</td>
                            <td>{{$socials->contact_no}}</td>
                            <td>{{$socials->email}}</td>
                            <td>{{$socials->address}}</td>
                            <td>{{$socials->facilitation}}</td> 
                            <td>{{$socials->birthdate}}</td>
                            <td>{{$socials->school}}</td>
                            <td>{{$socials->major}}</td>
                            <td>{{$socials->noa_no}}</td>
                            <td>{{$socials->take}}</td>
                            <td>{{$socials->contact_person}}</td>
                            <td>{{$socials->contact_details}}</td>
                            <td>{{\Carbon\Carbon::parse($socials->created_at)->format('M-d-Y')}}</td>
                            @foreach ($sale1 as $s1item)
                            @if($socials->id == $s1item->student_id && $s1item->program=="Social Work" && $s1item->year == $socials->year && $socials->season == "Season 1")
                            <div style="display:none">
                                {{$tot_amount_paid = $tot_amount_paid + $s1item->amount_paid}}
                                {{$total_balance = ($s1item->facilitation_fee+$s1item->tuition_fee) - $tot_amount_paid}}
                            </div>
                            @endif
                        @endforeach
                        <td>{{$s1item->tuition_fee}}</td>
                        <td>{{$tot_amount_paid}}</td>
                        <td>{{$total_balance}}</td>
                        <td>{{$s1item->facilitation_fee}}</td>
                        <td>{{$s1item->discount}}</td>
                        </tr>
                        @endif
                       @endforeach
                            @endif
                        @if(isset($agri))
                            @foreach($agri as $agris)
                            @if($agris->season == "Season 1")
                        <tr>
                            <td>{{$agris->last_name}}, {{$agris->first_name}} {{$agris->middle_name}}</td>
                            <td>{{$agris->cbrc_id}}</td>
                            <td>{{$agris->course}}</td>
                            <td>{{$agris->section}}</td>
                            <td>{{$agris->category}}</td>
                            <td>{{$agris->contact_no}}</td>
                            <td>{{$agris->email}}</td>
                            <td>{{$agris->address}}</td>
                            <td>{{$agris->facilitation}}</td> 
                            <td>{{$agris->birthdate}}</td>
                            <td>{{$agris->school}}</td>
                            <td>{{$agris->major}}</td>
                            <td>{{$agris->noa_no}}</td>
                            <td>{{$agris->take}}</td>
                            <td>{{$agris->contact_person}}</td>
                            <td>{{$agris->contact_details}}</td>
                            <td>{{\Carbon\Carbon::parse($agris->created_at)->format('M-d-Y')}}</td>
                            @foreach ($sale1 as $s1item)
                            @if($agris->id == $s1item->student_id && $s1item->program=="Agriculture" && $s1item->year == $agris->year && $agris->season == "Season 1")
                            <div style="display:none">
                                {{$tot_amount_paid = $tot_amount_paid + $s1item->amount_paid}}
                                {{$total_balance = ($s1item->facilitation_fee+$s1item->tuition_fee) - $tot_amount_paid}}
                            </div>
                            @endif
                        @endforeach
                        <td>{{$s1item->tuition_fee}}</td>
                        <td>{{$tot_amount_paid}}</td>
                        <td>{{$total_balance}}</td>
                        <td>{{$s1item->facilitation_fee}}</td>
                        <td>{{$s1item->discount}}</td>
                        </tr>
                        @endif
                       @endforeach
                            @endif

                            @if(isset($mid))
                            @foreach($mid as $mids)
                            @if($mids->season == "Season 1")
                        <tr>
                            <td>{{$mids->last_name}}, {{$mids->first_name}} {{$mids->middle_name}}</td>
                            <td>{{$mids->cbrc_id}}</td>
                            <td>{{$mids->course}}</td>
                            <td>{{$mids->section}}</td>
                            <td>{{$mids->category}}</td>
                            <td>{{$mids->contact_no}}</td>
                            <td>{{$mids->email}}</td>
                            <td>{{$mids->address}}</td>
                            <td>{{$mids->facilitation}}</td> 
                            <td>{{$mids->birthdate}}</td>
                            <td>{{$mids->school}}</td>
                            <td>{{$mids->major}}</td>
                            <td>{{$mids->noa_no}}</td>
                            <td>{{$mids->take}}</td>
                            <td>{{$mids->contact_person}}</td>
                            <td>{{$mids->contact_details}}</td>
                            <td>{{\Carbon\Carbon::parse($mids->created_at)->format('M-d-Y')}}</td>
                            @foreach ($sale1 as $s1item)
                            @if($mids->id == $s1item->student_id && $s1item->program=="Midwifery" && $s1item->year == $mids->year && $mids->season == "Season 1")
                            <div style="display:none">
                                {{$tot_amount_paid = $tot_amount_paid + $s1item->amount_paid}}
                                {{$total_balance = ($s1item->facilitation_fee+$s1item->tuition_fee) - $tot_amount_paid}}
                            </div>
                            @endif
                        @endforeach
                        <td>{{$s1item->tuition_fee}}</td>
                        <td>{{$tot_amount_paid}}</td>
                        <td>{{$total_balance}}</td>
                        <td>{{$s1item->facilitation_fee}}</td>
                        <td>{{$s1item->discount}}</td>
                        </tr>
                        @endif
                       @endforeach
                            @endif

                            @if(isset($online))
                            @foreach($online as $onlines)
                            @if($onlines->season == "Season 1")
                        <tr>
                            <td>{{$onlines->last_name}}, {{$onlines->first_name}} {{$onlines->middle_name}}</td>
                            <td>{{$onlines->cbrc_id}}</td>
                            <td>{{$onlines->course}}</td>
                            <td>{{$onlines->section}}</td>
                            <td>{{$onlines->category}}</td>
                            <td>{{$onlines->contact_no}}</td>
                            <td>{{$onlines->email}}</td>
                            <td>{{$onlines->address}}</td>
                            <td>{{$onlines->facilitation}}</td> 
                            <td>{{$onlines->birthdate}}</td>
                            <td>{{$onlines->school}}</td>
                            <td>{{$onlines->major}}</td>
                            <td>{{$onlines->noa_no}}</td>
                            <td>{{$onlines->take}}</td>
                            <td>{{$onlines->contact_person}}</td>
                            <td>{{$onlines->contact_details}}</td>
                            <td>{{\Carbon\Carbon::parse($onlines->created_at)->format('M-d-Y')}}</td>
                            @foreach ($sale1 as $s1item)
                            @if($onlines->id == $s1item->student_id && $s1item->program=="Online Only" && $s1item->year == $onlines->year && $onlines->season == "Season 1")
                            <div style="display:none">
                                {{$tot_amount_paid = $tot_amount_paid + $s1item->amount_paid}}
                                {{$total_balance = ($s1item->facilitation_fee+$s1item->tuition_fee) - $tot_amount_paid}}
                            </div>
                            @endif
                        @endforeach
                        <td>{{$s1item->tuition_fee}}</td>
                        <td>{{$tot_amount_paid}}</td>
                        <td>{{$total_balance}}</td>
                        <td>{{$s1item->facilitation_fee}}</td>
                        <td>{{$s1item->discount}}</td>
                        </tr>
                        @endif
                       @endforeach
                            @endif

                            {{-- end of season 1 --}}

                                {{-- season 2  --}}
                         @if(isset($let))
                         @foreach($let as $lets) 
                         @if($lets->season == "Season 2")
                     <tr>
                         <td>{{$lets->last_name}}, {{$lets->first_name}} {{$lets->middle_name}}</td>
                         <td>{{$lets->cbrc_id}}</td>
                         <td>{{$lets->course}}</td>
                         <td>{{$lets->section}}</td>
                         <td>{{$lets->category}}</td>
                         <td>{{$lets->contact_no}}</td>
                         <td>{{$lets->email}}</td>
                         <td>{{$lets->address}}</td>
                         <td>{{$lets->facilitation}}</td> 
                         <td>{{$lets->birthdate}}</td>
                         <td>{{$lets->school}}</td>
                         <td>{{$lets->major}}</td>
                         <td>{{$lets->noa_no}}</td>
                         <td>{{$lets->take}}</td>
                         <td>{{$lets->contact_person}}</td>
                         <td>{{$lets->contact_details}}</td>
                         <td>{{\Carbon\Carbon::parse($lets->created_at)->format('M-d-Y')}}</td>
                         @foreach($sale2 as $s2item)
                             @if($lets->id == $s2item->student_id && $s2item->program=="LET" && $s2item->year == $lets->year && $lets->season == "Season 2")
                             <div style="display:none">
                                 {{$tot_amount_paid = $tot_amount_paid + $s2item->amount_paid}}
                                 {{$total_balance = ($s2item->facilitation_fee+$s2item->tuition_fee) - $tot_amount_paid}}
                             </div>
                             @endif
                         @endforeach
                         <td>{{$s2item->tuition_fee}}</td>
                         <td>{{$tot_amount_paid}}</td>
                         <td>{{$total_balance}}</td>
                         <td>{{$s2item->facilitation_fee}}</td>
                         <td>{{$s2item->discount}}</td>
                         
                     </tr>
                     @endif
                    @endforeach
                         @endif
                     @if(isset($nle))
                         @foreach($nle as $nles)
                         @if($nles->season == "Season 2")
                     <tr>
                         <td>{{$nles->last_name}}, {{$nles->first_name}} {{$nles->middle_name}}</td>
                         <td>{{$nles->cbrc_id}}</td>
                         <td>{{$nles->course}}</td>
                         <td>{{$nles->section}}</td>
                         <td>{{$nles->category}}</td>
                         <td>{{$nles->contact_no}}</td>
                         <td>{{$nles->email}}</td>
                         <td>{{$nles->address}}</td>
                         <td>{{$nles->facilitation}}</td> 
                         <td>{{$nles->birthdate}}</td>
                         <td>{{$nles->school}}</td>
                         <td>{{$nles->major}}</td>
                         <td>{{$nles->noa_no}}</td>
                         <td>{{$nles->take}}</td>
                         <td>{{$nles->contact_person}}</td>
                         <td>{{$nles->contact_details}}</td>
                         <td>{{\Carbon\Carbon::parse($nles->created_at)->format('M-d-Y')}}</td>
                         @foreach ($sale2 as $s2item)
                         @if($nles->id == $s2item->student_id && $s2item->program=="NLE" && $s2item->year == $nles->year && $nles->season == "Season 2")
                         <div style="display:none">
                             {{$tot_amount_paid = $tot_amount_paid + $s2item->amount_paid}}
                             {{$total_balance = ($s2item->facilitation_fee+$s2item->tuition_fee) - $tot_amount_paid}}
                         </div>
                         @endif
                         @endforeach
                         <td>{{$s2item->tuition_fee}}</td>
                         <td>{{$tot_amount_paid}}</td>
                         <td>{{$total_balance}}</td>
                         <td>{{$s2item->facilitation_fee}}</td>
                         <td>{{$s2item->discount}}</td>
                     </tr>
                     @endif
                    @endforeach
                         @endif
                     @if(isset($crim))
                         @foreach($crim as $crims)
                         @if($crims->season == "Season 2")
                     <tr>
                         <td>{{$crims->last_name}}, {{$crims->first_name}} {{$crims->middle_name}}</td>
                         <td>{{$crims->last_name}}, {{$crims->first_name}} {{$crims->middle_name}}</td>
                         <td>{{$crims->cbrc_id}}</td>
                         <td>{{$crims->course}}</td>
                         <td>{{$crims->section}}</td>
                         <td>{{$crims->category}}</td>
                         <td>{{$crims->contact_no}}</td>
                         <td>{{$crims->email}}</td>
                         <td>{{$crims->address}}</td>
                         <td>{{$crims->facilitation}}</td> 
                         <td>{{$crims->birthdate}}</td>
                         <td>{{$crims->school}}</td>
                         <td>{{$crims->major}}</td>
                         <td>{{$crims->noa_no}}</td>
                         <td>{{$crims->take}}</td>
                         <td>{{$crims->contact_person}}</td>
                         <td>{{$crims->contact_details}}</td>
                         <td>{{\Carbon\Carbon::parse($crims->created_at)->format('M-d-Y')}}</td>
                         @foreach ($sale2 as $s2item)
                         @if($crims->id == $s2item->student_id && $s2item->program=="Criminology" && $s2item->year == $crims->year && $crims->season == "Season 2")
                         <div style="display:none">
                             {{$tot_amount_paid = $tot_amount_paid + $s2item->amount_paid}}
                             {{$total_balance = ($s2item->facilitation_fee+$s2item->tuition_fee) - $tot_amount_paid}}
                         </div>
                         @endif
                     @endforeach
                     <td>{{$s2item->tuition_fee}}</td>
                     <td>{{$tot_amount_paid}}</td>
                     <td>{{$total_balance}}</td>
                     <td>{{$s2item->facilitation_fee}}</td>
                     <td>{{$s2item->discount}}</td>
                     </tr>
                     @endif
                    @endforeach
                         @endif
                     @if(isset($civil))
                         @foreach($civil as $civils)
                         @if($civils->season == "Season 2")
                         
                     <tr>
                        <td>{{$civils->last_name}}, {{$civils->first_name}} {{$civils->middle_name}}</td>
                         <td>{{$civils->cbrc_id}}</td>
                         <td>{{$civils->course}}</td>
                         <td>{{$civils->section}}</td>
                         <td>{{$civils->category}}</td>
                         <td>{{$civils->contact_no}}</td>
                         <td>{{$civils->email}}</td>
                         <td>{{$civils->address}}</td>
                         <td>{{$civils->facilitation}}</td> 
                         <td>{{$civils->birthdate}}</td>
                         <td>{{$civils->school}}</td>
                         <td>{{$civils->major}}</td>
                         <td>{{$civils->noa_no}}</td>
                         <td>{{$civils->take}}</td>
                         <td>{{$civils->contact_person}}</td>
                         <td>{{$civils->contact_details}}</td>
                         <td>{{\Carbon\Carbon::parse($civils->created_at)->format('M-d-Y')}}</td>
                         @foreach ($sale2 as $s2item)
                         @if($civils->id == $s2item->student_id && $s2item->program=="Civil Service" && $s2item->year == $civils->year && $civils->season == "Season 2")
                         <div style="display:none">
                             {{$tot_amount_paid = $tot_amount_paid + $s2item->amount_paid}}
                             {{$total_balance = ($s2item->facilitation_fee+$s2item->tuition_fee) - $tot_amount_paid}}
                         </div>
                         @endif
                     @endforeach
                     <td>{{$s2item->tuition_fee}}</td>
                     <td>{{$tot_amount_paid}}</td>
                     <td>{{$total_balance}}</td>
                     <td>{{$s2item->facilitation_fee}}</td>
                     <td>{{$s2item->discount}}</td>
                     </tr>
                     @endif
                    @endforeach
                         @endif
                     @if(isset($psyc))
                         @foreach($psyc as $psycs)
                         @if($psycs->season == "Season 2")
                     <tr>
                         <td>{{$psycs->last_name}}, {{$psycs->first_name}} {{$psycs->middle_name}}</td>
                         <td>{{$psycs->cbrc_id}}</td>
                         <td>{{$psycs->course}}</td>
                         <td>{{$psycs->section}}</td>
                         <td>{{$psycs->category}}</td>
                         <td>{{$psycs->contact_no}}</td>
                         <td>{{$psycs->email}}</td>
                         <td>{{$psycs->address}}</td>
                         <td>{{$psycs->facilitation}}</td> 
                         <td>{{$psycs->birthdate}}</td>
                         <td>{{$psycs->school}}</td>
                         <td>{{$psycs->major}}</td>
                         <td>{{$psycs->noa_no}}</td>
                         <td>{{$psycs->take}}</td>
                         <td>{{$psycs->contact_person}}</td>
                         <td>{{$psycs->contact_details}}</td>
                         <td>{{\Carbon\Carbon::parse($psycs->created_at)->format('M-d-Y')}}</td>
                         @foreach ($sale2 as $s2item)
                         @if($psycs->id == $s2item->student_id && $s2item->program=="Psychometrician" && $s2item->year == $psycs->year && $psycs->season == "Season 2")
                         <div style="display:none">
                             {{$tot_amount_paid = $tot_amount_paid + $s2item->amount_paid}}
                             {{$total_balance = ($s2item->facilitation_fee+$s2item->tuition_fee) - $tot_amount_paid}}
                         </div>
                         @endif
                     @endforeach
                     <td>{{$s2item->tuition_fee}}</td>
                     <td>{{$tot_amount_paid}}</td>
                     <td>{{$total_balance}}</td>
                     <td>{{$s2item->facilitation_fee}}</td>
                     <td>{{$s2item->discount}}</td>
                     </tr>
                     @endif
                    @endforeach
                         @endif
                     @if(isset($nclex))
                         @foreach($nclex as $nclexs)
                         @if($nclexs->season == "Season 2")
                     <tr>
                         <td>{{$nclexs->last_name}}, {{$nclexs->first_name}} {{$nclexs->middle_name}}</td>
                         <td>{{$nclexs->cbrc_id}}</td>
                         <td>{{$nclexs->course}}</td>
                         <td>{{$nclexs->section}}</td>
                         <td>{{$nclexs->category}}</td>
                         <td>{{$nclexs->contact_no}}</td>
                         <td>{{$nclexs->email}}</td>
                         <td>{{$nclexs->address}}</td>
                         <td>{{$nclexs->facilitation}}</td> 
                         <td>{{$nclexs->birthdate}}</td>
                         <td>{{$nclexs->school}}</td>
                         <td>{{$nclexs->major}}</td>
                         <td>{{$nclexs->noa_no}}</td>
                         <td>{{$nclexs->take}}</td>
                         <td>{{$nclexs->contact_person}}</td>
                         <td>{{$nclexs->contact_details}}</td>
                         <td>{{\Carbon\Carbon::parse($nclexs->created_at)->format('M-d-Y')}}</td>
                         @foreach ($sale2 as $s2item)
                         @if($nclexs->id == $s2item->student_id && $s2item->program=="NCLEX" && $s2item->year == $nclexs->year && $nclexs->season == "Season 2")
                         <div style="display:none">
                             {{$tot_amount_paid = $tot_amount_paid + $s2item->amount_paid}}
                             {{$total_balance = ($s2item->facilitation_fee+$s2item->tuition_fee) - $tot_amount_paid}}
                         </div>
                         @endif
                     @endforeach
                     <td>{{$s2item->tuition_fee}}</td>
                     <td>{{$tot_amount_paid}}</td>
                     <td>{{$total_balance}}</td>
                     <td>{{$s2item->facilitation_fee}}</td>
                     <td>{{$s2item->discount}}</td>
                     </tr>
                     @endif
                    @endforeach
                         @endif
                     @if(isset($ielt))
                         @foreach($ielt as $ielts)
                         @if($ielts->season == "Season 2")
                     <tr>
                         <td>{{$ielts->last_name}}, {{$ielts->first_name}} {{$ielts->middle_name}}</td>
                         <td>{{$ielts->cbrc_id}}</td>
                         <td>{{$ielts->course}}</td>
                         <td>{{$ielts->section}}</td>
                         <td>{{$ielts->category}}</td>
                         <td>{{$ielts->contact_no}}</td>
                         <td>{{$ielts->email}}</td>
                         <td>{{$ielts->address}}</td>
                         <td>{{$ielts->facilitation}}</td> 
                         <td>{{$ielts->birthdate}}</td>
                         <td>{{$ielts->school}}</td>
                         <td>{{$ielts->major}}</td>
                         <td>{{$ielts->noa_no}}</td>
                         <td>{{$ielts->take}}</td>
                         <td>{{$ielts->contact_person}}</td>
                         <td>{{$ielts->contact_details}}</td>
                         <td>{{\Carbon\Carbon::parse($ielts->created_at)->format('M-d-Y')}}</td>
                         @foreach ($sale2 as $s2item)
                         @if($ielts->id == $s2item->student_id && $s2item->program=="IELTS" && $s2item->year == $ielts->year && $ielts->season == "Season 2")
                         <div style="display:none">
                             {{$tot_amount_paid = $tot_amount_paid + $s2item->amount_paid}}
                             {{$total_balance = ($s2item->facilitation_fee+$s2item->tuition_fee) - $tot_amount_paid}}
                         </div>
                         @endif
                     @endforeach
                     <td>{{$s2item->tuition_fee}}</td>
                     <td>{{$tot_amount_paid}}</td>
                     <td>{{$total_balance}}</td>
                     <td>{{$s2item->facilitation_fee}}</td>
                     <td>{{$s2item->discount}}</td>
                     </tr>
                     @endif
                    @endforeach
                         @endif
                     @if(isset($social))
                         @foreach($social as $socials)
                         @if($socials->season == "Season 2")
                     <tr>
                         <td>{{$socials->last_name}}, {{$socials->first_name}} {{$socials->middle_name}}</td>
                         <td>{{$socials->cbrc_id}}</td>
                         <td>{{$socials->course}}</td>
                         <td>{{$socials->section}}</td>
                         <td>{{$socials->category}}</td>
                         <td>{{$socials->contact_no}}</td>
                         <td>{{$socials->email}}</td>
                         <td>{{$socials->address}}</td>
                         <td>{{$socials->facilitation}}</td> 
                         <td>{{$socials->birthdate}}</td>
                         <td>{{$socials->school}}</td>
                         <td>{{$socials->major}}</td>
                         <td>{{$socials->noa_no}}</td>
                         <td>{{$socials->take}}</td>
                         <td>{{$socials->contact_person}}</td>
                         <td>{{$socials->contact_details}}</td>
                         <td>{{\Carbon\Carbon::parse($socials->created_at)->format('M-d-Y')}}</td>
                         @foreach ($sale2 as $s2item)
                         @if($socials->id == $s2item->student_id && $s2item->program=="Social Work" && $s2item->year == $socials->year && $socials->season == "Season 2")
                         <div style="display:none">
                             {{$tot_amount_paid = $tot_amount_paid + $s2item->amount_paid}}
                             {{$total_balance = ($s2item->facilitation_fee+$s2item->tuition_fee) - $tot_amount_paid}}
                         </div>
                         @endif
                     @endforeach
                     <td>{{$s2item->tuition_fee}}</td>
                     <td>{{$tot_amount_paid}}</td>
                     <td>{{$total_balance}}</td>
                     <td>{{$s2item->facilitation_fee}}</td>
                     <td>{{$s2item->discount}}</td>
                     </tr>
                     @endif
                    @endforeach
                         @endif
                     @if(isset($agri))
                         @foreach($agri as $agris)
                         @if($agris->season == "Season 2")
                     <tr>
                         <td>{{$agris->last_name}}, {{$agris->first_name}} {{$agris->middle_name}}</td>
                         <td>{{$agris->cbrc_id}}</td>
                         <td>{{$agris->course}}</td>
                         <td>{{$agris->section}}</td>
                         <td>{{$agris->category}}</td>
                         <td>{{$agris->contact_no}}</td>
                         <td>{{$agris->email}}</td>
                         <td>{{$agris->address}}</td>
                         <td>{{$agris->facilitation}}</td> 
                         <td>{{$agris->birthdate}}</td>
                         <td>{{$agris->school}}</td>
                         <td>{{$agris->major}}</td>
                         <td>{{$agris->noa_no}}</td>
                         <td>{{$agris->take}}</td>
                         <td>{{$agris->contact_person}}</td>
                         <td>{{$agris->contact_details}}</td>
                         <td>{{\Carbon\Carbon::parse($agris->created_at)->format('M-d-Y')}}</td>
                         @foreach ($sale2 as $s2item)
                         @if($agris->id == $s2item->student_id && $s2item->program=="Agriculture" && $s2item->year == $agris->year && $agris->season == "Season 2")
                         <div style="display:none">
                             {{$tot_amount_paid = $tot_amount_paid + $s2item->amount_paid}}
                             {{$total_balance = ($s2item->facilitation_fee+$s2item->tuition_fee) - $tot_amount_paid}}
                         </div>
                         @endif
                     @endforeach
                     <td>{{$s2item->tuition_fee}}</td>
                     <td>{{$tot_amount_paid}}</td>
                     <td>{{$total_balance}}</td>
                     <td>{{$s2item->facilitation_fee}}</td>
                     <td>{{$s2item->discount}}</td>
                     </tr>
                     @endif
                    @endforeach
                         @endif

                         @if(isset($mid))
                         @foreach($mid as $mids)
                         @if($mids->season == "Season 2")
                     <tr>
                         <td>{{$mids->last_name}}, {{$mids->first_name}} {{$mids->middle_name}}</td>
                         <td>{{$mids->cbrc_id}}</td>
                         <td>{{$mids->course}}</td>
                         <td>{{$mids->section}}</td>
                         <td>{{$mids->category}}</td>
                         <td>{{$mids->contact_no}}</td>
                         <td>{{$mids->email}}</td>
                         <td>{{$mids->address}}</td>
                         <td>{{$mids->facilitation}}</td> 
                         <td>{{$mids->birthdate}}</td>
                         <td>{{$mids->school}}</td>
                         <td>{{$mids->major}}</td>
                         <td>{{$mids->noa_no}}</td>
                         <td>{{$mids->take}}</td>
                         <td>{{$mids->contact_person}}</td>
                         <td>{{$mids->contact_details}}</td>
                         <td>{{\Carbon\Carbon::parse($mids->created_at)->format('M-d-Y')}}</td>
                         @foreach ($sale2 as $s2item)
                         @if($mids->id == $s2item->student_id && $s2item->program=="Midwifery" && $s2item->year == $mids->year && $mids->season == "Season 2")
                         <div style="display:none">
                             {{$tot_amount_paid = $tot_amount_paid + $s2item->amount_paid}}
                             {{$total_balance = ($s2item->facilitation_fee+$s2item->tuition_fee) - $tot_amount_paid}}
                         </div>
                         @endif
                     @endforeach
                     <td>{{$s2item->tuition_fee}}</td>
                     <td>{{$tot_amount_paid}}</td>
                     <td>{{$total_balance}}</td>
                     <td>{{$s2item->facilitation_fee}}</td>
                     <td>{{$s2item->discount}}</td>
                     </tr>
                     @endif
                    @endforeach
                         @endif

                         @if(isset($online))
                         @foreach($online as $onlines)
                         @if($onlines->season == "Season 2")
                     <tr>
                         <td>{{$onlines->last_name}}, {{$onlines->first_name}} {{$onlines->middle_name}}</td>
                         <td>{{$onlines->cbrc_id}}</td>
                         <td>{{$onlines->course}}</td>
                         <td>{{$onlines->section}}</td>
                         <td>{{$onlines->category}}</td>
                         <td>{{$onlines->contact_no}}</td>
                         <td>{{$onlines->email}}</td>
                         <td>{{$onlines->address}}</td>
                         <td>{{$onlines->facilitation}}</td> 
                         <td>{{$onlines->birthdate}}</td>
                         <td>{{$onlines->school}}</td>
                         <td>{{$onlines->major}}</td>
                         <td>{{$onlines->noa_no}}</td>
                         <td>{{$onlines->take}}</td>
                         <td>{{$onlines->contact_person}}</td>
                         <td>{{$onlines->contact_details}}</td>
                         <td>{{\Carbon\Carbon::parse($onlines->created_at)->format('M-d-Y')}}</td>
                         @foreach ($sale2 as $s2item)
                         @if($onlines->id == $s2item->student_id && $s2item->program=="Online Only" && $s2item->year == $onlines->year && $onlines->season == "Season 2")
                         <div style="display:none">
                             {{$tot_amount_paid = $tot_amount_paid + $s2item->amount_paid}}
                             {{$total_balance = ($s2item->facilitation_fee+$s2item->tuition_fee) - $tot_amount_paid}}
                         </div>
                         @endif
                     @endforeach
                     <td>{{$s2item->tuition_fee}}</td>
                     <td>{{$tot_amount_paid}}</td>
                     <td>{{$total_balance}}</td>
                     <td>{{$s2item->facilitation_fee}}</td>
                     <td>{{$s2item->discount}}</td>
                     </tr>
                     @endif
                    @endforeach
                         @endif

                         {{-- end of season 2 --}}
                    </tbody>                  
                    
                </table>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
  $('.record').addClass('active');
  $('.record').addClass('collapse in');
});

</script>
@stop