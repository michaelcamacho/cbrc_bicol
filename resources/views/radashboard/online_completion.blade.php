@extends('main')
@section('title')
 Online Completion
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@stop
@section('main-content')
<main class="main-wrapper clearfix">
    <div class="page-content-wrapper">
     <div class="container" style="margin-top: 10px;">
            <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">
                                <div class="cont">
                                    <div class="form-group">
                                            <p style="margin-bottom: 0px;"><b>Branch :</b> Novaliches <span class="float-right">
                                             <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                                            Filter Season and Year
                                          </button>
                                          </span></p>
                                            <p style="margin-bottom: 0px;"><b>CBRC</b> Online Completion Reports</p>               
                                            <p id="season"></p>
                                        </div>
                                         <table class="table table-hover table-striped" id="myTable">
                                              <thead>
                                                <tr>
                                                  <th scope="col" class="center">CBRC_ID</th>
                                                  <th scope="col" class="center">STUDENT</th>
                                                  <th scope="col" class="center">MAJOR</th>
                                                  <th scope="col" class="center">GEN_ED</th>
                                                  <th scope="col" class="center">PRO_ED</th>
                                                  <th scope="col" class="center">STATUS</th>
                                                </tr> 
                                              </thead>
                                              <tbody id="studentCompl">
                                            
                                              </tbody>
                                          </table>   

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      <div class="modal-body">
                         <form>
                            <div class="container">
                               <div class="row">      
                                     <div class="col-md-12">
                                      <p style="margin-top: 10px; font-weight: bold;font-size: 18px;margin-bottom: 0px;color: blue;">*Please Select Year and Season</p>  
                                        <select id="cs" name ="cs"  class="form-control studentSeason" required>
                                           <option value="0" selected="true" disabled = "true"> Select Season</option>           
                                           <option value="Season 1">Season 1</option>  
                                           <option value="Season 2">Season 2</option>
                                        </select>  
                                      </div> 
                                      <div class="col-md-12" style="margin-top: 10px;">
                                        <select id="cs" name ="cs" class="form-control studentYear" required>
                                           <option value="0" selected="true" disabled = "true"> Select Year</option>           
                                            @foreach($years as $years)
                                              <option>
                                                {{$years}}    
                                              </option>  
                                            @endforeach
                                        </select>  
                                      </div>
                                       <div class="col-md-12" style="margin-top: 10px;">
                                        <select id="cs" name ="cs" class="form-control studentMajor" required>
                                           <option value="0" selected="true" disabled = "true"> Select Major</option>           
                                            <option value="BEED"> BEED </option>
                                            <option value="BEED"> BSED </option>  
                                        </select>  
                                      </div>
                               </div>
                            </div>
                         </form>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btn-filter">Submit</button>
                      </div>
                    </div>
                  </div>
                </div>


</main>
        

@stop
@section('js')
<script  src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">


    $('#btn-filter').on('click',function(){
     var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"}
     $.ajax({
    url: 'https://beta.cbrc.solutions/api/auth/login',
    method: 'POST',
    data: auth
    }).done(function(response){
     var apiToString = JSON.stringify(response);
     var apiTokenParse = JSON.parse(apiToString);
     var apiToken = apiTokenParse.access_token;
 
    var season = $('.studentSeason').val();
    var year = $('.studentYear').val();
    var major = $('.studentMajor').val();


       $.get('https://cbrc.solutions/api/main/online-completion/'+season+'/'+year+'/'+major+'?token='+apiToken,function(data){  
       $('#stdlist').empty();
         $.each(data, function (index, studentCompletion) {
                $('#myTable').dataTable();
          });
       });
   
   });
});
</script>

@stop