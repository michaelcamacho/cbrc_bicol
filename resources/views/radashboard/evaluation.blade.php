@extends('main')
@section('title')
Lecturer Evaluation
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
.btn-group {
    top: -14px;
}
div.dataTables_wrapper div.dataTables_info {
    display: none;
}
#table-header{
    float: right;
}
#lec3_filter{
    display: none;
}
#titleLecturer{
    font-size: 15px;
    font-weight: bold;
}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">{{Auth::user()->branch}}</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Lecturer Evaluation</li>
                            <li class="breadcrumb-item active">Lecturer Evaluation Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->

            <div class="container">
                  <div class="row">
                    <div class="col-md-6">
                       <div class="card shadow-sm p-3  mx-sm-1 p-3" style="margin-top: 10px; margin-bottom: 10px;">
                            <div class="col-md-12"> 
                              <p style="font-size: 18px;font-weight: bold;margin-bottom: 5px;">EVALUATION REPORT</p>
                            <hr>
                            <div class="form-group">
                                <label>Branch</label>
                                <input class="form-control" type="text" 
                                value="{{Auth::user()->branch}}" name="branch" id="branch" readonly>    
                            </div>
                            <div class="form-group">
                                <label>Lecturer</label>
                                <select class="form-control"
                                name="program" id="program" required>
                                <option value="">--- Select Lecturer ---</option>
                                    <option value="">
                                        
                                    </option>
                                </select>                 
                            </div>
                            <div class="form-group">
                                <label>Subject</label>
                                <select class="form-control" 
                                 name="class" id="class" readonly>
                                 <option value="">--- Select Subject ---</option>
                              </select>
                            </div>
                            <div class="form-group">
                                <label>Season</label>
                                <select class="form-control" 
                                 name="subject" id="subject" readonly>
                                 <option value="">--- Select Season ---</option>
                              </select>
                                
                            </div>
                             <div class="form-group">
                                <label>Year</label>
                                <select class="form-control" 
                                 name="section" id="section" readonly>
                                 <option value="">--- Select Year ---</option>
                              </select>
                            </div>
                             <div class="form-group">
                                <a class="btn btn-success search scroll" style="width: 100%;" href="#form2">Search Lecturer</a>
                            </div>
                      </div>
                    </div>       
                  </div>

                   <div class="col-md-6">
                       <div class="card shadow-sm p-3  mx-sm-1 p-3" style="margin-top: 10px; margin-bottom: 10px;">
                            <div class="col-md-12"> 
                              <p style="font-size: 18px;font-weight: bold;margin-bottom: 5px;">LECTURER INFORMATION</p>
                            <hr>
                            <p id="titleLecturer"><i class="fa fa-tv"></i> BRANCH :</p>
                            <p id="titleLecturer"><i class="fa fa-user"></i> LECTURER :</p>
                            <p id="titleLecturer"><i class="fa fa-bookmark"></i> SUBJECT :</p>
                            <p id="titleLecturer"><i class="fa fa-calendar-plus"></i> SEASON :</p>
                            <p id="titleLecturer"><i class="fa fa-calendar-check"></i> YEAR :</p>
                           
                  </div>
                </div>
              </div> <!-- end of container -->






            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <p style="font-size: 18px;font-weight: bold;margin-bottom: 5px;">EVALUATION RESULT</p>
                                    <hr>
                                </div>
                                <div class="widget-body clearfix">

                                 
                            </div>
                          
                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
        
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                            <table id="lec3" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Lecturer</th>
                            <th>Program</th>
                            <th>Class</th>
                            <th>Subject</th>
                            <th>Section</th>
                            <th>Likes</th>
                            <th>Dislikes</th>
                            <th>Comment</th>
                            <th>Date Of Lecturer</th>
                            <th>Student Name</th>
                            
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($comment))
                            @foreach($comment as $comments)
                        <tr>
                            <td>{{$comments->lecturer}}</td>
                            <td>{{$comments->program}}</td>
                            <td>{{$comments->class}}</td>
                            <td>{{$comments->subject}}</td>
                            <td>{{$comments->section}}</td>
                            <td>{{$comments->like}}</td>
                            <td>{{$comments->dislike}}</td>
                            <td>{{$comments->comment}}</td>
                            <td>{{$comments->date}}</td>
                            <td>{{$comments->name}}</td>
                        </tr>
                       @endforeach
                            @endif

                            </tbody>                  
                    
                </table>

                @role('admin')
                <br/>
                <a class="btn btn-danger clear" id="clear">Clear Lecturer Tables</a>
                @endrole
                            </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
        </main>

@stop
@section('js')

<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/evaluate.js') }}"></script>
@include('js.evaluation')
<script src="{{ asset('/js/lecturer-evaluation.js') }}"></script>
@stop