@extends('main')
@section('title')
 Lecturer Monitoring
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
    <div class="page-content-wrapper">
     <div class="container" style="margin-top: 10px;">
            <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                           <div class="card-body">
                              <div class="cont">
                                        <div class="form-group">
                                            <p style="margin-bottom: 0px;"><b>Branch :</b> Novaliches <span class="float-right">
                                             <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                                            Filter Season and Year
                                          </button>
                                          </span></p>
                                            <p style="margin-bottom: 0px;"><b>CBRC</b> Lecturer Monitor Reports</p>               
                                            <p id="season"></p>
                                        </div>
                                          <div class="table-responsive">
                                            <table class="table table-hover" id="myTable">
                                              <thead>
                                                <tr>
                                                  <th scope="col" class="center">DATE LECTURER</th>
                                                  <th scope="col" class="center">LECTURER</th>
                                                  <th scope="col" class="center">SUBJECT</th>
                                                  <th scope="col" class="center">TIME START</th>
                                                  <th scope="col" class="center">START AM BREAK</th>
                                                  <th scope="col" class="center">END AM BREAK</th>
                                                  <th scope="col" class="center">START LUNCH</th>
                                                  <th scope="col" class="center">END LUNCH</th>
                                                  <th scope="col" class="center">START PM BREAK</th>
                                                  <th scope="col" class="text-center">END PM BREAK</th>
                                                  <th scope="col" class="text-center">END CLASS</th>
                                                </tr> 
                                              </thead>
                                              <tbody id="stdlist">
                                          
                                              </tbody>
                                          </table>                  
                                        </div>
                                        </div>
                                    </div>        
                                </div>  
                           </div>
                        </div>
                    </div>
                </div>  
            </main>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      <div class="modal-body">
         <form>
            <div class="container">
               <div class="row">      
                     <div class="col-md-12">
                      <p style="margin-top: 10px; font-weight: bold;font-size: 18px;margin-bottom: 0px;color: blue;">*Please Select Year and Season</p>  
                        <select id="cs" name ="cs"  class="form-control seasonSearch" required>
                           <option value="0" selected="true" disabled = "true"> Select Season</option>           
                           <option value="Season 1">Season 1</option>  
                           <option value="Season 2">Season 2</option>
                        </select>  
                      </div> 
                      <div class="col-md-12" style="margin-top: 10px;">
                        <select id="cs" name ="cs" class="form-control yearSearch" required>
                           <option value="0" selected="true" disabled = "true"> Select Year</option>           
                            @foreach($years as $years)
                              <option>
                                 {{$years}}
                              </option>  
                           @endforeach
                        </select>  
                      </div>
               </div>
            </div>
         </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn-filter">Submit</button>
      </div>
    </div>
  </div>
</div>
        

@stop
@section('js')
<script  src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">

   $('#btn-filter').on('click',function(){

    var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"}
   $.ajax({
   url: 'https://beta.cbrc.solutions/api/auth/login',
    method: 'POST',
    data: auth
  }).done(function(response){
     var apiToString = JSON.stringify(response);
     var apiTokenParse = JSON.parse(apiToString);
     var apiToken = apiTokenParse.access_token;
     console.log(apiToken);  
     var season = $('.seasonSearch').val();
     var year = $('.yearSearch').val();

 

       $.get('https://cbrc.solutions/api/main/lecturer-report/'+season+'/'+year+'?token='+apiToken,function(data){  
       $('#stdlist').empty();
         $.each(data, function (index, lecturerReports) {
             $('#season').empty();
              $('#stdlist').append('<tr><td class="text-center">'+lecturerReports.Lecture_Date+'</td><td class="text-center">'+lecturerReports.Lastname+ ' '+lecturerReports.Firstname+'</td><td class="text-center">'+lecturerReports.Subject+'</td><td class="text-center">'+lecturerReports.start_time+'</td><td class="text-center">'+lecturerReports.Am_BreakOut+'</td><td class="text-center">'+lecturerReports.Am_BreakIn+'</td><td class="text-center">'+lecturerReports.LunchOut+'</td><td class="text-center">'+lecturerReports.LunchIn+'</td><td class="text-center">'+lecturerReports.Pm_BreakOut+'</td><td class="text-center">'+lecturerReports.Pm_BreakIn+'</td><td class="text-center">'+lecturerReports.TimeIn+'</td></tr>');
                $('#season').append('<b>Season :</b> <span>'+season+'</span>');

                $('#myTable').dataTable();
          });
       });
   });
});




 $('#btn-filter').on('click',function(){
  $('#exampleModalCenter').modal('hide');
 })
</script>
@stop