@extends('main')
@section('title')
 Exam Monitoring
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
    <div class="page-content-wrapper">
     <div class="container" style="margin-top: 10px;">
            <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">
                                <div class="cont">
                                       <form class="form-control" action="/exam-result" method="POST">
                                    @csrf
                                    <table class="table table-striped table-bordered"  id="myTable" cellspacing="0">
                                        <div class="form-group">
                                            <label for="exam">Exam Category</label>
                                                <select id="exam" name ="examcat" class="form-control" required>
                                                    <option value="0" selected="true" disabled = "true"> Select Exam Category</option>
                                                                <option value=""></option>                                            
                                                    </select>                
                                                </div> 
                                                <input type="hidden" value="{{$season}}" name="" id="season"/>   
                                                <input type="hidden" value="{{$year}}" name="" id="year" />
                                                <input type="hidden" value="{{$program}}" name="" id="program"/>   
                                                <input type="hidden" value="{{$major}}" name="" id="major" />            
                                                <thead>
                                                <tr>
                                                    <th class="text-center">CBRC ID</th>
                                                    <th class="text-center">Name</th>
                                                    <Th class="text-center">School</Th> 
                                                    <th class="text-center"># of Take</th>
                                                    <th class="text-center">RAW SCORE</th>
                                                    <th class="text-center">Percent</th>
                                                    <th class="text-center">Quartile</th>
                                                </tr>       
                                            </thead> 
                                              <tbody id="stdlist"> 
                                                                                    
                                                      <tr>      
                                                        <input type="hidden" name="std[]" value="" />
                                                            <td class="text-center"></td>
                                                            <td class="text-center"><b></b></td>
                                                            <td class="text-center"><b></b></td>
                                                            <td class="text-center"><b></b></td>
                                                            <td class="text-center"><center><input class = "form-control"  type="text" class="input-sm"  id="" name="prct[]" readonly/></center></td>
                                                            <td class="text-center"><center><input class = "form-control"  type="text" class="input-sm"  id="" name="prct[]" readonly/></center></td>
                                                            <td class="text-center"><center><input class = "form-control" type="text" class="input-sm"  id="" name="quartile[]"/></center></td>
                                                        </tr>
                                            
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>           

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
</main>
        

@stop
@section('js')
<script  src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">

    $(window).on('load',function(){
       var season = $('#season').val();
       var year = $('#year').val();
       var program = $('#program').val();
       var major = $('#major').val();


       $.get('https://cbrc.solutions/api/main/exam_monitoring/'+season+'/'+year+'/'+program+'/'+major+'?token='+apiToken,function(data){  
       $('#stdlist').empty();
         $.each(data, function (index, monitoringExam) {
                $('#myTable').dataTable();
          });
       });

    });

$(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
@stop