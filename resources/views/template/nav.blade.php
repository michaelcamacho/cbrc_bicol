  <aside class="site-sidebar scrollbar-enabled" data-suppress-scroll-x="true">
            <!-- User Details -->
            <div class="side-user">
                <figure class="side-user-bg" style="background-image: url(assets/demo/user-image-cropped.jpg)">
                    <img src="../assets/demo/user-image-cropped.jpg" alt="" class="d-none">
                </figure>
                <div class="col-sm-12 text-center p-0 clearfix">
                    <div class="d-inline-block pos-relative mr-b-10">
                        <span class="avatar-text">{{substr(Auth::user()->name,0,1)}}</span>
                        <figure class="avatar-img thumb-sm mr-b-0 d-none">
                            <img src="../assets/demo/users/user1.jpg" class="rounded-circle" alt="">
                        </figure>
                    </div>
                    <!-- /.d-inline-block -->
                    <div class="lh-14 mr-t-5 sidebar-collapse-hidden">
                        <h6 class="hide-menu side-user-heading">{{ Auth::user()->name }}</h6><small class="hide-menu">{{ Auth::user()->email}}</small>
                    </div>
                </div>
                <!-- /.col-sm-12 -->
            </div>
            <!-- /.side-user -->
            <!-- Sidebar Menu -->
            <nav class="sidebar-nav">
                <ul class="nav in side-menu">
                    <li class="current-page admin-dashboard"><a href="{{ url('/dashboard')}}"><i class="list-icon material-icons">home</i> <span class="hide-menu">Dashboard</span></a>
                    </li>
                    <li class="menu-item-has-children branches "><a href="javascript:void(0);"><i class="list-icon material-icons">apps</i> <span class="hide-menu">Branches</span></a>
                        <ul class="list-unstyled sub-menu branches">

                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu">Daet</span></a>
                                <ul class="list-unstyled sub-menu">
                                    <li class="current-page dashboard"><a href="{{ url('/daet/dashboard')}}">Dashboard</a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/daet/add-enrollee')}}">Add Enrollee</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/daet/new-payment')}}">New Payment</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/daet/new-reservation')}}">New Reservation</a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/daet/add-expense')}}">Add Expense</a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/daet/add-budget')}}">Add Budget</a></li>

                                    <li class="current-page book-transfer"><a href="{{ url('/daet/book-transfer')}}">Book Transfer</a></li>

                                    <li class="current-page book-payment"><a href="{{ url('/daet/book-payment')}}">Book Payment</a></li>

                                    <li class="current-page"><a href="{{ url('/daet/new-remit')}}">Cash Remit</a></li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/daet/let')}}">LET</a></li>
                                            <li><a href="{{ url('/daet/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/daet/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/daet/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/daet/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/daet/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/daet/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/daet/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/daet/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/daet/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/daet/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/daet/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/daet/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/daet/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/daet/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/daet/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/daet/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/daet/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/daet/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/daet/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/daet/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/daet/books')}}">Books</a></li>
                                             <li><a href="{{ url('/daet/bookTransferRecord')}}">Book Transfer</a></li>
                                            <li><a href="{{ url('/daet/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/daet/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                    {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Lecturers</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/daet/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/daet/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/daet/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                    </li> --}}
                                </ul>
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu">Naga</span></a>
                                <ul class="list-unstyled sub-menu">
                                    <li class="current-page dashboard"><a href="{{ url('/naga/dashboard')}}">Dashboard</a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/naga/add-enrollee')}}">Add Enrollee</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/naga/new-payment')}}">New Payment</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/naga/new-reservation')}}">New Reservation</a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/naga/add-expense')}}">Add Expense</a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/naga/add-budget')}}">Add Budget</a></li>

                                    <li class="current-page book-transfer"><a href="{{ url('/naga/book-transfer')}}">Book Transfer</a></li>

                                    <li class="current-page book-payment"><a href="{{ url('/naga/book-payment')}}">Book Payment</a></li>

                                    <li class="current-page"><a href="{{ url('/naga/new-remit')}}">Cash Remit</a></li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/naga/let')}}">LET</a></li>
                                            <li><a href="{{ url('/naga/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/naga/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/naga/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/naga/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/naga/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/naga/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/naga/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/naga/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/naga/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/naga/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/naga/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/naga/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/naga/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/naga/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/naga/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/naga/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/naga/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/naga/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/naga/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/naga/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/naga/books')}}">Books</a></li>
                                             <li><a href="{{ url('/naga/bookTransferRecord')}}">Book Transfer</a></li>
                                            <li><a href="{{ url('/naga/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/naga/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                    {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Lecturers</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/naga/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/naga/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/naga/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                    </li> --}}
                                </ul>
                            
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu">Masbate</span></a>
                                <ul class="list-unstyled sub-menu">
                                    <li class="current-page dashboard"><a href="{{ url('/masbate/dashboard')}}">Dashboard</a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/masbate/add-enrollee')}}">Add Enrollee</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/masbate/new-payment')}}">New Payment</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/masbate/new-reservation')}}">New Reservation</a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/masbate/add-expense')}}">Add Expense</a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/masbate/add-budget')}}">Add Budget</a></li>

                                    <li class="current-page book-transfer"><a href="{{ url('/masbate/book-transfer')}}">Book Transfer</a></li>

                                    <li class="current-page book-payment"><a href="{{ url('/masbate/book-payment')}}">Book Payment</a></li>

                                    <li class="current-page"><a href="{{ url('/masbate/new-remit')}}">Cash Remit</a></li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/masbate/let')}}">LET</a></li>
                                            <li><a href="{{ url('/masbate/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/masbate/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/masbate/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/masbate/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/masbate/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/masbate/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/masbate/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/masbate/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/masbate/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/masbate/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/masbate/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/masbate/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/masbate/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/masbate/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/masbate/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/masbate/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/masbate/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/masbate/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/masbate/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/masbate/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/masbate/books')}}">Books</a></li>
                                             <li><a href="{{ url('/masbate/bookTransferRecord')}}">Book Transfer</a></li>
                                            <li><a href="{{ url('/masbate/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/masbate/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                    {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Lecturers</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/masbate/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/masbate/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/masbate/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                    </li> --}}
                                </ul>
                            
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu">Sorsogon</span></a>
                                <ul class="list-unstyled sub-menu">
                                    <li class="current-page dashboard"><a href="{{ url('/sorsogon/dashboard')}}">Dashboard</a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/sorsogon/add-enrollee')}}">Add Enrollee</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/sorsogon/new-payment')}}">New Payment</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/sorsogon/new-reservation')}}">New Reservation</a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/sorsogon/add-expense')}}">Add Expense</a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/sorsogon/add-budget')}}">Add Budget</a></li>

                                    <li class="current-page book-transfer"><a href="{{ url('/sorsogon/book-transfer')}}">Book Transfer</a></li> 

                                    <li class="current-page book-payment"><a href="{{ url('/sorsogon/book-payment')}}">Book Payment</a></li>

                                    <li class="current-page"><a href="{{ url('/sorsogon/new-remit')}}">Cash Remit</a></li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/sorsogon/let')}}">LET</a></li>
                                            <li><a href="{{ url('/sorsogon/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/sorsogon/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/sorsogon/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/sorsogon/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/sorsogon/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/sorsogon/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/sorsogon/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/sorsogon/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/sorsogon/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/sorsogon/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/sorsogon/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/sorsogon/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/sorsogon/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/sorsogon/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/sorsogon/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/sorsogon/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/sorsogon/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/sorsogon/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/sorsogon/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/sorsogon/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/sorsogon/books')}}">Books</a></li>
                                            <li><a href="{{ url('/sorsogon/bookTransferRecord')}}">Book Transfer</a></li>
                                            <li><a href="{{ url('/sorsogon/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/sorsogon/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                    {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Lecturers</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/sorsogon/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/sorsogon/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/sorsogon/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                    </li> --}}
                                </ul>
                            
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu">Legaspi</span></a>
                                <ul class="list-unstyled sub-menu">
                                    <li class="current-page dashboard"><a href="{{ url('/legaspi/dashboard')}}">Dashboard</a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/legaspi/add-enrollee')}}">Add Enrollee</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/legaspi/new-payment')}}">New Payment</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/legaspi/new-reservation')}}">New Reservation</a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/legaspi/add-expense')}}">Add Expense</a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/legaspi/add-budget')}}">Add Budget</a></li>

                                    <li class="current-page book-transfer"><a href="{{ url('/legaspi/book-transfer')}}">Book Transfer</a></li> 

                                    <li class="current-page book-payment"><a href="{{ url('/legaspi/book-payment')}}">Book Payment</a></li>

                                    <li class="current-page"><a href="{{ url('/legaspi/new-remit')}}">Cash Remit</a></li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/legaspi/let')}}">LET</a></li>
                                            <li><a href="{{ url('/legaspi/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/legaspi/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/legaspi/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/legaspi/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/legaspi/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/legaspi/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/legaspi/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/legaspi/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/legaspi/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/legaspi/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/legaspi/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/legaspi/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/legaspi/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/legaspi/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/legaspi/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/legaspi/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/legaspi/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/legaspi/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/legaspi/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/legaspi/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/legaspi/books')}}">Books</a></li>
                                            <li><a href="{{ url('/legaspi/bookTransferRecord')}}">Book Transfer</a></li>
                                            <li><a href="{{ url('/legaspi/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/legaspi/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                    {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Lecturers</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/legaspi/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/legaspi/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/legaspi/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                    </li> --}}
                                </ul>
                            
                            </li>




                            {{-- end --}}
                        </ul>
                    </li>

                    <li class="menu-item-has-children reports"><a href="javascript:void(0);">
                        <i class="list-icon material-icons">apps</i> <span class="hide-menu">Reports</span></a>  
                        <ul class="list-unstyled sub-menu reports">
                        
                        
                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu sub-reports">Daet</span></a>
                                <ul class="list-unstyled sub-menu sub-reports">
    
                               <li><a href="{{ url('/daet/today')}}">Today Transaction</a>
                                </li>
                                
                                <li><a href="{{ url('/daet/yesterday')}}">Yesterday Transaction</a>
                                </li>
                                 <li class="item-has-children score-card1"><a href="javascript:void(0);">
                                        <span class="hide-menu">Score Card</span></a>
                                        <ul class="list-unstyled sub-menu score-card1">
                                            <li>
                                                <a href="{{ url('/daet/scorecard-season/1')}}">Season 1</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/daet/scorecard-season/2')}}">Season 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/daet/financial-report')}}">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>



                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu sub-reports">Naga</span></a>
                                <ul class="list-unstyled sub-menu sub-reports">
    
                               <li><a href="{{ url('/naga/today')}}">Today Transaction</a>
                                </li>
                                
                                <li><a href="{{ url('/naga/yesterday')}}">Yesterday Transaction</a>
                                </li>
                                 <li class="item-has-children score-card1"><a href="javascript:void(0);">
                                        <span class="hide-menu">Score Card</span></a>
                                        <ul class="list-unstyled sub-menu score-card1">
                                            <li>
                                                <a href="{{ url('/naga/scorecard-season/1')}}">Season 1</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/naga/scorecard-season/2')}}">Season 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/naga/financial-report')}}">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu sub-reports">Masbate</span></a>
                                <ul class="list-unstyled sub-menu sub-reports">
    
                               <li><a href="{{ url('/masbate/today')}}">Today Transaction</a>
                                </li>
                                
                                <li><a href="{{ url('/masbate/yesterday')}}">Yesterday Transaction</a>
                                </li>
                                 <li class="item-has-children score-card1"><a href="javascript:void(0);">
                                        <span class="hide-menu">Score Card</span></a>
                                        <ul class="list-unstyled sub-menu score-card1">
                                            <li>
                                                <a href="{{ url('/masbate/scorecard-season/1')}}">Season 1</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/masbate/scorecard-season/2')}}">Season 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/masbate/financial-report')}}">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu sub-reports">Sorsogon</span></a>
                                <ul class="list-unstyled sub-menu sub-reports">
    
                               <li><a href="{{ url('/sorsogon/today')}}">Today Transaction</a>
                                </li>
                                
                                <li><a href="{{ url('/sorsogon/yesterday')}}">Yesterday Transaction</a>
                                </li>
                                 <li class="item-has-children score-card1"><a href="javascript:void(0);">
                                        <span class="hide-menu">Score Card</span></a>
                                        <ul class="list-unstyled sub-menu score-card1">
                                            <li>
                                                <a href="{{ url('/sorsogon/scorecard-season/1')}}">Season 1</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/sorsogon/scorecard-season/2')}}">Season 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/sorsogon/financial-report')}}">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu sub-reports">Legaspi</span></a>
                                <ul class="list-unstyled sub-menu sub-reports">
    
                               <li><a href="{{ url('/legaspi/today')}}">Today Transaction</a>
                                </li>
                                
                                <li><a href="{{ url('/legaspi/yesterday')}}">Yesterday Transaction</a>
                                </li>
                                 <li class="item-has-children score-card1"><a href="javascript:void(0);">
                                        <span class="hide-menu">Score Card</span></a>
                                        <ul class="list-unstyled sub-menu score-card1">
                                            <li>
                                                <a href="{{ url('/legaspi/scorecard-season/1')}}">Season 1</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/legaspi/scorecard-season/2')}}">Season 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/legaspi/financial-report')}}">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>


                        {{-- end --}}
                        </ul>
                    </li>

            <li class="menu-item-has-children ad-record"><a href="javascript:void(0);"><i class="list-icon material-icons">apps</i> <span class="hide-menu">Records</span></a>                    
                <ul class="list-unstyled sub-menu ad-record">

                   <li><a href="{{ url('/total-program')}}">Programs</a>
                    </li>
                    <li class="menu-item-has-children ad-enrollee">
                            <a href="javascript:void(0);">
                                <span class="hide-menu">Enrollees</span></a>
                        <ul class="list-unstyled sub-menu ad-enrollee">
                                <li><a href="{{ url('/total-let')}}">LET</a></li>
                                <li><a href="{{ url('/total-nle')}}">NLE</a></li>
                                <li><a href="{{ url('/total-crim')}}">Criminology</a></li>
                                <li><a href="{{ url('/total-civil')}}">Civil Service</a></li>
                                <li><a href="{{ url('/total-psyc')}}">Psychometrician</a></li>
                                <li><a href="{{ url('/total-nclex')}}">NCLEX</a></li>
                                <li><a href="{{ url('/total-ielts')}}">IELTS</a></li>
                                <li><a href="{{ url('/total-social')}}">Social Work</a></li>
                                <li><a href="{{ url('/total-agri')}}">Agriculture</a></li>
                                <li><a href="{{ url('/total-mid')}}">Midwifery</a></li>
                                <li><a href="{{ url('/total-online')}}">Online Only</a></li>
                            </ul>
                    </li>
                    <li><a href="{{ url('/total-dropped')}}">Dropped</a>
                    </li>
                    <li><a href="{{ url('/total-scholars')}}">Scholars</a>
                    </li>
                    <li><a href="{{ url('/tuition-fees')}}">Tuition Fees</a>
                    </li>
                    <li><a href="{{ url('/discounts')}}">Discounts</a>
                    </li>
                    <li><a href="{{ url('/total-books')}}">Books</a>
                    </li>
                    <li><a href="{{ url('/total-expense')}}">Expenses</a>
                    </li>
                    <li><a href="{{ url('/users')}}">Users</a>
                    </li>

                </ul> 
            </li>
            
            
                <li>
                    <li class="menu-item-has-children hris"><a href="javascript:void(0);"><i class="list-icon material-icons">apps</i> <span class="hide-menu">HRIS</span></a>                    
                        <ul class="list-unstyled sub-menu hris">

                            <li class="menu-item-has-children daet"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Daet</span></a>
                                <ul class="list-unstyled sub-menu daet">
                                    <li><a href="{{ url('/daet/add-employee')}}">Add Employee</a></li>
                                    <li><a href="{{ url('/daet/employee-record')}}">Employee Records</a></li>
                                </ul>
                            </li>


                            <li class="menu-item-has-children naga"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Naga</span></a>
                                <ul class="list-unstyled sub-menu naga">
                                    <li><a href="{{ url('/naga/add-employee')}}">Add Employee</a></li>
                                    <li><a href="{{ url('/naga/employee-record')}}">Employee Records</a></li>
                                </ul>
                            </li>
                            
                            <li class="menu-item-has-children masbate"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Masbate</span></a>
                                <ul class="list-unstyled sub-menu masbate">
                                    <li><a href="{{ url('/masbate/add-employee')}}">Add Employee</a></li>
                                    <li><a href="{{ url('/masbate/employee-record')}}">Employee Records</a></li>
                                </ul>
                            </li>
                            

                            <li class="menu-item-has-children sorsogon"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Sorsogon</span></a>
                                <ul class="list-unstyled sub-menu sorsogon">
                                    <li><a href="{{ url('/sorsogon/add-employee')}}">Add Employee</a></li>
                                    <li><a href="{{ url('/sorsogon/employee-record')}}">Employee Records</a></li>
                                </ul>
                            </li>

                            <li class="menu-item-has-children legaspi"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Legaspi</span></a>
                                <ul class="list-unstyled sub-menu legaspi">
                                    <li><a href="{{ url('/legaspi/add-employee')}}">Add Employee</a></li>
                                    <li><a href="{{ url('/legaspi/employee-record')}}">Employee Records</a></li>
                                </ul>
                            </li>



                            {{-- end --}}
                        </ul>
                    </li> 
                </li>
            {{-- <li class="menu-item-has-children ad-RADashboard"><a href="javascript:void(0);"><i class="list-icon material-icons">apps</i> <span class="hide-menu">RA Files</span></a>                    
            <ul class="list-unstyled sub-menu evaluate">
                <li><a href="{{ url('/radashboard/evaluation')}}">Evaluation</a></li>
                <li><a href="{{ url('/radashboard/attendance')}}">Attendance</a></li>
                <li><a href="{{ url('/radashboard/result-analysis')}}">Result Analysis</a></li>
                <li><a href="{{ url('/radashboard/filter-exam')}}">Exam Monitoring</a></li>
                <li><a href="{{ url('/radashboard/onlineCompletion')}}">Online Completion</a></li>
                <li><a href="{{ url('/radashboard/viewLecturerReports')}}">Lecturer Monitoring</a></li>
            </ul>
        </li> --}}
                   
                    <li class="menu-item-has-children others"><a href="javascript:void(0);"><i class="list-icon material-icons">apps</i> <span class="hide-menu">Others</span></a>                    
                        <ul class="list-unstyled sub-menu others">

                        <li class="menu-item-has-children daet"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Daet</span></a>
                            <ul class="list-unstyled sub-menu daet">
                                <li><a href="{{ url('/daet/student-id')}}">Passer's Templates</a></li>
                                {{-- <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li> --}}
                            </ul>
                        </li>

                        <li class="menu-item-has-children naga"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Naga</span></a>
                            <ul class="list-unstyled sub-menu naga">
                                <li><a href="{{ url('/naga/student-id')}}">Passer's Templates</a></li>
                                {{-- <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li> --}}
                            </ul>
                        </li>

                        <li class="menu-item-has-children masbate"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Masbate</span></a>
                            <ul class="list-unstyled sub-menu masbate">
                                <li><a href="{{ url('/masbate/student-id')}}">Passer's Templates</a></li>
                                {{-- <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li> --}}
                            </ul>
                        </li>

                        <li class="menu-item-has-children sorsogon"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Sorsogon</span></a>
                            <ul class="list-unstyled sub-menu sorsogon">
                                <li><a href="{{ url('/sorsogon/student-id')}}">Passer's Templates</a></li>
                                {{-- <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li> --}}
                            </ul>
                        </li>

                        <li class="menu-item-has-children legaspi"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Legaspi</span></a>
                            <ul class="list-unstyled sub-menu legaspi">
                                <li><a href="{{ url('/legaspi/student-id')}}">Passer's Templates</a></li>
                                {{-- <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li> --}}
                            </ul>
                        </li>

                            {{-- end --}}
                        </ul>
                    </li>

                    <li class="menu-item-has-children settings"><a href="javascript:void(0);">
                        <i class="list-icon material-icons">settings</i> <span class="hide-menu">Settings</span></a>  
                        <ul class="list-unstyled sub-menu settings">
                                
                                <li><a href="{{ url('/daet/settings/admin')}}">Admin Dashboard</a></li>
                                    
                                
                                <li class="menu-item-has-children settings-daet"><a href="javascript:void(0);"><span class="hide-menu">Daet</span></a>
                                    <ul class="list-unstyled sub-menu settings-daet">
                        
                                        <li><a href="{{ url('/daet/settings/member')}}">Member Dashboard</a></li>     
                                        <li><a href="{{ url('expense-settings')}}">Expense Settings</a></li>
                            
                                    </ul>
                                </li>

                                <li class="menu-item-has-children settings-naga"><a href="javascript:void(0);"><span class="hide-menu">Naga</span></a>
                                    <ul class="list-unstyled sub-menu settings-naga">
                        
                                        <li><a href="{{ url('/naga/settings/member')}}">Member Dashboard</a></li>     
                                        <li><a href="{{ url('expense-settings')}}">Expense Settings</a></li>
                            
                                    </ul>
                                </li>

                                <li class="menu-item-has-children  settings-masbate"><a href="javascript:void(0);"><span class="hide-menu">Masbate</span></a>
                                    <ul class="list-unstyled sub-menu settings-masbate">
                        
                                        <li><a href="{{ url('/masbate/settings/member')}}">Member Dashboard</a></li>     
                                        <li><a href="{{ url('expense-settings')}}">Expense Settings</a></li>
                            
                                    </ul>
                                </li>

                                <li class="menu-item-has-children settings-sorsogon"><a href="javascript:void(0);"><span class="hide-menu">Sorsogon</span></a>
                                    <ul class="list-unstyled sub-menu settings-sorsogon">
                        
                                        <li><a href="{{ url('/sorsogon/settings/member')}}">Member Dashboard</a></li>     
                                        <li><a href="{{ url('expense-settings')}}">Expense Settings</a></li>
                            
                                    </ul>
                                </li>

                                <li class="menu-item-has-children settings-legaspi"><a href="javascript:void(0);"><span class="hide-menu">Legaspi</span></a>
                                    <ul class="list-unstyled sub-menu settings-legaspi">
                        
                                        <li><a href="{{ url('/legaspi/settings/member')}}">Member Dashboard</a></li>     
                                        <li><a href="{{ url('expense-settings')}}">Expense Settings</a></li>
                            
                                    </ul>
                                </li>
                        {{-- end --}}
                        </ul>
                    </li>


                      




                        
                </ul>
                <!-- /.side-menu -->
            </nav>
            <!-- /.sidebar-nav -->
        </aside>