  <aside class="site-sidebar scrollbar-enabled" data-suppress-scroll-x="true">
            <!-- User Details -->
            <div class="side-user">
                <figure class="side-user-bg" style="background-image: url(assets/demo/user-image-cropped.jpg)">
                    <img src="assets/demo/user-image-cropped.jpg" alt="" class="d-none">
                </figure>
                <div class="col-sm-12 text-center p-0 clearfix">
                    <div class="d-inline-block pos-relative mr-b-10">
                        <span class="avatar-text">{{substr(Auth::user()->name,0,1)}}</span>
                        <figure class="avatar-img thumb-sm mr-b-0 d-none">
                            <img src="assets/demo/users/user1.jpg" class="rounded-circle" alt="">
                        </figure>
                    </div>
                    <!-- /.d-inline-block -->
                    <div class="lh-14 mr-t-5 sidebar-collapse-hidden">
                        <h6 class="hide-menu side-user-heading">{{ Auth::user()->name }}</h6><small class="hide-menu">{{ Auth::user()->email}}</small>
                    </div>
                </div>
                <!-- /.col-sm-12 -->
            </div>
            <!-- /.side-user -->
            <!-- Sidebar Menu -->
            <nav class="sidebar-nav">
                <ul class="nav in side-menu">
                  
                                     
                                    <li class="current-page add-enrollee">
                                        <a href="{{ url('/naga-enrollment/add-enrollee')}}">Add Enrollee</a>
                                    </li>

                                    <li>
                                        <a href="{{ url('/naga-enrollment/new-payment')}}">New Payment</a>
                                    </li>

                                    <li>
                                        <a href="{{ url('/naga-enrollment/new-reservation')}}">New Reservation</a>
                                    </li>

                                    
                                       

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons">folder</i>Records</span></a>
                                        <ul class="list-unstyled sub-menu record">
                                                <li class="menu-item-has-children Sales">
                                                <a href="javascript:void(0);">
                                                <span class="hide-menu">Sales</span></a>
                                                <ul class="list-unstyled sub-menu sale">
                                                <li><a href="{{ url('/naga-enrollment/sales-enrollee')}}">per Enrollee</a></li>
                                                
                                                </ul>
                                                </li>

                                        <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                        <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/naga-enrollment/let')}}">LET</a></li>
                                            <li><a href="{{ url('/naga-enrollment/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/naga-enrollment/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/naga-enrollment/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/naga-enrollment/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/naga-enrollment/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/naga-enrollment/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/naga-enrollment/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/naga-enrollment/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/naga-enrollment/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/naga-enrollment/online')}}">Online Only</a></li>
                                        </ul>
                                        </li>
                                            <li><a href="{{ url('/naga-enrollment/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/naga-enrollment/enrolled')}}">Enrolled</a></li>
                                        </ul>

                                            

                                </li>
                                
                                </ul>
                            
                           

                           
            </nav>
            <!-- /.sidebar-nav -->
        </aside>