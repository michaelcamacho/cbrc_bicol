<footer class="footer bg-primary text-inverse text-center">
        <div class="container-fluid"><span class="fs-13 heading-font-family">Copyright @ 2019. All rights reserved <a class="fw-800" href="#">CBRC - BICOL</a><br/>Powered by <a class="fw-800" href="https://www.cipherfusionphilippines.com/services" target="_blank">Cipher Fusion Philippines</a></span>
        </div>
        <!-- /.container-fluid -->
    </footer>