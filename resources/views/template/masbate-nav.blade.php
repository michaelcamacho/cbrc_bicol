  <aside class="site-sidebar scrollbar-enabled" data-suppress-scroll-x="true">
            <!-- User Details -->
            <div class="side-user">
                <figure class="side-user-bg" style="background-image: url(assets/demo/user-image-cropped.jpg)">
                    <img src="assets/demo/user-image-cropped.jpg" alt="" class="d-none">
                </figure>
                <div class="col-sm-12 text-center p-0 clearfix">
                    <div class="d-inline-block pos-relative mr-b-10">
                        <span class="avatar-text">{{substr(Auth::user()->name,0,1)}}</span>
                        <figure class="avatar-img thumb-sm mr-b-0 d-none">
                            <img src="assets/demo/users/user1.jpg" class="rounded-circle" alt="">
                        </figure>
                    </div>
                    <!-- /.d-inline-block -->
                    <div class="lh-14 mr-t-5 sidebar-collapse-hidden">
                        <h6 class="hide-menu side-user-heading">{{ Auth::user()->name }}</h6><small class="hide-menu">{{ Auth::user()->email}}</small>
                    </div>
                </div>
                <!-- /.col-sm-12 -->
            </div>
            <!-- /.side-user -->
            <!-- Sidebar Menu -->
            <nav class="sidebar-nav">
                <ul class="nav in side-menu">
                  
                                     <li class="current-page dashboard"><a href="{{ url('/masbate/dashboard')}}"><i class="list-icon material-icons">home</i> <span class="hide-menu">Dashboard</span></a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/masbate/add-enrollee')}}"><i class="list-icon material-icons">add</i> <span class="hide-menu">Add Enrollee </span></a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/masbate/new-payment')}}"><i class="list-icon material-icons">add</i> <span class="hide-menu">New Payment </span></a></li>

                                    <li class="current-page new-reservation"><a href="{{ url('/masbate/new-reservation')}}"><i class="list-icon material-icons">add</i> <span class="hide-menu">New Reservation </span></a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/masbate/add-expense')}}"><i class="list-icon material-icons">add</i> <span class="hide-menu">Add Expense </span></a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/masbate/add-budget')}}"><i class="list-icon material-icons">add</i> <span class="hide-menu">Add Budget </span></li>

                                    <li class="current-page book-payment"><a href="{{ url('/masbate/book-payment')}}"><i class="list-icon material-icons">add</i> <span class="hide-menu">Book Payment </span></a></li>

                                    <li class="current-page remit"><a href="{{ url('/masbate/new-remit')}}"><i class="list-icon material-icons">add</i> <span class="hide-menu">Cash Remit </span></a></li>

                                    <li class="menu-item-has-children reports"><a href="javascript:void(0);">
                                        <i class="list-icon material-icons">apps</i> <span class="hide-menu">Reports</span></a>  
                                        <ul class="list-unstyled sub-menu reports">
                                      
                
                                           <li><a href="{{ url('/masbate/today')}}">Today Transaction</a>
                                            </li>
                                            
                                            <li><a href="{{ url('/masbate/yesterday')}}">Yesterday Transaction</a>
                                            </li>
                                             <li class="item-has-children scorecard"><a href="javascript:void(0);">
                                                    <span class="hide-menu">Score Card</span></a>
                                                    <ul class="list-unstyled sub-menu scorecard">
                                                        <li>
                                                            <a href="{{ url('/masbate/scorecard-season/1')}}">Season 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ url('/masbate/scorecard-season/2')}}">Season 2</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="{{ url('/masbate/financial-report')}}">Financial Report</a>
                                                </li>
                                               
                                          
                                        </ul>
                                    </li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons">folder</i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/masbate/let')}}">LET</a></li>
                                            <li><a href="{{ url('/masbate/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/masbate/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/masbate/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/masbate/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/masbate/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/masbate/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/masbate/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/masbate/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/masbate/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/masbate/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/masbate/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/masbate/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/masbate/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/masbate/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/masbate/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/masbate/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/masbate/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/masbate/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/masbate/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/masbate/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/masbate/books')}}">Books</a></li>
                                            <li><a href="{{ url('/masbate/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/masbate/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                <li>
                                    <li class="menu-item-has-children hris"><a href="javascript:void(0);"><i class="list-icon material-icons">folder</i> <span class="hide-menu">HRIS</span></a>                    
                                    <ul class="list-unstyled sub-menu hris">
                                        <li><a href="{{ url('/masbate/add-employee')}}">Add Employee</a></li>
                                        <li><a href="{{ url('/masbate/employee-record')}}">Employee Records</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children others"><a href="javascript:void(0);"><i class="list-icon material-icons">folder</i> <span class="hide-menu">Others</span></a>                    
                                    <ul class="list-unstyled sub-menu others">
                                        <li><a href="{{ url('/masbate/student-id')}}">Passer's Templates</a></li>
                                        <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li>
                                    </ul>
                                </li>
                                
                                {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons" style="margin-right: 13px;">content_paste</i>Lecturer Evaluation</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/masbate/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/masbate/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/masbate/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                </li> --}}
                                </ul>
                            
                           

                           
            </nav>
            <!-- /.sidebar-nav -->
        </aside>