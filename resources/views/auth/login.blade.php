@extends('auth.main')
@section('title')
LOGIN
@stop
@section('css')
<style type="text/css">
    .cbrc-logo{
        width: 100%;
    }
</style>
@stop
@section('main-content')
<div id="wrapper" class="row wrapper">
        <div class="container-min-full-height d-flex justify-content-center align-items-center">
            <div class="login-center" style="width: 38%">
                <div class="navbar-header text-center mt-2 mb-4">
                    <a href="index-2.html">
                        <img alt="" src="img/cbrc-logo2.png" class="cbrc-logo">
                    </a>
                </div>
                <!-- /.navbar-header -->
                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}" class="form-element">
                    @csrf
                    <div class="form-group has-feedback">
                        <label for="example-email">Email or Username</label>

                        <input id="example-email" type="text" class="form-control-line form-control{{ $errors->has('email') || $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus placeholder="Email">

                        @if ($errors->has('username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                    </div>
                    <div class="form-group has-feedback">
                         <label for="example-password">Password</label>
                        <input id="example-password" type="password" class="form-control-line form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
                        <span class="form-control-feedback"><i class="fas fa-lock"></i></span>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                         
                    </div>
                    
                    <div class="form-group no-gutters mb-0">
                        <div class="col-md-12 d-flex">
                            <div class="checkbox checkbox-primary mr-auto mr-0-rtl ml-auto-rtl">
                                <label class="d-flex">
                                    <input type="checkbox" id="basic_checkbox_1" name="remember" {{ old('remember') ? 'checked' : '' }}> <span class="label-text">Remember me</span>
                                </label>
                            </div><a href="#" id="to-recover" class="my-auto pb-2 text-right"><i class="material-icons mr-2 fs-18">lock</i> Forgot Password? <br/>Contact your Administrator.</a><br/>
                            
                        </div>

                        <!-- /.col-md-12 -->
                    </div>

                    <div class="form-group">
                        <button class="btn btn-block btn-lg btn-primary text-uppercase fs-12 fw-600" type="submit">Login</button>
                    </div>

                    <!-- /.form-group -->
                </form>
                <!-- /.form-material -->
               
                <!-- /.btn-list -->
               
            </div>
            <!-- /.login-center -->
        </div>
        <!-- /.d-flex -->
    </div>
@endsection
@section('js')

@stop
