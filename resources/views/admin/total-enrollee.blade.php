@extends('main')
@section('title')
 Total {{$program}} Enrollee
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Total {{$program}} Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Total {{$program}} Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="total-en" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                     <thead>
                       
                        <tr>
                            <th>Name</th>
                            <th>CBRC ID</th>
                            <th>Branch</th>
                            <th>Contact #</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>School</th>
                            <th>Birthdate</th>                           
                            <th>Program</th>
                            <th>Section</th>
                            <th>Major</th>
                            <th>Take</th>
                            <th>NOA #</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Emer. Contact</th>
                            <th>Emer. #</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                            @if(isset($daet_a_prog))
                            @foreach($daet_a_prog as $daet_a_progs)
                                <tr>
                                    <td>{{$daet_a_progs->last_name}}, {{$daet_a_progs->first_name}} {{$daet_a_progs->middle_name}}</td>
                                    <td>{{$daet_a_progs->cbrc_id}}</td>
                                    <td>{{$daet_a_progs->branch}}</td>
                                    <td>{{$daet_a_progs->contact_no}}</td>
                                    <td>{{$daet_a_progs->email}}</td>
                                    <td>{{$daet_a_progs->address}}</td>
                                    <td>{{$daet_a_progs->school}}</td>
                                    <td>{{$daet_a_progs->birthdate}}</td>
                                    <td>{{$daet_a_progs->course}}</td>
                                    <td>{{$daet_a_progs->section}}</td>
                                    <td>{{$daet_a_progs->major}}</td>
                                    <td>{{$daet_a_progs->take}}</td>
                                    <td>{{$daet_a_progs->noa_no}}</td>                            
                                    <td>{{$daet_a_progs->username}}</td>
                                    <td>{{$daet_a_progs->password}}</td>
                                    <td>{{$daet_a_progs->contact_person}}</td>
                                    <td>{{$daet_a_progs->contact_details}}</td>
                                </tr>
                            @endforeach
                        @endif


                        @if(isset($naga_a_prog))
                        @foreach($naga_a_prog as $naga_a_progs)
                            <tr>
                                <td>{{$naga_a_progs->last_name}}, {{$naga_a_progs->first_name}} {{$naga_a_progs->middle_name}}</td>
                                <td>{{$naga_a_progs->cbrc_id}}</td>
                                <td>{{$naga_a_progs->branch}}</td>
                                <td>{{$naga_a_progs->contact_no}}</td>
                                <td>{{$naga_a_progs->email}}</td>
                                <td>{{$naga_a_progs->address}}</td>
                                <td>{{$naga_a_progs->school}}</td>
                                <td>{{$naga_a_progs->birthdate}}</td>
                                <td>{{$naga_a_progs->course}}</td>
                                <td>{{$naga_a_progs->section}}</td>
                                <td>{{$naga_a_progs->major}}</td>
                                <td>{{$naga_a_progs->take}}</td>
                                <td>{{$naga_a_progs->noa_no}}</td>                            
                                <td>{{$naga_a_progs->username}}</td>
                                <td>{{$naga_a_progs->password}}</td>
                                <td>{{$naga_a_progs->contact_person}}</td>
                                <td>{{$naga_a_progs->contact_details}}</td>
                            </tr>
                        @endforeach
                    @endif


                    @if(isset($masbate_a_prog))
                    @foreach($masbate_a_prog as $masbate_a_progs)
                        <tr>
                            <td>{{$masbate_a_progs->last_name}}, {{$masbate_a_progs->first_name}} {{$masbate_a_progs->middle_name}}</td>
                            <td>{{$masbate_a_progs->cbrc_id}}</td>
                            <td>{{$masbate_a_progs->branch}}</td>
                            <td>{{$masbate_a_progs->contact_no}}</td>
                            <td>{{$masbate_a_progs->email}}</td>
                            <td>{{$masbate_a_progs->address}}</td>
                            <td>{{$masbate_a_progs->school}}</td>
                            <td>{{$masbate_a_progs->birthdate}}</td>
                            <td>{{$masbate_a_progs->course}}</td>
                            <td>{{$masbate_a_progs->section}}</td>
                            <td>{{$masbate_a_progs->major}}</td>
                            <td>{{$masbate_a_progs->take}}</td>
                            <td>{{$masbate_a_progs->noa_no}}</td>                            
                            <td>{{$masbate_a_progs->username}}</td>
                            <td>{{$masbate_a_progs->password}}</td>
                            <td>{{$masbate_a_progs->contact_person}}</td>
                            <td>{{$masbate_a_progs->contact_details}}</td>
                        </tr>
                    @endforeach
                @endif


                @if(isset($sorsogon_a_prog))
                @foreach($sorsogon_a_prog as $sorsogon_a_progs)
                    <tr>
                        <td>{{$sorsogon_a_progs->last_name}}, {{$sorsogon_a_progs->first_name}} {{$sorsogon_a_progs->middle_name}}</td>
                        <td>{{$sorsogon_a_progs->cbrc_id}}</td>
                        <td>{{$sorsogon_a_progs->branch}}</td>
                        <td>{{$sorsogon_a_progs->contact_no}}</td>
                        <td>{{$sorsogon_a_progs->email}}</td>
                        <td>{{$sorsogon_a_progs->address}}</td>
                        <td>{{$sorsogon_a_progs->school}}</td>
                        <td>{{$sorsogon_a_progs->birthdate}}</td>
                        <td>{{$sorsogon_a_progs->course}}</td>
                        <td>{{$sorsogon_a_progs->section}}</td>
                        <td>{{$sorsogon_a_progs->major}}</td>
                        <td>{{$sorsogon_a_progs->take}}</td>
                        <td>{{$sorsogon_a_progs->noa_no}}</td>                            
                        <td>{{$sorsogon_a_progs->username}}</td>
                        <td>{{$sorsogon_a_progs->password}}</td>
                        <td>{{$sorsogon_a_progs->contact_person}}</td>
                        <td>{{$sorsogon_a_progs->contact_details}}</td>
                    </tr>
                @endforeach
            @endif


            @if(isset($legaspi_a_prog))
            @foreach($legaspi_a_prog as $legaspi_a_progs)
                <tr>
                    <td>{{$legaspi_a_progs->last_name}}, {{$legaspi_a_progs->first_name}} {{$legaspi_a_progs->middle_name}}</td>
                    <td>{{$legaspi_a_progs->cbrc_id}}</td>
                    <td>{{$legaspi_a_progs->branch}}</td>
                    <td>{{$legaspi_a_progs->contact_no}}</td>
                    <td>{{$legaspi_a_progs->email}}</td>
                    <td>{{$legaspi_a_progs->address}}</td>
                    <td>{{$legaspi_a_progs->school}}</td>
                    <td>{{$legaspi_a_progs->birthdate}}</td>
                    <td>{{$legaspi_a_progs->course}}</td>
                    <td>{{$legaspi_a_progs->section}}</td>
                    <td>{{$legaspi_a_progs->major}}</td>
                    <td>{{$legaspi_a_progs->take}}</td>
                    <td>{{$legaspi_a_progs->noa_no}}</td>                            
                    <td>{{$legaspi_a_progs->username}}</td>
                    <td>{{$legaspi_a_progs->password}}</td>
                    <td>{{$legaspi_a_progs->contact_person}}</td>
                    <td>{{$legaspi_a_progs->contact_details}}</td>
                </tr>
            @endforeach
        @endif

                        
                    </tbody>           
                    
                </table>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
  $('.ad-record').addClass('active');
  $('.ad-record').addClass('collapse in');
  $('.ad-enrollee').addClass('active');
  $('.ad-enrollee').addClass('collapse in');
});

</script>
@stop