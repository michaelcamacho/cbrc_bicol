@extends('main')
@section('title')
 Total Sales per Program
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Total Sales per Program Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Total Sales per Program Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Program</th>
                            <th>Enrollees</th>
                            <th>Sales Season 1</th>
                            <th>Sales Season 2</th>

                        </tr>
                       
                    </thead>
                    <tbody>

                            <tr>
                                    <td>LET</td>
                                    <td>{{$daet_let}}</td>
                                    <td>{{$daet_let_1_sale}}</td>
                                    <td>{{$daet_let_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>NLE</td>
                                    <td>{{$daet_nle}}</td>
                                    <td>{{$daet_nle_1_sale}}</td>
                                    <td>{{$daet_nle_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>Criminology</td>
                                    <td>{{$daet_crim}}</td>
                                    <td>{{$daet_crim_1_sale}}</td>
                                    <td>{{$daet_crim_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>Civil Service</td>
                                    <td>{{$daet_civil}}</td>
                                    <td>{{$daet_civil_1_sale}}</td>
                                    <td>{{$daet_civil_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>Psychometrician</td>
                                    <td>{{$daet_psyc}}</td>
                                    <td>{{$daet_psyc_1_sale}}</td>
                                    <td>{{$daet_psyc_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>NCLEX</td>
                                    <td>{{$daet_nclex}}</td>
                                    <td>{{$daet_nclex_1_sale}}</td>
                                    <td>{{$daet_nclex_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>IELTS</td>
                                    <td>{{$daet_ielts}}</td>
                                    <td>{{$daet_ielts_1_sale}}</td>
                                    <td>{{$daet_ielts_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>Social Work</td>
                                    <td>{{$daet_social}}</td>
                                    <td>{{$daet_social_1_sale}}</td>
                                    <td>{{$daet_social_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>Agriculture</td>
                                    <td>{{$daet_agri}}</td>
                                    <td>{{$daet_agri_1_sale}}</td>
                                    <td>{{$daet_agri_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>Midwifery</td>
                                    <td>{{$daet_mid}}</td>
                                    <td>{{$daet_mid_1_sale}}</td>
                                    <td>{{$daet_mid_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>Online Only</td>
                                    <td>{{$daet_online}}</td>
                                    <td>{{$daet_online_1_sale}}</td>
                                    <td>{{$daet_online_2_sale}}</td>
                                </tr>



                                <tr>
                                        <td>LET</td>
                                        <td>{{$naga_let}}</td>
                                        <td>{{$naga_let_1_sale}}</td>
                                        <td>{{$naga_let_2_sale}}</td>
                                    </tr>
                                    <tr>
                                        <td>NLE</td>
                                        <td>{{$naga_nle}}</td>
                                        <td>{{$naga_nle_1_sale}}</td>
                                        <td>{{$naga_nle_2_sale}}</td>
                                    </tr>
                                    <tr>
                                        <td>Criminology</td>
                                        <td>{{$naga_crim}}</td>
                                        <td>{{$naga_crim_1_sale}}</td>
                                        <td>{{$naga_crim_2_sale}}</td>
                                    </tr>
                                    <tr>
                                        <td>Civil Service</td>
                                        <td>{{$naga_civil}}</td>
                                        <td>{{$naga_civil_1_sale}}</td>
                                        <td>{{$naga_civil_2_sale}}</td>
                                    </tr>
                                    <tr>
                                        <td>Psychometrician</td>
                                        <td>{{$naga_psyc}}</td>
                                        <td>{{$naga_psyc_1_sale}}</td>
                                        <td>{{$naga_psyc_2_sale}}</td>
                                    </tr>
                                    <tr>
                                        <td>NCLEX</td>
                                        <td>{{$naga_nclex}}</td>
                                        <td>{{$naga_nclex_1_sale}}</td>
                                        <td>{{$naga_nclex_2_sale}}</td>
                                    </tr>
                                    <tr>
                                        <td>IELTS</td>
                                        <td>{{$naga_ielts}}</td>
                                        <td>{{$naga_ielts_1_sale}}</td>
                                        <td>{{$naga_ielts_2_sale}}</td>
                                    </tr>
                                    <tr>
                                        <td>Social Work</td>
                                        <td>{{$naga_social}}</td>
                                        <td>{{$naga_social_1_sale}}</td>
                                        <td>{{$naga_social_2_sale}}</td>
                                    </tr>
                                    <tr>
                                        <td>Agriculture</td>
                                        <td>{{$naga_agri}}</td>
                                        <td>{{$naga_agri_1_sale}}</td>
                                        <td>{{$naga_agri_2_sale}}</td>
                                    </tr>
                                    <tr>
                                        <td>Midwifery</td>
                                        <td>{{$naga_mid}}</td>
                                        <td>{{$naga_mid_1_sale}}</td>
                                        <td>{{$naga_mid_2_sale}}</td>
                                    </tr>
                                    <tr>
                                        <td>Online Only</td>
                                        <td>{{$naga_online}}</td>
                                        <td>{{$naga_online_1_sale}}</td>
                                        <td>{{$naga_online_2_sale}}</td>
                                    </tr>


                                    <tr>
                                            <td>LET</td>
                                            <td>{{$masbate_let}}</td>
                                            <td>{{$masbate_let_1_sale}}</td>
                                            <td>{{$masbate_let_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>NLE</td>
                                            <td>{{$masbate_nle}}</td>
                                            <td>{{$masbate_nle_1_sale}}</td>
                                            <td>{{$masbate_nle_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Criminology</td>
                                            <td>{{$masbate_crim}}</td>
                                            <td>{{$masbate_crim_1_sale}}</td>
                                            <td>{{$masbate_crim_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Civil Service</td>
                                            <td>{{$masbate_civil}}</td>
                                            <td>{{$masbate_civil_1_sale}}</td>
                                            <td>{{$masbate_civil_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Psychometrician</td>
                                            <td>{{$masbate_psyc}}</td>
                                            <td>{{$masbate_psyc_1_sale}}</td>
                                            <td>{{$masbate_psyc_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>NCLEX</td>
                                            <td>{{$masbate_nclex}}</td>
                                            <td>{{$masbate_nclex_1_sale}}</td>
                                            <td>{{$masbate_nclex_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>IELTS</td>
                                            <td>{{$masbate_ielts}}</td>
                                            <td>{{$masbate_ielts_1_sale}}</td>
                                            <td>{{$masbate_ielts_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Social Work</td>
                                            <td>{{$masbate_social}}</td>
                                            <td>{{$masbate_social_1_sale}}</td>
                                            <td>{{$masbate_social_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Agriculture</td>
                                            <td>{{$masbate_agri}}</td>
                                            <td>{{$masbate_agri_1_sale}}</td>
                                            <td>{{$masbate_agri_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Midwifery</td>
                                            <td>{{$masbate_mid}}</td>
                                            <td>{{$masbate_mid_1_sale}}</td>
                                            <td>{{$masbate_mid_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Online Only</td>
                                            <td>{{$masbate_online}}</td>
                                            <td>{{$masbate_online_1_sale}}</td>
                                            <td>{{$masbate_online_2_sale}}</td>
                                        </tr>


                                    <tr>
                                            <td>LET</td>
                                            <td>{{$sorsogon_let}}</td>
                                            <td>{{$sorsogon_let_1_sale}}</td>
                                            <td>{{$sorsogon_let_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>NLE</td>
                                            <td>{{$sorsogon_nle}}</td>
                                            <td>{{$sorsogon_nle_1_sale}}</td>
                                            <td>{{$sorsogon_nle_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Criminology</td>
                                            <td>{{$sorsogon_crim}}</td>
                                            <td>{{$sorsogon_crim_1_sale}}</td>
                                            <td>{{$sorsogon_crim_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Civil Service</td>
                                            <td>{{$sorsogon_civil}}</td>
                                            <td>{{$sorsogon_civil_1_sale}}</td>
                                            <td>{{$sorsogon_civil_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Psychometrician</td>
                                            <td>{{$sorsogon_psyc}}</td>
                                            <td>{{$sorsogon_psyc_1_sale}}</td>
                                            <td>{{$sorsogon_psyc_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>NCLEX</td>
                                            <td>{{$sorsogon_nclex}}</td>
                                            <td>{{$sorsogon_nclex_1_sale}}</td>
                                            <td>{{$sorsogon_nclex_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>IELTS</td>
                                            <td>{{$sorsogon_ielts}}</td>
                                            <td>{{$sorsogon_ielts_1_sale}}</td>
                                            <td>{{$sorsogon_ielts_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Social Work</td>
                                            <td>{{$sorsogon_social}}</td>
                                            <td>{{$sorsogon_social_1_sale}}</td>
                                            <td>{{$sorsogon_social_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Agriculture</td>
                                            <td>{{$sorsogon_agri}}</td>
                                            <td>{{$sorsogon_agri_1_sale}}</td>
                                            <td>{{$sorsogon_agri_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Midwifery</td>
                                            <td>{{$sorsogon_mid}}</td>
                                            <td>{{$sorsogon_mid_1_sale}}</td>
                                            <td>{{$sorsogon_mid_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Online Only</td>
                                            <td>{{$sorsogon_online}}</td>
                                            <td>{{$sorsogon_online_1_sale}}</td>
                                            <td>{{$sorsogon_online_2_sale}}</td>
                                        </tr>


                                        <tr>
                                                <td>LET</td>
                                                <td>{{$legaspi_let}}</td>
                                                <td>{{$legaspi_let_1_sale}}</td>
                                                <td>{{$legaspi_let_2_sale}}</td>
                                            </tr>
                                            <tr>
                                                <td>NLE</td>
                                                <td>{{$legaspi_nle}}</td>
                                                <td>{{$legaspi_nle_1_sale}}</td>
                                                <td>{{$legaspi_nle_2_sale}}</td>
                                            </tr>
                                            <tr>
                                                <td>Criminology</td>
                                                <td>{{$legaspi_crim}}</td>
                                                <td>{{$legaspi_crim_1_sale}}</td>
                                                <td>{{$legaspi_crim_2_sale}}</td>
                                            </tr>
                                            <tr>
                                                <td>Civil Service</td>
                                                <td>{{$legaspi_civil}}</td>
                                                <td>{{$legaspi_civil_1_sale}}</td>
                                                <td>{{$legaspi_civil_2_sale}}</td>
                                            </tr>
                                            <tr>
                                                <td>Psychometrician</td>
                                                <td>{{$legaspi_psyc}}</td>
                                                <td>{{$legaspi_psyc_1_sale}}</td>
                                                <td>{{$legaspi_psyc_2_sale}}</td>
                                            </tr>
                                            <tr>
                                                <td>NCLEX</td>
                                                <td>{{$legaspi_nclex}}</td>
                                                <td>{{$legaspi_nclex_1_sale}}</td>
                                                <td>{{$legaspi_nclex_2_sale}}</td>
                                            </tr>
                                            <tr>
                                                <td>IELTS</td>
                                                <td>{{$legaspi_ielts}}</td>
                                                <td>{{$legaspi_ielts_1_sale}}</td>
                                                <td>{{$legaspi_ielts_2_sale}}</td>
                                            </tr>
                                            <tr>
                                                <td>Social Work</td>
                                                <td>{{$legaspi_social}}</td>
                                                <td>{{$legaspi_social_1_sale}}</td>
                                                <td>{{$legaspi_social_2_sale}}</td>
                                            </tr>
                                            <tr>
                                                <td>Agriculture</td>
                                                <td>{{$legaspi_agri}}</td>
                                                <td>{{$legaspi_agri_1_sale}}</td>
                                                <td>{{$legaspi_agri_2_sale}}</td>
                                            </tr>
                                            <tr>
                                                <td>Midwifery</td>
                                                <td>{{$legaspi_mid}}</td>
                                                <td>{{$legaspi_mid_1_sale}}</td>
                                                <td>{{$legaspi_mid_2_sale}}</td>
                                            </tr>
                                            <tr>
                                                <td>Online Only</td>
                                                <td>{{$legaspi_online}}</td>
                                                <td>{{$legaspi_online_1_sale}}</td>
                                                <td>{{$legaspi_online_2_sale}}</td>
                                            </tr>





                    </tbody>                  
                    
                </table>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
  $('.ad-record').addClass('active');
  $('.ad-record').addClass('collapse in');
});

</script>
@stop