@extends('main')
@section('title')
Total Expenses
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Total Expenses Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <br/>
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Total Expenses Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                <div id="sale-table">
                                    <table id="sale" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                     <thead>
                       
                        <tr>
                            <th>Date</th>
                            <th>Branch</th>
                            <th>Program</th>
                            <th>Category</th>
                            <th>Sub Category</th>
                            <th>Amount</th>
                            <th>Remarks</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                            @if(isset($daet_a_sale))
                            @foreach($daet_a_sale as $daet_a_sales)
                        <tr id="row_{{$daet_a_sales->id}}">
                            <td>{{$daet_a_sales->date}}</td>
                            <td>{{$daet_a_sales->branch}}</td>
                            <td>{{$daet_a_sales->program}}</td>
                            <td>{{$daet_a_sales->category}}</td>
                            <td>{{$daet_a_sales->sub_category}}</td>
                            <td>{{$daet_a_sales->amount}}</td>
                            <td>{{$daet_a_sales->remarks}}</td>
                            
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($naga_a_sale))
                            @foreach($naga_a_sale as $naga_a_sales)
                        <tr id="row_{{$naga_a_sales->id}}">
                            <td>{{$naga_a_sales->date}}</td>
                            <td>{{$naga_a_sales->branch}}</td>
                            <td>{{$naga_a_sales->program}}</td>
                            <td>{{$naga_a_sales->category}}</td>
                            <td>{{$naga_a_sales->sub_category}}</td>
                            <td>{{$naga_a_sales->amount}}</td>
                            <td>{{$naga_a_sales->remarks}}</td>
                            
                        </tr>
                       @endforeach
                            @endif


                            @if(isset($masbate_a_sale))
                            @foreach($masbate_a_sale as $masbate_a_sales)
                        <tr id="row_{{$masbate_a_sales->id}}">
                            <td>{{$masbate_a_sales->date}}</td>
                            <td>{{$masbate_a_sales->branch}}</td>
                            <td>{{$masbate_a_sales->program}}</td>
                            <td>{{$masbate_a_sales->category}}</td>
                            <td>{{$masbate_a_sales->sub_category}}</td>
                            <td>{{$masbate_a_sales->amount}}</td>
                            <td>{{$masbate_a_sales->remarks}}</td>
                            
                        </tr>
                       @endforeach
                            @endif

                        
                            @if(isset($sorsogon_a_sale))
                            @foreach($sorsogon_a_sale as $sorsogon_a_sales)
                        <tr id="row_{{$sorsogon_a_sales->id}}">
                            <td>{{$sorsogon_a_sales->date}}</td>
                            <td>{{$sorsogon_a_sales->branch}}</td>
                            <td>{{$sorsogon_a_sales->program}}</td>
                            <td>{{$sorsogon_a_sales->category}}</td>
                            <td>{{$sorsogon_a_sales->sub_category}}</td>
                            <td>{{$sorsogon_a_sales->amount}}</td>
                            <td>{{$sorsogon_a_sales->remarks}}</td>
                            
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($legaspi_a_sale))
                            @foreach($legaspi_a_sale as $legaspi_a_sales)
                        <tr id="row_{{$legaspi_a_sales->id}}">
                            <td>{{$legaspi_a_sales->date}}</td>
                            <td>{{$legaspi_a_sales->branch}}</td>
                            <td>{{$legaspi_a_sales->program}}</td>
                            <td>{{$legaspi_a_sales->category}}</td>
                            <td>{{$legaspi_a_sales->sub_category}}</td>
                            <td>{{$legaspi_a_sales->amount}}</td>
                            <td>{{$legaspi_a_sales->remarks}}</td>
                            
                        </tr>
                       @endforeach
                            @endif

                    </tbody>           
                    
                </table>
            </div>

             </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
        </main>
        

@stop
@section('js')

<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/book.js') }}"></script>
<script type="text/javascript">

@stop