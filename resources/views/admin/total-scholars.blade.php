@extends('main')
@section('title')
 Total Scholar
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Total Scholar Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Total Scholar Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="total-en" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                     <thead>
                       
                        <tr>
                            <th>Name</th>
                            <th>CBRC ID</th>
                            <th>Branch</th>
                            <th>Program</th>
                            <th>School</th>
                            <th>Contact #</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                            @if(isset($daet_a_let))
                            @foreach($daet_a_let as $daet_a_lets)
                        <tr>
                            <td>{{$daet_a_lets->last_name}}, {{$daet_a_lets->first_name}} {{$daet_a_lets->middle_name}}</td>
                            <td>{{$daet_a_lets->cbrc_id}}</td>
                            <td>{{$daet_a_lets->branch}}</td>
                            <td>{{$daet_a_lets->program}}</td>
                            <td>{{$daet_a_lets->school}}</td>
                            <td>{{$daet_a_lets->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($daet_a_nle))
                            @foreach($daet_a_nle as $daet_a_nles)
                        <tr>
                            <td>{{$daet_a_nles->last_name}}, {{$daet_a_nles->first_name}} {{$daet_a_nles->middle_name}}</td>
                            <td>{{$daet_a_nles->cbrc_id}}</td>
                            <td>{{$daet_a_nles->branch}}</td>
                            <td>{{$daet_a_nles->program}}</td>
                            <td>{{$daet_a_nles->school}}</td>
                            <td>{{$daet_a_nles->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($daet_a_crim))
                            @foreach($daet_a_crim as $daet_a_crims)
                        <tr>
                            <td>{{$daet_a_crims->last_name}}, {{$daet_a_crims->first_name}} {{$daet_a_crims->middle_name}}</td>
                            <td>{{$daet_a_crims->cbrc_id}}</td>
                            <td>{{$daet_a_crims->branch}}</td>
                            <td>{{$daet_a_crims->program}}</td>
                            <td>{{$daet_a_crims->school}}</td>
                            <td>{{$daet_a_crims->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($daet_a_civil))
                            @foreach($daet_a_civil as $daet_a_civils)
                        <tr>
                            <td>{{$daet_a_civils->last_name}}, {{$daet_a_civils->first_name}} {{$daet_a_civils->middle_name}}</td>
                            <td>{{$daet_a_civils->cbrc_id}}</td>
                            <td>{{$daet_a_civils->branch}}</td>
                            <td>{{$daet_a_civils->program}}</td>
                            <td>{{$daet_a_civils->school}}</td>
                            <td>{{$daet_a_civils->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                        @if(isset($daet_a_psyc))
                            @foreach($daet_a_psyc as $daet_a_psycs)
                        <tr>
                            <td>{{$daet_a_psycs->last_name}}, {{$daet_a_psycs->first_name}} {{$daet_a_psycs->middle_name}}</td>
                            <td>{{$daet_a_psycs->cbrc_id}}</td>
                            <td>{{$daet_a_psycs->branch}}</td>
                            <td>{{$daet_a_psycs->program}}</td>
                            <td>{{$daet_a_psycs->school}}</td>
                            <td>{{$daet_a_psycs->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                        @if(isset($daet_a_nclex))
                            @foreach($daet_a_nclex as $daet_a_nclexs)
                        <tr>
                            <td>{{$daet_a_nclexs->last_name}}, {{$daet_a_nclexs->first_name}} {{$daet_a_nclexs->middle_name}}</td>
                            <td>{{$daet_a_nclexs->cbrc_id}}</td>
                            <td>{{$daet_a_nclexs->branch}}</td>
                            <td>{{$daet_a_nclexs->program}}</td>
                            <td>{{$daet_a_nclexs->school}}</td>
                            <td>{{$daet_a_nclexs->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($daet_a_ielt))
                            @foreach($daet_a_ielt as $daet_a_ielts)
                        <tr>
                            <td>{{$daet_a_ielts->last_name}}, {{$daet_a_ielts->first_name}} {{$daet_a_ielts->middle_name}}</td>
                            <td>{{$daet_a_ielts->cbrc_id}}</td>
                            <td>{{$daet_a_ielts->branch}}</td>
                            <td>{{$daet_a_ielts->program}}</td>
                            <td>{{$daet_a_ielts->school}}</td>
                            <td>{{$daet_a_ielts->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($daet_a_social))
                            @foreach($daet_a_social as $daet_a_socials)
                        <tr>
                            <td>{{$daet_a_socials->last_name}}, {{$daet_a_socials->first_name}} {{$daet_a_socials->middle_name}}</td>
                            <td>{{$daet_a_socials->cbrc_id}}</td>
                            <td>{{$daet_a_socials->branch}}</td>
                            <td>{{$daet_a_socials->program}}</td>
                            <td>{{$daet_a_socials->school}}</td>
                            <td>{{$daet_a_socials->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($daet_a_agri))
                            @foreach($daet_a_agri as $daet_a_agris)
                        <tr>
                            <td>{{$daet_a_agris->last_name}}, {{$daet_a_agris->first_name}} {{$daet_a_agris->middle_name}}</td>
                            <td>{{$daet_a_agris->cbrc_id}}</td>
                            <td>{{$daet_a_agris->branch}}</td>
                            <td>{{$daet_a_agris->program}}</td>
                            <td>{{$daet_a_agris->school}}</td>
                            <td>{{$daet_a_agris->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($daet_a_mid))
                            @foreach($daet_a_mid as $daet_a_mids)
                        <tr>
                            <td>{{$daet_a_mids->last_name}}, {{$daet_a_mids->first_name}} {{$daet_a_mids->middle_name}}</td>
                            <td>{{$daet_a_mids->cbrc_id}}</td>
                            <td>{{$daet_a_mids->branch}}</td>
                            <td>{{$daet_a_mids->program}}</td>
                            <td>{{$daet_a_mids->school}}</td>
                            <td>{{$daet_a_mids->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($daet_a_online))
                            @foreach($daet_a_online as $daet_a_onlines)
                        <tr>
                            <td>{{$daet_a_onlines->last_name}}, {{$daet_a_onlines->first_name}} {{$daet_a_onlines->middle_name}}</td>
                            <td>{{$daet_a_onlines->cbrc_id}}</td>
                            <td>{{$daet_a_onlines->branch}}</td>
                            <td>{{$daet_a_onlines->program}}</td>
                            <td>{{$daet_a_onlines->school}}</td>
                            <td>{{$daet_a_onlines->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                            @if(isset($naga_a_let))
                            @foreach($naga_a_let as $naga_a_lets)
                        <tr>
                            <td>{{$naga_a_lets->last_name}}, {{$naga_a_lets->first_name}} {{$naga_a_lets->middle_name}}</td>
                            <td>{{$naga_a_lets->cbrc_id}}</td>
                            <td>{{$naga_a_lets->branch}}</td>
                            <td>{{$naga_a_lets->program}}</td>
                            <td>{{$naga_a_lets->school}}</td>
                            <td>{{$naga_a_lets->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($naga_a_nle))
                            @foreach($naga_a_nle as $naga_a_nles)
                        <tr>
                            <td>{{$naga_a_nles->last_name}}, {{$naga_a_nles->first_name}} {{$naga_a_nles->middle_name}}</td>
                            <td>{{$naga_a_nles->cbrc_id}}</td>
                            <td>{{$naga_a_nles->branch}}</td>
                            <td>{{$naga_a_nles->program}}</td>
                            <td>{{$naga_a_nles->school}}</td>
                            <td>{{$naga_a_nles->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($naga_a_crim))
                            @foreach($naga_a_crim as $naga_a_crims)
                        <tr>
                            <td>{{$naga_a_crims->last_name}}, {{$naga_a_crims->first_name}} {{$naga_a_crims->middle_name}}</td>
                            <td>{{$naga_a_crims->cbrc_id}}</td>
                            <td>{{$naga_a_crims->branch}}</td>
                            <td>{{$naga_a_crims->program}}</td>
                            <td>{{$naga_a_crims->school}}</td>
                            <td>{{$naga_a_crims->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($naga_a_civil))
                            @foreach($naga_a_civil as $naga_a_civils)
                        <tr>
                            <td>{{$naga_a_civils->last_name}}, {{$naga_a_civils->first_name}} {{$naga_a_civils->middle_name}}</td>
                            <td>{{$naga_a_civils->cbrc_id}}</td>
                            <td>{{$naga_a_civils->branch}}</td>
                            <td>{{$naga_a_civils->program}}</td>
                            <td>{{$naga_a_civils->school}}</td>
                            <td>{{$naga_a_civils->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                        @if(isset($naga_a_psyc))
                            @foreach($naga_a_psyc as $naga_a_psycs)
                        <tr>
                            <td>{{$naga_a_psycs->last_name}}, {{$naga_a_psycs->first_name}} {{$naga_a_psycs->middle_name}}</td>
                            <td>{{$naga_a_psycs->cbrc_id}}</td>
                            <td>{{$naga_a_psycs->branch}}</td>
                            <td>{{$naga_a_psycs->program}}</td>
                            <td>{{$naga_a_psycs->school}}</td>
                            <td>{{$naga_a_psycs->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                        @if(isset($naga_a_nclex))
                            @foreach($naga_a_nclex as $naga_a_nclexs)
                        <tr>
                            <td>{{$naga_a_nclexs->last_name}}, {{$naga_a_nclexs->first_name}} {{$naga_a_nclexs->middle_name}}</td>
                            <td>{{$naga_a_nclexs->cbrc_id}}</td>
                            <td>{{$naga_a_nclexs->branch}}</td>
                            <td>{{$naga_a_nclexs->program}}</td>
                            <td>{{$naga_a_nclexs->school}}</td>
                            <td>{{$naga_a_nclexs->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($naga_a_ielt))
                            @foreach($naga_a_ielt as $naga_a_ielts)
                        <tr>
                            <td>{{$naga_a_ielts->last_name}}, {{$naga_a_ielts->first_name}} {{$naga_a_ielts->middle_name}}</td>
                            <td>{{$naga_a_ielts->cbrc_id}}</td>
                            <td>{{$naga_a_ielts->branch}}</td>
                            <td>{{$naga_a_ielts->program}}</td>
                            <td>{{$naga_a_ielts->school}}</td>
                            <td>{{$naga_a_ielts->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($naga_a_social))
                            @foreach($naga_a_social as $naga_a_socials)
                        <tr>
                            <td>{{$naga_a_socials->last_name}}, {{$naga_a_socials->first_name}} {{$naga_a_socials->middle_name}}</td>
                            <td>{{$naga_a_socials->cbrc_id}}</td>
                            <td>{{$naga_a_socials->branch}}</td>
                            <td>{{$naga_a_socials->program}}</td>
                            <td>{{$naga_a_socials->school}}</td>
                            <td>{{$naga_a_socials->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($naga_a_agri))
                            @foreach($naga_a_agri as $naga_a_agris)
                        <tr>
                            <td>{{$naga_a_agris->last_name}}, {{$naga_a_agris->first_name}} {{$naga_a_agris->middle_name}}</td>
                            <td>{{$naga_a_agris->cbrc_id}}</td>
                            <td>{{$naga_a_agris->branch}}</td>
                            <td>{{$naga_a_agris->program}}</td>
                            <td>{{$naga_a_agris->school}}</td>
                            <td>{{$naga_a_agris->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($naga_a_mid))
                            @foreach($naga_a_mid as $naga_a_mids)
                        <tr>
                            <td>{{$naga_a_mids->last_name}}, {{$naga_a_mids->first_name}} {{$naga_a_mids->middle_name}}</td>
                            <td>{{$naga_a_mids->cbrc_id}}</td>
                            <td>{{$naga_a_mids->branch}}</td>
                            <td>{{$naga_a_mids->program}}</td>
                            <td>{{$naga_a_mids->school}}</td>
                            <td>{{$naga_a_mids->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($naga_a_online))
                            @foreach($naga_a_online as $naga_a_onlines)
                        <tr>
                            <td>{{$naga_a_onlines->last_name}}, {{$naga_a_onlines->first_name}} {{$naga_a_onlines->middle_name}}</td>
                            <td>{{$naga_a_onlines->cbrc_id}}</td>
                            <td>{{$naga_a_onlines->branch}}</td>
                            <td>{{$naga_a_onlines->program}}</td>
                            <td>{{$naga_a_onlines->school}}</td>
                            <td>{{$naga_a_onlines->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                            @if(isset($masbate_a_let))
                            @foreach($masbate_a_let as $masbate_a_lets)
                        <tr>
                            <td>{{$masbate_a_lets->last_name}}, {{$masbate_a_lets->first_name}} {{$masbate_a_lets->middle_name}}</td>
                            <td>{{$masbate_a_lets->cbrc_id}}</td>
                            <td>{{$masbate_a_lets->branch}}</td>
                            <td>{{$masbate_a_lets->program}}</td>
                            <td>{{$masbate_a_lets->school}}</td>
                            <td>{{$masbate_a_lets->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($masbate_a_nle))
                            @foreach($masbate_a_nle as $masbate_a_nles)
                        <tr>
                            <td>{{$masbate_a_nles->last_name}}, {{$masbate_a_nles->first_name}} {{$masbate_a_nles->middle_name}}</td>
                            <td>{{$masbate_a_nles->cbrc_id}}</td>
                            <td>{{$masbate_a_nles->branch}}</td>
                            <td>{{$masbate_a_nles->program}}</td>
                            <td>{{$masbate_a_nles->school}}</td>
                            <td>{{$masbate_a_nles->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($masbate_a_crim))
                            @foreach($masbate_a_crim as $masbate_a_crims)
                        <tr>
                            <td>{{$masbate_a_crims->last_name}}, {{$masbate_a_crims->first_name}} {{$masbate_a_crims->middle_name}}</td>
                            <td>{{$masbate_a_crims->cbrc_id}}</td>
                            <td>{{$masbate_a_crims->branch}}</td>
                            <td>{{$masbate_a_crims->program}}</td>
                            <td>{{$masbate_a_crims->school}}</td>
                            <td>{{$masbate_a_crims->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($masbate_a_civil))
                            @foreach($masbate_a_civil as $masbate_a_civils)
                        <tr>
                            <td>{{$masbate_a_civils->last_name}}, {{$masbate_a_civils->first_name}} {{$masbate_a_civils->middle_name}}</td>
                            <td>{{$masbate_a_civils->cbrc_id}}</td>
                            <td>{{$masbate_a_civils->branch}}</td>
                            <td>{{$masbate_a_civils->program}}</td>
                            <td>{{$masbate_a_civils->school}}</td>
                            <td>{{$masbate_a_civils->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                        @if(isset($masbate_a_psyc))
                            @foreach($masbate_a_psyc as $masbate_a_psycs)
                        <tr>
                            <td>{{$masbate_a_psycs->last_name}}, {{$masbate_a_psycs->first_name}} {{$masbate_a_psycs->middle_name}}</td>
                            <td>{{$masbate_a_psycs->cbrc_id}}</td>
                            <td>{{$masbate_a_psycs->branch}}</td>
                            <td>{{$masbate_a_psycs->program}}</td>
                            <td>{{$masbate_a_psycs->school}}</td>
                            <td>{{$masbate_a_psycs->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                        @if(isset($masbate_a_nclex))
                            @foreach($masbate_a_nclex as $masbate_a_nclexs)
                        <tr>
                            <td>{{$masbate_a_nclexs->last_name}}, {{$masbate_a_nclexs->first_name}} {{$masbate_a_nclexs->middle_name}}</td>
                            <td>{{$masbate_a_nclexs->cbrc_id}}</td>
                            <td>{{$masbate_a_nclexs->branch}}</td>
                            <td>{{$masbate_a_nclexs->program}}</td>
                            <td>{{$masbate_a_nclexs->school}}</td>
                            <td>{{$masbate_a_nclexs->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($masbate_a_ielt))
                            @foreach($masbate_a_ielt as $masbate_a_ielts)
                        <tr>
                            <td>{{$masbate_a_ielts->last_name}}, {{$masbate_a_ielts->first_name}} {{$masbate_a_ielts->middle_name}}</td>
                            <td>{{$masbate_a_ielts->cbrc_id}}</td>
                            <td>{{$masbate_a_ielts->branch}}</td>
                            <td>{{$masbate_a_ielts->program}}</td>
                            <td>{{$masbate_a_ielts->school}}</td>
                            <td>{{$masbate_a_ielts->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($masbate_a_social))
                            @foreach($masbate_a_social as $masbate_a_socials)
                        <tr>
                            <td>{{$masbate_a_socials->last_name}}, {{$masbate_a_socials->first_name}} {{$masbate_a_socials->middle_name}}</td>
                            <td>{{$masbate_a_socials->cbrc_id}}</td>
                            <td>{{$masbate_a_socials->branch}}</td>
                            <td>{{$masbate_a_socials->program}}</td>
                            <td>{{$masbate_a_socials->school}}</td>
                            <td>{{$masbate_a_socials->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($masbate_a_agri))
                            @foreach($masbate_a_agri as $masbate_a_agris)
                        <tr>
                            <td>{{$masbate_a_agris->last_name}}, {{$masbate_a_agris->first_name}} {{$masbate_a_agris->middle_name}}</td>
                            <td>{{$masbate_a_agris->cbrc_id}}</td>
                            <td>{{$masbate_a_agris->branch}}</td>
                            <td>{{$masbate_a_agris->program}}</td>
                            <td>{{$masbate_a_agris->school}}</td>
                            <td>{{$masbate_a_agris->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($masbate_a_mid))
                            @foreach($masbate_a_mid as $masbate_a_mids)
                        <tr>
                            <td>{{$masbate_a_mids->last_name}}, {{$masbate_a_mids->first_name}} {{$masbate_a_mids->middle_name}}</td>
                            <td>{{$masbate_a_mids->cbrc_id}}</td>
                            <td>{{$masbate_a_mids->branch}}</td>
                            <td>{{$masbate_a_mids->program}}</td>
                            <td>{{$masbate_a_mids->school}}</td>
                            <td>{{$masbate_a_mids->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($masbate_a_online))
                            @foreach($masbate_a_online as $masbate_a_onlines)
                        <tr>
                            <td>{{$masbate_a_onlines->last_name}}, {{$masbate_a_onlines->first_name}} {{$masbate_a_onlines->middle_name}}</td>
                            <td>{{$masbate_a_onlines->cbrc_id}}</td>
                            <td>{{$masbate_a_onlines->branch}}</td>
                            <td>{{$masbate_a_onlines->program}}</td>
                            <td>{{$masbate_a_onlines->school}}</td>
                            <td>{{$masbate_a_onlines->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($sorsogon_a_let))
                            @foreach($sorsogon_a_let as $sorsogon_a_lets)
                        <tr>
                            <td>{{$sorsogon_a_lets->last_name}}, {{$sorsogon_a_lets->first_name}} {{$sorsogon_a_lets->middle_name}}</td>
                            <td>{{$sorsogon_a_lets->cbrc_id}}</td>
                            <td>{{$sorsogon_a_lets->branch}}</td>
                            <td>{{$sorsogon_a_lets->program}}</td>
                            <td>{{$sorsogon_a_lets->school}}</td>
                            <td>{{$sorsogon_a_lets->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($sorsogon_a_nle))
                            @foreach($sorsogon_a_nle as $sorsogon_a_nles)
                        <tr>
                            <td>{{$sorsogon_a_nles->last_name}}, {{$sorsogon_a_nles->first_name}} {{$sorsogon_a_nles->middle_name}}</td>
                            <td>{{$sorsogon_a_nles->cbrc_id}}</td>
                            <td>{{$sorsogon_a_nles->branch}}</td>
                            <td>{{$sorsogon_a_nles->program}}</td>
                            <td>{{$sorsogon_a_nles->school}}</td>
                            <td>{{$sorsogon_a_nles->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($sorsogon_a_crim))
                            @foreach($sorsogon_a_crim as $sorsogon_a_crims)
                        <tr>
                            <td>{{$sorsogon_a_crims->last_name}}, {{$sorsogon_a_crims->first_name}} {{$sorsogon_a_crims->middle_name}}</td>
                            <td>{{$sorsogon_a_crims->cbrc_id}}</td>
                            <td>{{$sorsogon_a_crims->branch}}</td>
                            <td>{{$sorsogon_a_crims->program}}</td>
                            <td>{{$sorsogon_a_crims->school}}</td>
                            <td>{{$sorsogon_a_crims->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($sorsogon_a_civil))
                            @foreach($sorsogon_a_civil as $sorsogon_a_civils)
                        <tr>
                            <td>{{$sorsogon_a_civils->last_name}}, {{$sorsogon_a_civils->first_name}} {{$sorsogon_a_civils->middle_name}}</td>
                            <td>{{$sorsogon_a_civils->cbrc_id}}</td>
                            <td>{{$sorsogon_a_civils->branch}}</td>
                            <td>{{$sorsogon_a_civils->program}}</td>
                            <td>{{$sorsogon_a_civils->school}}</td>
                            <td>{{$sorsogon_a_civils->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                        @if(isset($sorsogon_a_psyc))
                            @foreach($sorsogon_a_psyc as $sorsogon_a_psycs)
                        <tr>
                            <td>{{$sorsogon_a_psycs->last_name}}, {{$sorsogon_a_psycs->first_name}} {{$sorsogon_a_psycs->middle_name}}</td>
                            <td>{{$sorsogon_a_psycs->cbrc_id}}</td>
                            <td>{{$sorsogon_a_psycs->branch}}</td>
                            <td>{{$sorsogon_a_psycs->program}}</td>
                            <td>{{$sorsogon_a_psycs->school}}</td>
                            <td>{{$sorsogon_a_psycs->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                        @if(isset($sorsogon_a_nclex))
                            @foreach($sorsogon_a_nclex as $sorsogon_a_nclexs)
                        <tr>
                            <td>{{$sorsogon_a_nclexs->last_name}}, {{$sorsogon_a_nclexs->first_name}} {{$sorsogon_a_nclexs->middle_name}}</td>
                            <td>{{$sorsogon_a_nclexs->cbrc_id}}</td>
                            <td>{{$sorsogon_a_nclexs->branch}}</td>
                            <td>{{$sorsogon_a_nclexs->program}}</td>
                            <td>{{$sorsogon_a_nclexs->school}}</td>
                            <td>{{$sorsogon_a_nclexs->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($sorsogon_a_ielt))
                            @foreach($sorsogon_a_ielt as $sorsogon_a_ielts)
                        <tr>
                            <td>{{$sorsogon_a_ielts->last_name}}, {{$sorsogon_a_ielts->first_name}} {{$sorsogon_a_ielts->middle_name}}</td>
                            <td>{{$sorsogon_a_ielts->cbrc_id}}</td>
                            <td>{{$sorsogon_a_ielts->branch}}</td>
                            <td>{{$sorsogon_a_ielts->program}}</td>
                            <td>{{$sorsogon_a_ielts->school}}</td>
                            <td>{{$sorsogon_a_ielts->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($sorsogon_a_social))
                            @foreach($sorsogon_a_social as $sorsogon_a_socials)
                        <tr>
                            <td>{{$sorsogon_a_socials->last_name}}, {{$sorsogon_a_socials->first_name}} {{$sorsogon_a_socials->middle_name}}</td>
                            <td>{{$sorsogon_a_socials->cbrc_id}}</td>
                            <td>{{$sorsogon_a_socials->branch}}</td>
                            <td>{{$sorsogon_a_socials->program}}</td>
                            <td>{{$sorsogon_a_socials->school}}</td>
                            <td>{{$sorsogon_a_socials->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($sorsogon_a_agri))
                            @foreach($sorsogon_a_agri as $sorsogon_a_agris)
                        <tr>
                            <td>{{$sorsogon_a_agris->last_name}}, {{$sorsogon_a_agris->first_name}} {{$sorsogon_a_agris->middle_name}}</td>
                            <td>{{$sorsogon_a_agris->cbrc_id}}</td>
                            <td>{{$sorsogon_a_agris->branch}}</td>
                            <td>{{$sorsogon_a_agris->program}}</td>
                            <td>{{$sorsogon_a_agris->school}}</td>
                            <td>{{$sorsogon_a_agris->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($sorsogon_a_mid))
                            @foreach($sorsogon_a_mid as $sorsogon_a_mids)
                        <tr>
                            <td>{{$sorsogon_a_mids->last_name}}, {{$sorsogon_a_mids->first_name}} {{$sorsogon_a_mids->middle_name}}</td>
                            <td>{{$sorsogon_a_mids->cbrc_id}}</td>
                            <td>{{$sorsogon_a_mids->branch}}</td>
                            <td>{{$sorsogon_a_mids->program}}</td>
                            <td>{{$sorsogon_a_mids->school}}</td>
                            <td>{{$sorsogon_a_mids->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($sorsogon_a_online))
                            @foreach($sorsogon_a_online as $sorsogon_a_onlines)
                        <tr>
                            <td>{{$sorsogon_a_onlines->last_name}}, {{$sorsogon_a_onlines->first_name}} {{$sorsogon_a_onlines->middle_name}}</td>
                            <td>{{$sorsogon_a_onlines->cbrc_id}}</td>
                            <td>{{$sorsogon_a_onlines->branch}}</td>
                            <td>{{$sorsogon_a_onlines->program}}</td>
                            <td>{{$sorsogon_a_onlines->school}}</td>
                            <td>{{$sorsogon_a_onlines->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                            @if(isset($legaspi_a_let))
                            @foreach($legaspi_a_let as $legaspi_a_lets)
                        <tr>
                            <td>{{$legaspi_a_lets->last_name}}, {{$legaspi_a_lets->first_name}} {{$legaspi_a_lets->middle_name}}</td>
                            <td>{{$legaspi_a_lets->cbrc_id}}</td>
                            <td>{{$legaspi_a_lets->branch}}</td>
                            <td>{{$legaspi_a_lets->program}}</td>
                            <td>{{$legaspi_a_lets->school}}</td>
                            <td>{{$legaspi_a_lets->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($legaspi_a_nle))
                            @foreach($legaspi_a_nle as $legaspi_a_nles)
                        <tr>
                            <td>{{$legaspi_a_nles->last_name}}, {{$legaspi_a_nles->first_name}} {{$legaspi_a_nles->middle_name}}</td>
                            <td>{{$legaspi_a_nles->cbrc_id}}</td>
                            <td>{{$legaspi_a_nles->branch}}</td>
                            <td>{{$legaspi_a_nles->program}}</td>
                            <td>{{$legaspi_a_nles->school}}</td>
                            <td>{{$legaspi_a_nles->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($legaspi_a_crim))
                            @foreach($legaspi_a_crim as $legaspi_a_crims)
                        <tr>
                            <td>{{$legaspi_a_crims->last_name}}, {{$legaspi_a_crims->first_name}} {{$legaspi_a_crims->middle_name}}</td>
                            <td>{{$legaspi_a_crims->cbrc_id}}</td>
                            <td>{{$legaspi_a_crims->branch}}</td>
                            <td>{{$legaspi_a_crims->program}}</td>
                            <td>{{$legaspi_a_crims->school}}</td>
                            <td>{{$legaspi_a_crims->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($legaspi_a_civil))
                            @foreach($legaspi_a_civil as $legaspi_a_civils)
                        <tr>
                            <td>{{$legaspi_a_civils->last_name}}, {{$legaspi_a_civils->first_name}} {{$legaspi_a_civils->middle_name}}</td>
                            <td>{{$legaspi_a_civils->cbrc_id}}</td>
                            <td>{{$legaspi_a_civils->branch}}</td>
                            <td>{{$legaspi_a_civils->program}}</td>
                            <td>{{$legaspi_a_civils->school}}</td>
                            <td>{{$legaspi_a_civils->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                        @if(isset($legaspi_a_psyc))
                            @foreach($legaspi_a_psyc as $legaspi_a_psycs)
                        <tr>
                            <td>{{$legaspi_a_psycs->last_name}}, {{$legaspi_a_psycs->first_name}} {{$legaspi_a_psycs->middle_name}}</td>
                            <td>{{$legaspi_a_psycs->cbrc_id}}</td>
                            <td>{{$legaspi_a_psycs->branch}}</td>
                            <td>{{$legaspi_a_psycs->program}}</td>
                            <td>{{$legaspi_a_psycs->school}}</td>
                            <td>{{$legaspi_a_psycs->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                        @if(isset($legaspi_a_nclex))
                            @foreach($legaspi_a_nclex as $legaspi_a_nclexs)
                        <tr>
                            <td>{{$legaspi_a_nclexs->last_name}}, {{$legaspi_a_nclexs->first_name}} {{$legaspi_a_nclexs->middle_name}}</td>
                            <td>{{$legaspi_a_nclexs->cbrc_id}}</td>
                            <td>{{$legaspi_a_nclexs->branch}}</td>
                            <td>{{$legaspi_a_nclexs->program}}</td>
                            <td>{{$legaspi_a_nclexs->school}}</td>
                            <td>{{$legaspi_a_nclexs->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($legaspi_a_ielt))
                            @foreach($legaspi_a_ielt as $legaspi_a_ielts)
                        <tr>
                            <td>{{$legaspi_a_ielts->last_name}}, {{$legaspi_a_ielts->first_name}} {{$legaspi_a_ielts->middle_name}}</td>
                            <td>{{$legaspi_a_ielts->cbrc_id}}</td>
                            <td>{{$legaspi_a_ielts->branch}}</td>
                            <td>{{$legaspi_a_ielts->program}}</td>
                            <td>{{$legaspi_a_ielts->school}}</td>
                            <td>{{$legaspi_a_ielts->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($legaspi_a_social))
                            @foreach($legaspi_a_social as $legaspi_a_socials)
                        <tr>
                            <td>{{$legaspi_a_socials->last_name}}, {{$legaspi_a_socials->first_name}} {{$legaspi_a_socials->middle_name}}</td>
                            <td>{{$legaspi_a_socials->cbrc_id}}</td>
                            <td>{{$legaspi_a_socials->branch}}</td>
                            <td>{{$legaspi_a_socials->program}}</td>
                            <td>{{$legaspi_a_socials->school}}</td>
                            <td>{{$legaspi_a_socials->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($legaspi_a_agri))
                            @foreach($legaspi_a_agri as $legaspi_a_agris)
                        <tr>
                            <td>{{$legaspi_a_agris->last_name}}, {{$legaspi_a_agris->first_name}} {{$legaspi_a_agris->middle_name}}</td>
                            <td>{{$legaspi_a_agris->cbrc_id}}</td>
                            <td>{{$legaspi_a_agris->branch}}</td>
                            <td>{{$legaspi_a_agris->program}}</td>
                            <td>{{$legaspi_a_agris->school}}</td>
                            <td>{{$legaspi_a_agris->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($legaspi_a_mid))
                            @foreach($legaspi_a_mid as $legaspi_a_mids)
                        <tr>
                            <td>{{$legaspi_a_mids->last_name}}, {{$legaspi_a_mids->first_name}} {{$legaspi_a_mids->middle_name}}</td>
                            <td>{{$legaspi_a_mids->cbrc_id}}</td>
                            <td>{{$legaspi_a_mids->branch}}</td>
                            <td>{{$legaspi_a_mids->program}}</td>
                            <td>{{$legaspi_a_mids->school}}</td>
                            <td>{{$legaspi_a_mids->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($legaspi_a_online))
                            @foreach($legaspi_a_online as $legaspi_a_onlines)
                        <tr>
                            <td>{{$legaspi_a_onlines->last_name}}, {{$legaspi_a_onlines->first_name}} {{$legaspi_a_onlines->middle_name}}</td>
                            <td>{{$legaspi_a_onlines->cbrc_id}}</td>
                            <td>{{$legaspi_a_onlines->branch}}</td>
                            <td>{{$legaspi_a_onlines->program}}</td>
                            <td>{{$legaspi_a_onlines->school}}</td>
                            <td>{{$legaspi_a_onlines->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                    </tbody>           
                    
                </table>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
  $('.ad-record').addClass('active');
  $('.ad-record').addClass('collapse in');
});

</script>
@stop