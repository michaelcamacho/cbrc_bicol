$(document).ready(function() {
  $('.ad-record').addClass('active');
  $('.ad-record').addClass('collapse in');
  $('button[data-dismiss="modal"]').click(function(){ $(this).parent().parent().parent().parent().modal('hide'); })
});
        var table = $('#discount').DataTable( {
        retrieve: true,
        responsive: true,
        "scrollX": true,
        dom: 'Bfrtip',
        buttons: [
        'excel', 'pdf','csv', 
        
        ]
        
    });

$(document.body).on('click','.a_edit',function(){
  window.edit_id = $(this).attr('id').replace('a_edit_','');
  branch = $('#a_branch_'+edit_id).text();
  program = $('#a_program_'+edit_id).text();
  category = $('#a_category_'+edit_id).text();
  discount_category = $('#a_discount_category_'+edit_id).text();
  discount_amount = $('#a_amount_'+edit_id).text();
  
  $('#update_id').val(edit_id);
  $('#update_branch').val(branch);
  $('#update_program').val(program);
  $('#update_category').val(category);
  $('#update_discount_category').val(discount_category);
  $('#update_discount_amount').val(discount_amount);

  });

$(document.body).on('click','.b_edit',function(){
  window.edit_id = $(this).attr('id').replace('b_edit_','');
  branch = $('#b_branch_'+edit_id).text();
  program = $('#b_program_'+edit_id).text();
  category = $('#b_category_'+edit_id).text();
  discount_category = $('#b_discount_category_'+edit_id).text();
  discount_amount = $('#b_amount_'+edit_id).text();
  
  $('#update_id').val(edit_id);
  $('#update_branch').val(branch);
  $('#update_program').val(program);
  $('#update_category').val(category);
  $('#update_discount_category').val(discount_category);
  $('#update_discount_amount').val(discount_amount);

  });
$(document.body).on('click','.c_edit',function(){
  window.edit_id = $(this).attr('id').replace('c_edit_','');
  branch = $('#c_branch_'+edit_id).text();
  program = $('#c_program_'+edit_id).text();
  category = $('#c_category_'+edit_id).text();
  discount_category = $('#c_discount_category_'+edit_id).text();
  discount_amount = $('#c_amount_'+edit_id).text();
  
  $('#update_id').val(edit_id);
  $('#update_branch').val(branch);
  $('#update_program').val(program);
  $('#update_category').val(category);
  $('#update_discount_category').val(discount_category);
  $('#update_discount_amount').val(discount_amount);

  });
$(document.body).on('click','.d_edit',function(){
  window.edit_id = $(this).attr('id').replace('d_edit_','');
  branch = $('#d_branch_'+edit_id).text();
  program = $('#d_program_'+edit_id).text();
  category = $('#d_category_'+edit_id).text();
  discount_category = $('#d_discount_category_'+edit_id).text();
  discount_amount = $('#d_amount_'+edit_id).text();
  
  $('#update_id').val(edit_id);
  $('#update_branch').val(branch);
  $('#update_program').val(program);
  $('#update_category').val(category);
  $('#update_discount_category').val(discount_category);
  $('#update_discount_amount').val(discount_amount);

  });

$(document.body).on('click','.e_edit',function(){
  window.edit_id = $(this).attr('id').replace('e_edit_','');
  branch = $('#e_branch_'+edit_id).text();
  program = $('#e_program_'+edit_id).text();
  category = $('#e_category_'+edit_id).text();
  discount_category = $('#e_discount_category_'+edit_id).text();
  discount_amount = $('#e_amount_'+edit_id).text();
  
  $('#update_id').val(edit_id);
  $('#update_branch').val(branch);
  $('#update_program').val(program);
  $('#update_category').val(category);
  $('#update_discount_category').val(discount_category);
  $('#update_discount_amount').val(discount_amount);

  });
  