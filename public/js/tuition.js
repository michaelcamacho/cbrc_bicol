$(document).ready(function() {
  $('.ad-record').addClass('active');
  $('.ad-record').addClass('collapse in');
  $('button[data-dismiss="modal"]').click(function(){ $(this).parent().parent().parent().parent().modal('hide'); })
});
        var table = $('#tuition').DataTable( {
        retrieve: true,
        responsive: true,
        "scrollX": true,
        dom: 'Bfrtip',
        buttons: [
        'excel', 'pdf','csv', 
        
        ]
        
    });

$(document.body).on('click','.a_edit',function(){
  window.edit_id = $(this).attr('id').replace('a_edit_','');
  branch = $('#a_branch_'+edit_id).text();
  program = $('#a_program_'+edit_id).text();
  category = $('#a_category_'+edit_id).text();
  tuition = $('#a_tuition_'+edit_id).text();
  facilitation = $('#a_facilitation_'+edit_id).text();
  season = $('#a_season_'+edit_id).text();
  year = $('#a_year_'+edit_id).text();
  $('#update_id').val(edit_id);
  $('#update_branch').val(branch);
  $('#update_program').val(program);
  $('#update_category').val(category);
  $('#update_tuition_fee').val(tuition);
  $('#update_facilitation_fee').val(facilitation);
  $('#update_season').val(season);
  $('#update_year').val(year);

  });

$(document.body).on('click','.b_edit',function(){
  window.edit_id = $(this).attr('id').replace('b_edit_','');
  branch = $('#b_branch_'+edit_id).text();
  program = $('#b_program_'+edit_id).text();
  category = $('#b_category_'+edit_id).text();
  tuition = $('#b_tuition_'+edit_id).text();
  facilitation = $('#b_facilitation_'+edit_id).text();
  season = $('#b_season_'+edit_id).text();
  year = $('#b_year_'+edit_id).text();
  $('#update_id').val(edit_id);
  $('#update_branch').val(branch);
  $('#update_program').val(program);
  $('#update_category').val(category);
  $('#update_tuition_fee').val(tuition);
  $('#update_facilitation_fee').val(facilitation);
  $('#update_season').val(season);
  $('#update_year').val(year);

  });
$(document.body).on('click','.c_edit',function(){
  window.edit_id = $(this).attr('id').replace('c_edit_','');
  branch = $('#c_branch_'+edit_id).text();
  program = $('#c_program_'+edit_id).text();
  category = $('#c_category_'+edit_id).text();
  tuition = $('#c_tuition_'+edit_id).text();
  facilitation = $('#c_facilitation_'+edit_id).text();
  season = $('#c_season_'+edit_id).text();
  year = $('#c_year_'+edit_id).text();
  $('#update_id').val(edit_id);
  $('#update_branch').val(branch);
  $('#update_program').val(program);
  $('#update_category').val(category);
  $('#update_tuition_fee').val(tuition);
  $('#update_facilitation_fee').val(facilitation);
  $('#update_season').val(season);
  $('#update_year').val(year);

  });
$(document.body).on('click','.d_edit',function(){
  window.edit_id = $(this).attr('id').replace('d_edit_','');
  branch = $('#d_branch_'+edit_id).text();
  program = $('#d_program_'+edit_id).text();
  category = $('#d_category_'+edit_id).text();
  tuition = $('#d_tuition_'+edit_id).text();
  facilitation = $('#d_facilitation_'+edit_id).text();
  season = $('#d_season_'+edit_id).text();
  year = $('#d_year_'+edit_id).text();
  $('#update_id').val(edit_id);
  $('#update_branch').val(branch);
  $('#update_program').val(program);
  $('#update_category').val(category);
  $('#update_tuition_fee').val(tuition);
  $('#update_facilitation_fee').val(facilitation);
  $('#update_season').val(season);
  $('#update_year').val(year);

  });

$(document.body).on('click','.e_edit',function(){
  window.edit_id = $(this).attr('id').replace('e_edit_','');
  branch = $('#e_branch_'+edit_id).text();
  program = $('#e_program_'+edit_id).text();
  category = $('#e_category_'+edit_id).text();
  tuition = $('#e_tuition_'+edit_id).text();
  facilitation = $('#e_facilitation_'+edit_id).text();
  season = $('#e_season_'+edit_id).text();
  year = $('#e_year_'+edit_id).text();
  $('#update_id').val(edit_id);
  $('#update_branch').val(branch);
  $('#update_program').val(program);
  $('#update_category').val(category);
  $('#update_tuition_fee').val(tuition);
  $('#update_facilitation_fee').val(facilitation);
  $('#update_season').val(season);
  $('#update_year').val(year);

  });
  