$(document).ready(function() {
setTimeout(function() {
    document.documentElement.scrollTop =
        document.body.scrollTop = 0;
}, 0);
    $(window).scroll(function() {
        if($(this).scrollTop() > 20) {
            $('.navbar').addClass('solid');
            $('.dropdown-content').addClass('drop');
            $('nav').removeClass('bg-dark');
            $('.Vl').removeClass('vl');
        } else {
            $('.navbar').removeClass('solid');
            $('.dropdown-content').removeClass('drop');
            $('nav').addClass('bg-dark');
            $('.Vl').addClass('vl');
        }
    });
});