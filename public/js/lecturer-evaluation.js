var dataSet = [
["<strong>CLASS:</strong>"," "," "," "," "," "," "],
["<strong>SUBJECT:</strong>"," "," "," "," "," "," "],
["<strong>PROGRAM:</strong>"," "," "," "," "," "," "],
["<strong>SECTION:</strong>"," "," "," "," "," "," "],
["<strong>BRANCH:</strong>"," "," "," "," "," "," "],
["<strong>NAME OF REVIEWER:</strong>"," "," "," "," "," "," "],
["<strong>DATE OF LECTURE:</strong>"," "," "," "," "," "," "],
["<strong>NAME OF REVIEW AMBASSADOR:</strong>"," "," "," "," "," "," "],
[" "," "," "," "," "," "," "],
["","<strong>EXCELLENT</strong>","<strong>GOOD</strong>","<strong>FAIR</strong>","<strong>POOR</strong>","<strong>VERY POOR</strong>","<strong>TOTAL</strong>"],
["<strong>CONTENT</strong>"," "," "," "," "," "," "],
["Comprehensiveness – shows knowledge of the subject matter"," "," "," "," "," "," "],
["Substantiveness – delivers enough and substantial concepts"," "," "," "," "," "," "],
["Relevance – provides topics that are relevant to the actual exam"," "," "," "," "," "," "],
["Organization – executes the topic in an organized manner"," "," "," "," "," "," "],
["<strong>MASTERY</strong>"," "," "," "," "," "," "],
["Authority – exhibits confidence and authority in presenting the topic"," "," "," "," "," "," "],
["Ability to Answer Questions – able to answer students questions satisfactorily"," "," "," "," "," "," "],
["Preparedness – well-prepared to discuss the lecture"," "," "," "," "," "," "],
["<strong>DELIVERY</strong>"," "," "," "," "," "," "],
["Enthusiasm - shows energy that stimulates participation"," "," "," "," "," "," "],
["Pacing – moves to one topic to another understandable to the students"," "," "," "," "," "," "],
["Voice Projection –has effetive and clear voice"," "," "," "," "," "," "],
["Approach/ Strategy – has dynamic technique in discussing"," "," "," "," "," "," "],
["<strong>MOTIVATIONAL</strong>"," "," "," "," "," "," "],
["Inspiration – inspires me to strive harder and makes the topic more interesting"," "," "," "," "," "," "],
["Self-Esteem – boosts me to be a better person"," "," "," "," "," "," "],
["Authenticity – shows sincerity and authenticity in presenting himself/herself"," "," "," "," "," "," "],
["<strong>IMAGING</strong>"," "," "," "," "," "," "],
["Appearance & Grooming – shows neatness, proper posture, and an aesthetic appeal"," "," "," "," "," "," "],
["Character & Personality - shows uniqueness and carried himself/herself very well"," "," "," "," "," "," "],
["Impact – has stage presence, charisma, and a contagious aura"," "," "," "," "," "," "],
];

var table = $('#lecturers').DataTable( {
		data: dataSet,
		columns: [
            { title: "" },
            { title: "" },
            { title: "" },
            { title: "" },
            { title: "" },
            { title: "" },
            { title: ""}
        ],

		responsive: false,
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		],
		"order": false,
		"paging": false,
		"searching": false,
		exportOptions : {
            columns: ':visible'
        },

		
	} );


$('.search').on('click', function(e){
e.preventDefault()

var eval_program = $('#eval_program').val();
var eval_class = $('#eval_class').val();
var eval_subj = $('#eval_subj').val();
var eval_section = $('#eval_section').val();
var eval_lecturer = $('#eval_lecturer').val();
var eval_id = $('#eval_id').val();
var eval_date = $('#eval_date').val();
var eval_ambassador = $('#eval_ambassador').val();
var eval_branch = $('#eval_branch').val();
var excellentA = $('#excellentA').val();
var excellentB = $('#excellentB').val();
var excellentC = $('#excellentC').val();
var excellentD = $('#excellentD').val();
var excellentE = $('#excellentE').val();
var excellentF = $('#excellentF').val();
var excellentG = $('#excellentG').val();
var goodA = $('#goodA').val();
var goodB = $('#goodB').val();
var goodC = $('#goodC').val();
var goodD = $('#goodD').val();
var goodE = $('#goodE').val();
var goodF = $('#goodF').val();
var goodG = $('#goodG').val();
var fairA = $('#fairA').val();
var fairB = $('#fairB').val();
var fairC = $('#fairC').val();
var fairD = $('#fairD').val();
var fairE = $('#fairE').val();
var fairF = $('#fairF').val();
var fairG = $('#fairG').val();
var poorA = $('#poorA').val();
var poorB = $('#poorB').val();
var poorC = $('#poorC').val();
var poorD = $('#poorD').val();
var poorE = $('#poorE').val();
var poorF = $('#poorF').val();
var poorG = $('#poorG').val();
var vpoorA = $('#vpoorA').val();
var vpoorB = $('#vpoorB').val();
var vpoorC = $('#vpoorC').val();
var vpoorD = $('#vpoorD').val();
var vpoorE = $('#vpoorE').val();
var vpoorF = $('#vpoorF').val();
var vpoorG = $('#vpoorG').val();
var totalA = $('#totalA').val();
var totalB = $('#totalB').val();
var totalC = $('#totalC').val();
var totalD = $('#totalD').val();
var totalE = $('#totalE').val();
var totalF = $('#totalF').val();
var totalG = $('#totalG').val();
var excellentH = $('#excellentH').val();
var excellentI = $('#excellentI').val();
var excellentJ = $('#excellentJ').val();
var excellentK = $('#excellentK').val();
var excellentL = $('#excellentL').val();
var excellentM = $('#excellentM').val();
var excellentN = $('#excellentN').val();
var excellentO = $('#excellentO').val();
var excellentP = $('#excellentP').val();
var excellentQ = $('#excellentQ').val();
var goodH = $('#goodH').val();
var goodI = $('#goodI').val();
var goodJ = $('#goodJ').val();
var goodK = $('#goodK').val();
var goodL = $('#goodL').val();
var goodM = $('#goodM').val();
var goodN = $('#goodN').val();
var goodO = $('#goodO').val();
var goodP = $('#goodP').val();
var goodQ = $('#goodQ').val();
var fairH = $('#fairH').val();
var fairI = $('#fairI').val();
var fairJ = $('#fairJ').val();
var fairK = $('#fairK').val();
var fairL = $('#fairL').val();
var fairM = $('#fairM').val();
var fairN = $('#fairN').val();
var fairO = $('#fairO').val();
var fairP = $('#fairP').val();
var fairQ = $('#fairQ').val();
var poorH = $('#poorH').val();
var poorI = $('#poorI').val();
var poorJ = $('#poorJ').val();
var poorK = $('#poorK').val();
var poorL = $('#poorL').val();
var poorM = $('#poorM').val();
var poorN = $('#poorN').val();
var poorO = $('#poorO').val();
var poorP = $('#poorP').val();
var poorQ = $('#poorQ').val();
var vpoorH = $('#vpoorH').val();
var vpoorI = $('#vpoorI').val();
var vpoorJ = $('#vpoorJ').val();
var vpoorK = $('#vpoorK').val();
var vpoorL = $('#vpoorL').val();
var vpoorM = $('#vpoorM').val();
var vpoorN = $('#vpoorN').val();
var vpoorO = $('#vpoorO').val();
var vpoorP = $('#vpoorP').val();
var vpoorQ = $('#vpoorQ').val();
var totalH = $('#totalH').val();
var totalI = $('#totalI').val();
var totalJ = $('#totalJ').val();
var totalK = $('#totalK').val();
var totalL = $('#totalL').val();
var totalM = $('#totalM').val();
var totalN = $('#totalN').val();
var totalO = $('#totalO').val();
var totalP = $('#totalP').val();
var totalQ = $('#totalQ').val();
var dataSet2 = [
["<strong>CLASS:</strong><strong id='table-header'>"+ eval_class +"</strong>"," "," "," "," "," "," "],
["<strong>SUBJECT: </strong><strong id='table-header'>"+ eval_subj +"</strong>"," "," "," "," "," "," "],
["<strong>PROGRAM: </strong><strong id='table-header'>"+ eval_program +"</strong>"," "," "," "," "," "," "],
["<strong>SECTION: </strong><strong id='table-header'>"+ eval_section +"</strong>"," "," "," "," "," "," "],
["<strong>BRANCH: </strong><strong id='table-header'>"+ eval_branch +"</strong>"," "," "," "," "," "," "],
["<strong>NAME OF REVIEWER: </strong><strong id='table-header'>"+ eval_lecturer +"</strong>"," "," "," "," "," "," "],
["<strong>DATE OF LECTURE: </strong><strong id='table-header'>"+ eval_date +"</strong>"," "," "," "," "," "," "],
["<strong>NAME OF REVIEW AMBASSADOR: </strong><strong id='table-header'>"+ eval_ambassador +"</strong>"," "," "," "," "," "," "],
[" "," "," "," "," "," "," "],
["","<strong>EXCELLENT</strong>","<strong>GOOD</strong>","<strong>FAIR</strong>","<strong>POOR</strong>","<strong>VERY POOR</strong>","<strong>TOTAL</strong>"],
["<strong>CONTENT</strong>"," "," "," "," "," "," "],
["Comprehensiveness – shows knowledge of the subject matter",excellentA,goodA,fairA,poorA,vpoorA,totalA],
["Substantiveness – delivers enough and substantial concepts",excellentB,goodB,fairB,poorB,vpoorB,totalB],
["Relevance – provides topics that are relevant to the actual exam",excellentC,goodC,fairC,poorC,vpoorC,totalC],
["Organization – executes the topic in an organized manner",excellentD,goodD,fairD,poorD,vpoorD,totalD],
["<strong>MASTERY</strong>"," "," "," "," "," "," "],
["Authority – exhibits confidence and authority in presenting the topic",excellentE,goodE,fairE,poorE,vpoorE,totalE],
["Ability to Answer Questions – able to answer students questions satisfactorily",excellentF,goodF,fairF,poorF,vpoorF,totalF],
["Preparedness – well-prepared to discuss the lecture",excellentG,goodG,fairG,poorG,vpoorG,totalG],
["<strong>DELIVERY</strong>"," "," "," "," "," "," "],
["Enthusiasm - shows energy that stimulates participation",excellentH,goodH,fairH,poorH,vpoorH,totalH],
["Pacing – moves to one topic to another understandable to the students",excellentI,goodI,fairI,poorI,vpoorI,totalI],
["Voice Projection –has effetive and clear voice",excellentJ,goodJ,fairJ,poorJ,vpoorJ,totalJ],
["Approach/ Strategy – has dynamic technique in discussing",excellentK,goodK,fairK,poorK,vpoorK,totalK],
["<strong>MOTIVATIONAL</strong>"," "," "," "," "," "," "],
["Inspiration – inspires me to strive harder and makes the topic more interesting",excellentL,goodL,fairL,poorL,vpoorL,totalL],
["Self-Esteem – boosts me to be a better person",excellentM,goodM,fairM,poorM,vpoorM,totalM],
["Authenticity – shows sincerity and authenticity in presenting himself/herself",excellentN,goodN,fairN,poorN,vpoorN,totalN],
["<strong>IMAGING</strong>"," "," "," "," "," "," "],
["Appearance & Grooming – shows neatness, proper posture, and an aesthetic appeal",excellentO,goodO,fairO,poorO,vpoorO,totalO],
["Character & Personality - shows uniqueness and carried himself/herself very well",excellentP,goodP,fairP,poorP,vpoorP,totalP],
["Impact – has stage presence, charisma, and a contagious aura",excellentQ,goodQ,fairQ,poorQ,vpoorQ,totalQ],
];


$('#lecturers').dataTable().fnClearTable();
    $('#lecturers').dataTable().fnDestroy();

    var table = $('#lecturers').DataTable( {
		data: dataSet2,
		columns: [
            { title: "" },
            { title: "" },
            { title: "" },
            { title: "" },
            { title: "" },
            { title: "" },
            { title: "" }
        ],

		responsive: false,
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		],
		"order": false,
		"paging": false,
		"searching": false,
		exportOptions : {
            columns: ':visible'
        },

		
	} );

$('.form-control-sm').val(eval_lecturer +" "+eval_program+" "+eval_class+" "+eval_subj+" "+eval_section+" "+eval_date).keyup();
     });
