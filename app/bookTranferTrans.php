<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bookTranferTrans extends Model
{
    protected $fillable = [

        'book_date',
        'book_branchSender',
        'book_branchReciever',
        'book_remarks'
    ];
}
