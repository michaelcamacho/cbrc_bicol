<?php

namespace App\Model\Daet;

use Illuminate\Database\Eloquent\Model;

class DaetReceivable extends Model
{
    protected $fillable = [
        'name',
        'program',
        'contact_no',
        'season',
        'balance',

    ];
}
