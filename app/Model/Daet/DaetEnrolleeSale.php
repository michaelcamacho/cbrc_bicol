<?php

namespace App\Model\Daet;

use Illuminate\Database\Eloquent\Model;

class DaetEnrolleeSale extends Model
{
    protected $fillable = [
    	'cash_sales'
    ];
}
