<?php

namespace App\Model\Daet;

use Illuminate\Database\Eloquent\Model;

class DaetScoreCards extends Model
{
    protected $fillable = [
      'date',
      'year',
      'season',

      'beed',
      'math',
      'tle',
      'english',
      'filipino',
      'biosci',
      'mapeh',
      'values',
      'afa',
      'ufo',

      'nles_retakers','nles_1stTimers',

      'crims_retakers','crims_1stTimers',

      'civils_retakers','civils_1stTimers',

      'psycs_retakers','psycs_1stTimers',

      'nclexes_retakers','nclexes_1stTimers',

      'ielts_retakers','ielts_1stTimers',

      'socials_retakers','socials_1stTimers',

      'agris_retakers','agris_1stTimers',

      'mids_retakers','mids_1stTimers',

      'onlines_retakers','onlines_1stTimers',

      'major',
    ];

}
