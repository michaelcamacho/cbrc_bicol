<?php

namespace App\Model\Daet;

use Illuminate\Database\Eloquent\Model;

class DaetDiscount extends Model
{
    protected $fillable = [
        'branch',
        'program',
        'category',
        'discount_category',
        'discount_amount',

    ];

}
