<?php

namespace App\Model\Daet;

use Illuminate\Database\Eloquent\Model;

class DaetBooksInventorie extends Model
{
    protected $fillable = [
    	'branch',
        'program',
        'book_title',
        'price',
        'available',
        
    ];
}
