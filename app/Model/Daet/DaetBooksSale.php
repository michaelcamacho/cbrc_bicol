<?php

namespace App\Model\Daet;

use Illuminate\Database\Eloquent\Model;

class DaetBooksSale extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'name',
        'program',
        'book_title',
        'amount',
        
    ];
}
