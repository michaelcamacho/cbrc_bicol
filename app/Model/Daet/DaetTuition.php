<?php

namespace App\Model\Daet;

use Illuminate\Database\Eloquent\Model;

class DaetTuition extends Model
{
    protected $fillable = [
        'program',
        'branch',
        'category',
        'tuition_fee',
        'season',
        'year',
        'facilitation_fee',

    ];
}
