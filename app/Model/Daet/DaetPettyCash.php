<?php

namespace App\Model\Daet;

use Illuminate\Database\Eloquent\Model;

class DaetPettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
