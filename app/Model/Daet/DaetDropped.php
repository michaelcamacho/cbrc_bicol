<?php

namespace App\Model\Daet;

use Illuminate\Database\Eloquent\Model;

class DaetDropped extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'branch',
        'program',
        'school',
        'email',
        'contact_no',

    ];

}
