<?php

namespace App\Model\Daet;

use Illuminate\Database\Eloquent\Model;

class DaetRemit extends Model
{
    protected $fillable = [
        'date',
        'category',
        'season',
        'amount',
        'remarks',
        'author',

    ];
}
