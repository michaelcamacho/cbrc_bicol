<?php

namespace App\Model\Daet;

use Illuminate\Database\Eloquent\Model;

class DaetScholar extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'program',
        'school',
        'branch',
        'contact_no',

    ];

}
