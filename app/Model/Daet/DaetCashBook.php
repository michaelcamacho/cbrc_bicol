<?php

namespace App\Model\Daet;

use Illuminate\Database\Eloquent\Model;

class DaetCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
