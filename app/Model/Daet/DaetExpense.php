<?php

namespace App\Model\Daet;

use Illuminate\Database\Eloquent\Model;

class DaetExpense extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'program',
        'category',
        'sub_category',
        'amount',
        'remarks',
        'author',

    ];
}
