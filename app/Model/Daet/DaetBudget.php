<?php

namespace App\Model\Daet;

use Illuminate\Database\Eloquent\Model;

class DaetBudget extends Model
{
    protected $fillable = [
        'date',
        'amount',
        'category',
        'remarks',
        
    ];
}
