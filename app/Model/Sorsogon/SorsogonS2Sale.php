<?php

namespace App\Model\Sorsogon;

use Illuminate\Database\Eloquent\Model;

class SorsogonS2Sale extends Model
{
    protected $fillable = [
    	'date',
        'name',
        'program',
        'category',
        'discount_category',
        'tuition_fee',
        'facilitation_fee',
        'discount',
        'amount_paid',
        'balance',
        'year',

    ];
}
