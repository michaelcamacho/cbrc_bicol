<?php

namespace App\Model\Sorsogon;

use Illuminate\Database\Eloquent\Model;

class SorsogonReservation extends Model
{
	protected $fillable = [
    		'enrollee_id',
            'name',
            'branch',
            'program',
            'category',
            'discount_amount',
            'discount_category',
            'prog',
            'school',
            'email',
            'contact_no',
            'reservation_fee',
        ];
}
