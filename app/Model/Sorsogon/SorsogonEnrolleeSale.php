<?php

namespace App\Model\Sorsogon;

use Illuminate\Database\Eloquent\Model;

class SorsogonEnrolleeSale extends Model
{
    protected $fillable = [
    	'cash_sales'
    ];
}
