<?php

namespace App\Model\Sorsogon;

use Illuminate\Database\Eloquent\Model;

class SorsogonCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
