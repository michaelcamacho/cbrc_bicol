<?php

namespace App\Model\Sorsogon;

use Illuminate\Database\Eloquent\Model;

class SorsogonBudget extends Model
{
    protected $fillable = [
        'date',
        'amount',
        'category',
        'remarks',
        
    ];
}
