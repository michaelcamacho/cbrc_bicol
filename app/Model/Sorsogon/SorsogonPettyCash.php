<?php

namespace App\Model\Sorsogon;

use Illuminate\Database\Eloquent\Model;

class SorsogonPettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
