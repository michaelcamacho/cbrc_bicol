<?php

namespace App\Model\Sorsogon;

use Illuminate\Database\Eloquent\Model;

class SorsogonExpense extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'program',
        'category',
        'sub_category',
        'amount',
        'remarks',
        'author',

    ];
}
