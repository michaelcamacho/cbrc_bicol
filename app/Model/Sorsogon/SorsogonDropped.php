<?php

namespace App\Model\Sorsogon;

use Illuminate\Database\Eloquent\Model;

class SorsogonDropped extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'branch',
        'program',
        'school',
        'email',
        'contact_no',

    ];

}
