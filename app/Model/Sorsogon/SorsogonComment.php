<?php

namespace App\Model\Sorsogon;

use Illuminate\Database\Eloquent\Model;

class SorsogonComment extends Model
{
    protected $fillable = [
    	'name',
		'lecturer',
		'like',
		'dislike',
		'comment',
		'branch',
		'program',
		'section',
		'class',
		'subject',
		'date',
    ];
}
