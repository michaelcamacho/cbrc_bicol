<?php

namespace App\Model\Legaspi;

use Illuminate\Database\Eloquent\Model;

class LegaspiCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
