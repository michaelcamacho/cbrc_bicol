<?php

namespace App\Model\Legaspi;

use Illuminate\Database\Eloquent\Model;

class LegaspiReceivable extends Model
{
    protected $fillable = [
        'name',
        'program',
        'contact_no',
        'season',
        'balance',

    ];
}
