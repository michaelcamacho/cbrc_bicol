<?php

namespace App\Model\Legaspi;

use Illuminate\Database\Eloquent\Model;

class LegaspiBooksInventorie extends Model
{
    protected $fillable = [
    	'branch',
        'program',
        'book_title',
        'price',
        'available',
        
    ];
}
