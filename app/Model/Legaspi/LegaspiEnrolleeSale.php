<?php

namespace App\Model\Legaspi;

use Illuminate\Database\Eloquent\Model;

class LegaspiEnrolleeSale extends Model
{
    protected $fillable = [
    	'cash_sales'
    ];
}
