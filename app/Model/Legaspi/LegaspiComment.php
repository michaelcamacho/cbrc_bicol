<?php

namespace App\Model\Legaspi;

use Illuminate\Database\Eloquent\Model;

class LegaspiComment extends Model
{
    protected $fillable = [
    	'name',
		'lecturer',
		'like',
		'dislike',
		'comment',
		'branch',
		'program',
		'section',
		'class',
		'subject',
		'date',
    ];
}
