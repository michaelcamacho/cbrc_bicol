<?php

namespace App\Model\Legaspi;

use Illuminate\Database\Eloquent\Model;

class LegaspiTuition extends Model
{
    protected $fillable = [
        'program',
        'branch',
        'category',
        'tuition_fee',
        'season',
        'year',
        'facilitation_fee',

    ];
}
