<?php

namespace App\Model\Legaspi;

use Illuminate\Database\Eloquent\Model;

class LegaspiPettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
