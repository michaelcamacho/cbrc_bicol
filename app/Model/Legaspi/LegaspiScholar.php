<?php

namespace App\Model\Legaspi;

use Illuminate\Database\Eloquent\Model;

class LegaspiScholar extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'program',
        'school',
        'branch',
        'contact_no',

    ];

}
