<?php

namespace App\Model\Legaspi;

use Illuminate\Database\Eloquent\Model;

class LegaspiBooksSale extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'name',
        'program',
        'book_title',
        'amount',
        
    ];
}
