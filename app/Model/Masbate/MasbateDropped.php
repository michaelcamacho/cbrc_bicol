<?php

namespace App\Model\Masbate;

use Illuminate\Database\Eloquent\Model;

class MasbateDropped extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'branch',
        'program',
        'school',
        'email',
        'contact_no',

    ];

}
