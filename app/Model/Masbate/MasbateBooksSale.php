<?php

namespace App\Model\Masbate;

use Illuminate\Database\Eloquent\Model;

class MasbateBooksSale extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'name',
        'program',
        'book_title',
        'amount',
        
    ];
}
