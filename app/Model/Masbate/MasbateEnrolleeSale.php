<?php

namespace App\Model\Masbate;

use Illuminate\Database\Eloquent\Model;

class MasbateEnrolleeSale extends Model
{
    protected $fillable = [
    	'cash_sales'
    ];
}
