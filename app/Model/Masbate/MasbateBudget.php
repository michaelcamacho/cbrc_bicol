<?php

namespace App\Model\Masbate;

use Illuminate\Database\Eloquent\Model;

class MasbateBudget extends Model
{
    protected $fillable = [
        'date',
        'amount',
        'category',
        'remarks',
        
    ];
}
