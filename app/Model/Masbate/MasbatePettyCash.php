<?php

namespace App\Model\Masbate;

use Illuminate\Database\Eloquent\Model;

class MasbatePettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
