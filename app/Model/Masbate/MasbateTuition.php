<?php

namespace App\Model\Masbate;

use Illuminate\Database\Eloquent\Model;

class MasbateTuition extends Model
{
    protected $fillable = [
        'program',
        'branch',
        'category',
        'tuition_fee',
        'season',
        'year',
        'facilitation_fee',

    ];
}
