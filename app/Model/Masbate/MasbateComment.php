<?php

namespace App\Model\Masbate;

use Illuminate\Database\Eloquent\Model;

class MasbateComment extends Model
{
    protected $fillable = [
    	'name',
		'lecturer',
		'like',
		'dislike',
		'comment',
		'branch',
		'program',
		'section',
		'class',
		'subject',
		'date',
    ];
}
