<?php

namespace App\Model\Masbate;

use Illuminate\Database\Eloquent\Model;

class MasbateScholar extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'program',
        'school',
        'branch',
        'contact_no',

    ];

}
