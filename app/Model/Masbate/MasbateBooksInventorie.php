<?php

namespace App\Model\Masbate;

use Illuminate\Database\Eloquent\Model;

class MasbateBooksInventorie extends Model
{
    protected $fillable = [
    	'branch',
        'program',
        'book_title',
        'price',
        'available',
        
    ];
}
