<?php

namespace App\Model\Masbate;

use Illuminate\Database\Eloquent\Model;

class MasbateS1Sale extends Model
{
    protected $fillable = [
    	'date',
        'name',
        'program',
        'category',
        'discount_category',
        'tuition_fee',
        'facilitation_fee',
        'discount',
        'amount_paid',
        'balance',
        'year',

    ];
}
