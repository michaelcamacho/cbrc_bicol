<?php

namespace App\Model\Masbate;

use Illuminate\Database\Eloquent\Model;

class MasbateExpense extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'program',
        'category',
        'sub_category',
        'amount',
        'remarks',
        'author',

    ];
}
