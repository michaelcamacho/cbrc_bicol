<?php

namespace App\Model\Masbate;

use Illuminate\Database\Eloquent\Model;

class MasbateCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
