<?php

namespace App\Model\Masbate;

use Illuminate\Database\Eloquent\Model;

class MasbateRemit extends Model
{
    protected $fillable = [
        'date',
        'category',
        'season',
        'amount',
        'remarks',
        'author',

    ];
}
