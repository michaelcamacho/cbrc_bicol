<?php

namespace App\Model\Masbate;

use Illuminate\Database\Eloquent\Model;

class MasbateDiscount extends Model
{
    protected $fillable = [
        'branch',
        'program',
        'category',
        'discount_category',
        'discount_amount',

    ];

}
