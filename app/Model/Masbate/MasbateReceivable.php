<?php

namespace App\Model\Masbate;

use Illuminate\Database\Eloquent\Model;

class MasbateReceivable extends Model
{
    protected $fillable = [
        'name',
        'program',
        'contact_no',
        'season',
        'balance',

    ];
}
