<?php

namespace App\Model\Naga;

use Illuminate\Database\Eloquent\Model;

class NagaCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
