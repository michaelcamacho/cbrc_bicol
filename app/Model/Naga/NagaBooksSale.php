<?php

namespace App\Model\Naga;

use Illuminate\Database\Eloquent\Model;

class NagaBooksSale extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'name',
        'program',
        'book_title',
        'amount',
        
    ];
}
