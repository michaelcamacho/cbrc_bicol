<?php

namespace App\Model\Naga;

use Illuminate\Database\Eloquent\Model;

class NagaRemit extends Model
{
    protected $fillable = [
        'date',
        'category',
        'season',
        'amount',
        'remarks',
        'author',

    ];
}
