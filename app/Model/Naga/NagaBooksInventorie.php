<?php

namespace App\Model\Naga;

use Illuminate\Database\Eloquent\Model;

class NagaBooksInventorie extends Model
{
    protected $fillable = [
    	'branch',
        'program',
        'book_title',
        'price',
        'available',
        
    ];
}
