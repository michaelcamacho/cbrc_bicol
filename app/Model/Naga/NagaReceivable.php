<?php

namespace App\Model\Naga;

use Illuminate\Database\Eloquent\Model;

class NagaReceivable extends Model
{
    protected $fillable = [
        'name',
        'program',
        'contact_no',
        'season',
        'balance',

    ];
}
