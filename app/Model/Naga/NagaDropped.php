<?php

namespace App\Model\Naga;

use Illuminate\Database\Eloquent\Model;

class NagaDropped extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'branch',
        'program',
        'school',
        'email',
        'contact_no',

    ];

}
