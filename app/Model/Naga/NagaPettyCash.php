<?php

namespace App\Model\Naga;

use Illuminate\Database\Eloquent\Model;

class NagaPettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
