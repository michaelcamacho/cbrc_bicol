<?php

namespace App\Model\Naga;

use Illuminate\Database\Eloquent\Model;

class NagaBudget extends Model
{
    protected $fillable = [
        'date',
        'amount',
        'category',
        'remarks',
        
    ];
}
