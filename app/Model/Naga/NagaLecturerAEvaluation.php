<?php

namespace App\Model\Naga;

use Illuminate\Database\Eloquent\Model;

class NagaLecturerAEvaluation extends Model
{
	protected $fillable =[
    'date',
	'lecturer',
	'branch',
	'program',
	'section',
	'class',
	'subject',
	'aka_class',
	'aka_subject',
	'review_ambassador',
	'excellentA',
	'goodA',
	'fairA',
	'poorA',
	'verypoorA',
	'excellentB',
	'goodB',
	'fairB',
	'poorB',
	'verypoorB',
	'excellentC',
	'goodC',
	'fairC',
	'poorC',
	'verypoorC',
	'excellentD',
	'goodD',
	'fairD',
	'poorD',
	'verypoorD',
	'excellentE',
	'goodE',
	'fairE',
	'poorE',
	'verypoorE',
	'excellentF',
	'goodF',
	'fairF',
	'poorF',
	'verypoorF',
	'excellentG',
	'goodG',
	'fairG',
	'poorG',
	'verypoorG',
	];
}
