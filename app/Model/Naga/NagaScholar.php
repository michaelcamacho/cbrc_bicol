<?php

namespace App\Model\Naga;

use Illuminate\Database\Eloquent\Model;

class NagaScholar extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'program',
        'school',
        'branch',
        'contact_no',

    ];

}
