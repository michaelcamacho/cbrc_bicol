<?php

namespace App\Model\Naga;

use Illuminate\Database\Eloquent\Model;

class NagaExpense extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'program',
        'category',
        'sub_category',
        'amount',
        'remarks',
        'author',

    ];
}
