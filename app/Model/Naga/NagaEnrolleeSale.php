<?php

namespace App\Model\Naga;

use Illuminate\Database\Eloquent\Model;

class NagaEnrolleeSale extends Model
{
    protected $fillable = [
    	'cash_sales'
    ];
}
