<?php

namespace App\Model\Naga;

use Illuminate\Database\Eloquent\Model;

class NagaDiscount extends Model
{
    protected $fillable = [
        'branch',
        'program',
        'category',
        'discount_category',
        'discount_amount',

    ];

}
