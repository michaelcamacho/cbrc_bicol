<?php

namespace App\Model\Naga;

use Illuminate\Database\Eloquent\Model;

class NagaTuition extends Model
{
    protected $fillable = [
        'program',
        'branch',
        'category',
        'tuition_fee',
        'season',
        'year',
        'facilitation_fee',

    ];
}
