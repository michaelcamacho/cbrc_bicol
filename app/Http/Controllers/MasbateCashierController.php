<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use App\Branch;
use App\Model\Masbate\MasbateAgri;
use App\Model\Masbate\MasbateBookCash;
use App\Model\Masbate\MasbateBooksInventorie;
use App\Model\Masbate\MasbateBooksSale;
use App\Model\Masbate\MasbateBudget;
use App\Model\Masbate\MasbateCivil;
use App\Model\Masbate\MasbateCrim;
use App\Model\Masbate\MasbateDiscount;
use App\Model\Masbate\MasbateDropped;
use App\Model\Masbate\MasbateExpense;
use App\Model\Masbate\MasbateIelt;
use App\Model\Masbate\MasbateLet;
use App\Model\Masbate\MasbateMid;
use App\Model\Masbate\MasbateNclex;
use App\Model\Masbate\MasbateNle;
use App\Model\Masbate\MasbateOnline;
use App\Model\Masbate\MasbatePsyc;
use App\Model\Masbate\MasbateReceivable;
use App\Model\Masbate\MasbateS1Sale;
use App\Model\Masbate\MasbateS2Sale;
use App\Model\Masbate\MasbateS1Cash;
use App\Model\Masbate\MasbateS2Cash;
use App\Model\Masbate\MasbateScholar;
use App\Model\Masbate\MasbateSocial;
use App\Model\Masbate\MasbateTuition;
use App\Model\Masbate\MasbatePettyCash;
use App\Model\Masbate\MasbateRemit;
use App\Model\Masbate\MasbateReservation;
use App\Model\Masbate\MasbateEmployee;
use App\Model\Masbate\MasbateScoreCards;
use App\Model\Masbate\MasbateBookTransfer;

use App\Model\Masbate\MasbateLecturerAEvaluation;
use App\Model\Masbate\MasbateLecturerBEvaluation;
use App\Model\Masbate\MasbateComment;

use App\facilitation;
use App\bookTranferTrans;
use App\Expense;
use App\Program;
use App\Subject;
use App\Section;
use Alert;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

use Auth;
use User;
use DB;
use File;


class MasbateCashierController extends Controller
{
    private $branch = "Masbate";

    private $sbranch = "masbate";

public function __construct()
    {

         $this->middleware('role:masbate_cashier');
        
    }


public function book_payment(){

    $branch=$this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view('member.book-payment')->with('branch',$branch)->with('date',$date)->with('program',$program);
}
           
    
public function fetch_book(){

        $program = Input::get('program');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $book = MasbateBooksInventorie::where('program','=', $program)->get();
        return response()->json($book);
    }

public function fetch_book_price(){

    $program = Input::get('program');
    $book_title = Input::get('book_title');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        }
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $avai = MasbateBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->value('available');

        if($avai == 0){
            $price[] = array('price' => '0' , );
        return response()->json($price);

        }
        else{
            $price = MasbateBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->get();
                return response()->json($price);
        }


}
public function insert_book_payment(Request $request){

 $input = $request->except(['_token']);


 
$validatedData = $request->validate([
    'name' => 'required'
]);

 $program = $input['program'];
 $price = $input['price'];
 $book_title = $input['book_title'];

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 
        if($input['total_amount'] <= $input['amount_paid'] || $input['amount_paid'] == 0 || $input['amount_paid'] == 0.00 )
        {

        if(isset($input['book_title'])){
            foreach ($input['book_title'] as $book => $value ) {

             MasbateBooksSale::create([
                'date'      => $input['date'],
                'branch'    => $this->branch,
                'name'      => $input['name'],
                'program'   => $program,
                'book_title'=> $value,
                'amount'    => $price[$book],
             ]);
        
        $avai = MasbateBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->value('available');

        $new = $avai - 1;

        MasbateBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->update([

            'available' => $new,
        ]);     
        }
        $book_cash = MasbateBookCash::where('id','=','1')->value('cash');
        $new_book_cash = $book_cash + $input['amount_paid'];

        MasbateBookCash::where('id','=','1')->update([
            'cash'  => $new_book_cash,
        ]);
            }

            Alert::success('Success!', 'New book payment has been submitted.');
         return redirect ('masbate-cashier/book-payment');
                }

        else{
            Alert::error('Failed!', 'Payment is insufficient, please try again');
            return redirect ('masbate-cashier/book-payment');
        }
}
public function books_table(){

$book = MasbateBooksInventorie::all();
$sale = MasbateBooksSale::all();

return view ('member.books')->with('book',$book)->with('sale',$sale);

}

    }