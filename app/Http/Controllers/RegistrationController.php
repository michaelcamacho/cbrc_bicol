<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Branch;

use DB;
use Auth;
use App\User;
use App\Role;
use App\Program;

use Alert;

class RegistrationController extends Controller
{
    public function __construct()
	{
		//$this->middleware('auth');
		
	}

	public function index(){

		$branch = Branch::all();
        $program = Program::all();

		return view ('registration')->with('branch',$branch)->with('program',$program);
	}

	public function register(Request $request){

		$input = $request->except(['_token']);
        $branch = $input['branch'];
        $course = $input['program'];

        if($course == 'lets'){
            $course = 'LET';
        }
        if($course == 'nles'){
            $course = 'NLE';
        }
        if($course == 'crims'){
            $course = 'Criminology';
        }
        if($course == 'civils'){
            $course = 'Civil Service';
        }

        if($course == 'psycs'){
            $course = 'Psychometrician';
        }
        if($course == 'nclexs'){
            $course = 'NCLEX';
        }
        if($course == 'ielts'){
            $course = 'IELTS';
        }
        if($course == 'socials'){
            $course = 'Social Work';
        }
        if($course == 'agris'){
            $course = 'Agriculture';
        }
        if($course == 'mids'){
            $course = 'Midwifery';
        }

        if($course == 'onlines'){
            $course = 'Online Only';
        }

    	$program = $branch.'_'.$input['program'];

        $lastname = strtoupper($input['last_name']);
        $firstname = strtoupper($input['first_name']);
        $middlename = strtoupper($input['middle_name']);

        $existent = DB::table($program)->where('last_name','=',$lastname)->where('first_name','=',$firstname)->where('middle_name','=',$middlename)->first();

        if ($branch == 'novaliches'){
            $branch = 'Novaliches';
        }

        if($existent != null){
            return response()->json([
            'success' => true,
            'message' => 'This name is already registered.',
            'check' => 'error',
            'title' => 'Failed!',
        ]);
            
        }
        if($existent == null){
 		DB::table($program)->insert([

            'last_name'     => strtoupper($input['last_name']),
            'first_name'    => strtoupper($input['first_name']),
            'middle_name'   => strtoupper($input['middle_name']),
            'course'        => $course,
            'major'         => $input['major'],
            'program'       => $input['program'],
            'school'        => $input['school'],
            'take'          => $input['take'],
            'branch'        => $branch,
            'birthdate'     => $input['birthdate'],
            'contact_no'    => $input['contact_no'],
            'email'         => $input['email'],
            'address'       => $input['address'],
            'contact_person'=> $input['contact_person'],
            'contact_details'=> $input['contact_details'],
            'registration'  => 'Online',
            'created_at'    => date('Y-m-d'),
        ]);
 		return response()->json([
            'success' => true,
            'message' => 'Congtatulation! You are now registered.',
            'check' => 'success',
            'title' => 'Success!',

        ]);
        echo "<script>window.close();</script>";
    }
	}

public function thank_you(){

    return view ('/thank-you');
}
}
