<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Branch;
use App\Program;

/* Daet */
use App\Model\Daet\DaetScoreCards;
use App\Model\Daet\DaetAgri;
use App\Model\Daet\DaetBooksInventorie;
use App\Model\Daet\DaetBooksSale;
use App\Model\Daet\DaetBudget;
use App\Model\Daet\DaetCivil;
use App\Model\Daet\DaetCrim;
use App\Model\Daet\DaetDiscount;
use App\Model\Daet\DaetDropped;
use App\Model\Daet\DaetExpense;
use App\Model\Daet\DaetIelt;
use App\Model\Daet\DaetLet;
use App\Model\Daet\DaetNclex;
use App\Model\Daet\DaetNle;
use App\Model\Daet\DaetPsyc;
use App\Model\Daet\DaetMid;
use App\Model\Daet\DaetOnline;
use App\Model\Daet\DaetReceivable;
use App\Model\Daet\DaetS1Sale;
use App\Model\Daet\DaetS2Sale;
use App\Model\Daet\DaetScholar;
use App\Model\Daet\DaetSocial;
use App\Model\Daet\DaetTuition;
use App\Model\Daet\DaetPettyCash;

/* Daet */


/* Naga */
use App\Model\Naga\NagaScoreCards;
use App\Model\Naga\NagaAgri;
use App\Model\Naga\NagaBooksInventorie;
use App\Model\Naga\NagaBooksSale;
use App\Model\Naga\NagaBudget;
use App\Model\Naga\NagaCivil;
use App\Model\Naga\NagaCrim;
use App\Model\Naga\NagaDiscount;
use App\Model\Naga\NagaDropped;
use App\Model\Naga\NagaExpense;
use App\Model\Naga\NagaIelt;
use App\Model\Naga\NagaLet;
use App\Model\Naga\NagaNclex;
use App\Model\Naga\NagaNle;
use App\Model\Naga\NagaPsyc;
use App\Model\Naga\NagaMid;
use App\Model\Naga\NagaOnline;
use App\Model\Naga\NagaReceivable;
use App\Model\Naga\NagaS1Sale;
use App\Model\Naga\NagaS2Sale;
use App\Model\Naga\NagaScholar;
use App\Model\Naga\NagaSocial;
use App\Model\Naga\NagaTuition;
use App\Model\Naga\NagaPettyCash;

/* Naga */

/* Masbate */
use App\Model\Masbate\MasbateScoreCards;
use App\Model\Masbate\MasbateAgri;
use App\Model\Masbate\MasbateBooksInventorie;
use App\Model\Masbate\MasbateBooksSale;
use App\Model\Masbate\MasbateBudget;
use App\Model\Masbate\MasbateCivil;
use App\Model\Masbate\MasbateCrim;
use App\Model\Masbate\MasbateDiscount;
use App\Model\Masbate\MasbateDropped;
use App\Model\Masbate\MasbateExpense;
use App\Model\Masbate\MasbateIelt;
use App\Model\Masbate\MasbateLet;
use App\Model\Masbate\MasbateNclex;
use App\Model\Masbate\MasbateNle;
use App\Model\Masbate\MasbatePsyc;
use App\Model\Masbate\MasbateMid;
use App\Model\Masbate\MasbateOnline;
use App\Model\Masbate\MasbateReceivable;
use App\Model\Masbate\MasbateS1Sale;
use App\Model\Masbate\MasbateS2Sale;
use App\Model\Masbate\MasbateScholar;
use App\Model\Masbate\MasbateSocial;
use App\Model\Masbate\MasbateTuition;
use App\Model\Masbate\MasbatePettyCash;


/* Masbate */

/* Legaspi */
use App\Model\Legaspi\LegaspiScoreCards;
use App\Model\Legaspi\LegaspiAgri;
use App\Model\Legaspi\LegaspiBooksInventorie;
use App\Model\Legaspi\LegaspiBooksSale;
use App\Model\Legaspi\LegaspiBudget;
use App\Model\Legaspi\LegaspiCivil;
use App\Model\Legaspi\LegaspiCrim;
use App\Model\Legaspi\LegaspiDiscount;
use App\Model\Legaspi\LegaspiDropped;
use App\Model\Legaspi\LegaspiExpense;
use App\Model\Legaspi\LegaspiIelt;
use App\Model\Legaspi\LegaspiLet;
use App\Model\Legaspi\LegaspiNclex;
use App\Model\Legaspi\LegaspiNle;
use App\Model\Legaspi\LegaspiPsyc;
use App\Model\Legaspi\LegaspiMid;
use App\Model\Legaspi\LegaspiOnline;
use App\Model\Legaspi\LegaspiReceivable;
use App\Model\Legaspi\LegaspiS1Sale;
use App\Model\Legaspi\LegaspiS2Sale;
use App\Model\Legaspi\LegaspiScholar;
use App\Model\Legaspi\LegaspiSocial;
use App\Model\Legaspi\LegaspiTuition;
use App\Model\Legaspi\LegaspiPettyCash;

/* Legaspi */


/* Sorsogon */
use App\Model\Sorsogon\SorsogonScoreCards;
use App\Model\Sorsogon\SorsogonAgri;
use App\Model\Sorsogon\SorsogonBooksInventorie;
use App\Model\Sorsogon\SorsogonBooksSale;
use App\Model\Sorsogon\SorsogonBudget;
use App\Model\Sorsogon\SorsogonCivil;
use App\Model\Sorsogon\SorsogonCrim;
use App\Model\Sorsogon\SorsogonDiscount;
use App\Model\Sorsogon\SorsogonDropped;
use App\Model\Sorsogon\SorsogonExpense;
use App\Model\Sorsogon\SorsogonIelt;
use App\Model\Sorsogon\SorsogonLet;
use App\Model\Sorsogon\SorsogonNclex;
use App\Model\Sorsogon\SorsogonNle;
use App\Model\Sorsogon\SorsogonPsyc;
use App\Model\Sorsogon\SorsogonMid;
use App\Model\Sorsogon\SorsogonOnline;
use App\Model\Sorsogon\SorsogonReceivable;
use App\Model\Sorsogon\SorsogonS1Sale;
use App\Model\Sorsogon\SorsogonS2Sale;
use App\Model\Sorsogon\SorsogonScholar;
use App\Model\Sorsogon\SorsogonSocial;
use App\Model\Sorsogon\SorsogonTuition;
use App\Model\Sorsogon\SorsogonPettyCash;

/* Sorsogon */

use App\facilitation;
use DB;
use Auth;
use App\User;
use App\Role;
use App\Major;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;


use App\Model\ExpenseSetup;
use App\Model\AdminSettings;

class AdminController extends Controller
{


public function __construct()
	{
		$this->middleware('role:admin');
		
	}


public function dashboard(){
		
		$this->daet_populateScoreCard();
		$this->naga_populateScoreCard();
		$this->masbate_populateScoreCard();
		$this->sorsogon_populateScoreCard();
		$this->legaspi_populateScoreCard();

	$sid = AdminSettings::where('Account','=','admin')->value('id');
	$setting = AdminSettings::findorfail($sid);



			
	$facilitation = facilitation::where('Year','=',$setting->Year)->where('branch','=',"Daet")->sum('facilitation');
	$daet_let_1=DaetS1Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_nle_1=DaetS1Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_crim_1=DaetS1Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_civil_1=DaetS1Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$daet_psyc_1=DaetS1Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_nclex_1=DaetS1Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$daet_ielts_1=DaetS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$daet_social_1=DaetS1Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_agri_1=DaetS1Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_mid_1=DaetS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_online_1=DaetS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_let_2=DaetS2Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_nle_2=DaetS2Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_crim_2=DaetS2Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_civil_2=DaetS2Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_psyc_2=DaetS2Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_nclex_2=DaetS2Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_ielts_2=DaetS2Sale::where('program','=','IELTS')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_social_2=DaetS2Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_agri_2=DaetS2Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_mid_2=DaetS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$daet_online_2=DaetS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');



	
	$facilitation = facilitation::where('Year','=',$setting->Year)->where('branch','=',"Naga")->sum('facilitation');
	$naga_let_1=NagaS1Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_nle_1=NagaS1Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_crim_1=NagaS1Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_civil_1=NagaS1Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$naga_psyc_1=NagaS1Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_nclex_1=NagaS1Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$naga_ielts_1=NagaS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$naga_social_1=NagaS1Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_agri_1=NagaS1Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_mid_1=NagaS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_online_1=NagaS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_let_2=NagaS2Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_nle_2=NagaS2Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_crim_2=NagaS2Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_civil_2=NagaS2Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_psyc_2=NagaS2Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_nclex_2=NagaS2Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_ielts_2=NagaS2Sale::where('program','=','IELTS')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_social_2=NagaS2Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_agri_2=NagaS2Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_mid_2=NagaS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$naga_online_2=NagaS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');


	
	$facilitation = facilitation::where('Year','=',$setting->Year)->where('branch','=',"Masbate")->sum('facilitation');
	$masbate_let_1=MasbateS1Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_nle_1=MasbateS1Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_crim_1=MasbateS1Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_civil_1=MasbateS1Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$masbate_psyc_1=MasbateS1Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_nclex_1=MasbateS1Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$masbate_ielts_1=MasbateS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$masbate_social_1=MasbateS1Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_agri_1=MasbateS1Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_mid_1=MasbateS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_online_1=MasbateS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_let_2=MasbateS2Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_nle_2=MasbateS2Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_crim_2=MasbateS2Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_civil_2=MasbateS2Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_psyc_2=MasbateS2Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_nclex_2=MasbateS2Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_ielts_2=MasbateS2Sale::where('program','=','IELTS')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_social_2=MasbateS2Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_agri_2=MasbateS2Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_mid_2=MasbateS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$masbate_online_2=MasbateS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');


	
	$facilitation = facilitation::where('Year','=',$setting->Year)->where('branch','=',"Sorsogon")->sum('facilitation');
	$sorsogon_let_1=SorsogonS1Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_nle_1=SorsogonS1Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_crim_1=SorsogonS1Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_civil_1=SorsogonS1Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$sorsogon_psyc_1=SorsogonS1Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_nclex_1=SorsogonS1Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$sorsogon_ielts_1=SorsogonS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$sorsogon_social_1=SorsogonS1Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_agri_1=SorsogonS1Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_mid_1=SorsogonS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_online_1=SorsogonS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_let_2=SorsogonS2Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_nle_2=SorsogonS2Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_crim_2=SorsogonS2Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_civil_2=SorsogonS2Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_psyc_2=SorsogonS2Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_nclex_2=SorsogonS2Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_ielts_2=SorsogonS2Sale::where('program','=','IELTS')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_social_2=SorsogonS2Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_agri_2=SorsogonS2Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_mid_2=SorsogonS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$sorsogon_online_2=SorsogonS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');



	
	$facilitation = facilitation::where('Year','=',$setting->Year)->where('branch','=',"Legaspi")->sum('facilitation');
	$legaspi_let_1=LegaspiS1Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_nle_1=LegaspiS1Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_crim_1=LegaspiS1Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_civil_1=LegaspiS1Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$legaspi_psyc_1=LegaspiS1Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_nclex_1=LegaspiS1Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$legaspi_ielts_1=LegaspiS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$legaspi_social_1=LegaspiS1Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_agri_1=LegaspiS1Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_mid_1=LegaspiS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_online_1=LegaspiS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_let_2=LegaspiS2Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_nle_2=LegaspiS2Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_crim_2=LegaspiS2Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_civil_2=LegaspiS2Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_psyc_2=LegaspiS2Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_nclex_2=LegaspiS2Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_ielts_2=LegaspiS2Sale::where('program','=','IELTS')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_social_2=LegaspiS2Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_agri_2=LegaspiS2Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_mid_2=LegaspiS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$legaspi_online_2=LegaspiS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');





		$total_s1 =   $daet_let_1 + $daet_nle_1 + $daet_crim_1 + $daet_civil_1 + $daet_psyc_1 + $daet_nclex_1 + $daet_ielts_1 + $daet_social_1 + $daet_agri_1 + $daet_mid_1 + $daet_online_1
			 		+ $naga_let_1 + $naga_nle_1 + $naga_crim_1 + $naga_civil_1 + $naga_psyc_1 + $naga_nclex_1 + $naga_ielts_1 + $naga_social_1 + $naga_agri_1 + $naga_mid_1 + $naga_online_1
					+ $masbate_let_1 + $masbate_nle_1 + $masbate_crim_1 + $masbate_civil_1 + $masbate_psyc_1 + $masbate_nclex_1 + $masbate_ielts_1 + $masbate_social_1 + $masbate_agri_1 + $masbate_mid_1 + $masbate_online_1
					+ $sorsogon_let_1 + $sorsogon_nle_1 + $sorsogon_crim_1 + $sorsogon_civil_1 + $sorsogon_psyc_1 + $sorsogon_nclex_1 + $sorsogon_ielts_1 + $sorsogon_social_1 + $sorsogon_agri_1 + $sorsogon_mid_1 + $sorsogon_online_1
					+ $legaspi_let_1 + $legaspi_nle_1 + $legaspi_crim_1 + $legaspi_civil_1 + $legaspi_psyc_1 + $legaspi_nclex_1 + $legaspi_ielts_1 + $legaspi_social_1 + $legaspi_agri_1 + $legaspi_mid_1 + $legaspi_online_1;
		
		$total_s2 =   $daet_let_2 + $daet_nle_2 + $daet_crim_2 + $daet_civil_2 + $daet_psyc_2 + $daet_nclex_2 + $daet_ielts_2 + $daet_social_2 + $daet_agri_2 + $daet_mid_2 + $daet_online_2
					+ $naga_let_2 + $naga_nle_2 + $naga_crim_2 + $naga_civil_2 + $naga_psyc_2 + $naga_nclex_2 + $naga_ielts_2 + $naga_social_2 + $naga_agri_2 + $naga_mid_2 + $naga_online_2
				    + $masbate_let_2 + $masbate_nle_2 + $masbate_crim_2 + $masbate_civil_2 + $masbate_psyc_2 + $masbate_nclex_2 + $masbate_ielts_2 + $masbate_social_2 + $masbate_agri_2 + $masbate_mid_2 + $masbate_online_2
				    + $sorsogon_let_2 + $sorsogon_nle_2 + $sorsogon_crim_2 + $sorsogon_civil_2 + $sorsogon_psyc_2 + $sorsogon_nclex_2 + $sorsogon_ielts_2 + $sorsogon_social_2 + $sorsogon_agri_2 + $sorsogon_mid_2 + $sorsogon_online_2
				    + $legaspi_let_2 + $legaspi_nle_2 + $legaspi_crim_2 + $legaspi_civil_2 + $legaspi_psyc_2 + $legaspi_nclex_2 + $legaspi_ielts_2 + $legaspi_social_2 + $legaspi_agri_2 + $legaspi_mid_2 + $legaspi_online_2;
		   
						
					$total_daet_s1 = $daet_let_1 + $daet_nle_1 + $daet_crim_1 + $daet_civil_1 + $daet_psyc_1 + $daet_nclex_1 +  $daet_ielts_1 + $daet_social_1 + $daet_agri_1 + $daet_mid_1 + $daet_online_1;
					$total_daet_s2 = $daet_let_2 + $daet_nle_2 + $daet_crim_2 + $daet_civil_2 + $daet_psyc_2 + $daet_nclex_2 +  $daet_ielts_2 + $daet_social_2 + $daet_agri_2 + $daet_mid_2 + $daet_online_2;
					   
					$total_naga_s1 = $naga_let_1 + $naga_nle_1 + $naga_crim_1 + $naga_civil_1 + $naga_psyc_1 + $naga_nclex_1 +  $naga_ielts_1 + $naga_social_1 + $naga_agri_1 + $naga_mid_1 + $naga_online_1;
					$total_naga_s2 = $naga_let_2 + $naga_nle_2 + $naga_crim_2 + $naga_civil_2 + $naga_psyc_2 + $naga_nclex_2 +  $naga_ielts_2 + $naga_social_2 + $naga_agri_2 + $naga_mid_2 + $naga_online_2;
					   
					$total_masbate_s1 = $masbate_let_1 + $masbate_nle_1 + $masbate_crim_1 + $masbate_civil_1 + $masbate_psyc_1 + $masbate_nclex_1 +  $masbate_ielts_1 + $masbate_social_1 + $masbate_agri_1 + $masbate_mid_1 + $masbate_online_1;
					$total_masbate_s2 = $masbate_let_2 + $masbate_nle_2 + $masbate_crim_2 + $masbate_civil_2 + $masbate_psyc_2 + $masbate_nclex_2 +  $masbate_ielts_2 + $masbate_social_2 + $masbate_agri_2 + $masbate_mid_2 + $masbate_online_2;
					   
					$total_sorsogon_s1 = $sorsogon_let_1 + $sorsogon_nle_1 + $sorsogon_crim_1 + $sorsogon_civil_1 + $sorsogon_psyc_1 + $sorsogon_nclex_1 +  $sorsogon_ielts_1 + $sorsogon_social_1 + $sorsogon_agri_1 + $sorsogon_mid_1 + $sorsogon_online_1;
					$total_sorsogon_s2 = $sorsogon_let_2 + $sorsogon_nle_2 + $sorsogon_crim_2 + $sorsogon_civil_2 + $sorsogon_psyc_2 + $sorsogon_nclex_2 +  $sorsogon_ielts_2 + $sorsogon_social_2 + $sorsogon_agri_2 + $sorsogon_mid_2 + $sorsogon_online_2;
					   
					$total_legaspi_s1 = $legaspi_let_1 + $legaspi_nle_1 + $legaspi_crim_1 + $legaspi_civil_1 + $legaspi_psyc_1 + $legaspi_nclex_1 +  $legaspi_ielts_1 + $legaspi_social_1 + $legaspi_agri_1 + $legaspi_mid_1 + $legaspi_online_1;
					$total_legaspi_s2 = $legaspi_let_2 + $legaspi_nle_2 + $legaspi_crim_2 + $legaspi_civil_2 + $legaspi_psyc_2 + $legaspi_nclex_2 +  $legaspi_ielts_2 + $legaspi_social_2 + $legaspi_agri_2 + $legaspi_mid_2 + $legaspi_online_2;
					   
					
			 
/* end total sale */



$daet_let = DaetLet::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$daet_nle = DaetNle::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$daet_crim = DaetCrim::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$daet_civil = DaetCivil::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$daet_psyc = DaetPsyc::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$daet_nclex = DaetNclex::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$daet_ielts = DaetIelt::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$daet_social = DaetSocial::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$daet_agri = DaetAgri::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$daet_mid = DaetMid::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$daet_online = DaetOnline::where('status','=','Enrolled')->count();

$total_daet = $daet_let + $daet_nle + $daet_crim + $daet_civil + $daet_psyc + $daet_nclex + $daet_ielts + $daet_social + $daet_agri + $daet_mid + $daet_online;




$naga_let = NagaLet::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$naga_nle = NagaNle::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$naga_crim = NagaCrim::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$naga_civil = NagaCivil::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$naga_psyc = NagaPsyc::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$naga_nclex = NagaNclex::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$naga_ielts = NagaIelt::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$naga_social = NagaSocial::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$naga_agri = NagaAgri::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$naga_mid = NagaMid::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$naga_online = NagaOnline::where('status','=','Enrolled')->count();

$total_naga = $naga_let + $naga_nle + $naga_crim + $naga_civil + $naga_psyc + $naga_nclex + $naga_ielts + $naga_social + $naga_agri + $naga_mid + $naga_online;





$masbate_let = MasbateLet::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$masbate_nle = MasbateNle::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$masbate_crim = MasbateCrim::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$masbate_civil = MasbateCivil::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$masbate_psyc = MasbatePsyc::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$masbate_nclex = MasbateNclex::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$masbate_ielts = MasbateIelt::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$masbate_social = MasbateSocial::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$masbate_agri = MasbateAgri::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$masbate_mid = MasbateMid::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$masbate_online = MasbateOnline::where('status','=','Enrolled')->count();

$total_masbate = $masbate_let + $masbate_nle + $masbate_crim + $masbate_civil + $masbate_psyc + $masbate_nclex + $masbate_ielts + $masbate_social + $masbate_agri + $masbate_mid + $masbate_online;




$sorsogon_let = SorsogonLet::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$sorsogon_nle = SorsogonNle::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$sorsogon_crim = SorsogonCrim::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$sorsogon_civil = SorsogonCivil::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$sorsogon_psyc = SorsogonPsyc::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$sorsogon_nclex = SorsogonNclex::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$sorsogon_ielts = SorsogonIelt::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$sorsogon_social = SorsogonSocial::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$sorsogon_agri = SorsogonAgri::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$sorsogon_mid = SorsogonMid::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$sorsogon_online = SorsogonOnline::where('status','=','Enrolled')->count();

$total_sorsogon = $sorsogon_let + $sorsogon_nle + $sorsogon_crim + $sorsogon_civil + $sorsogon_psyc + $sorsogon_nclex + $sorsogon_ielts + $sorsogon_social + $sorsogon_agri + $sorsogon_mid + $sorsogon_online;


$legaspi_let = LegaspiLet::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$legaspi_nle = LegaspiNle::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$legaspi_crim = LegaspiCrim::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$legaspi_civil = LegaspiCivil::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$legaspi_psyc = LegaspiPsyc::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$legaspi_nclex = LegaspiNclex::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$legaspi_ielts = LegaspiIelt::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$legaspi_social = LegaspiSocial::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$legaspi_agri = LegaspiAgri::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$legaspi_mid = LegaspiMid::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$legaspi_online = LegaspiOnline::where('status','=','Enrolled')->count();

$total_legaspi = $legaspi_let + $legaspi_nle + $legaspi_crim + $legaspi_civil + $legaspi_psyc + $legaspi_nclex + $legaspi_ielts + $legaspi_social + $legaspi_agri + $legaspi_mid + $legaspi_online;




$total_enrollee = $total_daet + $total_naga + $total_masbate + $total_sorsogon + $total_legaspi;



$id = ExpenseSetup::value('id');

$setup = ExpenseSetup::findorfail($id);



$daet_expense = DaetExpense::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->sum('amount');
$naga_expense = NagaExpense::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->sum('amount');
$masbate_expense = MasbateExpense::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->sum('amount');
$sorsogon_expense = SorsogonExpense::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->sum('amount');
$legaspi_expense = LegaspiExpense::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->sum('amount');
$total_expense = $daet_expense + $naga_expense + $masbate_expense + $sorsogon_expense + $legaspi_expense;



$daet_receivable = DaetReceivable::sum('balance');
$naga_receivable = NagaReceivable::sum('balance');
$masbate_receivable = MasbateReceivable::sum('balance');
$sorsogon_receivable = SorsogonReceivable::sum('balance');
$legaspi_receivable = LegaspiReceivable::sum('balance');

$total_receivable = $daet_receivable + $naga_receivable + $masbate_receivable + $sorsogon_receivable + $legaspi_receivable;


$daet_s1_dis = DaetS1Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
$daet_s2_dis = DaetS2Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
   // $discount = $s1_dis + $s2_dis;

$naga_s1_dis = NagaS1Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
$naga_s2_dis = NagaS2Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
	// $discount = $s1_dis + $s2_dis;
   
$masbate_s1_dis = MasbateS1Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
$masbate_s2_dis = MasbateS2Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
	// $discount = $s1_dis + $s2_dis;
	   
$sorsogon_s1_dis = SorsogonS1Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
$sorsogon_s2_dis = SorsogonS2Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
	// $discount = $s1_dis + $s2_dis;
	
$legaspi_s1_dis = LegaspiS1Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
$legaspi_s2_dis = LegaspiS2Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
	// $discount = $s1_dis + $s2_dis;
	

	if($setting->Season == 'Season 1'){
		 $discount = $daet_s1_dis;
		 $discount = $naga_s1_dis;
		 $discount = $masbate_s1_dis;
		 $discount = $sorsogon_s1_dis;
		 $discount = $legaspi_s1_dis;
	}
	else{
		$discount = $daet_s2_dis;
		$discount = $naga_s2_dis;
		$discount = $masbate_s2_dis;
		$discount = $sorsogon_s2_dis;
		$discount = $legaspi_s2_dis;
	}


    	return view ('/admin.dashboard')
    	->with('total_s1',$total_s1)
		->with('total_s2',$total_s2)
		->with('total_expense',$total_expense)
		->with('total_enrollee',$total_enrollee)
		->with('total_receivable',$total_receivable)
		->with('discount',$discount)
		->with('facilitation',$facilitation)
		
    	->with('total_daet_s1',$total_daet_s1)
		->with('total_daet_s2',$total_daet_s2)
		
    	->with('total_naga_s1',$total_naga_s1)
		->with('total_naga_s2',$total_naga_s2)
		
    	->with('total_masbate_s1',$total_masbate_s1)
		->with('total_masbate_s2',$total_masbate_s2)
		
    	->with('total_sorsogon_s1',$total_sorsogon_s1)
		->with('total_sorsogon_s2',$total_sorsogon_s2)
		
    	->with('total_legaspi_s1',$total_legaspi_s1)
		->with('total_legaspi_s2',$total_legaspi_s2)
		
		->with('daet_expense',$daet_expense)
		->with('naga_expense',$naga_expense)
		->with('masbate_expense',$masbate_expense)
		->with('sorsogon_expense',$sorsogon_expense)
		->with('legaspi_expense',$legaspi_expense)

		->with('total_daet',$total_daet)
		->with('total_naga',$total_naga)
		->with('total_masbate',$total_masbate)
		->with('total_sorsogon',$total_sorsogon)
		->with('total_legaspi',$total_legaspi);

    }

public function total_program(){

	$daet_let_1_sale=DaetS1Sale::where('program','=','LET')->sum('amount_paid');

	$daet_nle_1_sale=DaetS1Sale::where('program','=','NLE')->sum('amount_paid');

	$daet_crim_1_sale=DaetS1Sale::where('program','=','Criminology')->sum('amount_paid');

	$daet_civil_1_sale=DaetS1Sale::where('program','=','Civil Service')->sum('amount_paid');

	$daet_psyc_1_sale=DaetS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

	$daet_nclex_1_sale=DaetS1Sale::where('program','=','NCLEX')->sum('amount_paid');

	$daet_ielts_1_sale=DaetS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$daet_social_1_sale=DaetS1Sale::where('program','=','Social')->sum('amount_paid');

	$daet_agri_1_sale=DaetS1Sale::where('program','=','Agriculture')->sum('amount_paid');

	$daet_mid_1_sale=DaetS1Sale::where('program','=','Midwifery')->sum('amount_paid');

	$daet_online_1_sale=DaetS1Sale::where('program','=','Online Only')->sum('amount_paid');

	$daet_let_2_sale=DaetS2Sale::where('program','=','LET')->sum('amount_paid');

	$daet_nle_2_sale=DaetS2Sale::where('program','=','NLE')->sum('amount_paid');

	$daet_crim_2_sale=DaetS2Sale::where('program','=','Criminology')->sum('amount_paid');

	$daet_civil_2_sale=DaetS2Sale::where('program','=','Civil Service')->sum('amount_paid');

	$daet_psyc_2_sale=DaetS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

	$daet_nclex_2_sale=DaetS2Sale::where('program','=','NCLEX')->sum('amount_paid');

	$daet_ielts_2_sale=DaetS2Sale::where('program','=','IELTS')->sum('amount_paid');

	$daet_social_2_sale=DaetS2Sale::where('program','=','Social')->sum('amount_paid');

	$daet_agri_2_sale=DaetS2Sale::where('program','=','Agriculture')->sum('amount_paid');

	$daet_mid_2_sale=DaetS2Sale::where('program','=','Midwifery')->sum('amount_paid');

	$daet_online_2_sale=DaetS2Sale::where('program','=','Online Only')->sum('amount_paid');

	$daet_let = 	DaetLet::where('status','=','Enrolled')->count();

	$daet_nle = 	DaetNle::where('status','=','Enrolled')->count();

	$daet_crim = DaetCrim::where('status','=','Enrolled')->count();

	$daet_civil= DaetCivil::where('status','=','Enrolled')->count();

	$daet_psyc = DaetPsyc::where('status','=','Enrolled')->count();

	$daet_nclex = DaetNclex::where('status','=','Enrolled')->count();

	$daet_ielts = DaetIelt::where('status','=','Enrolled')->count();

	$daet_social = DaetSocial::where('status','=','Enrolled')->count();

	$daet_agri = DaetAgri::where('status','=','Enrolled')->count();

	$daet_mid = DaetMid::where('status','=','Enrolled')->count();

	$daet_online = DaetOnline::where('status','=','Enrolled')->count();


	$naga_let_1_sale=NagaS1Sale::where('program','=','LET')->sum('amount_paid');

    	$naga_nle_1_sale=NagaS1Sale::where('program','=','NLE')->sum('amount_paid');

		$naga_crim_1_sale=NagaS1Sale::where('program','=','Criminology')->sum('amount_paid');

		$naga_civil_1_sale=NagaS1Sale::where('program','=','Civil Service')->sum('amount_paid');

		$naga_psyc_1_sale=NagaS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$naga_nclex_1_sale=NagaS1Sale::where('program','=','NCLEX')->sum('amount_paid');

		$naga_ielts_1_sale=NagaS1Sale::where('program','=','IELTS')->sum('amount_paid');

		$naga_social_1_sale=NagaS1Sale::where('program','=','Social')->sum('amount_paid');

		$naga_agri_1_sale=NagaS1Sale::where('program','=','Agriculture')->sum('amount_paid');

		$naga_mid_1_sale=NagaS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$naga_online_1_sale=NagaS1Sale::where('program','=','Online Only')->sum('amount_paid');

		$naga_let_2_sale=NagaS2Sale::where('program','=','LET')->sum('amount_paid');

    	$naga_nle_2_sale=NagaS2Sale::where('program','=','NLE')->sum('amount_paid');

		$naga_crim_2_sale=NagaS2Sale::where('program','=','Criminology')->sum('amount_paid');

		$naga_civil_2_sale=NagaS2Sale::where('program','=','Civil Service')->sum('amount_paid');

		$naga_psyc_2_sale=NagaS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$naga_nclex_2_sale=NagaS2Sale::where('program','=','NCLEX')->sum('amount_paid');

		$naga_ielts_2_sale=NagaS2Sale::where('program','=','IELTS')->sum('amount_paid');

		$naga_social_2_sale=NagaS2Sale::where('program','=','Social')->sum('amount_paid');

		$naga_agri_2_sale=NagaS2Sale::where('program','=','Agriculture')->sum('amount_paid');

		$naga_mid_2_sale=NagaS2Sale::where('program','=','Midwifery')->sum('amount_paid');

		$naga_online_2_sale=NagaS2Sale::where('program','=','Online Only')->sum('amount_paid');

		$naga_let = 	NagaLet::where('status','=','Enrolled')->count();

		$naga_nle = 	NagaNle::where('status','=','Enrolled')->count();

		$naga_crim = NagaCrim::where('status','=','Enrolled')->count();

		$naga_civil= NagaCivil::where('status','=','Enrolled')->count();

		$naga_psyc = NagaPsyc::where('status','=','Enrolled')->count();

		$naga_nclex = NagaNclex::where('status','=','Enrolled')->count();

		$naga_ielts = NagaIelt::where('status','=','Enrolled')->count();

		$naga_social = NagaSocial::where('status','=','Enrolled')->count();

		$naga_agri = NagaAgri::where('status','=','Enrolled')->count();

		$naga_mid = NagaMid::where('status','=','Enrolled')->count();

		$naga_online = NagaOnline::where('status','=','Enrolled')->count();


		$masbate_let_1_sale=MasbateS1Sale::where('program','=','LET')->sum('amount_paid');

    	$masbate_nle_1_sale=MasbateS1Sale::where('program','=','NLE')->sum('amount_paid');

		$masbate_crim_1_sale=MasbateS1Sale::where('program','=','Criminology')->sum('amount_paid');

		$masbate_civil_1_sale=MasbateS1Sale::where('program','=','Civil Service')->sum('amount_paid');

		$masbate_psyc_1_sale=MasbateS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$masbate_nclex_1_sale=MasbateS1Sale::where('program','=','NCLEX')->sum('amount_paid');

		$masbate_ielts_1_sale=MasbateS1Sale::where('program','=','IELTS')->sum('amount_paid');

		$masbate_social_1_sale=MasbateS1Sale::where('program','=','Social')->sum('amount_paid');

		$masbate_agri_1_sale=MasbateS1Sale::where('program','=','Agriculture')->sum('amount_paid');

		$masbate_mid_1_sale=MasbateS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$masbate_online_1_sale=MasbateS1Sale::where('program','=','Online Only')->sum('amount_paid');

		$masbate_let_2_sale=MasbateS2Sale::where('program','=','LET')->sum('amount_paid');

    	$masbate_nle_2_sale=MasbateS2Sale::where('program','=','NLE')->sum('amount_paid');

		$masbate_crim_2_sale=MasbateS2Sale::where('program','=','Criminology')->sum('amount_paid');

		$masbate_civil_2_sale=MasbateS2Sale::where('program','=','Civil Service')->sum('amount_paid');

		$masbate_psyc_2_sale=MasbateS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$masbate_nclex_2_sale=MasbateS2Sale::where('program','=','NCLEX')->sum('amount_paid');

		$masbate_ielts_2_sale=MasbateS2Sale::where('program','=','IELTS')->sum('amount_paid');

		$masbate_social_2_sale=MasbateS2Sale::where('program','=','Social')->sum('amount_paid');

		$masbate_agri_2_sale=MasbateS2Sale::where('program','=','Agriculture')->sum('amount_paid');

		$masbate_mid_2_sale=MasbateS2Sale::where('program','=','Midwifery')->sum('amount_paid');

		$masbate_online_2_sale=MasbateS2Sale::where('program','=','Online Only')->sum('amount_paid');

		$masbate_let = 	MasbateLet::where('status','=','Enrolled')->count();

		$masbate_nle = 	MasbateNle::where('status','=','Enrolled')->count();

		$masbate_crim = MasbateCrim::where('status','=','Enrolled')->count();

		$masbate_civil= MasbateCivil::where('status','=','Enrolled')->count();

		$masbate_psyc = MasbatePsyc::where('status','=','Enrolled')->count();

		$masbate_nclex = MasbateNclex::where('status','=','Enrolled')->count();

		$masbate_ielts = MasbateIelt::where('status','=','Enrolled')->count();

		$masbate_social = MasbateSocial::where('status','=','Enrolled')->count();

		$masbate_agri = MasbateAgri::where('status','=','Enrolled')->count();

		$masbate_mid = MasbateMid::where('status','=','Enrolled')->count();

		$masbate_online = MasbateOnline::where('status','=','Enrolled')->count();


		$sorsogon_let_1_sale=SorsogonS1Sale::where('program','=','LET')->sum('amount_paid');

    	$sorsogon_nle_1_sale=SorsogonS1Sale::where('program','=','NLE')->sum('amount_paid');

		$sorsogon_crim_1_sale=SorsogonS1Sale::where('program','=','Criminology')->sum('amount_paid');

		$sorsogon_civil_1_sale=SorsogonS1Sale::where('program','=','Civil Service')->sum('amount_paid');

		$sorsogon_psyc_1_sale=SorsogonS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$sorsogon_nclex_1_sale=SorsogonS1Sale::where('program','=','NCLEX')->sum('amount_paid');

		$sorsogon_ielts_1_sale=SorsogonS1Sale::where('program','=','IELTS')->sum('amount_paid');

		$sorsogon_social_1_sale=SorsogonS1Sale::where('program','=','Social')->sum('amount_paid');

		$sorsogon_agri_1_sale=SorsogonS1Sale::where('program','=','Agriculture')->sum('amount_paid');

		$sorsogon_mid_1_sale=SorsogonS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$sorsogon_online_1_sale=SorsogonS1Sale::where('program','=','Online Only')->sum('amount_paid');

		$sorsogon_let_2_sale=SorsogonS2Sale::where('program','=','LET')->sum('amount_paid');

    	$sorsogon_nle_2_sale=SorsogonS2Sale::where('program','=','NLE')->sum('amount_paid');

		$sorsogon_crim_2_sale=SorsogonS2Sale::where('program','=','Criminology')->sum('amount_paid');

		$sorsogon_civil_2_sale=SorsogonS2Sale::where('program','=','Civil Service')->sum('amount_paid');

		$sorsogon_psyc_2_sale=SorsogonS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$sorsogon_nclex_2_sale=SorsogonS2Sale::where('program','=','NCLEX')->sum('amount_paid');

		$sorsogon_ielts_2_sale=SorsogonS2Sale::where('program','=','IELTS')->sum('amount_paid');

		$sorsogon_social_2_sale=SorsogonS2Sale::where('program','=','Social')->sum('amount_paid');

		$sorsogon_agri_2_sale=SorsogonS2Sale::where('program','=','Agriculture')->sum('amount_paid');

		$sorsogon_mid_2_sale=SorsogonS2Sale::where('program','=','Midwifery')->sum('amount_paid');

		$sorsogon_online_2_sale=SorsogonS2Sale::where('program','=','Online Only')->sum('amount_paid');

		$sorsogon_let = 	SorsogonLet::where('status','=','Enrolled')->count();

		$sorsogon_nle = 	SorsogonNle::where('status','=','Enrolled')->count();

		$sorsogon_crim = SorsogonCrim::where('status','=','Enrolled')->count();

		$sorsogon_civil= SorsogonCivil::where('status','=','Enrolled')->count();

		$sorsogon_psyc = SorsogonPsyc::where('status','=','Enrolled')->count();

		$sorsogon_nclex = SorsogonNclex::where('status','=','Enrolled')->count();

		$sorsogon_ielts = SorsogonIelt::where('status','=','Enrolled')->count();

		$sorsogon_social = SorsogonSocial::where('status','=','Enrolled')->count();

		$sorsogon_agri = SorsogonAgri::where('status','=','Enrolled')->count();

		$sorsogon_mid = SorsogonMid::where('status','=','Enrolled')->count();

		$sorsogon_online = SorsogonOnline::where('status','=','Enrolled')->count();

		$legaspi_let_1_sale=LegaspiS1Sale::where('program','=','LET')->sum('amount_paid');

    	$legaspi_nle_1_sale=LegaspiS1Sale::where('program','=','NLE')->sum('amount_paid');

		$legaspi_crim_1_sale=LegaspiS1Sale::where('program','=','Criminology')->sum('amount_paid');

		$legaspi_civil_1_sale=LegaspiS1Sale::where('program','=','Civil Service')->sum('amount_paid');

		$legaspi_psyc_1_sale=LegaspiS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$legaspi_nclex_1_sale=LegaspiS1Sale::where('program','=','NCLEX')->sum('amount_paid');

		$legaspi_ielts_1_sale=LegaspiS1Sale::where('program','=','IELTS')->sum('amount_paid');

		$legaspi_social_1_sale=LegaspiS1Sale::where('program','=','Social')->sum('amount_paid');

		$legaspi_agri_1_sale=LegaspiS1Sale::where('program','=','Agriculture')->sum('amount_paid');

		$legaspi_mid_1_sale=LegaspiS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$legaspi_online_1_sale=LegaspiS1Sale::where('program','=','Online Only')->sum('amount_paid');

		$legaspi_let_2_sale=LegaspiS2Sale::where('program','=','LET')->sum('amount_paid');

    	$legaspi_nle_2_sale=LegaspiS2Sale::where('program','=','NLE')->sum('amount_paid');

		$legaspi_crim_2_sale=LegaspiS2Sale::where('program','=','Criminology')->sum('amount_paid');

		$legaspi_civil_2_sale=LegaspiS2Sale::where('program','=','Civil Service')->sum('amount_paid');

		$legaspi_psyc_2_sale=LegaspiS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$legaspi_nclex_2_sale=LegaspiS2Sale::where('program','=','NCLEX')->sum('amount_paid');

		$legaspi_ielts_2_sale=LegaspiS2Sale::where('program','=','IELTS')->sum('amount_paid');

		$legaspi_social_2_sale=LegaspiS2Sale::where('program','=','Social')->sum('amount_paid');

		$legaspi_agri_2_sale=LegaspiS2Sale::where('program','=','Agriculture')->sum('amount_paid');

		$legaspi_mid_2_sale=LegaspiS2Sale::where('program','=','Midwifery')->sum('amount_paid');

		$legaspi_online_2_sale=LegaspiS2Sale::where('program','=','Online Only')->sum('amount_paid');

		$legaspi_let = 	LegaspiLet::where('status','=','Enrolled')->count();

		$legaspi_nle = 	LegaspiNle::where('status','=','Enrolled')->count();

		$legaspi_crim = LegaspiCrim::where('status','=','Enrolled')->count();

		$legaspi_civil= LegaspiCivil::where('status','=','Enrolled')->count();

		$legaspi_psyc = LegaspiPsyc::where('status','=','Enrolled')->count();

		$legaspi_nclex = LegaspiNclex::where('status','=','Enrolled')->count();

		$legaspi_ielts = LegaspiIelt::where('status','=','Enrolled')->count();

		$legaspi_social = LegaspiSocial::where('status','=','Enrolled')->count();

		$legaspi_agri = LegaspiAgri::where('status','=','Enrolled')->count();

		$legaspi_mid = LegaspiMid::where('status','=','Enrolled')->count();

		$legaspi_online = LegaspiOnline::where('status','=','Enrolled')->count();

	return view ('admin.total-program')
	->with('daet_let_1_sale',$daet_let_1_sale)
	->with('daet_nle_1_sale',$daet_nle_1_sale)
	->with('daet_crim_1_sale',$daet_crim_1_sale)
	->with('daet_civil_1_sale',$daet_civil_1_sale)
	->with('daet_psyc_1_sale',$daet_psyc_1_sale)
	->with('daet_nclex_1_sale',$daet_nclex_1_sale)
	->with('daet_ielts_1_sale',$daet_ielts_1_sale)
	->with('daet_social_1_sale',$daet_social_1_sale)
	->with('daet_agri_1_sale',$daet_agri_1_sale)
	->with('daet_mid_1_sale',$daet_mid_1_sale)
	->with('daet_online_1_sale',$daet_online_1_sale)
	->with('daet_let_2_sale',$daet_let_2_sale)
	->with('daet_nle_2_sale',$daet_nle_2_sale)
	->with('daet_crim_2_sale',$daet_crim_2_sale)
	->with('daet_civil_2_sale',$daet_civil_2_sale)
	->with('daet_psyc_2_sale',$daet_psyc_2_sale)
	->with('daet_nclex_2_sale',$daet_nclex_2_sale)
	->with('daet_ielts_2_sale',$daet_ielts_2_sale)
	->with('daet_social_2_sale',$daet_social_2_sale)
	->with('daet_agri_2_sale',$daet_agri_2_sale)
	->with('daet_mid_2_sale',$daet_mid_1_sale)
	->with('daet_online_2_sale',$daet_online_1_sale)
	->with('daet_let',$daet_let)
	->with('daet_nle',$daet_nle)
	->with('daet_crim',$daet_crim)
	->with('daet_civil',$daet_civil)
	->with('daet_psyc',$daet_psyc)
	->with('daet_nclex',$daet_nclex)
	->with('daet_ielts',$daet_ielts)
	->with('daet_social',$daet_social)
	->with('daet_agri',$daet_agri)
	->with('daet_mid',$daet_mid)
	->with('daet_online',$daet_online)

	->with('naga_let_1_sale',$naga_let_1_sale)
	->with('naga_nle_1_sale',$naga_nle_1_sale)
	->with('naga_crim_1_sale',$naga_crim_1_sale)
	->with('naga_civil_1_sale',$naga_civil_1_sale)
	->with('naga_psyc_1_sale',$naga_psyc_1_sale)
	->with('naga_nclex_1_sale',$naga_nclex_1_sale)
	->with('naga_ielts_1_sale',$naga_ielts_1_sale)
	->with('naga_social_1_sale',$naga_social_1_sale)
	->with('naga_agri_1_sale',$naga_agri_1_sale)
	->with('naga_mid_1_sale',$naga_mid_1_sale)
	->with('naga_online_1_sale',$naga_online_1_sale)
	->with('naga_let_2_sale',$naga_let_2_sale)
	->with('naga_nle_2_sale',$naga_nle_2_sale)
	->with('naga_crim_2_sale',$naga_crim_2_sale)
	->with('naga_civil_2_sale',$naga_civil_2_sale)
	->with('naga_psyc_2_sale',$naga_psyc_2_sale)
	->with('naga_nclex_2_sale',$naga_nclex_2_sale)
	->with('naga_ielts_2_sale',$naga_ielts_2_sale)
	->with('naga_social_2_sale',$naga_social_2_sale)
	->with('naga_agri_2_sale',$naga_agri_2_sale)
	->with('naga_mid_2_sale',$naga_mid_1_sale)
	->with('naga_online_2_sale',$naga_online_1_sale)
	->with('naga_let',$naga_let)
	->with('naga_nle',$naga_nle)
	->with('naga_crim',$naga_crim)
	->with('naga_civil',$naga_civil)
	->with('naga_psyc',$naga_psyc)
	->with('naga_nclex',$naga_nclex)
	->with('naga_ielts',$naga_ielts)
	->with('naga_social',$naga_social)
	->with('naga_agri',$naga_agri)
	->with('naga_mid',$naga_mid)
	->with('naga_online',$naga_online)

	->with('masbate_let_1_sale',$masbate_let_1_sale)
	->with('masbate_nle_1_sale',$masbate_nle_1_sale)
	->with('masbate_crim_1_sale',$masbate_crim_1_sale)
	->with('masbate_civil_1_sale',$masbate_civil_1_sale)
	->with('masbate_psyc_1_sale',$masbate_psyc_1_sale)
	->with('masbate_nclex_1_sale',$masbate_nclex_1_sale)
	->with('masbate_ielts_1_sale',$masbate_ielts_1_sale)
	->with('masbate_social_1_sale',$masbate_social_1_sale)
	->with('masbate_agri_1_sale',$masbate_agri_1_sale)
	->with('masbate_mid_1_sale',$masbate_mid_1_sale)
	->with('masbate_online_1_sale',$masbate_online_1_sale)
	->with('masbate_let_2_sale',$masbate_let_2_sale)
	->with('masbate_nle_2_sale',$masbate_nle_2_sale)
	->with('masbate_crim_2_sale',$masbate_crim_2_sale)
	->with('masbate_civil_2_sale',$masbate_civil_2_sale)
	->with('masbate_psyc_2_sale',$masbate_psyc_2_sale)
	->with('masbate_nclex_2_sale',$masbate_nclex_2_sale)
	->with('masbate_ielts_2_sale',$masbate_ielts_2_sale)
	->with('masbate_social_2_sale',$masbate_social_2_sale)
	->with('masbate_agri_2_sale',$masbate_agri_2_sale)
	->with('masbate_mid_2_sale',$masbate_mid_1_sale)
	->with('masbate_online_2_sale',$masbate_online_1_sale)
	->with('masbate_let',$masbate_let)
	->with('masbate_nle',$masbate_nle)
	->with('masbate_crim',$masbate_crim)
	->with('masbate_civil',$masbate_civil)
	->with('masbate_psyc',$masbate_psyc)
	->with('masbate_nclex',$masbate_nclex)
	->with('masbate_ielts',$masbate_ielts)
	->with('masbate_social',$masbate_social)
	->with('masbate_agri',$masbate_agri)
	->with('masbate_mid',$masbate_mid)
	->with('masbate_online',$masbate_online)

	->with('sorsogon_let_1_sale',$sorsogon_let_1_sale)
	->with('sorsogon_nle_1_sale',$sorsogon_nle_1_sale)
	->with('sorsogon_crim_1_sale',$sorsogon_crim_1_sale)
	->with('sorsogon_civil_1_sale',$sorsogon_civil_1_sale)
	->with('sorsogon_psyc_1_sale',$sorsogon_psyc_1_sale)
	->with('sorsogon_nclex_1_sale',$sorsogon_nclex_1_sale)
	->with('sorsogon_ielts_1_sale',$sorsogon_ielts_1_sale)
	->with('sorsogon_social_1_sale',$sorsogon_social_1_sale)
	->with('sorsogon_agri_1_sale',$sorsogon_agri_1_sale)
	->with('sorsogon_mid_1_sale',$sorsogon_mid_1_sale)
	->with('sorsogon_online_1_sale',$sorsogon_online_1_sale)
	->with('sorsogon_let_2_sale',$sorsogon_let_2_sale)
	->with('sorsogon_nle_2_sale',$sorsogon_nle_2_sale)
	->with('sorsogon_crim_2_sale',$sorsogon_crim_2_sale)
	->with('sorsogon_civil_2_sale',$sorsogon_civil_2_sale)
	->with('sorsogon_psyc_2_sale',$sorsogon_psyc_2_sale)
	->with('sorsogon_nclex_2_sale',$sorsogon_nclex_2_sale)
	->with('sorsogon_ielts_2_sale',$sorsogon_ielts_2_sale)
	->with('sorsogon_social_2_sale',$sorsogon_social_2_sale)
	->with('sorsogon_agri_2_sale',$sorsogon_agri_2_sale)
	->with('sorsogon_mid_2_sale',$sorsogon_mid_1_sale)
	->with('sorsogon_online_2_sale',$sorsogon_online_1_sale)
	->with('sorsogon_let',$sorsogon_let)
	->with('sorsogon_nle',$sorsogon_nle)
	->with('sorsogon_crim',$sorsogon_crim)
	->with('sorsogon_civil',$sorsogon_civil)
	->with('sorsogon_psyc',$sorsogon_psyc)
	->with('sorsogon_nclex',$sorsogon_nclex)
	->with('sorsogon_ielts',$sorsogon_ielts)
	->with('sorsogon_social',$sorsogon_social)
	->with('sorsogon_agri',$sorsogon_agri)
	->with('sorsogon_mid',$sorsogon_mid)
	->with('sorsogon_online',$sorsogon_online)

	->with('legaspi_let_1_sale',$legaspi_let_1_sale)
	->with('legaspi_nle_1_sale',$legaspi_nle_1_sale)
	->with('legaspi_crim_1_sale',$legaspi_crim_1_sale)
	->with('legaspi_civil_1_sale',$legaspi_civil_1_sale)
	->with('legaspi_psyc_1_sale',$legaspi_psyc_1_sale)
	->with('legaspi_nclex_1_sale',$legaspi_nclex_1_sale)
	->with('legaspi_ielts_1_sale',$legaspi_ielts_1_sale)
	->with('legaspi_social_1_sale',$legaspi_social_1_sale)
	->with('legaspi_agri_1_sale',$legaspi_agri_1_sale)
	->with('legaspi_mid_1_sale',$legaspi_mid_1_sale)
	->with('legaspi_online_1_sale',$legaspi_online_1_sale)
	->with('legaspi_let_2_sale',$legaspi_let_2_sale)
	->with('legaspi_nle_2_sale',$legaspi_nle_2_sale)
	->with('legaspi_crim_2_sale',$legaspi_crim_2_sale)
	->with('legaspi_civil_2_sale',$legaspi_civil_2_sale)
	->with('legaspi_psyc_2_sale',$legaspi_psyc_2_sale)
	->with('legaspi_nclex_2_sale',$legaspi_nclex_2_sale)
	->with('legaspi_ielts_2_sale',$legaspi_ielts_2_sale)
	->with('legaspi_social_2_sale',$legaspi_social_2_sale)
	->with('legaspi_agri_2_sale',$legaspi_agri_2_sale)
	->with('legaspi_mid_2_sale',$legaspi_mid_1_sale)
	->with('legaspi_online_2_sale',$legaspi_online_1_sale)
	->with('legaspi_let',$legaspi_let)
	->with('legaspi_nle',$legaspi_nle)
	->with('legaspi_crim',$legaspi_crim)
	->with('legaspi_civil',$legaspi_civil)
	->with('legaspi_psyc',$legaspi_psyc)
	->with('legaspi_nclex',$legaspi_nclex)
	->with('legaspi_ielts',$legaspi_ielts)
	->with('legaspi_social',$legaspi_social)
	->with('legaspi_agri',$legaspi_agri)
	->with('legaspi_mid',$legaspi_mid)
	->with('legaspi_online',$legaspi_online)
	;
	

}

public function total_agri(){
	$program = "Agriculture";
	$daet_a_prog = DaetAgri::where('status','=','Enrolled')->get();
	$naga_a_prog = NagaAgri::where('status','=','Enrolled')->get();
	$masbate_a_prog = MasbateAgri::where('status','=','Enrolled')->get();
	$sorsogon_a_prog = SorsogonAgri::where('status','=','Enrolled')->get();
	$legaspi_a_prog = LegaspiAgri::where('status','=','Enrolled')->get();


	return view ('admin.total-enrollee')
	->with('daet_a_prog',$daet_a_prog)
	->with('naga_a_prog',$naga_a_prog)
	->with('masbate_a_prog',$masbate_a_prog)
	->with('sorsogon_a_prog',$sorsogon_a_prog)
	->with('legaspi_a_prog',$legaspi_a_prog)
	->with('program',$program);
}

public function total_civil(){
	$program = "Civil Service";
	$daet_a_prog = DaetCivil::where('status','=','Enrolled')->get();
	$naga_a_prog = NagaCivil::where('status','=','Enrolled')->get();
	$masbate_a_prog = MasbateCivil::where('status','=','Enrolled')->get();
	$sorsogon_a_prog = SorsogonCivil::where('status','=','Enrolled')->get();
	$legaspi_a_prog = LegaspiCivil::where('status','=','Enrolled')->get();


	return view ('admin.total-enrollee')
	->with('daet_a_prog',$daet_a_prog)
	->with('naga_a_prog',$naga_a_prog)
	->with('masbate_a_prog',$masbate_a_prog)
	->with('sorsogon_a_prog',$sorsogon_a_prog)
	->with('legaspi_a_prog',$legaspi_a_prog)
	->with('program',$program);
}

public function total_crim(){
	$program = "Criminology";
	
	$daet_a_prog = DaetCrim::where('status','=','Enrolled')->get();
	$naga_a_prog = NagaCrim::where('status','=','Enrolled')->get();
	$masbate_a_prog = MasbateCrim::where('status','=','Enrolled')->get();
	$sorsogon_a_prog = SorsogonCrim::where('status','=','Enrolled')->get();
	$legaspi_a_prog = LegaspiCrim::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('daet_a_prog',$daet_a_prog)
	->with('naga_a_prog',$naga_a_prog)
	->with('masbate_a_prog',$masbate_a_prog)
	->with('sorsogon_a_prog',$sorsogon_a_prog)
	->with('legaspi_a_prog',$legaspi_a_prog)
	->with('program',$program);
}

public function total_ielts(){
	$program = "IELTS";
	
	$daet_a_prog = DaetIelt::where('status','=','Enrolled')->get();
	$naga_a_prog = NagaIelt::where('status','=','Enrolled')->get();
	$masbate_a_prog = MasbateIelt::where('status','=','Enrolled')->get();
	$sorsogon_a_prog = SorsogonIelt::where('status','=','Enrolled')->get();
	$legaspi_a_prog = LegaspiIelt::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('daet_a_prog',$daet_a_prog)
	->with('naga_a_prog',$naga_a_prog)
	->with('masbate_a_prog',$masbate_a_prog)
	->with('sorsogon_a_prog',$sorsogon_a_prog)
	->with('legaspi_a_prog',$legaspi_a_prog)
	->with('program',$program);
}

public function total_let(){
	$program = "LET";
	
	$daet_a_prog = DaetLet::where('status','=','Enrolled')->get();
	$naga_a_prog = NagaLet::where('status','=','Enrolled')->get();
	$masbate_a_prog = MasbateLet::where('status','=','Enrolled')->get();
	$sorsogon_a_prog = SorsogonLet::where('status','=','Enrolled')->get();
	$legaspi_a_prog = LegaspiLet::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('daet_a_prog',$daet_a_prog)
	->with('naga_a_prog',$naga_a_prog)
	->with('masbate_a_prog',$masbate_a_prog)
	->with('sorsogon_a_prog',$sorsogon_a_prog)
	->with('legaspi_a_prog',$legaspi_a_prog)
	->with('program',$program);
}

public function total_nclex(){
	$program = "NCLEX";
	
	$daet_a_prog = DaetNclex::where('status','=','Enrolled')->get();
	$naga_a_prog = NagaNclex::where('status','=','Enrolled')->get();
	$masbate_a_prog = MasbateNclex::where('status','=','Enrolled')->get();
	$sorsogon_a_prog = SorsogonNclex::where('status','=','Enrolled')->get();
	$legaspi_a_prog = LegaspiNclex::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('daet_a_prog',$daet_a_prog)
	->with('naga_a_prog',$naga_a_prog)
	->with('masbate_a_prog',$masbate_a_prog)
	->with('sorsogon_a_prog',$sorsogon_a_prog)
	->with('legaspi_a_prog',$legaspi_a_prog)
	->with('program',$program);
}

public function total_nle(){
	$program = "NLE";
	
	$daet_a_prog = DaetNle::where('status','=','Enrolled')->get();
	$naga_a_prog = NagaNle::where('status','=','Enrolled')->get();
	$masbate_a_prog = MasbateNle::where('status','=','Enrolled')->get();
	$sorsogon_a_prog = SorsogonNle::where('status','=','Enrolled')->get();
	$legaspi_a_prog = LegaspiNle::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('daet_a_prog',$daet_a_prog)
	->with('naga_a_prog',$naga_a_prog)
	->with('masbate_a_prog',$masbate_a_prog)
	->with('sorsogon_a_prog',$sorsogon_a_prog)
	->with('legaspi_a_prog',$legaspi_a_prog)
	->with('program',$program);
}

public function total_psyc(){
	$program = "Psychometrician";
	
	$daet_a_prog = DaetPsyc::where('status','=','Enrolled')->get();
	$naga_a_prog = NagaPsyc::where('status','=','Enrolled')->get();
	$masbate_a_prog = MasbatePsyc::where('status','=','Enrolled')->get();
	$sorsogon_a_prog = SorsogonPsyc::where('status','=','Enrolled')->get();
	$legaspi_a_prog = LegaspiPsyc::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('daet_a_prog',$daet_a_prog)
	->with('naga_a_prog',$naga_a_prog)
	->with('masbate_a_prog',$masbate_a_prog)
	->with('sorsogon_a_prog',$sorsogon_a_prog)
	->with('legaspi_a_prog',$legaspi_a_prog)
	->with('program',$program);
}

public function total_social(){
	$program = "Social Work";
	
	$daet_a_prog = DaetSocial::where('status','=','Enrolled')->get();
	$naga_a_prog = NagaSocial::where('status','=','Enrolled')->get();
	$masbate_a_prog = MasbateSocial::where('status','=','Enrolled')->get();
	$sorsogon_a_prog = SorsogonSocial::where('status','=','Enrolled')->get();
	$legaspi_a_prog = LegaspiSocial::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('daet_a_prog',$daet_a_prog)
	->with('naga_a_prog',$naga_a_prog)
	->with('masbate_a_prog',$masbate_a_prog)
	->with('sorsogon_a_prog',$sorsogon_a_prog)
	->with('legaspi_a_prog',$legaspi_a_prog)
	->with('program',$program);
}

public function total_mid(){
	$program = "Midwifery";
	
	$daet_a_prog = DaetMid::where('status','=','Enrolled')->get();
	$naga_a_prog = NagaMid::where('status','=','Enrolled')->get();
	$masbate_a_prog = MasbateMid::where('status','=','Enrolled')->get();
	$sorsogon_a_prog = SorsogonMid::where('status','=','Enrolled')->get();
	$legaspi_a_prog = LegaspiMid::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('daet_a_prog',$daet_a_prog)
	->with('naga_a_prog',$naga_a_prog)
	->with('masbate_a_prog',$masbate_a_prog)
	->with('sorsogon_a_prog',$sorsogon_a_prog)
	->with('legaspi_a_prog',$legaspi_a_prog)
	->with('program',$program);
}

public function total_online(){
	$program = "Online Only";
	
	$daet_a_prog = DaetOnline::where('status','=','Enrolled')->get();
	$naga_a_prog = NagaOnline::where('status','=','Enrolled')->get();
	$masbate_a_prog = MasbateOnline::where('status','=','Enrolled')->get();
	$sorsogon_a_prog = SorsogonOnline::where('status','=','Enrolled')->get();
	$legaspi_a_prog = LegaspiOnline::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('daet_a_prog',$daet_a_prog)
	->with('naga_a_prog',$naga_a_prog)
	->with('masbate_a_prog',$masbate_a_prog)
	->with('sorsogon_a_prog',$sorsogon_a_prog)
	->with('legaspi_a_prog',$legaspi_a_prog)
	->with('program',$program);
}

public function total_dropped(){

	$daet_a_let=DaetLet::where('status','=','Dropped')->get();

    $daet_a_nle=DaetNle::where('status','=','Dropped')->get();

    $daet_a_crim=DaetCrim::where('status','=','Dropped')->get();

    $daet_a_civil=DaetCivil::where('status','=','Dropped')->get();

    $daet_a_psyc=DaetPsyc::where('status','=','Dropped')->get();

    $daet_a_nclex=DaetNclex::where('status','=','Dropped')->get();

    $daet_a_ielt=DaetIelt::where('status','=','Dropped')->get();

    $daet_a_social=DaetSocial::where('status','=','Dropped')->get();

    $daet_a_agri=DaetAgri::where('status','=','Dropped')->get();

    $daet_a_mid=DaetMid::where('status','=','Dropped')->get();

	$daet_a_online=DaetOnline::where('status','=','Dropped')->get();
	
	$naga_a_let=NagaLet::where('status','=','Dropped')->get();

    $naga_a_nle=NagaNle::where('status','=','Dropped')->get();

    $naga_a_crim=NagaCrim::where('status','=','Dropped')->get();

    $naga_a_civil=NagaCivil::where('status','=','Dropped')->get();

    $naga_a_psyc=NagaPsyc::where('status','=','Dropped')->get();

    $naga_a_nclex=NagaNclex::where('status','=','Dropped')->get();

    $naga_a_ielt=NagaIelt::where('status','=','Dropped')->get();

    $naga_a_social=NagaSocial::where('status','=','Dropped')->get();

    $naga_a_agri=NagaAgri::where('status','=','Dropped')->get();

    $naga_a_mid=NagaMid::where('status','=','Dropped')->get();

	$naga_a_online=NagaOnline::where('status','=','Dropped')->get();
	
	$masbate_a_let=MasbateLet::where('status','=','Dropped')->get();

    $masbate_a_nle=MasbateNle::where('status','=','Dropped')->get();

    $masbate_a_crim=MasbateCrim::where('status','=','Dropped')->get();

    $masbate_a_civil=MasbateCivil::where('status','=','Dropped')->get();

    $masbate_a_psyc=MasbatePsyc::where('status','=','Dropped')->get();

    $masbate_a_nclex=MasbateNclex::where('status','=','Dropped')->get();

    $masbate_a_ielt=MasbateIelt::where('status','=','Dropped')->get();

    $masbate_a_social=MasbateSocial::where('status','=','Dropped')->get();

    $masbate_a_agri=MasbateAgri::where('status','=','Dropped')->get();

    $masbate_a_mid=MasbateMid::where('status','=','Dropped')->get();

	$masbate_a_online=MasbateOnline::where('status','=','Dropped')->get();
	
	$sorsogon_a_let=SorsogonLet::where('status','=','Dropped')->get();

    $sorsogon_a_nle=SorsogonNle::where('status','=','Dropped')->get();

    $sorsogon_a_crim=SorsogonCrim::where('status','=','Dropped')->get();

    $sorsogon_a_civil=SorsogonCivil::where('status','=','Dropped')->get();

    $sorsogon_a_psyc=SorsogonPsyc::where('status','=','Dropped')->get();

    $sorsogon_a_nclex=SorsogonNclex::where('status','=','Dropped')->get();

    $sorsogon_a_ielt=SorsogonIelt::where('status','=','Dropped')->get();

    $sorsogon_a_social=SorsogonSocial::where('status','=','Dropped')->get();

    $sorsogon_a_agri=SorsogonAgri::where('status','=','Dropped')->get();

    $sorsogon_a_mid=SorsogonMid::where('status','=','Dropped')->get();

	$sorsogon_a_online=SorsogonOnline::where('status','=','Dropped')->get();
	
	$legaspi_a_let=LegaspiLet::where('status','=','Dropped')->get();

    $legaspi_a_nle=LegaspiNle::where('status','=','Dropped')->get();

    $legaspi_a_crim=LegaspiCrim::where('status','=','Dropped')->get();

    $legaspi_a_civil=LegaspiCivil::where('status','=','Dropped')->get();

    $legaspi_a_psyc=LegaspiPsyc::where('status','=','Dropped')->get();

    $legaspi_a_nclex=LegaspiNclex::where('status','=','Dropped')->get();

    $legaspi_a_ielt=LegaspiIelt::where('status','=','Dropped')->get();

    $legaspi_a_social=LegaspiSocial::where('status','=','Dropped')->get();

    $legaspi_a_agri=LegaspiAgri::where('status','=','Dropped')->get();

    $legaspi_a_mid=LegaspiMid::where('status','=','Dropped')->get();

    $legaspi_a_online=LegaspiOnline::where('status','=','Dropped')->get();

    return view ('admin.total-dropped')
    ->with('daet_a_let',$daet_a_let)
    ->with('daet_a_nle',$daet_a_nle)
    ->with('daet_a_crim',$daet_a_crim)
    ->with('daet_a_civil',$daet_a_civil)
    ->with('daet_a_psyc',$daet_a_psyc)
    ->with('daet_a_nclex',$daet_a_nclex)
    ->with('daet_a_ielt',$daet_a_ielt)
    ->with('daet_a_social',$daet_a_social)
    ->with('daet_a_agri',$daet_a_agri)
    ->with('daet_a_mid',$daet_a_mid)
	->with('daet_a_online',$daet_a_online)

	->with('naga_a_let',$naga_a_let)
    ->with('naga_a_nle',$naga_a_nle)
    ->with('naga_a_crim',$naga_a_crim)
    ->with('naga_a_civil',$naga_a_civil)
    ->with('naga_a_psyc',$naga_a_psyc)
    ->with('naga_a_nclex',$naga_a_nclex)
    ->with('naga_a_ielt',$naga_a_ielt)
    ->with('naga_a_social',$naga_a_social)
    ->with('naga_a_agri',$naga_a_agri)
    ->with('naga_a_mid',$naga_a_mid)
	->with('naga_a_online',$naga_a_online)

	->with('masbate_a_let',$masbate_a_let)
    ->with('masbate_a_nle',$masbate_a_nle)
    ->with('masbate_a_crim',$masbate_a_crim)
    ->with('masbate_a_civil',$masbate_a_civil)
    ->with('masbate_a_psyc',$masbate_a_psyc)
    ->with('masbate_a_nclex',$masbate_a_nclex)
    ->with('masbate_a_ielt',$masbate_a_ielt)
    ->with('masbate_a_social',$masbate_a_social)
    ->with('masbate_a_agri',$masbate_a_agri)
    ->with('masbate_a_mid',$masbate_a_mid)
	->with('masbate_a_online',$masbate_a_online)

	->with('sorsogon_a_let',$sorsogon_a_let)
    ->with('sorsogon_a_nle',$sorsogon_a_nle)
    ->with('sorsogon_a_crim',$sorsogon_a_crim)
    ->with('sorsogon_a_civil',$sorsogon_a_civil)
    ->with('sorsogon_a_psyc',$sorsogon_a_psyc)
    ->with('sorsogon_a_nclex',$sorsogon_a_nclex)
    ->with('sorsogon_a_ielt',$sorsogon_a_ielt)
    ->with('sorsogon_a_social',$sorsogon_a_social)
    ->with('sorsogon_a_agri',$sorsogon_a_agri)
    ->with('sorsogon_a_mid',$sorsogon_a_mid)
	->with('sorsogon_a_online',$sorsogon_a_online)

	->with('legaspi_a_let',$legaspi_a_let)
    ->with('legaspi_a_nle',$legaspi_a_nle)
    ->with('legaspi_a_crim',$legaspi_a_crim)
    ->with('legaspi_a_civil',$legaspi_a_civil)
    ->with('legaspi_a_psyc',$legaspi_a_psyc)
    ->with('legaspi_a_nclex',$legaspi_a_nclex)
    ->with('legaspi_a_ielt',$legaspi_a_ielt)
    ->with('legaspi_a_social',$legaspi_a_social)
    ->with('legaspi_a_agri',$legaspi_a_agri)
    ->with('legaspi_a_mid',$legaspi_a_mid)
	->with('legaspi_a_online',$legaspi_a_online)
	;
}

public function total_scholars(){

	$daet_a_let=DaetLet::where('category','=','Scholar')->get();

    $daet_a_nle=DaetNle::where('category','=','Scholar')->get();

    $daet_a_crim=DaetCrim::where('category','=','Scholar')->get();

    $daet_a_civil=DaetCivil::where('category','=','Scholar')->get();

    $daet_a_psyc=DaetPsyc::where('category','=','Scholar')->get();

    $daet_a_nclex=DaetNclex::where('category','=','Scholar')->get();

    $daet_a_ielt=DaetIelt::where('category','=','Scholar')->get();

    $daet_a_social=DaetSocial::where('category','=','Scholar')->get();

    $daet_a_agri=DaetAgri::where('category','=','Scholar')->get();

    $daet_a_mid=DaetMid::where('category','=','Scholar')->get();

	$daet_a_online=DaetOnline::where('category','=','Scholar')->get();
	
	$naga_a_let=NagaLet::where('category','=','Scholar')->get();

    $naga_a_nle=NagaNle::where('category','=','Scholar')->get();

    $naga_a_crim=NagaCrim::where('category','=','Scholar')->get();

    $naga_a_civil=NagaCivil::where('category','=','Scholar')->get();

    $naga_a_psyc=NagaPsyc::where('category','=','Scholar')->get();

    $naga_a_nclex=NagaNclex::where('category','=','Scholar')->get();

    $naga_a_ielt=NagaIelt::where('category','=','Scholar')->get();

    $naga_a_social=NagaSocial::where('category','=','Scholar')->get();

    $naga_a_agri=NagaAgri::where('category','=','Scholar')->get();

    $naga_a_mid=NagaMid::where('category','=','Scholar')->get();

	$naga_a_online=NagaOnline::where('category','=','Scholar')->get();
	
	$masbate_a_let=MasbateLet::where('category','=','Scholar')->get();

    $masbate_a_nle=MasbateNle::where('category','=','Scholar')->get();

    $masbate_a_crim=MasbateCrim::where('category','=','Scholar')->get();

    $masbate_a_civil=MasbateCivil::where('category','=','Scholar')->get();

    $masbate_a_psyc=MasbatePsyc::where('category','=','Scholar')->get();

    $masbate_a_nclex=MasbateNclex::where('category','=','Scholar')->get();

    $masbate_a_ielt=MasbateIelt::where('category','=','Scholar')->get();

    $masbate_a_social=MasbateSocial::where('category','=','Scholar')->get();

    $masbate_a_agri=MasbateAgri::where('category','=','Scholar')->get();

    $masbate_a_mid=MasbateMid::where('category','=','Scholar')->get();

	$masbate_a_online=MasbateOnline::where('category','=','Scholar')->get();
	
	$sorsogon_a_let=SorsogonLet::where('category','=','Scholar')->get();

    $sorsogon_a_nle=SorsogonNle::where('category','=','Scholar')->get();

    $sorsogon_a_crim=SorsogonCrim::where('category','=','Scholar')->get();

    $sorsogon_a_civil=SorsogonCivil::where('category','=','Scholar')->get();

    $sorsogon_a_psyc=SorsogonPsyc::where('category','=','Scholar')->get();

    $sorsogon_a_nclex=SorsogonNclex::where('category','=','Scholar')->get();

    $sorsogon_a_ielt=SorsogonIelt::where('category','=','Scholar')->get();

    $sorsogon_a_social=SorsogonSocial::where('category','=','Scholar')->get();

    $sorsogon_a_agri=SorsogonAgri::where('category','=','Scholar')->get();

    $sorsogon_a_mid=SorsogonMid::where('category','=','Scholar')->get();

	$sorsogon_a_online=SorsogonOnline::where('category','=','Scholar')->get();
	
	$legaspi_a_let=LegaspiLet::where('category','=','Scholar')->get();

    $legaspi_a_nle=LegaspiNle::where('category','=','Scholar')->get();

    $legaspi_a_crim=LegaspiCrim::where('category','=','Scholar')->get();

    $legaspi_a_civil=LegaspiCivil::where('category','=','Scholar')->get();

    $legaspi_a_psyc=LegaspiPsyc::where('category','=','Scholar')->get();

    $legaspi_a_nclex=LegaspiNclex::where('category','=','Scholar')->get();

    $legaspi_a_ielt=LegaspiIelt::where('category','=','Scholar')->get();

    $legaspi_a_social=LegaspiSocial::where('category','=','Scholar')->get();

    $legaspi_a_agri=LegaspiAgri::where('category','=','Scholar')->get();

    $legaspi_a_mid=LegaspiMid::where('category','=','Scholar')->get();

    $legaspi_a_online=LegaspiOnline::where('category','=','Scholar')->get();


    return view ('admin.total-scholars')
    ->with('daet_a_let',$daet_a_let)
    ->with('daet_a_nle',$daet_a_nle)
    ->with('daet_a_crim',$daet_a_crim)
    ->with('daet_a_civil',$daet_a_civil)
    ->with('daet_a_psyc',$daet_a_psyc)
    ->with('daet_a_nclex',$daet_a_nclex)
    ->with('daet_a_ielt',$daet_a_ielt)
    ->with('daet_a_social',$daet_a_social)
    ->with('daet_a_agri',$daet_a_agri)
    ->with('daet_a_mid',$daet_a_mid)
	->with('daet_a_online',$daet_a_online)

	->with('naga_a_let',$naga_a_let)
    ->with('naga_a_nle',$naga_a_nle)
    ->with('naga_a_crim',$naga_a_crim)
    ->with('naga_a_civil',$naga_a_civil)
    ->with('naga_a_psyc',$naga_a_psyc)
    ->with('naga_a_nclex',$naga_a_nclex)
    ->with('naga_a_ielt',$naga_a_ielt)
    ->with('naga_a_social',$naga_a_social)
    ->with('naga_a_agri',$naga_a_agri)
    ->with('naga_a_mid',$naga_a_mid)
	->with('naga_a_online',$naga_a_online)

	->with('masbate_a_let',$masbate_a_let)
    ->with('masbate_a_nle',$masbate_a_nle)
    ->with('masbate_a_crim',$masbate_a_crim)
    ->with('masbate_a_civil',$masbate_a_civil)
    ->with('masbate_a_psyc',$masbate_a_psyc)
    ->with('masbate_a_nclex',$masbate_a_nclex)
    ->with('masbate_a_ielt',$masbate_a_ielt)
    ->with('masbate_a_social',$masbate_a_social)
    ->with('masbate_a_agri',$masbate_a_agri)
    ->with('masbate_a_mid',$masbate_a_mid)
	->with('masbate_a_online',$masbate_a_online)

	->with('sorsogon_a_let',$sorsogon_a_let)
    ->with('sorsogon_a_nle',$sorsogon_a_nle)
    ->with('sorsogon_a_crim',$sorsogon_a_crim)
    ->with('sorsogon_a_civil',$sorsogon_a_civil)
    ->with('sorsogon_a_psyc',$sorsogon_a_psyc)
    ->with('sorsogon_a_nclex',$sorsogon_a_nclex)
    ->with('sorsogon_a_ielt',$sorsogon_a_ielt)
    ->with('sorsogon_a_social',$sorsogon_a_social)
    ->with('sorsogon_a_agri',$sorsogon_a_agri)
    ->with('sorsogon_a_mid',$sorsogon_a_mid)
	->with('sorsogon_a_online',$sorsogon_a_online)

	->with('legaspi_a_let',$legaspi_a_let)
    ->with('legaspi_a_nle',$legaspi_a_nle)
    ->with('legaspi_a_crim',$legaspi_a_crim)
    ->with('legaspi_a_civil',$legaspi_a_civil)
    ->with('legaspi_a_psyc',$legaspi_a_psyc)
    ->with('legaspi_a_nclex',$legaspi_a_nclex)
    ->with('legaspi_a_ielt',$legaspi_a_ielt)
    ->with('legaspi_a_social',$legaspi_a_social)
    ->with('legaspi_a_agri',$legaspi_a_agri)
    ->with('legaspi_a_mid',$legaspi_a_mid)
	->with('legaspi_a_online',$legaspi_a_online)
	;
}

public function tuition_fees(){

	$daet_a_tuition = DaetTuition::all();
	$naga_a_tuition = NagaTuition::all();
	$masbate_a_tuition = MasbateTuition::all();
	$sorsogon_a_tuition = SorsogonTuition::all();
	$legaspi_a_tuition = LegaspiTuition::all();
    $branch = Branch::all();
	$program = Program::all();
	
	return view ('admin.tuition-fees')
	->with('daet_a_tuition',$daet_a_tuition)
	->with('naga_a_tuition',$naga_a_tuition)
	->with('masbate_a_tuition',$masbate_a_tuition)
	->with('sorsogon_a_tuition',$sorsogon_a_tuition)
	->with('legaspi_a_tuition',$legaspi_a_tuition)
	->with('branch',$branch)
	->with('program',$program);
}
public function insert_tuition(Request $request){

	$input = $request->except(['_token']);
	$branch = $input['branch'];
	

	if($branch == 'Daet'){
		
		$existent1 = DaetTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			DaetTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}
	if($branch == 'Naga'){
		
		$existent1 = NagaTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			NagaTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}
	if($branch == 'Masbate'){
		
		$existent1 = MasbateTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			MasbateTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}
	if($branch == 'Sorsogon'){
		
		$existent1 = SorsogonTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			SorsogonTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}
	if($branch == 'Legaspi'){
		
		$existent1 = LegaspiTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			LegaspiTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}

}

public function update_tuition(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];
$id = $input['id'];

	if($branch == 'Daet'){
		DaetTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
	if($branch == 'Naga'){
		NagaTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
	if($branch == 'Masbate'){
		MasbateTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
	if($branch == 'Sorsogon'){
		SorsogonTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
	if($branch == 'Legaspi'){
		LegaspiTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
}

public function delete_tuition(Request $request){
		$input = $request->except(['_token']);

		if($input['branch'] == 'Daet'){

              DaetTuition::where('id','=',$input['id'])->delete();
           
    	}
		if($input['branch'] == 'Naga'){

              NagaTuition::where('id','=',$input['id'])->delete();
           
    	}
		if($input['branch'] == 'Masbate'){

              MasbateTuition::where('id','=',$input['id'])->delete();
           
    	}
		if($input['branch'] == 'Sorsogon'){

              SorsogonTuition::where('id','=',$input['id'])->delete();
           
    	}
		if($input['branch'] == 'Legaspi'){

              LegaspiTuition::where('id','=',$input['id'])->delete();
           
    	}

              return response()->json([
            'success' => true,
            'message' => '1 tuition has been deleted',
        ]);
      
         }

public function discounts(){
	$branch = Branch::all();
	$daet_a_discount = DaetDiscount::all();
	$naga_a_discount = NagaDiscount::all();
	$masbate_a_discount = MasbateDiscount::all();
	$sorsogon_a_discount = SorsogonDiscount::all();
	$legaspi_a_discount = LegaspiDiscount::all();
	$program = Program::all();
	return view ('admin.discounts')
	->with('daet_a_discount',$daet_a_discount)
	->with('naga_a_discount',$naga_a_discount)
	->with('masbate_a_discount',$masbate_a_discount)
	->with('sorsogon_a_discount',$sorsogon_a_discount)
	->with('legaspi_a_discount',$legaspi_a_discount)
	->with('branch',$branch)
	->with('program',$program);
}

public function insert_discount(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];

	if($branch == 'Daet'){
		DaetDiscount::insert($input);
	}
	if($branch == 'Naga'){
		NagaDiscount::insert($input);
	}
	if($branch == 'Masbate'){
		MasbateDiscount::insert($input);
	}
	if($branch == 'Sorsogon'){
		SorsogonDiscount::insert($input);
	}
	if($branch == 'Legaspi'){
		LegaspiDiscount::insert($input);
	}

return response()->json([
            'success' => true,
            'message' => 'New discount has been added',
        ]);

}

public function update_discount(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];
$id = $input['id'];

	if($branch == 'Daet'){
		DaetDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}
	if($branch == 'Naga'){
		NagaDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}
	if($branch == 'Masbate'){
		MasbateDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}
	if($branch == 'Sorsogon'){
		SorsogonDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}
	if($branch == 'Legaspi'){
		LegaspiDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}

return response()->json([
            'success' => true,
            'message' => '1 discount has been updated',
        ]);

}

public function delete_discount(Request $request){
		$input = $request->except(['_token']);

		if($input['branch'] == 'Daet'){

              DaetDiscount::where('id','=',$input['id'])->delete();
		}
		if($input['branch'] == 'Naga'){

			NagaDiscount::where('id','=',$input['id'])->delete();
		}
		if($input['branch'] == 'Masbate'){

			MasbateDiscount::where('id','=',$input['id'])->delete();
		}
		if($input['branch'] == 'Sorsogon'){

			SorsogonDiscount::where('id','=',$input['id'])->delete();
		}
		if($input['branch'] == 'Legaspi'){

			LegaspiDiscount::where('id','=',$input['id'])->delete();
	  	}
		  
		  

       return response()->json([
            'success' => true,
            'message' => '1 discount has been deleted',
        ]);
      
         }
    

public function total_books(){
	$branch = Branch::all();
	$daet_a_book = DaetBooksInventorie::all();
	$daet_a_sale = DaetBooksSale::all();
	
	$naga_a_book = NagaBooksInventorie::all();
    $naga_a_sale = NagaBooksSale::all();

	$masbate_a_book = MasbateBooksInventorie::all();
	$masbate_a_sale = MasbateBooksSale::all();
	
	$sorsogon_a_book = SorsogonBooksInventorie::all();
    $sorsogon_a_sale = SorsogonBooksSale::all();

	$legaspi_a_book = LegaspiBooksInventorie::all();
	$legaspi_a_sale = LegaspiBooksSale::all();
	

	$program = Program::all();
	return view ('admin.total-books')
	->with('daet_a_book',$daet_a_book)
	->with('daet_a_sale',$daet_a_sale)

	->with('naga_a_book',$naga_a_book)
	->with('naga_a_sale',$naga_a_sale)

	->with('masbate_a_book',$masbate_a_book)
	->with('masbate_a_sale',$masbate_a_sale)
	
	->with('sorsogon_a_book',$sorsogon_a_book)
	->with('sorsogon_a_sale',$sorsogon_a_sale)

	->with('legaspi_a_book',$legaspi_a_book)
	->with('legaspi_a_sale',$legaspi_a_sale)

	->with('branch',$branch)
	->with('program',$program);
}

public function insert_book(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];

	if($branch == 'Daet'){
		DaetBooksInventorie::insert($input);
	}
	if($branch == 'Naga'){
		NagaBooksInventorie::insert($input);
	}
	if($branch == 'Masbate'){
		MasbateBooksInventorie::insert($input);
	}
	if($branch == 'Sorsogon'){
		SorsogonBooksInventorie::insert($input);
	}
	if($branch == 'Legaspi'){
		LegaspiBooksInventorie::insert($input);
	}

return response()->json([
            'success' => true,
            'message' => 'New book has been added',
        ]);

}

public function update_book(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];
$id = $input['id'];

	
	if($branch == 'Daet'){
		DaetBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	if($branch == 'Naga'){
		NagaBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	if($branch == 'Masbate'){
		MasbateBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	if($branch == 'Sorsogon'){
		SorsogonBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	if($branch == 'Legaspi'){
		LegaspiBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	

return response()->json([
            'success' => true,
            'message' => 'Book inventories has been updated',
        ]);

}

public function delete_book(Request $request){
		$input = $request->except(['_token']);

		if($input['branch'] == 'Daet'){

              DaetBooksInventorie::where('id','=',$input['id'])->delete();

		}
		if($input['branch'] == 'Naga'){

			NagaBooksInventorie::where('id','=',$input['id'])->delete();

		}
		if($input['branch'] == 'Masbate'){

			MasbateBooksInventorie::where('id','=',$input['id'])->delete();

		}
		if($input['branch'] == 'Sorsogon'){

			SorsogonBooksInventorie::where('id','=',$input['id'])->delete();

		}
		if($input['branch'] == 'Legaspi'){

			LegaspiBooksInventorie::where('id','=',$input['id'])->delete();

		}


    return response()->json([
            'success' => true,
            'message' => '1 book record has been deleted',
        ]);
}

public function total_expense(){

	$daet_a_sale = DaetExpense::all();
	$naga_a_sale = NagaExpense::all();
	$masbate_a_sale = MasbateExpense::all();
	$sorsogon_a_sale = SorsogonExpense::all();
	$legaspi_a_sale = LegaspiExpense::all();
	
	
	return view ('admin.total-expense')
	->with('daet_a_sale',$daet_a_sale)
	->with('naga_a_sale',$naga_a_sale)
	->with('masbate_a_sale',$masbate_a_sale)
	->with('sorsogon_a_sale',$sorsogon_a_sale)
	->with('legaspi_a_sale',$legaspi_a_sale);
}

public function user(){
	$branch = Branch::all();
	$user = User::where('show','=','1')->get();
	$role = Role::all();
	return view ('admin.user')->with('role',$role)->with('user',$user)->with('branch',$branch);
}

public function insert_user(Request $request){

$input = $request->except(['_token']);

$validatedData = $request->validate([
	'username' => 'unique:users|required',
	'email' => 'unique:users|required',
]);
$roleID = 0;
if($input['role']=='daet'){
	$roleID = 2;
}

if($input['role']=='legaspi'){
	$roleID = 3;
}

if($input['role']=='masbate'){
	$roleID = 4;
}

if($input['role']=='naga'){
	$roleID = 5;
}
if($input['role']=='sorsogon'){
	$roleID = 6;
}

if($input['role']=='daet_enrollment'){
	$roleID = 7;
}
if($input['role']=='daet_cashier'){
	$roleID = 8;
}

if($input['role']=='naga_enrollment'){
	$roleID = 9;
}
if($input['role']=='naga_cashier'){
	$roleID = 10;
}

if($input['role']=='masbate_enrollment'){
	$roleID = 11;
}
if($input['role']=='masbate_cashier'){
	$roleID = 12;
}

if($input['role']=='sorsogon_enrollment'){
	$roleID = 13;
}
if($input['role']=='sorsogon_cashier'){
	$roleID = 14;
}
if($input['role']=='legaspi_enrollment'){
	$roleID = 15;
}
if($input['role']=='legaspi_cashier'){
	$roleID = 16;
}

$branch = $input['branch'];

	if($branch == 'Main'){
		User::insert([
			'role' => 'Admin',
			'username' =>$input['username'],
			'email' => $input['email'],
			'password' => Hash::make($input['password']),
			'name' => $input['name'],
			'branch'	=> $input['branch'],
			'show'		=> '1',
			'remember_token' => str_random(60),
		]);

		$user_id = User::max('id');

		DB::table('role_user')->insert([

            'user_id' => $user_id,
            'role_id'    => '1',
            ]); 

		return response()->json([
            'success' => true,
            'message' => 'New Administrator has been added',
        ]);
	}
	if($branch != 'Admin'){

		
		User::insert([
			'role' => $input['role'],
			'username' =>$input['username'],
			'email' => $input['email'],
			'password' => Hash::make($input['password']),
			'name' => $input['name'],
			'branch'	=> $input['branch'],
			'show'		=> '1',
			'remember_token' => str_random(60),
		]);

		$user_id = User::max('id');

		DB::table('role_user')->insert([

            'user_id' => $user_id,
            'role_id'    => $roleID,
            ]); 

		return response()->json([
            'success' => true,
			'message' => 'New Member has been added',
			'input'	  =>  $input
        ]);
	}


}
public function update_user(Request $request){

$input = $request->except(['_token']);


$roleID = 0;
if($input['role']=='daet'){
	$roleID = 2;
}

if($input['role']=='legaspi'){
	$roleID = 3;
}

if($input['role']=='masbate'){
	$roleID = 4;
}

if($input['role']=='naga'){
	$roleID = 5;
}
if($input['role']=='sorsogon'){
	$roleID = 6;
}

if($input['role']=='daet_enrollment'){
	$roleID = 7;
}
if($input['role']=='daet_cashier'){
	$roleID = 8;
}

if($input['role']=='naga_enrollment'){
	$roleID = 9;
}
if($input['role']=='naga_cashier'){
	$roleID = 10;
}

if($input['role']=='masbate_enrollment'){
	$roleID = 11;
}
if($input['role']=='masbate_cashier'){
	$roleID = 12;
}

if($input['role']=='sorsogon_enrollment'){
	$roleID = 13;
}
if($input['role']=='sorsogon_cashier'){
	$roleID = 14;
}
if($input['role']=='legaspi_enrollment'){
	$roleID = 15;
}
if($input['role']=='legaspi_cashier'){
	$roleID = 16;
}


$id = $input['id'];
$branch = $input['branch'];

	if($branch == 'Main'){
		if($input['password'] == null){
		User::where('id','=',$id)->update([
			'role' => 'Admin',
			'username' =>$input['username'],
			'email' => $input['email'],
			'name' => $input['name'],
			'branch'	=> $input['branch'],
		]);
		}

		else{
		User::where('id','=',$id)->update([
			'role' => 'Admin',
			'username' =>$input['username'],
			'password' => Hash::make($input['password']),
			'email' => $input['email'],
			'name' => $input['name'],
			'branch'	=> $input['branch'],
		]);
		}



		DB::table('role_user')->where('user_id','=',$id)->update([

            'user_id' => $id,
            'role_id'    => '1',
            ]); 

		return response()->json([
            'success' => true,
            'message' => 'User has been updated',
        ]);
	}
	if($branch != 'Admin'){
		if($input['password'] == null){
		User::where('id','=',$id)->update([
			'role' => 'Admin',
			'username' =>$input['username'],
			'email' => $input['email'],
			'name' => $input['name'],
			'branch'	=> $input['branch'],
		]);
		}

		else{
		User::where('id','=',$id)->update([
			'role' => $input['role'],
			'username' =>$input['username'],
			'email' => $input['email'],
			'password' => Hash::make($input['password']),
			'name' => $input['name'],
			'branch'	=> $input['branch'],
		]);
	}



		DB::table('role_user')->where('user_id','=',$id)->update([

            'user_id' => $id,
            'role_id'    => $roleID,
            ]); 

		return response()->json([
            'success' => true,
            'message' => 'User has been updated',
        ]);
	}
	
}
public function delete_user(Request $request){

	if(isset($request->id)){
              $todo = User::findOrFail($request->id);
              $todo->delete();
              return response()->json([
            'success' => true,
            'message' => 'User has been deleted',
        ]);
        }


}
function checkemail(Request $request)
{
 if($request->get('email'))
 {
  $email = $request->get('email');
  $data = DB::table("users")
   ->where('email', $email)
   ->count();
  if($data > 0)
  {
   echo 'unique';
  }
  else
  {
   echo 'not_unique';
  }
 }
}//end check for email

function checkusername(Request $request)
{
 if($request->get('username'))
 {
  $username = $request->get('username');
  $data = DB::table("users")
   ->where('username', $username)
   ->count();
  if($data > 0)
  {
   echo 'unique';
  }
  else
  {
   echo 'not_unique';
  }
 }
}//e4nd check for username




function daet_populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = DaetScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = DaetScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = DaetScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 DaetScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card


function naga_populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = NagaScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = NagaScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = NagaScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 NagaScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card


function masbate_populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = MasbateScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = MasbateScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = MasbateScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 MasbateScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card

function sorsogon_populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = SorsogonScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = SorsogonScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = SorsogonScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 SorsogonScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card

function legaspi_populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = LegaspiScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = LegaspiScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = LegaspiScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 LegaspiScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card


public function expense_setup(Request $request){

	$branch = $request->input('branch');
	$Season = $request->input('season');
	$Year = $request->input('year');

	$setup = ExpenseSetup::where('Branch','=',$branch)->count();
	$id = ExpenseSetup::where('Branch','=',$branch)->value('id');

	if ($setup == 0) {
		//new settings
		$Set = New ExpenseSetup;
		$Set->Branch = $branch;
		$Set->Season = $Season;
		$Set->Year = $Year;
		$Set->save();

		return "save";
	} else {
		
		$updset = ExpenseSetup::find($id);
		$updset->Branch = $branch;
		$updset->Season = $Season;
		$updset->Year = $Year;
		$updset->save();
		return "update";
	}
	

}

public function dash_settings($param){

		$branch = Branch::all();
		
		
        
        
    return view('admin.admin-settings')
    ->with('branch',$branch)
    ->with('param',$param);
    
}

public function save_adsettings(Request $request){
    
    $param = $request->input('param');
    $branch = $request->input('branch');
	$Season = $request->input('season');
	$Year = $request->input('year');
    
    
   	$setup = AdminSettings::where('Branch','=',$branch)->where('Account','=',$param)->count();
	$id = AdminSettings::where('Branch','=',$branch)->where('Account','=',$param)->value('id');
    
	if ($setup == 0) {
		//new settings
		$Set = New AdminSettings;
		$Set->Account = $param;
		$Set->Branch = $branch;
		$Set->Season = $Season;
		$Set->Year = $Year;
		$Set->save();

     return Redirect()->back()->with(['message' => 'Setting has been Set']);
        // 	return response()->json([
        //     'success' => true,
        //     'message' => 'New Setting has been set',
        // ]);
		
	} else {
		
		$updset = AdminSettings::find($id);
		$updset->Account = $param;
		$updset->Branch = $branch;
		$updset->Season = $Season;
		$updset->Year = $Year;
		$updset->save();
		
		 return Redirect()->back()->with(['message' => 'Setting has been Updated']);
// 			return response()->json([
//             'success' => true,
//             'message' => 'Setting has been Updated',
//         ]);
		
	}
    

}


public function attendance(){

  //login to api
  $client  = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
  $res = $client->request('POST', 'https://cbrc.solutions/api/auth/login', [
	  'form_params' => [
		  "email"=>"admin@main.cbrc.solutions",
		  "password"=>"main@dmin"
	  ]
  ]);
//insert data to api

  if ($res->getStatusCode() == 200) { // 200 OK
	  $response_data = json_decode($res->getBody()->getContents());
   //save first payment to api   
	  $sendPayment = $client->request('GET', 'https://cbrc.solutions/api/main/attendance-report?token='.$response_data->access_token);
	

  }//end of 200 ok   

	return view('radashboard.attendance')->with($sendPayment->getContents());

}
public function evaluation(){
	return view('radashboard.evaluation');
}

public function filter_exam(){
	$program = Program::all();

	  $sy = date("Y")+1;
      $years = range($sy,2010);

	return view('radashboard.filter_monitoring_exam')
	->with('years',$years)
	->with('program',$program);
}

public function exam_monitoring(Request $request){

	$season = $request->input('season');
	$year = $request->input('year');
	$program = $request->input('program');
	$major = $request->input('major');

	return view('radashboard.exam_monitoring')
	->with('season',$season)
	->with('year',$year)
	->with('program',$program)
	->with('major',$major);

}



public function expense_settings(){
	$branch = Branch::all();
	return view('admin.expense-settings')->with('branch',$branch);
}



public function lecturer(){
	return view('radashboard.lecturer');
}
public function online_completion(){
	return view('radashboard.online_completion');
}
public function result_analysis(){
	return view('radashboard.result_analysis');
	
}



}
