<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use App\Branch;
use App\Model\Legaspi\LegaspiAgri;
use App\Model\Legaspi\LegaspiBookCash;
use App\Model\Legaspi\LegaspiBooksInventorie;
use App\Model\Legaspi\LegaspiBooksSale;
use App\Model\Legaspi\LegaspiBudget;
use App\Model\Legaspi\LegaspiCivil;
use App\Model\Legaspi\LegaspiCrim;
use App\Model\Legaspi\LegaspiDiscount;
use App\Model\Legaspi\LegaspiDropped;
use App\Model\Legaspi\LegaspiExpense;
use App\Model\Legaspi\LegaspiIelt;
use App\Model\Legaspi\LegaspiLet;
use App\Model\Legaspi\LegaspiMid;
use App\Model\Legaspi\LegaspiNclex;
use App\Model\Legaspi\LegaspiNle;
use App\Model\Legaspi\LegaspiOnline;
use App\Model\Legaspi\LegaspiPsyc;
use App\Model\Legaspi\LegaspiReceivable;
use App\Model\Legaspi\LegaspiS1Sale;
use App\Model\Legaspi\LegaspiS2Sale;
use App\Model\Legaspi\LegaspiS1Cash;
use App\Model\Legaspi\LegaspiS2Cash;
use App\Model\Legaspi\LegaspiScholar;
use App\Model\Legaspi\LegaspiSocial;
use App\Model\Legaspi\LegaspiTuition;
use App\Model\Legaspi\LegaspiPettyCash;
use App\Model\Legaspi\LegaspiRemit;
use App\Model\Legaspi\LegaspiReservation;
use App\Model\Legaspi\LegaspiEmployee;
use App\Model\Legaspi\LegaspiScoreCards;
use App\Model\Legaspi\LegaspiBookTransfer;

use App\Model\Legaspi\LegaspiLecturerAEvaluation;
use App\Model\Legaspi\LegaspiLecturerBEvaluation;
use App\Model\Legaspi\LegaspiComment;

use App\facilitation;
use App\bookTranferTrans;
use App\Expense;
use App\Program;
use App\Subject;
use App\Section;
use Alert;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

use Auth;
use User;
use DB;
use File;


class LegaspiCashierController extends Controller
{
    private $branch = "Legaspi";

    private $sbranch = "legaspi";

public function __construct()
    {

         $this->middleware('role:legaspi_cashier');
        
    }


public function book_payment(){

    $branch=$this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view('member.book-payment')->with('branch',$branch)->with('date',$date)->with('program',$program);
}
           
    
public function fetch_book(){

        $program = Input::get('program');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $book = LegaspiBooksInventorie::where('program','=', $program)->get();
        return response()->json($book);
    }

public function fetch_book_price(){

    $program = Input::get('program');
    $book_title = Input::get('book_title');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        }
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $avai = LegaspiBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->value('available');

        if($avai == 0){
            $price[] = array('price' => '0' , );
        return response()->json($price);

        }
        else{
            $price = LegaspiBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->get();
                return response()->json($price);
        }


}
public function insert_book_payment(Request $request){

 $input = $request->except(['_token']);


 
$validatedData = $request->validate([
    'name' => 'required'
]);

 $program = $input['program'];
 $price = $input['price'];
 $book_title = $input['book_title'];

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 
        if($input['total_amount'] <= $input['amount_paid'] || $input['amount_paid'] == 0 || $input['amount_paid'] == 0.00 )
        {

        if(isset($input['book_title'])){
            foreach ($input['book_title'] as $book => $value ) {

             LegaspiBooksSale::create([
                'date'      => $input['date'],
                'branch'    => $this->branch,
                'name'      => $input['name'],
                'program'   => $program,
                'book_title'=> $value,
                'amount'    => $price[$book],
             ]);
        
        $avai = LegaspiBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->value('available');

        $new = $avai - 1;

        LegaspiBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->update([

            'available' => $new,
        ]);     
        }
        $book_cash = LegaspiBookCash::where('id','=','1')->value('cash');
        $new_book_cash = $book_cash + $input['amount_paid'];

        LegaspiBookCash::where('id','=','1')->update([
            'cash'  => $new_book_cash,
        ]);
            }

            Alert::success('Success!', 'New book payment has been submitted.');
         return redirect ('legaspi-cashier/book-payment');
                }

        else{
            Alert::error('Failed!', 'Payment is insufficient, please try again');
            return redirect ('legaspi-cashier/book-payment');
        }
}
public function books_table(){

$book = LegaspiBooksInventorie::all();
$sale = LegaspiBooksSale::all();

return view ('member.books')->with('book',$book)->with('sale',$sale);

}

    }