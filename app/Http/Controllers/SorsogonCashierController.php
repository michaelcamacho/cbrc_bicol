<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use App\Branch;
use App\Model\Sorsogon\SorsogonAgri;
use App\Model\Sorsogon\SorsogonBookCash;
use App\Model\Sorsogon\SorsogonBooksInventorie;
use App\Model\Sorsogon\SorsogonBooksSale;
use App\Model\Sorsogon\SorsogonBudget;
use App\Model\Sorsogon\SorsogonCivil;
use App\Model\Sorsogon\SorsogonCrim;
use App\Model\Sorsogon\SorsogonDiscount;
use App\Model\Sorsogon\SorsogonDropped;
use App\Model\Sorsogon\SorsogonExpense;
use App\Model\Sorsogon\SorsogonIelt;
use App\Model\Sorsogon\SorsogonLet;
use App\Model\Sorsogon\SorsogonMid;
use App\Model\Sorsogon\SorsogonNclex;
use App\Model\Sorsogon\SorsogonNle;
use App\Model\Sorsogon\SorsogonOnline;
use App\Model\Sorsogon\SorsogonPsyc;
use App\Model\Sorsogon\SorsogonReceivable;
use App\Model\Sorsogon\SorsogonS1Sale;
use App\Model\Sorsogon\SorsogonS2Sale;
use App\Model\Sorsogon\SorsogonS1Cash;
use App\Model\Sorsogon\SorsogonS2Cash;
use App\Model\Sorsogon\SorsogonScholar;
use App\Model\Sorsogon\SorsogonSocial;
use App\Model\Sorsogon\SorsogonTuition;
use App\Model\Sorsogon\SorsogonPettyCash;
use App\Model\Sorsogon\SorsogonRemit;
use App\Model\Sorsogon\SorsogonReservation;
use App\Model\Sorsogon\SorsogonEmployee;
use App\Model\Sorsogon\SorsogonScoreCards;
use App\Model\Sorsogon\SorsogonBookTransfer;

use App\Model\Sorsogon\SorsogonLecturerAEvaluation;
use App\Model\Sorsogon\SorsogonLecturerBEvaluation;
use App\Model\Sorsogon\SorsogonComment;

use App\facilitation;
use App\bookTranferTrans;
use App\Expense;
use App\Program;
use App\Subject;
use App\Section;
use Alert;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

use Auth;
use User;
use DB;
use File;


class SorsogonCashierController extends Controller
{
    private $branch = "Sorsogon";

    private $sbranch = "sorsogon";

public function __construct()
    {

         $this->middleware('role:sorsogon_cashier');
        
    }


public function book_payment(){

    $branch=$this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view('member.book-payment')->with('branch',$branch)->with('date',$date)->with('program',$program);
}
           
    
public function fetch_book(){

        $program = Input::get('program');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $book = SorsogonBooksInventorie::where('program','=', $program)->get();
        return response()->json($book);
    }

public function fetch_book_price(){

    $program = Input::get('program');
    $book_title = Input::get('book_title');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        }
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $avai = SorsogonBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->value('available');

        if($avai == 0){
            $price[] = array('price' => '0' , );
        return response()->json($price);

        }
        else{
            $price = SorsogonBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->get();
                return response()->json($price);
        }


}
public function insert_book_payment(Request $request){

 $input = $request->except(['_token']);


 
$validatedData = $request->validate([
    'name' => 'required'
]);

 $program = $input['program'];
 $price = $input['price'];
 $book_title = $input['book_title'];

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 
        if($input['total_amount'] <= $input['amount_paid'] || $input['amount_paid'] == 0 || $input['amount_paid'] == 0.00 )
        {

        if(isset($input['book_title'])){
            foreach ($input['book_title'] as $book => $value ) {

             SorsogonBooksSale::create([
                'date'      => $input['date'],
                'branch'    => $this->branch,
                'name'      => $input['name'],
                'program'   => $program,
                'book_title'=> $value,
                'amount'    => $price[$book],
             ]);
        
        $avai = SorsogonBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->value('available');

        $new = $avai - 1;

        SorsogonBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->update([

            'available' => $new,
        ]);     
        }
        $book_cash = SorsogonBookCash::where('id','=','1')->value('cash');
        $new_book_cash = $book_cash + $input['amount_paid'];

        SorsogonBookCash::where('id','=','1')->update([
            'cash'  => $new_book_cash,
        ]);
            }

            Alert::success('Success!', 'New book payment has been submitted.');
         return redirect ('sorsogon-cashier/book-payment');
                }

        else{
            Alert::error('Failed!', 'Payment is insufficient, please try again');
            return redirect ('sorsogon-cashier/book-payment');
        }
}
public function books_table(){

$book = SorsogonBooksInventorie::all();
$sale = SorsogonBooksSale::all();

return view ('member.books')->with('book',$book)->with('sale',$sale);

}

    }