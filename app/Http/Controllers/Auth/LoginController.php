<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectAfterLogout = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated()
    {
        $uid = auth()->user()->id;
        if(auth()->user()->hasRole('admin'))
        {
        return redirect('/dashboard')->with('log',$uid);
        } 

    if(auth()->user()->hasRole('ra_novaliches'))
    {
        return redirect('/novaliches/dashboard');
    }  
    
    if(auth()->user()->hasRole('daet'))
    {
        return redirect('/daet/dashboard')->with('log',$uid);
    }
   
    if(auth()->user()->hasRole('daet_enrollment'))
    {
        
        return redirect('/daet-enrollment/new-payment')->with('log',$uid);;
    }  
    if(auth()->user()->hasRole('daet_cashier'))
    {
        return redirect('/daet-cashier/book-payment')->with('log',$uid);;
    }


    if(auth()->user()->hasRole('naga'))
    {
        return redirect('/naga/dashboard')->with('log',$uid);
    }
   
    if(auth()->user()->hasRole('naga_enrollment'))
    {
        
        return redirect('/naga-enrollment/new-payment')->with('log',$uid);;
    }  
    if(auth()->user()->hasRole('naga_cashier'))
    {
        return redirect('/naga-cashier/book-payment')->with('log',$uid);;
    }
    

    if(auth()->user()->hasRole('masbate'))
    {
        return redirect('/masbate/dashboard')->with('log',$uid);
    }
   
    if(auth()->user()->hasRole('masbate_enrollment'))
    {
        
        return redirect('/masbate-enrollment/new-payment')->with('log',$uid);;
    }  
    if(auth()->user()->hasRole('masbate_cashier'))
    {
        return redirect('/masbate-cashier/book-payment')->with('log',$uid);;
    }
    

    if(auth()->user()->hasRole('sorsogon'))
    {
        return redirect('/sorsogon/dashboard')->with('log',$uid);
    }
   
    if(auth()->user()->hasRole('sorsogon_enrollment'))
    {
        
        return redirect('/sorsogon-enrollment/new-payment')->with('log',$uid);;
    }  
    if(auth()->user()->hasRole('sorsogon_cashier'))
    {
        return redirect('/sorsogon-cashier/book-payment')->with('log',$uid);;
    }
    

    if(auth()->user()->hasRole('legaspi'))
    {
        return redirect('/legaspi/dashboard')->with('log',$uid);
    }
   
    if(auth()->user()->hasRole('legaspi_enrollment'))
    {
        
        return redirect('/legaspi-enrollment/new-payment')->with('log',$uid);;
    }  
    if(auth()->user()->hasRole('legaspi_cashier'))
    {
        return redirect('/legaspi-cashier/book-payment')->with('log',$uid);;
    }
    
    // else{
       
    //     return auth()->user()->id;
    // } 
   
    
}

public function username(){
    $loginType = request()->input('username');

    $this->username = filter_var($loginType, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

    request()->merge([$this->username => $loginType]);

    return property_exists($this, 'username') ? $this->username : 'email';
}


}
